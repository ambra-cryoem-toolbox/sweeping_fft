cmake_minimum_required(VERSION 3.0)

################################################################################################################################
##USER INTERACTION##############################################################################################################
##in this section modifications and local paths need to be modified by the user#################################################
################################################################################################################################
#choose which target system to use
set(USE_MPI30 OFF)
set(USE_TALOS ON)

#update GitSHA of the paraprobe repository prior commit, i.e. pre-processing upon compilation pulling last GitSHA
#we cannot update the current GitSHA as this would create a circular dependency, namely once we have commit, i.e. know the file
#set(MYGITSHA "")


#tell the top directory where this local AMBRA version is stored
if(USE_MPI30)
	set(MYPROJECTPATH "/home/m.kuehbach/GITHUB/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/")
endif()
if(USE_TALOS)
	set(MYPROJECTPATH "/talos/u/mkuehbac/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/")
endif()

#define where to find the utility tools
set(MYUTILSPATH "${MYPROJECTPATH}/ambra-utils/cxx/build/CMakeFiles/ambra-utils.dir/src/")

#where are key external libraries
if(USE_MPI30)
	#HDF5
	#set(MYHDFPATH "/home/m.kuehbach/APT3DOIM/src/thirdparty/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")
	#set(MYHDFPATH "/home/m.kuehbach/Maintain/paraprobe/src/thirdparty/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")
	set(MYHDFPATH "/home/m.kuehbach/ParaprobeTraining/paraprobe/code/thirdparty/HDF5/CMake-hdf5-1.10.6/HDF5-1.10.6-Linux/HDF_Group/HDF5/1.10.6")
	#IMKL
	#for help on specific setting for your system please consult https://software.intel.com/content/www/us/en/develop/articles/intel-mkl-link-line-advisor.html
	set(MYMKLROOT "/opt/intel/compilers_and_libraries_2018/linux/mkl") ####"${MKLROOT}") #####"/mpcdf/soft/SLE_15/packages/x86_64/intel_parallel_studio/2018.4/mkl")
endif()
if(USE_TALOS)
	#HDF5
	#set(MYHDFPATH "/talos/u/mkuehbac/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")
	set(MYHDFPATH "/talos/u/mkuehbac/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/thirdparty/HDF5/CMake-hdf5-1.10.6/HDF5-1.10.6-Linux/HDF_Group/HDF5/1.10.6")
	#IMKL
	set(MYMKLROOT "/mpcdf/soft/SLE_15/packages/x86_64/intel_parallel_studio/2020.2/mkl") ####"${MKLROOT}")
endif()




