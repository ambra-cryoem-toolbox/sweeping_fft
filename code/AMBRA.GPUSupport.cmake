cmake_minimum_required(VERSION 3.0)

################################################################################################################################
##USER INTERACTION##############################################################################################################
##in this section all GPU CUDA specific information are configured##############################################################
################################################################################################################################
#choose which target system to use

include("../AMBRA.ExternalLibraries.cmake")
if(EMPLOY_MYCUDA)
	#include_directories("/home/m.kuehbach/Maintain/TestCUFFT/cuda-samples-master/Common")
	if(USE_MPI30)
		set(MYCUDAHOME "/usr/local/cuda")
	endif()
	if(USE_TALOS)
		###set(MYCUDAHOME "/mpcdf/soft/SLE_15/packages/skylake/cuda/10.0.130")
		set(MYCUDAHOME "/mpcdf/soft/SLE_15/packages/x86_64/cuda/10.2.89")
	endif()
	include_directories("${MYCUDAHOME}/inc")
	link_directories("${MYCUDAHOME}/lib64")
	set(MYCUFFTLINKFLAGS "-I${MYCUDAHOME}/inc -L${MYCUDAHOME}/lib64 -lcufft")
elseif()
	set(MYCUFFTLINKFLAGS "")
endif()


