# -*- coding: utf-8 -*-
"""
Created on Tue May  5 12:45:43 2020
@author: Markus K\"uhbach, Max-Planck-Institut für Eisenforschung GmbH
"""

#mt19937
#import random
#rndseed=-1234
#random.seed( rndseed, version=2 )

import os, sys, glob, subprocess
import pandas as pd
import numpy as np
from scipy import stats
import scipy.special as sc
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
import h5py

def five_digit_taskid_str( val ):
    """
    Parameters
    ----------
    val : integer
        transforms integer to five digit integer by adding leading zeros.

    Returns
    -------
    TYPE
        five digit integer.

    """
    if val < 10:
        return str('0000' + str(val))
    if val < 100:
        return str('000' + str(val))
    if val < 1000:
        return str('00' + str(val))
    if val < 10000:
        return str('0' + str(val))
    #error if val > 99999
    return str(val)

#the key components of an AmbraH5 file
toplevel_groups = np.array(['00_Input', '00_Input/Metadata', '00_Input/Data',
                            '01_Environment', '01_Environment/Hardware', '01_Environment/Software',
                            '02_Settings', 
                            '03_Results'])

class AmbraH5():
    """
    top-level class for creating Ambra HDF5 files
    """
    def __init__(self, wkdir, toolnm, simid, *args, **kwargs):
        self.wkdir = wkdir
        self.toolname = toolnm
        self.simid = simid
        self.h5fn = 'AMBRA.' + str(self.toolname) + '.SimID.' + str(self.simid) + '.h5'
        self.h5w = h5py.File(self.wkdir + '/' + self.h5fn, 'w')
        print('Creating Ambra HDF5 file ' + str(self.h5fn))
        #create top-level groups
        print('Creating ' + str(toolnm))
        self.h5w.create_group( toolnm )
        for grp in toplevel_groups:
            grpnm = toolnm + '/' + grp
            print('Creating ' + str(grpnm))
            grp = self.h5w.create_group( grpnm )

    def __del__(self):
        self.h5w.close()
        
rmcurtaining_groups = np.array(['02_Settings/Fourier', '02_Settings/Wavelet',
                                '03_Results/Fourier', '03_Results/Fourier/Metadata', '03_Results/Fourier/Data',
                                '03_Results/Wavelet', '03_Results/Wavelet/Metadata', '03_Results/Wavelet/Data'])
