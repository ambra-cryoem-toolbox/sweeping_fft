//##MK::GPLV3

#ifndef __AMBRA_UNITCONVERSIONS_H__
#define __AMBRA_UNITCONVERSIONS_H__

//unit conversions
#define CELSIUS2KELVIN(T)					((273.15) + (T))
#define KELVIN2CELSIUS(T)					((T) - (273.15))

#define DEGREE2RADIANT(theta)				((theta)/(180.0)*(MYPI))
#define RADIANT2DEGREE(rad)					((rad)/(MYPI)*(180.0))

//scaling conversions
#define NANOMETER2ANGSTROEM(nm)				((10.0)*(nm))
#define ANGSTROEM2NANOMETER(ang)			((ang)/(10.0))
#define METER2NANOMETER(m)					((1.0e9)*(m))
#define NANOMETER2METER(nm)					((1.0e-9)*(nm))
#define METER2MILLIMETER(m)					((1.0e3)*(m))
#define MILLIMETER2METER(mm)				((1.0e-3)*(mm))

#define BYTE2KILOBYTE(b)					((b)/(1024.0))
#define KILOBYTE2BYTE(b)					((kb)*(1024.0))
#define BYTE2MEGABYTE(b)					((b)/((1024.0)*(1024.0)))
#define MEGABYTE2BYTE(b)					((b)*((1024.0)*(1024.0)))
#define BYTE2GIGABYTE(b)					((b)/((1024.0)*(1024.0)*(1024.0)))
#define GIGABYTE2BYTE(gb)					((gb)*(1024.0)*(1024.0)*(1024.0))

#endif
