//##MK::GPLV3

#ifndef __AMBRA_UTILS_DATATYPES_H__
#define __AMBRA_UTILS_DATATYPES_H__

#include "AMBRA_Profiling.h"

struct p3d
{
	bio_real x;
	bio_real y;
	bio_real z;

	p3d() : x(0.0), y(0.0), z(0.0) {}
	p3d(const bio_real _x, const bio_real _y, const bio_real _z) :
		x(_x), y(_y), z(_z) {}

	bio_real dot( p3d const & b );
	p3d cross( p3d const & b );
};

ostream& operator<<(ostream& in, p3d const & val);


struct p3dm1
{
	bio_real x;
	bio_real y;
	bio_real z;
	size_t m1;

	p3dm1() : x(0.0), y(0.0), z(0.0), m1(UNKNOWNTYPE) {}
	p3dm1(const bio_real _x, const bio_real _y, const bio_real _z, const size_t _m1) :
		x(_x), y(_y), z(_z), m1(_m1) {}
};

ostream& operator<<(ostream& in, p3dm1 const & val);


struct px3d
{
	int x;
	int y;
	int z;
	px3d() : x(0), y(0), z(0) {}
	px3d(const int _x, const int _y, const int _z) :
		x(_x), y(_y), z(_z) {}
};

ostream& operator<<(ostream& in, px3d const & val);


struct px3dm1
{
	int x;
	int y;
	int z;
	size_t m1;
	px3dm1() : x(0), y(0), z(0), m1(UNKNOWNTYPE) {}
	px3dm1(const int _x, const int _y, const int _z, const size_t _m1) :
		x(_x), y(_y), z(_z), m1(_m1) {}
};

ostream& operator<<(ostream& in, px3dm1 const & val);


struct v3d
{
	bio_real u;
	bio_real v;
	bio_real w;
	v3d() : u(0.0), v(0.0), w(0.0) {}
	v3d( const bio_real _u, const bio_real _v, const bio_real _w ) :
		u(_u), v(_v), w(_w) {}
	bio_real len();
	void normalize();
};

ostream& operator<<(ostream& in, v3d const & val);


struct aabb_p3d
{
	bio_real xmi;
	bio_real xmx;
	bio_real ymi;
	bio_real ymx;
	bio_real zmi;
	bio_real zmx;
	bio_real xsz;
	bio_real ysz;
	bio_real zsz;
	aabb_p3d() :
		xmi(FMX), xmx(FMI), ymi(FMX), ymx(FMI), zmi(FMX), zmx(FMI), xsz(0.0), ysz(0.0), zsz(0.0) {}
	aabb_p3d(const bio_real _xmi, const bio_real _xmx, const bio_real _ymi, const bio_real _ymx, const bio_real _zmi, const bio_real _zmx) :
		xmi(_xmi), xmx(_xmx), ymi(_ymi), ymx(_ymx), zmi(_zmi), zmx(_zmx),
		xsz(_xmx-_xmi), ysz(_ymx-_ymi), zsz(_zmx-_zmi) {}

	void scale();
	p3d center();
	bio_real volume();
	void blowup( const bio_real di );
	void blowup( const bio_real dx, const bio_real dy, const bio_real dz );

	void add_epsilon_guard();

	bool is_inside( p3d const & test );

	//p3i blockpartitioning( const size_t p_total, const size_t p_perblock_target );
};

ostream& operator<<(ostream& in, aabb_p3d const & val);


struct aabb_px3d
{
	//the cuboidal continuum space region covered or on the boundary of the axis-aligned bounding box
	aabb_p3d bounded_volume;
	//the cuboidal continuum space region with the halo about the bounded volume
	/*
	aabb_p3d guarded_volume;
	*/
	//the number of voxel along each axis direction into which the volume is voxelated
	int bounded_nx;
	int bounded_ny;
	int bounded_nz;
	//plus an eventual halo about the box
	/*
	int guarded_nx;
	int guarded_ny;
	int guarded_nz;
	*/
	//the voxel IDs of the bounded_volume
	int inside_xmi;			//imi inclusive
	int inside_xmx;
	int inside_ymi;
	int inside_ymx;			//imx is exclusive, i.e. one past the end of the range but the end of the range
	int inside_zmi;
	int inside_zmx;
	//the voxel IDs of the guarded_volume, i.e. bounded_voxel including the halo
	/*
	int halo_xmi;
	int halo_xmx;
	int halo_ymi;
	int halo_ymx;
	int halo_zmi;
	int halo_zmx;
	*/

	aabb_px3d() :
		bounded_volume(aabb_p3d()),
		bounded_nx(0), bounded_ny(0), bounded_nz(0),
		inside_xmi(INT32MX), inside_xmx(INT32MI),
		inside_ymi(INT32MX), inside_ymx(INT32MI),
		inside_zmi(INT32MX), inside_zmx(INT32MI) {}
	//guarded_volume(aabb_p3d()), guarded_nx(0), guarded_ny(0), guarded_nz(0),halo_xmi(INT32MX), halo_xmx(INT32MI), halo_ymi(INT32MX), halo_ymx(INT32MI), halo_zmi(INT32MX), halo_zmx(INT32MI)
	aabb_px3d( aabb_p3d const & cuboidal_region );

	//##MK::add getter/computer for the physical length of a pixel based on physical dimensions bounded volume imi, imx and bounded_ni

	//aabb_px3d( aabb_p3d const & bounded_region, aabb_p3d const & guarded_region, const bio_real di, const int halo );
	//px3d where( const int ix, const int iy, const int iz );
};

ostream& operator<<(ostream& in, aabb_px3d const & val);

/*
inline bool SortNeighborsForAscDistance(const nbor & a, const nbor & b)
{
	return a.d < b.d;
}
*/

struct bibtex
{
	string entrytype;
	string author;
	string title;
	string journal;		//or book comment
	string year;
	string vol;
	string pages;
	string doi;
	bibtex() : entrytype(""), author(""), title(""), journal(""), year(""), vol(""), pages(""), doi("") {}
	bibtex( const string _etyp, const string _auth, const string _ttl, const string _jou,
			const string _yr, const string _vol, const string _pgs, const string _doi ) :
				entrytype(_etyp), author(_auth), title(_ttl), journal(_jou),
				year(_yr), vol(_vol), pages(_pgs), doi(_doi) {}
};


class discretized_volume
{
public:
	discretized_volume();
	~discretized_volume();

	//a cuboidal aggregate of cuboidal voxels, global because e.g. giving the geometry of an entire image stack the processes work on
	aabb_px3d global;
	//a cuboidal aggregate of cuboidal voxels, but local because a sub-set of global
	aabb_px3d local;
	//the voxel offset to translate local into global coordinates
	px3d local2global;
	//p_{global} = p_{local} + \Delta local2global \forall i
};


#endif
