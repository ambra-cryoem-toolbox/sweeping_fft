//##MK::GPLV3

#ifndef __AMBRA_UTILS_NUMERICS_H__
#define __AMBRA_UTILS_NUMERICS_H__

//choose which precision level to use for floating point numbers
//#define EMPLOY_SINGLEPRECISION
//implicit else if the above is not defined we use double precision

#ifdef EMPLOY_SINGLEPRECISION
	#define EPSILON						(1.0e-6)
	typedef float bio_real;
#else
	#define EPSILON						(1.0e-12)
	typedef double bio_real;
#endif

typedef unsigned int bio_uint;

//##MK::temporary before AMBRA_HDF5IO is fixed
typedef float apt_real;

//type range
#define UCHARMX							(numeric_limits<unsigned char>::max())
#define UCHARMI							(numeric_limits<unsigned char>::lowest())
#define INT16MX							(numeric_limits<short>::max())
#define INT16MI							(numeric_limits<short>::lowest())
#define UINT16MX						(numeric_limits<unsigned short>::max())
#define UINT16MI						(numeric_limits<unsigned short>::lowest())

#define INT32MX							(numeric_limits<int>::max())
#define INT32MI							(numeric_limits<int>::lowest())
#define UINT32MX						(numeric_limits<unsigned int>::max())
#define UINT32MI						(numeric_limits<unsigned int>::lowest())

#define UINT64MX						(numeric_limits<size_t>::max())
#define UINT64MI						(numeric_limits<size_t>::lowest())

#define FMX								(numeric_limits<bio_real>::max())
#define FMI								(numeric_limits<bio_real>::lowest())
#define BIMX 							(numeric_limits<bio_uint>::max())
#define BIMI							(numeric_limits<bio_uint>::lowest())

//handling of iontypes and flagging them for inclusion/exclusion in analyses
//##MK::unknown type needs to be zero-th type
#define UNKNOWNTYPE						0

//MK::Mersenne initialization
#define MT19937SEED						(-1)
#define MT19937WARMUP					(700000)

#define IS_HALO							0x00
#define IS_BOUNDARY						0x01
#define IS_INSIDE						0xFF


#endif
