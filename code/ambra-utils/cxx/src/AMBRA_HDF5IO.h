//##MK::GPLV3

#ifndef __AMBRA_UTILS_HDFIO_H__
#define __AMBRA_UTILS_HDFIO_H__

#include "AMBRA_HDF5Interface.h"

//all metadata definitions per paraprobe-tool
/*
#include "metadata/PARAPROBE_FourierMetadataDefsH5.h"
*/

void debug_hdf5();


enum MYHDF5_DATATYPES {
	NONE,
	I8,
	U8,
	I16,
	U16,
	I32,
	U32,
	I64,
	U64,
	F32,
	F64
};


//dummy values for filling chunk buffers
#define HDF5_U32LE_DUMMYVALUE				1 //UINT32MX //##MK::should be 1 if used for topology
#define HDF5_F64LE_DUMMYVALUE				0.0

//return codes for wrapped H5 functions
#define WRAPPED_HDF5_SUCCESS				+1 //MK::following the HDF5 convention that error values are positiv in case of success or negative else
#define WRAPPED_HDF5_ALLOCERR				-1
#define WRAPPED_HDF5_OUTOFBOUNDS			-2
#define WRAPPED_HDF5_ARGINCONSISTENT		-3
#define WRAPPED_HDF5_EXECUTIONORDERISSUE	-4
#define WRAPPED_HDF5_INCORRECTLOGIC			-5
#define WRAPPED_HDF5_INCORRECTDIMENSIONS	-6
#define WRAPPED_HDF5_INCORRECTOFFSETS		-7
#define WRAPPED_HDF5_FAILED					-8
#define WRAPPED_HDF5_RBUFALLOC_FAILED		-9
#define WRAPPED_HDF5_READ_FAILED			-10
#define WRAPPED_HDF5_DOESNOTEXIST			-11
#define WRAPPED_HDF5_FILEACCESS_FAILED		-12
#define WRAPPED_HDF5_DSETACCESS_FAILED		-13
#define WRAPPED_HDF5_MSPACE_FAILED			-14
#define WRAPPED_HDF5_DSPACE_FAILED			-15
#define WRAPPED_HDF5_HYPERSLAB_FAILED		-16
#define WRAPPED_HDF5_GRPACCESS_FAILED		-17
#define WRAPPED_HDF5_ATTRACCESS_FAILED		-18
#define WRAPPED_HDF5_ATTRWRITE_FAILED		-19

#define HDF5_STR_MAXLEN						256

struct io_bounds
{
	size_t n;
	size_t s;
	size_t e;
	io_bounds() : n(0), s(0), e(0) {}
	io_bounds( const size_t _n, const size_t _s, const size_t _e ) :
		n(_n), s(_s), e(_e) {}
};

struct voro_io_bounds
{
	//assists coordinates writing of tessellation portions from individual threads
	//by defining the array bounds between which data from the individual threads are placed
	size_t cell_n;
	size_t cell_s; //defining [cell_s, cell_e)
	size_t cell_e;
	size_t topo_n;
	size_t topo_s;
	size_t topo_e;
	size_t geom_n;
	size_t geom_s;
	size_t geom_e;
	voro_io_bounds() : 	cell_n(0), cell_s(0), cell_e(0),
						topo_n(0), topo_s(0), topo_e(0),
						geom_n(0), geom_s(0), geom_e(0) {}
	voro_io_bounds( const size_t _cn, const size_t _cs, const size_t _ce,
					const size_t _tn, const size_t _ts, const size_t _te,
					const size_t _gn, const size_t _gs, const size_t _ge ) :
						cell_n(_cn), cell_s(_cs), cell_e(_ce),
						topo_n(_tn), topo_s(_ts), topo_e(_te),
						geom_n(_gn), geom_s(_gs), geom_e(_ge) {}
};


struct voro_io_info
{
	size_t id;
	size_t nfacets;
	size_t ntopo;
	size_t ngeom;
	voro_io_info() : id(0), nfacets(0), ntopo(0), ngeom(0) {}
	voro_io_info(const size_t _id, const size_t _nf,
			const size_t _nt,	const size_t _ng) :
				id(_id), nfacets(_nf), ntopo(_nt), ngeom(_ng) {}
};


//utility structs for 2d tensor data
struct h5iometa
{
	string h5fn;
	string dsetnm;
	size_t nr;
	size_t nc;
	h5iometa() : h5fn(""), dsetnm(""), nr(0), nc(0) {}
	h5iometa( const string _dn, const size_t _nr, const size_t _nc ) :
		h5fn(""), dsetnm(_dn), nr(_nr), nc(_nc) {}
	h5iometa( const string _fn, const string _dn, const size_t _nr, const size_t _nc ):
		h5fn(_fn), dsetnm(_dn), nr(_nr), nc(_nc) {}
};

ostream& operator<<(ostream& in, h5iometa const & val);


struct h5dims
{
	size_t nr;
	size_t nc;
	h5dims() : nr(0), nc(0) {}
	h5dims(const size_t _nr, const size_t _nc) : nr(_nr), nc(_nc) {}
};

ostream& operator<<(ostream& in, h5dims const & val);


struct h5offsets
{
	size_t nr0;			//where to start reading writing off from row ID C indexing, i.e. inclusive bound
	size_t nr1;			//one past where to stop reading writing off from, i.e. exclusive bound
	size_t nc0;			//where to start reading writing off from col ID C indexing
	size_t nc1;
	size_t nrmax;		//maximum dimensions
	size_t ncmax;
	h5offsets() : nr0(-1), nr1(-1), nc0(-1), nc1(-1), nrmax(0), ncmax(0) {}
	h5offsets( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1,
			const size_t _nrmx, const size_t _ncmx ) :
		nr0(_nr0), nr1(_nr1), nc0(_nc0), nc1(_nc1), nrmax(_nrmx), ncmax(_ncmx) {}
	bool is_within_bounds( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1 );
	size_t nrows();
	size_t ncols();
	//size_t interval_length( const size_t _n0, const size_t _n1 );
};

ostream& operator<<(ostream& in, h5offsets const & val);


//utility structs for 2d tensor data
struct h5iometa2d
{
	string h5fn;
	string dsetnm;
	unsigned int dtype;
	size_t shape[2];
	h5iometa2d();
	h5iometa2d( const string _dn, const MYHDF5_DATATYPES _dtyp, const size_t _nr, const size_t _nc );
	size_t npx2d();
};

ostream& operator<<(ostream& in, h5iometa2d const & val);


struct h5dims2d
{
	size_t shape[2];
	//size_t nr;		//rows are along N, 0,
	//size_t nc;		//rows are along M, 1, N >= M, nr changing fastest
	h5dims2d();
	h5dims2d( const size_t _nr, const size_t _nc );
};

ostream& operator<<(ostream& in, h5dims2d const & val);


struct h5offs2d
{
	size_t nr0;			//where to start read/write off from row ID, C indexing, i.e. inclusive bound
	size_t nr1; 		//one past where to stop read/write off from row ID, i.e. exclusive bound
	size_t nc0;			//like above but for columns
	size_t nc1;
	h5offs2d();
	h5offs2d( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1 );
};

ostream& operator<<(ostream& in, h5offs2d const & val);


//utility structs for 3d tensor data
struct h5iometa3d
{
	string h5fn;
	string dsetnm;
	unsigned int dtype;
	size_t shape[3];
	h5iometa3d();
	h5iometa3d( const string _dn, const MYHDF5_DATATYPES _dtyp, const size_t _nc, const size_t _nr, const size_t _nl );
	size_t npx3d();
};

ostream& operator<<(ostream& in, h5iometa3d const & val);


struct h5dims3d
{
	size_t shape[3];
	//size_t nc;		//columns are along N, 0
	//size_t nr;		//rows are along M, 1
	//size_t nl;		//layers are along L, 2
	h5dims3d();
	h5dims3d( const size_t _nc, const size_t _nr, const size_t _nl );
};

ostream& operator<<(ostream& in, h5dims3d const & val);


struct h5offs3d
{
	size_t nc0;			//where to start read/write off from column ID, C indexing, i.e. inclusive bound
	size_t nc1; 		//one past where to stop read/write off from column ID, i.e. exclusive bound
	size_t nr0;			//like above but for rows
	size_t nr1;
	size_t nl0;			//like above but for layers
	size_t nl1;
	h5offs3d();
	h5offs3d( const size_t _nc0, const size_t _nc1, const size_t _nr0, const size_t _nr1, const size_t _nl0, const size_t _nl1 );
	//bool is_within_bounds( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1 );
	//size_t nrows();
	//size_t ncols();
	//size_t interval_length( const size_t _n0, const size_t _n1 );
};

ostream& operator<<(ostream& in, h5offs3d const & val);


class h5Hdl
{
//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls
public:
	h5Hdl();
	~h5Hdl();

	//file generation and closing
	void reinitialize();
	int create_file( const string h5fn );

	//query dataset dimensions
	int query_contiguous_matrix_dims( const string _dn, h5offsets & offsetinfo );


	//group generation
	int create_group( const string grpnm );
	int create_group( const string h5fn, const string grpnm );
	bool probe_group_existence( const string grpnm );

	//attribute management
	//add metadata explanation to an existent dataset dsnm to tell what the unit of the values are and what the data are, i.e. information for humans what to do with these data e.g.
	int add_attribute( const string dsnm, const string unit, const string descr );

	//contiguous dataset generation and fill in for subsequent hyperslab writing
	//2d matrices with N, M > 0 and M changing fastest
	int create_contiguous_matrix_u8le( h5iometa const & h5info );
	int create_contiguous_matrix_u16le( h5iometa const & h5info );
	int create_contiguous_matrix_u32le( h5iometa const & h5info );
	int create_contiguous_matrix_i32le( h5iometa const & h5info );
	int create_contiguous_matrix_i64le( h5iometa const & h5info );
	int create_contiguous_matrix_u64le( h5iometa const & h5info );
	int create_contiguous_matrix_i16le( h5iometa const & h5info );
	int create_contiguous_matrix_f32le( h5iometa const & h5info );
	int create_contiguous_matrix_f64le( h5iometa const & h5info );

	//3d matrices with N, M, L > 0
	//we use here that N || x, M || y, and L || z such that implicit 3d arrays are encoded as follows:
	//N,M,L --> ID is x + y * N + z * N*M
	//int create_contiguous_tensor3d( h5iometa3d const & ifo ); //later program with type specifier

	//strings
	int write_string_ascii( const string grpnm, const string val );
	int write_settings_entry( const string prefix, pparm in );

	//scalars
	int create_scalar_u64le( const string h5fn, const string grpnm, const size_t val );

	//writing and reading of 2d nr x nc matrices
	int init_chunked_matrix_u32le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc );
	int write_chunked_matrix_u32le(	const string h5fn, const string dsetnm, vector<unsigned int> const & buffer );
	int reset_chunked_matrix_u32le_aftercompletion();

	int write_contiguous_matrix_u8le_atonce( const string h5fn, const string dsetnm,
			const size_t nr, const size_t nc, vector<unsigned char> const & buffer );
	int write_contiguous_matrix_u32le_atonce( const string h5fn, const string dsetnm,
			const size_t nr, const size_t nc, vector<unsigned int> const & buffer );
	int write_contiguous_matrix_u64le_atonce( const string h5fn, const string dsetnm,
					const size_t nr, const size_t nc, vector<size_t> const & buffer );
	int write_contiguous_matrix_f32le_atonce( const string h5fn, const string dsetnm,
					const size_t nr, const size_t nc, vector<float> const & buffer );
	int write_contiguous_matrix_f64le_atonce( const string h5fn, const string dsetnm,
			const size_t nr, const size_t nc, vector<double> const & buffer );

	int write_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<unsigned char> const & buffer );
	int write_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<unsigned short> const & buffer );
	int write_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<unsigned int> const & buffer );
	int write_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<int> const & buffer );
	int write_contiguous_matrix_i64le_hyperslab( h5iometa const & h5info,
					h5offsets const & offsetinfo, vector<long> const & buffer );
	int write_contiguous_matrix_u64le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<size_t> const & buffer );
	int write_contiguous_matrix_i16le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<short> const & buffer );
	int write_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<float> const & buffer );
	int write_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, float* const buffer );
	int write_contiguous_matrix_f64le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<double> const & buffer );

	int read_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info,
					h5offsets const & offsetinfo, vector<unsigned char> & buffer );
	int read_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info,
					h5offsets const & offsetinfo, vector<unsigned short> & buffer );
	int read_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info,
					h5offsets const & offsetinfo, vector<int> & buffer );
	int read_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<float> & buffer );
	int read_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<unsigned int> & buffer );

	//writing and reading of 2d nr x nc tensors
	int add_contiguous_tensor2d_u8le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<unsigned char> & buf );
	int add_contiguous_tensor2d_u32le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<unsigned int> & buf );
	int add_contiguous_tensor2d_i64le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<long long> & buf );
	int add_contiguous_tensor2d_f32le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<float> & buf );
	int add_contiguous_tensor2d_f64le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<double> & buf );

	//writing and reading of 3d nc x nr x nl tensors
	int query_contiguous_tensor3d( const string dsetname, h5offs3d & offs );
	int read_contiguous_tensor3d_u8le_hyperslab( h5iometa3d const & ifo, h5offs3d const & offs, vector<unsigned char> & buf );
	int read_contiguous_tensor3d_f32le_hyperslab( h5iometa3d const & ifo, h5offs3d const & offs, vector<float> & buf );


	int init_chunked_matrix_f64le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc );
	int write_chunked_matrix_f64le( const string h5fn, const string dsetnm, vector<double> const & buffer );
	int reset_chunked_matrix_f64le_aftercompletion();

	//write data
	//we use buffer here that do not cover necessarily the entire dataset to
	//hide the entire writing of data to an HDF5 file from the higher level function call
	//how internally the data stripe [start, end) is stored chunked into only the HDF5 class object takes care of
	int write_chunked_matrix_f64le( const string dsetnm, const size_t nr, const size_t nc,
					const size_t start, const size_t end, vector<p3d> const & buffer );


	//add attributes a posteriori having written a dataset and already closed it...
	int write_attribute_string( const string dsetnm, vector<pair<string,string>> const & attr );

	//higher-level reading of agnostic PARAPROBE data objects
	/*
	bool read_trianglehull_from_apth5( const string fn, vector<tri3d> & out );
	bool read_voxelized_from_apth5( const string fn, vxlizer & out );
	bool read_xyz_from_apth5( const string fn, vector<p3d> & out );
	bool read_detxy_from_apth5( const string fn, vector<p2dm1> & out );
	bool read_voltages_from_apth5( const string fn, vector<voltm1> & out );
	bool read_iontypes_from_apth5( const string fn, vector<rangedIontype> & out );
	bool read_ityp_from_apth5( const string fn, vector<unsigned char> & out );
	bool read_m2q_from_apth5( const string fn, vector<apt_real> & out );
	bool read_dist_from_apth5( const string fn, vector<apt_real> & out );
	*/

	string h5resultsfn;

//private:
	//hold handler for the H5 file here
	herr_t status;
	hid_t fileid;
	hid_t ftypid;
	hid_t groupid;
	hid_t mtypid;
	hid_t mspcid;
	hid_t dsetid;
	hid_t dspcid;
	hid_t cparms;
	hid_t fspcid;
	hid_t attrid;
	hid_t attrtyp;

	hsize_t dims[2];
	hsize_t maxdims[2];
	hsize_t offset[2];
	hsize_t dimsnew[2];

	hsize_t nrows;
	hsize_t ncols;
	size_t BytesPerChunk;
	size_t RowsPerChunk;
	size_t ColsPerChunk;
	size_t RowsColsPerChunk;
	size_t NChunks;
	size_t TotalWriteHere; 			//specifies how many data elements in the currently processed dset were already written successfully
	size_t TotalWritePortion;		//specifies how many data elements are to be processed in an arbitrary portion as e.g. coming from threadlocal output
	size_t TotalWriteAllUsed;
	size_t TotalWriteAllChunk;			//specifies how many data elements ultimately have to become completed

	//temporary read/write buffer
	unsigned int* u32le_buf;
	double* f64le_buf;
};

#endif
