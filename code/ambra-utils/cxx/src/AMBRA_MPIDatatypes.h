//##MK::GPLV3

#ifndef __AMBRA_UTILS_MPIDATATYPES_H__
#define __AMBRA_UTILS_MPIDATATYPES_H__

#include "AMBRA_Datatypes.h"


#define MPIIO_OFFSET_INCR_F64			8		//##MK::improve portability with MPI_Aint and sizeof type queries
#define MPIIO_OFFSET_INCR_F32			4
#define MPIIO_OFFSET_INCR_I32			4
#define MPIIO_OFFSET_INCR_U32			4


struct MPI_aabb_px3d
{
//the cuboidal continuum space region covered or on the boundary of the axis-aligned bounding box
#ifdef EMPLOY_SINGLEPRECISION
	float bvxmi;
	float bvxmx;
	float bvymi;
	float bvymx;
	float bvzmi;
	float bvzmx;
/*
	float gvxmi;
	float gvxmx;
	float gvymi;
	float gvymx;
	float gvzmi;
	float gvzmx;
*/
#else
	double bvxmi;
	double bvxmx;
	double bvymi;
	double bvymx;
	double bvzmi;
	double bvzmx;
/*
	double gvxmi;
	double gvxmx;
	double gvymi;
	double gvymx;
	double gvzmi;
	double gvzmx;
*/
#endif
	//##MK::check handling of size_t or unsigned long
	int bounded_nx;
	int bounded_ny;
	int bounded_nz;
/*
	unsigned int guarded_nx;
	unsigned int guarded_ny;
	unsigned int guarded_nz;
*/

	int inside_xmi;
	int inside_xmx;
	int inside_ymi;
	int inside_ymx;
	int inside_zmi;
	int inside_zmx;
/*
	int halo_xmi;
	int halo_xmx;
	int halo_ymi;
	int halo_ymx;
	int halo_zmi;
	int halo_zmx;
*/

	MPI_aabb_px3d() :
		bvxmi(FMX), bvxmx(FMI), bvymi(FMX), bvymx(FMI), bvzmi(FMX), bvzmx(FMI),
		bounded_nx(0), bounded_ny(0), bounded_nz(0),
		inside_xmi(INT32MX), inside_xmx(INT32MI),
		inside_ymi(INT32MX), inside_ymx(INT32MI),
		inside_zmi(INT32MX), inside_zmx(INT32MI) {}

	//gvxmi(FMX), gvxmx(FMI), gvymi(FMX), gvymx(FMI), gvzmi(FMX), gvzmx(FMI), guarded_nx(0), guarded_ny(0), guarded_nz(0),halo_xmi(INT32MX), halo_xmx(INT32MI), halo_ymi(INT32MX), halo_ymx(INT32MI), halo_zmi(INT32MX), halo_zmx(INT32MI) {}
	MPI_aabb_px3d( aabb_px3d const & _in ) :
		bvxmi(_in.bounded_volume.xmi), bvxmx(_in.bounded_volume.xmx),
		bvymi(_in.bounded_volume.ymi), bvymx(_in.bounded_volume.ymx),
		bvzmi(_in.bounded_volume.zmi), bvzmx(_in.bounded_volume.zmx),
		bounded_nx(_in.bounded_nx), bounded_ny(_in.bounded_ny), bounded_nz(_in.bounded_nz),
		inside_xmi(_in.inside_xmi), inside_xmx(_in.inside_xmx),
		inside_ymi(_in.inside_ymi), inside_ymx(_in.inside_ymx),
		inside_zmi(_in.inside_zmi), inside_zmx(_in.inside_zmx) {}
		//gvxmi(_in.bounded_volume.xmi), gvxmx(_in.bounded_volume.xmx), gvymi(_in.bounded_volume.ymi), gvymx(_in.bounded_volume.ymx), gvzmi(_in.bounded_volume.zmi), gvzmx(_in.bounded_volume.zmx),guarded_nx(_in.guarded_nx), guarded_ny(_in.guarded_ny), guarded_nz(_in.guarded_nz),halo_xmi(_in.halo_xmi), halo_xmx(_in.halo_xmx), halo_ymi(_in.halo_ymi), halo_ymx(_in.halo_ymx), halo_zmi(_in.halo_zmi), halo_zmx(_in.halo_zmx) {}
};

#endif
