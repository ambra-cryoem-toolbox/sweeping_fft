//##MK::GPLV3

#ifndef __AMBRA_UTILS_STLINCLUDES_H__
#define __AMBRA_UTILS_STLINCLUDES_H__

//C++ STL
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstring>
#include <stdexcept>
#include <string>
#include <iomanip>
#include <limits>
#include <vector>
#include <algorithm>
#include <cmath>
#include <list>
#include <complex>

#include <cassert>
#include <map>
#include <iterator>
#include <utility>
#include <set>
#include <unordered_set>

#include <random>
#include <complex>

//low level stuff to interaction with operating system
//#define NDEBUG
#include <assert.h>
#include <stdint.h>
#include <ios>

//C header stuff to pull low level system and process status pieces of information
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


//Posixs pagesize to identify memory consumption
//##MK::some systems' POSIX library implements _SC_PAGESIZE
#define SYSTEMSPECIFIC_POSIX_PAGESIZE				(_SC_PAGE_SIZE)
#define SYSTEMSPECIFIC_POSIX_NPAGES					(_SC_PHYS_PAGES)
#define SYSTEMSPECIFIC_POSIX_HOSTNAMEMAX			(64)
//https://stackoverflow.com/questions/30084116/host-name-max-undefined-after-include-limits-h
//https://stackoverflow.com/questions/27914311/get-computer-name-and-logged-user-name/44152790

//forward declaration for global scope
using namespace std;


//##MK::add compile time check if target system is little endian
//##MK::this is probably not portable to Intel and should be improved to run watertight
//https://stackoverflow.com/questions/4239993/determining-endianness-at-compile-time
#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
	#error PARAPROBE requires LittleEndian Architecture
#endif


/*
#define UTILIZE_BOOST
#ifdef UTILIZE_BOOST
	//boost
	#include <boost/algorithm/string.hpp>
	#include <boost/math/special_functions/bessel.hpp>
#endif
*/

#endif
