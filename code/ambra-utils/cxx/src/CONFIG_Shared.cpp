//##MK::GPLV3

#include "CONFIG_Shared.h"

bio_real str2real( const string str )
{
#ifdef EMPLOY_SINGLEPRECISION
	return stof( str );
#else
	return stod( str );
#endif
}


string read_xml_attribute_string( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) )
		return in->first_node(keyword.c_str())->value();
	else
		return "";
}


bio_real read_xml_attribute_real( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) ) {
		return str2real(in->first_node(keyword.c_str())->value());
	}
	else {
#ifdef EMPLOY_SINGLEPRECISION
		return 0.f;
#else
		return 0.0;
#endif
	}
}


unsigned short read_xml_attribute_uint16( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) ) {
		unsigned long val = stoul(in->first_node(keyword.c_str())->value());
		if ( val < static_cast<unsigned long>(UINT16MX) ) {
			return static_cast<unsigned short>(val);
		}
		else {
			cerr << "WARNING:: Converting keyword " << keyword << " from parsed unsigned long " << val << " to unsigned short overflowed !" << "\n";
			return 0;
		}
	}
	else
		return 0;
}

unsigned int read_xml_attribute_uint32( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) ) {
		unsigned long val = stoul(in->first_node(keyword.c_str())->value());
		if ( val < static_cast<unsigned long>(UINT32MX) ) {
			return static_cast<unsigned int>(val);
		}
		else {
			cerr << "WARNING:: Converting keyword " << keyword << " from parsed unsigned int " << val << " to unsigned short overflowed !" << "\n";
			return 0;
		}
	}
	else
		return 0;
}


int read_xml_attribute_int32( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) )
		return atoi(in->first_node(keyword.c_str())->value());
	else
		return 0;
}


bool read_xml_attribute_bool( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) ) {
		int i32 = atoi(in->first_node(keyword.c_str())->value());
		if ( i32 == 1 )
			return true;
		else
			return false;
	}
	return false;
}


unsigned long read_xml_attribute_uint64( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) ) {
		unsigned long val = stoul(in->first_node(keyword.c_str())->value());
		if ( val < static_cast<unsigned long>(UINT64MX) ) {
			return static_cast<unsigned long>(val);
		}
		else {
			cerr << "WARNING:: Converting keyword " << keyword << " from parsed unsigned long " << val << " to unsigned long overflowed !" << "\n";
			return 0;
		}
	}
	else
		return 0;
}


string sizet2str( const size_t val ) {
	stringstream ss;
	ss << val;
	return ss.str();
}


string real2str( const bio_real val ) {
	stringstream ss;
	ss << val;
	return ss.str();
}


string uint2str( const unsigned int val ) {
	stringstream ss;
	ss << val;
	return ss.str();
}

string int2str( const int val ) {
	stringstream ss;
	ss << val;
	return ss.str();
}


string bool2str( const bool val ) {
	stringstream ss;
	ss << val;
	return ss.str();
}

//predefined values
unsigned int ConfigShared::SimID = 0;
unsigned int ConfigShared::RndDescrStatsPRNGSeed = -1;
unsigned int ConfigShared::RndDescrStatsPRNGDiscard = 700000;


/*
vector<size_t> ConfigShared::MySpacegroups = { 62, 141, 194, 221, 225, 229 };
map<unsigned char, elementinfo> ConfigShared::MyElementsZ = {
		{static_cast<unsigned char>( 1), elementinfo( 1.008, 1)},
		{static_cast<unsigned char>( 2), elementinfo( 4.002602, 2)} };
vector<isotopeinfo> ConfigShared::MyIsotopes = {
	isotopeinfo( 1.0078250322, 0.99972, 1, 1, false),
	isotopeinfo( 2.0141017781, 0.00028, 2, 1, false) }
*/
