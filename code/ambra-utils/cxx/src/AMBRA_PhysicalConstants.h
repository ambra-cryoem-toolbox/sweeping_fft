//##MK::GPLV3

#ifndef __AMBRA_ABINITIO_H__
#define __AMBRA_ABINITIO_H__

//natural constants
#define MYPI							(3.141592653589793238462643383279502884197169399375105820974)
#define KBOLTZMANN						(1.3806488e-23)		//J/Kmol
#define ECHARGE							(1.602176565e-19)	//Coulomb
#define NAVOGADRO						(6.022140857e+23)	//1/mol
#define RGAS							(8.31446154) 		//(Navogadro)*(kboltzmann)) //J/K/mol

//natural beauty
#define SQR(a)							((a)*(a))
#define CUBE(a)							((a)*(a)*(a))
#define MIN(X,Y)						(((X) < (Y)) ? (X) : (Y))
#define MAX(X,Y)						(((X) < (Y)) ? (Y) : (X))
#define CLAMP(x,lo,hi)					(MIN(MAX((x), (lo)), (hi)))


//MK::coordinate system is right-handed x,y,z
#define AMBRA_XAXIS						0
#define AMBRA_YAXIS						1
#define AMBRA_ZAXIS						2

#endif
