//##MK::GPLV3

#include "AMBRA_Profiling.h"


void profiler::prof_elpsdtime_and_mem(const string whichenv,
		const unsigned short category, const unsigned short parallelism,
		memsnapshot const & mem, const double st, const double en )
{
	evn.push_back( plog(st, en, mem.virtualmem, mem.residentmem, whichenv, category, parallelism, evn.size()) );
}


size_t profiler::get_memory_max_on_node()
{
	//http://nadeausoftware.com/articles/2012/09/c_c_tip_how_get_physical_memory_size_system
	//#ifdef defined(SYSTEMSPECIFIC_POSIX_PAGESIZE) && defined(SYSTEMSPECIFIC_POSIX_NPAGES)
	/* FreeBSD, Linux, OpenBSD, and Solaris. -------------------- */
	return static_cast<size_t>(sysconf(SYSTEMSPECIFIC_POSIX_PAGESIZE)) *
				static_cast<size_t>(sysconf(SYSTEMSPECIFIC_POSIX_NPAGES));
	//#endif
}


memsnapshot profiler::get_memoryconsumption( void )
{
	//from http://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-run-time-in-c
	//according to D. Morovoz/T. Peterka tess2 code https://github.com/diatomic/tess2/blob/90ca0536299cdc6bbd6b2d1547d939ffbea68539/examples/memory.h
	//'file' stat seems to give the most reliable results

	//ultimately this is file system interaction, so
	//MK::MUST NOT BE CALLED FROM WITHIN THREADED REGION
	memsnapshot out = memsnapshot();

	ifstream stat_stream("/proc/self/stat", ios_base::in);
	if ( stat_stream.good() == true ) {
		//dummies for leading entries in stat we don't care about
		string pid, comm, state, ppid, pgrp, session, tty_nr;
		string tpgid, flags, minflt, cminflt, majflt, cmajflt;
		string utime, stime, cutime, cstime, priority, nice;
		string O, itrealvalue, starttime;

		//the two fields we want
		unsigned long vsize;
		long rss;

		stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
					>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
					>> utime >> stime >> cutime >> cstime >> priority >> nice
					>> O >> itrealvalue >> starttime >> vsize >> rss;  //don't care about the rest

		stat_stream.close();

		long page_size_byte = sysconf(SYSTEMSPECIFIC_POSIX_PAGESIZE);
		out.virtualmem = static_cast<size_t>(vsize);
		out.residentmem = static_cast<size_t>(rss*page_size_byte);
	}
	return out;
}


size_t profiler::get_nentries( void ){
	return evn.size();
}


bool SortProfLogAscWallClock( plog & first, plog & second )
{
	return first.dt < second.dt;
}


void profiler::report_memory( pair<size_t,size_t> const & in )
{
	cout << "VM/RSS in Bytes\t\t" << in.first << "\t\t" << in.second << endl;

	cout << "VM/RSS in GB\t\t" << BYTE2GIGABYTE(static_cast<double>(in.first)) << "\t\t" << BYTE2GIGABYTE(static_cast<double>(in.second)) << endl;
}


void profiler::spit_profiling( const string toolnm, const unsigned int simid, const int rank )
{
	//##MK::further optimization aand convenience tasks: bundle all in one file, incr ID and so forth
	//##MK::suboptimal... one file per rank
	string fn = "AMBRA." + toolnm + ".SimID." + to_string(simid) + ".Rank." + to_string(rank) + ".MyProfiling.csv";

	ofstream csvlog;
	csvlog.open(fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "What;ID;ParallelismInfo;ProcessVirtualMemory;ProcessResidentSetSize;WallClock;CumulatedWallClock;WallClockFraction\n";
		csvlog<< ";;;B;B;s;s;1\n";
		csvlog << "What;ID;ParallelismInfo;ProcessVirtualMemory;ProcessResidentSetSize;WallClock;CumulatedWallClock;WallClockFraction\n";

		//build map of timing data categories
		map<unsigned short, string> parallelism;
		parallelism[PARALLEL] = "PARALLEL";
		parallelism[SEQUENTIAL] = "SEQUENTIAL";

		//sort events increasing wallclock time
		sort( evn.begin(), evn.end(), SortProfLogAscWallClock);

		//compute total time
		double dt_total = 0.f;
		for(auto it = evn.begin(); it != evn.end(); ++it) { dt_total += it->dt; }

		//report
		double dt_cumsum = 0.f;
		for (auto it = evn.begin(); it != evn.end(); ++it) {
			dt_cumsum += it->dt;
			csvlog << it->what << ";" << it->i;
			auto par = parallelism.find(it->pll);
			if ( par != parallelism.end() )
				csvlog << ";" << par->second;
			else
				csvlog << ";" << "UNKNOWN";

			if ( it->virtualmem != MEMORY_NOSNAPSHOT_TAKEN && it->residentmem != MEMORY_NOSNAPSHOT_TAKEN )
				csvlog << ";" << it->virtualmem << ";" << it->residentmem;
			else
				csvlog << ";" << "0" << ";" << "0"; //##MK::null here means not taken!

			csvlog << ";" << it->dt << ";" << dt_cumsum << ";" << (it->dt / dt_total) << "\n";
		}

		csvlog.flush();
		csvlog.close();
	}
	else {
		cerr << "Unable to write process-local profiling files" << "\n";
	}
}


vector<pparm> profiler::report_machine()
{
	//get name of the workstation currently executing the task, for MPI parallel queries of MASTER rank
	char* hostname = NULL;
	try {
		hostname = new char[SYSTEMSPECIFIC_POSIX_HOSTNAMEMAX];
		for( int i = 0; i < SYSTEMSPECIFIC_POSIX_HOSTNAMEMAX; i++ ) {
			hostname[i] = '\0'; //'' incompatible with gcc
		}
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error for hostname!" << "\n";
		return vector<pparm>();
	}

	gethostname( hostname, SYSTEMSPECIFIC_POSIX_HOSTNAMEMAX);
	string hnm = "";
	for( int i = 0; i < SYSTEMSPECIFIC_POSIX_HOSTNAMEMAX; i++ ) {
		if ( hostname[i] != 0x00 ) { //no reached the null terminator yet
			hnm = hnm + hostname[i];
		}
		else {
			break;
		}
	}
	delete [] hostname; hostname = NULL;

	//get maximum number of threads
	int nt = SINGLETHREADED;
	#pragma omp parallel shared(nt)
	{
		#pragma omp master
		{
			nt = omp_get_num_threads();
		}
	}
	stringstream ss_omp;
	ss_omp << nt;
	string str_omp = ss_omp.str();

	//get maximum number of processes
	int nrks = 0;
	MPI_Comm_size(MPI_COMM_WORLD, &nrks);
	stringstream ss_mpi;
	ss_mpi << nrks;
	string str_mpi = ss_mpi.str();

	vector<pparm> res;
	res.push_back( pparm( "HostName", hnm, "", "what is the name of the computer where the analysis was performed" ) );
	res.push_back( pparm( "OMPGetNumThreads", str_omp, "", "how many OpenMP threads were allowed per MPI process" ) );
	res.push_back( pparm( "MPIGetCommSize", str_mpi, "", "how many MPI processes worked on the analysis in total" ) );
	return res;
}

//program profiling should use double precision in general as MPI_Wtime() and omp_get_wtime() fires in double precision
