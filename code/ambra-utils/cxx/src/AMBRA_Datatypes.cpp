//##MK::GPLV3

#include "AMBRA_Datatypes.h"


bio_real p3d::dot( p3d const & b )
{
	return this->x*b.x + this->y*b.y + this->z*b.z;
}


p3d p3d::cross( p3d const & b )
{
	return p3d( this->y*b.z - this->z*b.y,
				this->z*b.x - this->x*b.z,
				this->x*b.y - this->y*b.x );
}


ostream& operator<<(ostream& in, p3d const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dm1 const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << ";" << val.m1 << "\n";
	return in;
}


ostream& operator<<(ostream& in, px3d const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


ostream& operator<<(ostream& in, px3dm1 const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << ";" << val.m1 << "\n";
	return in;
}


bio_real v3d::len()
{
	bio_real sqrlen = SQR(this->u) + SQR(this->v) + SQR(this->w);
	if ( sqrlen > EPSILON) {
		return sqrt(sqrlen);
	}
	else {
		return 0.0; //##MK::
	}
}


void v3d::normalize()
{
	bio_real sqrlen =  SQR(this->u) + SQR(this->v) + SQR(this->w);
	if ( sqrlen > EPSILON ) {
		bio_real norm = 1.0 / sqrt(sqrlen);
		this->u *= norm;
		this->v *= norm;
		this->w *= norm;
	}
	else {
		cerr << "v3d is too short to normalize numerically stable, v3d left unchanged !" << "\n";
	}
}


ostream& operator<<(ostream& in, v3d const & val)
{
	in << val.u << ";" << val.v << ";" << val.w << "\n";
	return in;
}


void aabb_p3d::scale()
{
	this->xsz = this->xmx - this->xmi;
	this->ysz = this->ymx - this->ymi;
	this->zsz = this->zmx - this->zmi;
}


p3d aabb_p3d::center()
{
	return p3d( 0.5*(this->xmi + this->xmx),
				0.5*(this->ymi + this->ymx),
				0.5*(this->zmi + this->zmx) );
}


bio_real aabb_p3d::volume()
{
	return 	(this->xmx - this->xmi) *
			(this->ymx - this->ymi) *
			(this->zmx - this->zmi);
}


void aabb_p3d::blowup( const bio_real di )
{
	this->xmi -= di;
	this->xmx += di;
	this->ymi -= di;
	this->ymx += di;
	this->zmi -= di;
	this->zmx += di;
	this->scale();
}


void aabb_p3d::blowup( const bio_real dx, const bio_real dy, const bio_real dz )
{
	this->xmi -= dx;
	this->xmx += dx;
	this->ymi -= dy;
	this->ymx += dy;
	this->zmi -= dz;
	this->zmx += dz;
	this->scale();
}


void aabb_p3d::add_epsilon_guard()
{
	this->xmi -= EPSILON;
	this->xmx += EPSILON;
	this->ymi -= EPSILON;
	this->ymx += EPSILON;
	this->zmi -= EPSILON;
	this->zmx += EPSILON;
	this->scale();
}


bool aabb_p3d::is_inside( p3d const & test )
{
	if ( test.x < this->xmi )
		return false;
	if ( test.x > this->xmx )
		return false;
	if ( test.y < this->ymi )
		return false;
	if ( test.y > this->ymx )
		return false;
	if ( test.z < this->zmi )
		return false;
	if ( test.z > this->zmx )
		return false;
	//not return so on box walls or inside
	return true;
}


/*
p3i aabb3d::blockpartitioning( const size_t p_total, const size_t p_perblock_target )
{
	p3i out = p3i( 1, 1, 1);
	if ( this->xsz > EPSILON ) {
		apt_real yrel = this->ysz / this->xsz;
		apt_real zrel = this->zsz / this->xsz;
		apt_real ix = pow(
			(static_cast<apt_real>(p_total) / p_perblock_target) / (yrel * zrel),
			(1.f/3.f) );

		out.x = ( static_cast<int>(floor(ix)) > 0 ) ? static_cast<int>(floor(ix)) : 1;
		apt_real iy = yrel * ix;
		out.y = ( static_cast<int>(floor(iy)) > 0 ) ? static_cast<int>(floor(iy)) : 1;
		apt_real iz = zrel * ix;
		out.z = ( static_cast<int>(floor(iz)) > 0 ) ? static_cast<int>(floor(iz)) : 1;
	}
	return out;
}
*/


ostream& operator<<(ostream& in, aabb_p3d const & val)
{
	in << "xmi / xmx = " << val.xmi << ";" << val.xmx << "\n";
	in << "ymi / ymx = " << val.ymi << ";" << val.ymx << "\n";
	in << "zmi / zmx = " << val.zmi << ";" << val.zmx << "\n";
	in << "xsz/ysz/zsz = " << val.xsz << ";" << val.ysz << ";" << val.zsz << "\n";
	//in << "center = " << val.center() << "\n";
	//in << "volume = " << val.volume() << "\n";
	return in;
}


aabb_px3d::aabb_px3d( aabb_p3d const & cuboidal_region )
{
	this->bounded_volume = aabb_p3d(
			cuboidal_region.xmi, cuboidal_region.xmx,
			cuboidal_region.ymi, cuboidal_region.ymx,
			cuboidal_region.zmi, cuboidal_region.zmx );
	/*
	this->guarded_volume = aabb_p3d(
			cuboidal_region.xmi, cuboidal_region.xmx,
			cuboidal_region.ymi, cuboidal_region.ymx,
			cuboidal_region.zmi, cuboidal_region.zmx );
	*/
	this->bounded_volume.scale();
	/*
	this->guarded_volume.scale();
	*/
	this->bounded_nx = 0;
	this->bounded_ny = 0;
	this->bounded_nz = 0;
	/*
	this->guarded_nx = 1;
	this->guarded_ny = 1;
	this->guarded_nz = 1;
	*/
	this->inside_xmi = 0;
	this->inside_xmx = 0; //imx is always one past the last
	this->inside_ymi = 0;
	this->inside_ymx = 0;
	this->inside_zmi = 0;
	this->inside_zmx = 0;
	/*
	this->halo_xmi = 0; //no halo upon default
	this->halo_xmx = 1;
	this->halo_ymi = 0;
	this->halo_ymx = 1;
	this->halo_zmi = 0;
	this->halo_zmx = 1;
	*/
}


/*
aabb_px3d::aabb_px3d( aabb_p3d const & bounded_region, aabb_p3d const & guarded_region, const bio_real di, const int halo )
{
	this->bounded_volume = aabb_p3d(
			bounded_region.xmi, bounded_region.xmx,
			bounded_region.ymi, bounded_region.ymx,
			bounded_region.zmi, bounded_region.zmx );
	this->guarded_volume = aabb_p3d(
			guarded_region.xmi, guarded_region.xmx,
			guarded_region.ymi, guarded_region.ymx,
			guarded_region.zmi, guarded_region.zmx );
	this->bounded_volume.scale();
	this->guarded_volume.scale();
	bio_real rnx = ceil( this->bounded_volume.xsz / di );
	bio_real rny = ceil( this->bounded_volume.ysz / di );
	bio_real rnz = ceil( this->bounded_volume.zsz / di );
	unsigned long inx = static_cast<unsigned long>(rnx);

	if ( ini > 0 ) {
		if ( ini < UINT32MX ) {
		}
	}
	else {
		ini = 1;
	}
}
*/

/*
px3d aabb_px3d::where( const int ix, const int iy, const int iz )
{

}
*/

ostream& operator<<(ostream& in, aabb_px3d const & val)
{
	in << "bounded_region = " << "\n";
	in << "   xmi / xmx = " << val.bounded_volume.xmi << ";" << val.bounded_volume.xmx << "\n";
	in << "   ymi / ymx = " << val.bounded_volume.ymi << ";" << val.bounded_volume.ymx << "\n";
	in << "   zmi / zmx = " << val.bounded_volume.zmi << ";" << val.bounded_volume.zmx << "\n";
	in << "   xsz/ysz/zsz = " << val.bounded_volume.xsz << ";" << val.bounded_volume.ysz << ";" << val.bounded_volume.zsz << "\n";
	in << "   nx/ny/nz = " << val.bounded_nx << ";" << val.bounded_ny << ";" << val.bounded_nz << "\n";
	in << "inside xmi/xmx/ymi/ymx/zmi/zmx = " << "\n";
	in << "   xmi / xmx = " << val.inside_xmi << ";" << val.inside_xmx << "\n";
	in << "   ymi / ymx = " << val.inside_ymi << ";" << val.inside_ymx << "\n";
	in << "   zmi / zmx = " << val.inside_zmi << ";" << val.inside_zmx << "\n";
	/*
	in << "guarded_region = " << "\n";
	in << "   xmi / xmx = " << val.guarded_volume.xmi << ";" << val.guarded_volume.xmx << "\n";
	in << "   ymi / ymx = " << val.guarded_volume.ymi << ";" << val.guarded_volume.ymx << "\n";
	in << "   zmi / zmx = " << val.guarded_volume.zmi << ";" << val.guarded_volume.zmx << "\n";
	in << "   xsz/ysz/zsz = " << val.guarded_volume.xsz << ";" << val.guarded_volume.ysz << ";" << val.guarded_volume.zsz << "\n";
	in << "   nx/ny/nz = " << val.guarded_nx << ";" << val.guarded_ny << ";" << val.guarded_nz << "\n";
	in << "halo xmi/xmx/ymi/ymx/zmi/zmx = " << "\n";
	in << "   xmi / xmx = " << val.halo_xmi << ";" << val.halo_xmx << "\n";
	in << "   ymi / ymx = " << val.halo_ymi << ";" << val.halo_ymx << "\n";
	in << "   zmi / zmx = " << val.halo_zmi << ";" << val.halo_zmx << "\n";
	*/
	return in;
}


discretized_volume::discretized_volume()
{
}


discretized_volume::~discretized_volume()
{
}
