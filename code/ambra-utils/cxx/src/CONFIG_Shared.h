//##MK::GPLV3

#ifndef __AMBRA_UTILS_CONFIG_SHARED_H__
#define __AMBRA_UTILS_CONFIG_SHARED_H__

#include "AMBRA_MPIDatatypes.h"

bio_real str2real( const string str );
string read_xml_attribute_string( xml_node<> const * in, const string keyword );
bio_real read_xml_attribute_real( xml_node<> const * in, const string keyword );
unsigned short read_xml_attribute_uint16( xml_node<> const * in, const string keyword );
unsigned int read_xml_attribute_uint32( xml_node<> const * in, const string keyword );
int read_xml_attribute_int32( xml_node<> const * in, const string keyword );
bool read_xml_attribute_bool( xml_node<> const * in, const string keyword );
unsigned long read_xml_attribute_uint64( xml_node<> const * in, const string keyword );
string sizet2str( const size_t val );
string real2str( const bio_real val );
string uint2str( const unsigned int val );
string int2str( const int val );
string bool2str( const bool val );


class ConfigShared
{
public:

	static unsigned int SimID;
	static unsigned int RndDescrStatsPRNGSeed;
	static unsigned int RndDescrStatsPRNGDiscard;

	/*
	static map<unsigned char, elementinfo> MyElementsZ;
	static vector<isotopeinfo> MyIsotopes;
	*/
};

#endif
