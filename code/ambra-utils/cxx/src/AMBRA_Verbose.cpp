//##MK::GPLV3

#include "AMBRA_Verbose.h"

//genering global functions to report state, warnings and erros
void reporting( const int rank, const string what )
{
	cout << "VERBOSE::" << rank << " " << what << "\n";
}

void reporting( const string what )
{
	cout << "VERBOSE::" << what << "\n";
}


void complaining( const int rank, const string what )
{
	cout << "WARNING::" << rank << " " << what << "\n";
}

void complaining( const string what )
{
	cout << "WARNING::" << what << "\n";
}


void stopping( const int rank, const string what )
{
	cerr << "ERROR::" << rank << " " << what << "\n";
}

void stopping( const string what )
{
	cerr << "ERROR::" << what << "\n";
}


ostream& operator<<(ostream& in, pparm const & val)
{
	in << "Keyword " << val.keyword << "\n";
	in << "Value " << val.value << "\n";
	in << "Unit " << val.unit << "\n";
	in << "Comment " << val.info << "\n";
	return in;
}

