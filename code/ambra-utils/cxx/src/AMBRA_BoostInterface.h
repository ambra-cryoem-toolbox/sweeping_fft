//##MK::GPLV3

#ifndef __AMBRA_UTILS_BOOST_INTERFACE_H__
#define __AMBRA_UTILS_BOOST_INTERFACE_H__

#include "AMBRA_STLIncludes.h"
#include "AMBRA_Numerics.h"
#include "AMBRA_PhysicalConstants.h"
#include "AMBRA_UnitConversions.h"
#include "AMBRA_Parallelization.h"

//#define UTILIZE_BOOST

#ifdef UTILIZE_BOOST
	//boost
	#include <boost/algorithm/string.hpp>
	#include <boost/math/special_functions/bessel.hpp>
#else
	//C++ processing of regular expression to avoid having to use boost for now
	#include <regex>
	using std::regex;
	using std::string;
	using std::sregex_token_iterator;
#endif


#endif
