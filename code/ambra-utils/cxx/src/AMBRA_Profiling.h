//##MK::GPLV3

#ifndef __AMBRA_UTILS_PROFILING_H__
#define __AMBRA_UTILS_PROFILING_H__

//program profiling should use double precision in general as
//MPI_Wtime() and omp_get_wtime() fires in double precision

#include "AMBRA_Verbose.h"

//type of computational operations
#define XXX							0		//default, unspecified

#define UNKNOWN						-1
#define SEQUENTIAL					0
#define PARALLEL					1
#define MPI							10
#define OMP							20
#define CUD							30
#define ACC							40


#define MEMORY_NOSNAPSHOT_TAKEN		-1


struct memsnapshot
{
	size_t virtualmem;
	size_t residentmem;
	memsnapshot() : virtualmem(MEMORY_NOSNAPSHOT_TAKEN),
			residentmem(MEMORY_NOSNAPSHOT_TAKEN) {}
	memsnapshot(const size_t _vm, const size_t _rm) :
		virtualmem(_vm), residentmem(_rm) {}
};


struct plog
{
	double dt;
	double tstart;
	double tend;
	size_t virtualmem;		//virtual memory consumption in bytes
	size_t residentmem;		//resident set size in bytes, i.e. number of pages process as in real memory times system specific page size
	string what;
	unsigned short typ;		//task identifier
	unsigned short pll;		//parallelism identifier
	unsigned int i;			//running number to identify the which-th snapshot
							//used because for easier utilizability of the result
							//we sort in ascending processing time thereby however having the mem data not as a time trajectory


	plog() : dt(0.0), tstart(0.0), tend(0.0), virtualmem(-1), residentmem(-1),
			what(""), typ(XXX), pll(SEQUENTIAL), i(0) {}
	plog(const double _dt, const size_t _vm, const size_t _rm, const string _s,
			const unsigned short _t, const unsigned short _p, const unsigned int _i) :
				dt(_dt), tstart(0.0), tend(0.0), virtualmem(_vm), residentmem(_rm),
				what(_s), typ(_t), pll(_p), i(_i) {}
	plog(const double _ts, const double _te, const string _s,
			const unsigned short _t, const unsigned short _p, const unsigned int _i) :
				dt(_te - _ts), tstart(_ts), tend(_te), virtualmem(-1), residentmem(-1),
				what(_s), typ(_t), pll(_p), i(_i) {}	//version not tracking memory consumption
	plog(const double _ts, const double _te, const size_t _vm, const size_t _rm,
			const string _s, const unsigned short _t, const unsigned short _p,
			const unsigned int _i) :
				dt(_te - _ts), tstart(_ts), tend(_te), virtualmem(_vm), residentmem(_rm),
				what(_s), typ(_t), pll(_p), i(_i) {}	//version tracking memory consumption
};


class profiler
{
public:
	profiler() {};
	~profiler() {};

	void prof_elpsdtime_and_mem(const string whichenv, const unsigned short category,
			const unsigned short parallelism, memsnapshot const & mem, const double st, const double en);
	size_t get_memory_max_on_node();
	memsnapshot get_memoryconsumption( void );
	size_t get_nentries( void );
	void report_memory( pair<size_t, size_t> const & in );
	void spit_profiling( const string toolnm, const unsigned int simid, const int rank );
	vector<pparm> report_machine();

//private:
	vector <plog> evn;
};


#endif
