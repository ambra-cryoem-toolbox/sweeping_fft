//##MK::GPLV3

#ifndef __AMBRA_UTILS_PARALLELIZATION_H__
#define __AMBRA_UTILS_PARALLELIZATION_H__

#include <mpi.h>
#include <omp.h>

#define MASTER									0
#define SINGLETHREADED							1
#define	SINGLEPROCESS							1

//implicitly performance affecting choices

//file read ahead system related settings
#define SEQIO_READ_CACHE						((10)*(1024)*(1024)) //bytes
#define MPIIO_READ_CACHE						((10)*(1024)*(1024)) //bytes

#endif
