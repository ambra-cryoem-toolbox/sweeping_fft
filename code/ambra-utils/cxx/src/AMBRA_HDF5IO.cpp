//##MK::GPLV3

#include "AMBRA_HDF5IO.h"

ostream& operator<<(ostream& in, h5iometa const & val)
{
	in << val.dsetnm << "__" << val.nr << "__" << val.nc << "\n";
	return in;
}


ostream& operator<<(ostream& in, h5dims const & val)
{
	in << val.nr << "__" << val.nc << "\n";
	return in;
}


ostream& operator<<(ostream& in, h5offsets const & val)
{
	in << "nr0/nr1__" << val.nr0 << "__" << val.nr1 << "__nc0/nc1__" << val.nc0 << "__" << val.nc1 << "__nrmx/ncrmx__" << val.nrmax << "__" << val.ncmax << "\n";
	return in;
}


bool h5offsets::is_within_bounds( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1 )
{
	if ( _nr0 >= this->nr0 && _nr1 <= (this->nr0 + this->nr1) &&
			_nc0 >= this->nc0 && _nc1 <= (this->nc0 + this->nc1) )
		return true;
	else
		return false;
}


size_t h5offsets::nrows()
{
	return this->nr1 - this->nr0;
}


size_t h5offsets::ncols()
{
	return this->nc1 - this->nc0;
}


h5iometa2d::h5iometa2d()
{
	h5fn = "";
	dsetnm = "";
	dtype = NONE;
	shape[0] = 0;
	shape[1] = 0;
}

h5iometa2d::h5iometa2d( const string _dn, const MYHDF5_DATATYPES _dtyp, const size_t _nr, const size_t _nc )
{
	h5fn = "";
	dsetnm = _dn;
	dtype = _dtyp;
	shape[0] = _nr;
	shape[1] = _nc;
};


size_t h5iometa2d::npx2d()
{
	return this->shape[0] * this->shape[1];
}


ostream& operator<<(ostream& in, h5iometa2d const & val)
{
	in << val.h5fn << "__" << val.dsetnm << "\t\t" << val.dtype << "\t\t" << val.shape[0] << ";" << val.shape[1] << "\n";
	return in;
}


h5dims2d::h5dims2d()
{
	for( int i = 0; i < 2; i++ ) {
		shape[i] = 0;
	}
}


h5dims2d::h5dims2d(const size_t _nr, const size_t _nc)
{
	shape[0] = _nr;
	shape[1] = _nc;
}


ostream& operator<<(ostream& in, h5dims2d const & val)
{
	in << val.shape[0] << ";" << val.shape[1] << "\n";
	return in;
}


h5offs2d::h5offs2d()
{
	nr0 = -1;
	nr1 = -1;
	nc0 = -1;
	nc1 = -1;
}


h5offs2d::h5offs2d( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1 )
{
	nr0 = _nr0;
	nr1 = _nr1;
	nc0 = _nc0;
	nc1 = _nc1;
}


ostream& operator<<(ostream& in, h5offs2d const & val)
{
	in << "nr0/nr1 " << val.nr0 << ";" << val.nr1 << "\n";
	in << "nc0/nc1 " << val.nc0 << ";" << val.nc1 << "\n";
	return in;
}


h5iometa3d::h5iometa3d()
{
	h5fn = "";
	dsetnm = "";
	dtype = NONE;
	for( int i = 0; i < 3; i++ ) {
		shape[i] = 0;
	}
}


h5iometa3d::h5iometa3d( const string _dn, const MYHDF5_DATATYPES _dtyp, const size_t _nc, const size_t _nr, const size_t _nl )
{
	h5fn = "";
	dsetnm = _dn;
	dtype = _dtyp;
	shape[0] = _nc;
	shape[1] = _nr;
	shape[2] = _nl;
}


size_t h5iometa3d::npx3d()
{
	return this->shape[0] * this->shape[1] * this->shape[2];
}


ostream& operator<<(ostream& in, h5iometa3d const & val)
{
	in << val.h5fn << "__" << val.dsetnm << "\t\t" << val.dtype << "\t\t" << val.shape[0] << ";" << val.shape[1] << ";" << val.shape[2] << "\n";
	return in;
}


h5dims3d::h5dims3d()
{
	for ( int i = 0; i < 3; i++ ) {
		shape[i] = 0;
	}
}


h5dims3d::h5dims3d( const size_t _nc, const size_t _nr, const size_t _nl )
{
	shape[0] = _nc;
	shape[1] = _nr;
	shape[2] = _nl;
}


ostream& operator<<(ostream& in, h5dims3d const & val)
{
	in << val.shape[0] << ";" << val.shape[1] << val.shape[2] << "\n";
	return in;
}


h5offs3d::h5offs3d()
{
	nc0 = -1;
	nc1 = -1;
	nr0 = -1;
	nr1 = -1;
	nl0 = -1;
	nl1 = -1;
}


h5offs3d::h5offs3d( const size_t _nc0, const size_t _nc1, const size_t _nr0, const size_t _nr1, const size_t _nl0, const size_t _nl1 )
{
	nc0 = _nc0;
	nc1 = _nc1;
	nr0 = _nr0;
	nr1 = _nr1;
	nl0 = _nl0;
	nl1 = _nl1;
}


ostream& operator<<(ostream& in, h5offs3d const & val)
{
	in << "nc0/nc1 " << val.nc0 << ";" << val.nc1 << "\n";
	in << "nr0/nr1 " << val.nr0 << ";" << val.nr1 << "\n";
	in << "nl0/nl1 " << val.nl0 << ";" << val.nl1 << "\n";
	return in;
}



void debug_hdf5( void )
{
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	* Copyright by The HDF Group.                                               *
	* Copyright by the Board of Trustees of the University of Illinois.         *
	* All rights reserved.                                                      *
	*                                                                           *
	* This file is part of HDF5.  The full HDF5 copyright notice, including     *
	* terms governing use, modification, and redistribution, is contained in    *
	* the COPYING file, which can be found at the root of the source code       *
	* distribution tree, or in https://support.hdfgroup.org/ftp/HDF5/releases.  *
	* If you do not have access to either file, you may request a copy from     *
	* help@hdfgroup.org.                                                        *
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	/*
	*  This example illustrates how to create a dataset that is a 4 x 6
	*  array.  It is used in the HDF5 Tutorial.
	*/

	//#include "hdf5.h"
	#define FILE "dset.h5"
	//int main() {
	hid_t       file_id, dataset_id, dataspace_id;  /* identifiers */
	hsize_t     dims[2];
	herr_t      status;

	/* Create a new file using default properties. */
	file_id = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	/* Create the data space for the dataset. */
	dims[0] = 4;
	dims[1] = 6;
	dataspace_id = H5Screate_simple(2, dims, NULL);

	/* Create the dataset. */
	dataset_id = H5Dcreate2(file_id, "/dset", H5T_STD_I32BE, dataspace_id,
						  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	/* End access to the dataset and release resources used by it. */
	status = H5Dclose(dataset_id);

	/* Terminate access to the data space. */
	status = H5Sclose(dataspace_id);

	/* Close the file. */
	status = H5Fclose(file_id);
}


h5Hdl::h5Hdl()
{
	status = 0;
	fileid = 0;
	ftypid = 0;
	groupid = 0;
	mtypid = 0;
	mspcid = 0;
	dsetid = 0;
	dspcid = 0;
	cparms = 0;
	fspcid = 0;
	attrid = 0;
	attrtyp = 0;

	dims[0] = 0; 		dims[1] = 0;
	maxdims[0] = 0; 	maxdims[1] = 0;
	offset[0] = 0; 		offset[1] = 0;
	dimsnew[0] = 0; 	dimsnew[1] = 0;

	nrows = 0;
	ncols = 0;
	BytesPerChunk = 0;
	RowsPerChunk = 0;
	ColsPerChunk = 0;
	RowsColsPerChunk = 0;
	NChunks = 0;
	TotalWriteHere = 0;
	TotalWritePortion = 0;
	TotalWriteAllUsed = 0;
	TotalWriteAllChunk = 0;

	h5resultsfn = "";

	u32le_buf = NULL;
	f64le_buf = NULL;
}


h5Hdl::~h5Hdl()
{
	delete [] u32le_buf; u32le_buf = NULL;
	delete [] f64le_buf; f64le_buf = NULL;
}


void h5Hdl::reinitialize()
{
	status = 0;
	fileid = 0;
	ftypid = 0;
	groupid = 0;
	mtypid = 0;
	dsetid = 0;
	dspcid = 0;
	cparms = 0;
	fspcid = 0;
	attrid = 0;

	dims[0] = 0; 		dims[1] = 0;
	maxdims[0] = 0; 	maxdims[1] = 0;
	offset[0] = 0; 		offset[1] = 0;
	dimsnew[0] = 0; 	dimsnew[1] = 0;

	nrows = 0;
	ncols = 0;
	BytesPerChunk = 0;
	RowsPerChunk = 0;
	ColsPerChunk = 0;
	RowsColsPerChunk = 0;
	NChunks = 0;
	TotalWriteHere = 0;
	TotalWritePortion = 0;
	TotalWriteAllUsed = 0;
	TotalWriteAllChunk = 0;

	h5resultsfn = "";

	delete [] u32le_buf;
	delete [] f64le_buf;
}


int h5Hdl::create_file( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::query_contiguous_matrix_dims( const string _dn, h5offsets & offsetinfo )
{
	offsetinfo = h5offsets();

	fileid = H5Fopen(h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	//status = H5Eset_auto(NULL, NULL);
	//cout << "Checking existence of " << _dn.c_str() << " status " << status << "\n";
	status = H5Gget_objinfo (fileid, _dn.c_str(), 0, NULL);
cout << "Checking existence of " << _dn.c_str() << " status " << status << "\n";

	if ( status == 0 ) {
		//group exists, so attempt to read dimensions of existent dataset
		dsetid = H5Dopen2( fileid, _dn.c_str(), H5P_DEFAULT );
	//cout << "Opening2 " << _dn.c_str() << dsetid << "\n";
		dspcid = H5Dget_space(dsetid);
	//cout << "Get space " << dspcid << "\n";
		const int ndims = H5Sget_simple_extent_ndims(dspcid);

		if ( ndims == 2 ) {
			hsize_t dims[ndims];
			status = H5Sget_simple_extent_dims(dspcid, dims, NULL);
		cout << "Getting simple extent dims " << status << "\n";

			offsetinfo = h5offsets( 0, dims[0], 0, dims[1], dims[0], dims[1] );
		}
		status = H5Sclose(dspcid);
		status = H5Dclose(dsetid);
		status = H5Fclose(fileid);

		return WRAPPED_HDF5_SUCCESS;
	}
	//implicit else
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_DOESNOTEXIST;
}


int h5Hdl::create_group( const string grpnm )
{
	//##MK::check if file exists
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
		cerr << "Opening file " << h5resultsfn << " failed !" << "\n";
		return WRAPPED_HDF5_FAILED;
	}
	groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Creation of group " << grpnm << " failed !" << "\n";
		return WRAPPED_HDF5_GRPACCESS_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Group handle closure failed " << status << "\n";
		return WRAPPED_HDF5_GRPACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "File handle closure failed " << status << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	return WRAPPED_HDF5_SUCCESS;
}



int h5Hdl::create_group( const string h5fn, const string grpnm )
{
	//##MK::check if file exists
	fileid = H5Fopen(h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
		cerr << "Opening file " << h5fn << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
	groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Creation of group " << grpnm << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Group handle closure failed " << status << "\n";
		return WRAPPED_HDF5_GRPACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "File handle closure failed " << status << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	return WRAPPED_HDF5_SUCCESS;
}


bool h5Hdl::probe_group_existence( const string grpnm )
{
	//##MK::check if file exists
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
		cerr << "Opening file " << h5resultsfn << " failed!" << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//status = H5Eset_auto( NULL, NULL );
	status = H5Gget_objinfo( fileid, grpnm.c_str(), 0, NULL );
	if ( status == 0 ) {
		status = H5Fclose( fileid );
		if ( status < 0 ) {
			cerr << "File handle closure failed " << status << "\n";
			return WRAPPED_HDF5_FILEACCESS_FAILED;
		}
		return true;
	}
	else {
		status = H5Fclose(fileid);
		if ( status < 0 ) {
			cerr << "File handle closure failed " << status << "\n";
			return WRAPPED_HDF5_FILEACCESS_FAILED;
		}
	}
	//cerr << "Group " << grpnm << " does either not exist or other error occurred!" << "\n";
	return false;
}


int h5Hdl::add_attribute( const string dsnm, const string unit, const string descr )
{
	//tags existent dataset dsnm with two string attributes: SIUnit (unit) and Description (descr)
	hid_t attr = 0;
	size_t len = 0;

	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
cout << "Writing " << dsnm << " unit attribute " << "\n";
	dsetid = H5Dopen2( fileid, dsnm.c_str(), H5P_DEFAULT );
	if ( dsetid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED " << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	attrid = H5Screate(H5S_SCALAR);
	if ( attrid < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED create" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	attrtyp = H5Tcopy(H5T_C_S1);
	if ( attrtyp < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED copy" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	//for example if unit = "seconds" len would be 7, strlen returns length minus null-terminator is not include
	len = strlen(unit.c_str());
	status = H5Tset_size(attrtyp, len);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED set size" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	status = H5Tset_strpad(attrtyp, H5T_STR_NULLTERM);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED set strpad" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	attr = H5Acreate2( dsetid, "SIUnit character attribute", attrtyp, attrid, H5P_DEFAULT, H5P_DEFAULT);
	if ( attr < 0 ) {
cerr << "WRAPPED_HDF5_ATTRWRITE_FAILED create" << "\n";
		return WRAPPED_HDF5_ATTRWRITE_FAILED;
	}
	status = H5Awrite( attr, attrtyp, unit.c_str() );
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_ATTRWRITE_FAILED write" << "\n";
		return WRAPPED_HDF5_ATTRWRITE_FAILED;
	}

cout << "Writing " << dsnm << " description attribute " << "\n";
	dsetid = H5Dopen2( fileid, dsnm.c_str(), H5P_DEFAULT );
	if ( dsetid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED " << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	attrid = H5Screate(H5S_SCALAR);
	if ( attrid < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED create" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	attrtyp = H5Tcopy(H5T_C_S1);
	if ( attrtyp < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED copy" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	//for example if unit = "seconds" len would be 7, strlen returns length minus null-terminator is not include
	len = strlen(descr.c_str());
	status = H5Tset_size(attrtyp, len);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED set size" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	status = H5Tset_strpad(attrtyp, H5T_STR_NULLTERM);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED set strpad" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	attr = H5Acreate2( dsetid, "Human-readable description character attribute", attrtyp, attrid, H5P_DEFAULT, H5P_DEFAULT);
	if ( attr < 0 ) {
cerr << "WRAPPED_HDF5_ATTRWRITE_FAILED create" << "\n";
		return WRAPPED_HDF5_ATTRWRITE_FAILED;
	}
	status = H5Awrite( attr, attrtyp, descr.c_str() );
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_ATTRWRITE_FAILED write" << "\n";
		return WRAPPED_HDF5_ATTRWRITE_FAILED;
	}

	status = H5Tclose(attrtyp);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED close" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	status = H5Sclose(attrid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_ATTRACCESS_FAILED close" << "\n";
		return WRAPPED_HDF5_ATTRACCESS_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_u8le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U8LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_u16le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U16LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_u32le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U32LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_i32le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_I32LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_i64le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_I64LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_u64le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_U64LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_i16le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_STD_I16LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_f32le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_IEEE_F32LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_contiguous_matrix_f64le( h5iometa const & h5info )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	hsize_t dims[2] = { h5info.nr, h5info.nc };
	hsize_t maxdims[2] = { h5info.nr, h5info.nc };
	dspcid = H5Screate_simple( 2, dims, maxdims );
	dsetid = H5Dcreate2( fileid, h5info.dsetnm.c_str(), H5T_IEEE_F64LE, dspcid,
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_string_ascii( const string grpnm, const string val )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );

	//get C string character representation of the ASCII string val
	hsize_t sdims[1] = { 1}; //a single string no an array of strings!

	size_t SDIM = val.length()+1;
	char* cstr = new char[SDIM];
	strcpy(cstr, val.c_str());

	//Create file and memory datatypes. For this example we will save
	//the strings as FORTRAN strings, therefore they do not need space
	//for the null terminator in the file.
	ftypid = H5Tcopy(H5T_FORTRAN_S1);
	status = H5Tset_size(ftypid, SDIM-1); //-1 because Fortran string we do not store null terminator
	mtypid = H5Tcopy(H5T_C_S1);
	status = H5Tset_size(mtypid, SDIM);

	//Create dataspace. Setting maximum size to NULL sets the maximum size to be the current size
	dspcid = H5Screate_simple(1, sdims, NULL);

	dsetid = H5Dcreate(fileid, grpnm.c_str(), ftypid, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dwrite(dsetid, mtypid, H5S_ALL, H5S_ALL, H5P_DEFAULT, cstr);

	//close and release resources
	delete [] cstr; cstr = NULL;
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Tclose(ftypid);
	status = H5Tclose(mtypid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_settings_entry( const string prefix, pparm in )
{
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );

	char u8[3][HDF5_STR_MAXLEN];
	for( int i = 0; i < 3; i++ ) {
		for( int j = 0; j < HDF5_STR_MAXLEN; j++ ) {
			u8[i][j] = '\0'; //'' not supported by gcc;
		}
	}
	for( size_t i = 0; i < in.value.length(); i++ ) { u8[0][i] = in.value.at(i); }
	for( size_t i = 0; i < in.unit.length(); i++ ) { u8[1][i] = in.unit.at(i); }
	for( size_t i = 0; i < in.info.length(); i++ ) { u8[2][i] = in.info.at(i); }

	hsize_t sdims[1] = { 3}; //an array of fixed-length ASCII strings!

	/*
	//get C string character representation of the ASCII string val
	size_t SDIM = in.value.length()+1;
	if ( (SDIM + 1) >= HDF5_STR_MAXLEN ) {
		cerr << "write_settings_entry for keyword " << in.keyword << " is longer than " << HDF5_STR_MAXLEN << "!" << "\n";
		return WRAPPED_HDF5_FAILED;
	}
	char* cstr = new char[HDF5_STR_MAXLEN];
	for( int i = 0; i < HDF5_STR_MAXLEN; i++ ) {
		cstr[i] = '\0';
	}
	strcpy(cstr, in.value.c_str());
	*/

	ftypid = H5Tcopy(H5T_FORTRAN_S1);
	status = H5Tset_size(ftypid, HDF5_STR_MAXLEN - 1);
	mtypid = H5Tcopy(H5T_C_S1);
	status = H5Tset_size(mtypid, HDF5_STR_MAXLEN);

	//Create dataspace. Setting maximum size to NULL sets the maximum size to be the current size
	dspcid = H5Screate_simple(1, sdims, NULL);
	string fwslash = "/";
	string dsnm = prefix + fwslash + in.keyword;
	dsetid = H5Dcreate(fileid, dsnm.c_str(), ftypid, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Dwrite(dsetid, mtypid, H5S_ALL, H5S_ALL, H5P_DEFAULT, u8[0] );

	//close and release resources
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Tclose(ftypid);
	status = H5Tclose(mtypid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::create_scalar_u64le( const string h5fn, const string grpnm, const size_t val )
{
	fileid = H5Fopen(h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	int rank = 1;
	hsize_t sdims[1] = { 1};
	dspcid = H5Screate_simple( rank, sdims, NULL);
	dsetid = H5Dcreate2(fileid, grpnm.c_str(), H5T_STD_U64LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	size_t wdata[1] = { val };
	status = H5Dwrite(dsetid, H5T_STD_U64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &wdata );
	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::init_chunked_matrix_u32le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc )
{
	//customizes h5Hdl properties for writing a matrix of U32LE nr x nc elements to an existent H5 file
	//we used a chunked data layout in the HDF5 file to allow for inplace compression
	//we caveat is that for arbitrary data the total number of elements nr*nc may not be integer multiple
	//of chunk size, hence we have to fill with buffer
	//##MK::in the future use new partial chunking feature of HDF5 1.10.2 to avoid section of trailing dummies on last chunk
	//we can still though use current concept for visualizing for instance Voronoi cells using XDMF
	//for this we need at least two implicit arrays, one encoding the explicit cell topology
	//another the cell vertex coordinates, however if nr*nc is not necessarily integer multiple of chunk size
	//this is ugly but works as within the XDMF file we can just tell the Dimension of the dataset
	//here we can use the trick that the dimension of the implicit 1d array can be defined shorter
	//than the actual size of the chunked data set

	//we use a several MB write buffer fill before writing the first chunk with dummy values
	//this is useful then fusing successively threadlocal data because threads know their local
	//write entry and exit positions on the global array thereby allowing to handle the complexity
	//of having threadlocal datasets potentially crossing chunk boundaries or generating partially filled chunks
	//which the next thread continues filling and writes to file
	nrows = nr; //real dataset size in number of elements on 1d implicit buffer of unsigned int is nr*nc
	ncols = nc;
	BytesPerChunk = static_cast<size_t>(1*1024*1024); //MK::use default chunk size 1MB, ##MK::performance study
	//##MK::value check needs to be integer multiple of sizeof(unsigned int)*ncols !
	RowsPerChunk = BytesPerChunk / (sizeof(unsigned int) * ncols);
	ColsPerChunk = ncols;
	RowsColsPerChunk = RowsPerChunk*ColsPerChunk;
	NChunks = static_cast<size_t>( ceil(static_cast<double>(nrows*ncols) / static_cast<double>(RowsColsPerChunk)) );

cout << "HDF5 RealDataRows/Cols_BytesPerChunk_Rows/Cols/Rows*ColsPerChunk_NChunksTotal\t\t" << nrows << ";" << ncols << ";" << BytesPerChunk << ";" << RowsPerChunk << ";" << ColsPerChunk << ";" << RowsColsPerChunk << ";" << NChunks << "\n";

	//allocate a read/write buffer to pass data to the H5 library call functions
	if ( u32le_buf != NULL ) {
		delete [] u32le_buf;
		u32le_buf = NULL;
	}
	else {
		try {
			u32le_buf = new unsigned int[RowsPerChunk*ColsPerChunk];
		}
		catch (bad_alloc &h5croak) {
			cerr << "Allocation error for write buffer in init_chunked_matrix_u32le HDF5" << "\n";
			return WRAPPED_HDF5_ALLOCERR;
		}
	}

	//open an existent H5 file with read/write access
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);

	//initialize in this file a chunked data set in an existent group

	//start processing chunks into buffer successively and write to HDF5 file
	dims[0] = RowsPerChunk; 		dims[1] = ColsPerChunk;
	maxdims[0] = H5S_UNLIMITED;		maxdims[1] = H5S_UNLIMITED;
	offset[0] = 0;					offset[1] = 0;
	dimsnew[0] = 0;					dimsnew[1] = ColsPerChunk;

	dspcid = H5Screate_simple(2, dims, maxdims);
	cparms = H5Pcreate(H5P_DATASET_CREATE);
	status = H5Pset_chunk( cparms, 2, dims);

	unsigned int fillval = HDF5_U32LE_DUMMYVALUE;
	status = H5Pset_fill_value(cparms, H5T_STD_U32LE, &fillval );
	dsetid = H5Dcreate2(fileid, dsetnm.c_str(), H5T_STD_U32LE, dspcid, H5P_DEFAULT, cparms, H5P_DEFAULT);

	//fill read/write buffer with dummy values which the preceeding write steps can parse
	for(size_t i = 0; i < RowsColsPerChunk; ++i) {
		u32le_buf[i] = HDF5_U32LE_DUMMYVALUE;
	}
	//and use in combination with the position offsets to always complete first individual chunks,
	//thereafter write them to file and refresh the read/write buffer with dummy values to avoid
	//having to read the file content for any preceeding thread whose data portion interval falls arbitrarily
	//on a chunk boundary
	TotalWriteHere = 0;
	TotalWritePortion = RowsColsPerChunk;
	TotalWriteAllUsed = nrows*ncols;
	TotalWriteAllChunk = NChunks*RowsColsPerChunk; //so many implicitly coded unsigned int to write in total (including potential buffer of trailing dummies)

	//do not close any resource handlers or the file, in what follows we will add data portions per thread
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_chunked_matrix_u32le( const string h5fn, const string dsetnm, vector<unsigned int> const & buffer )
{
	//write a local data portion using opened resources use previous allocated read/write buffer
	//and evaluating Total* interval positioning values to identify whether previous chunk is complete or not
	/*
	cout << "start/end/dims[0]/buffer.size()\t\t" << start << ";" << end << ";" << dims[0] << ";" << buffer.size() << "\n";
	//MK::[start, end) mark positions on the initialized current dataset were the buffer data should be written to
	//check that [start,end) is within initialized bounds
	if ( start >= dims[0] || end > dims[0] )
		return WRAPPED_HDF5_OUTOFBOUNDS;
	//check that [start,end) length is the same as the buffer
	if ( (end-start) != buffer.size() )
		return WRAPPED_HDF5_ARGINCONSISTENT;
	*/

	//challenge is that interval [start,end) is not necessarily aligned with the implicitly defined chunk boundaries
	//hence find first chunk which contains array index position start
	size_t LocalCompleted = 0; //how much of feed buffer already processed
	size_t LocalTotal = buffer.size(); //total size of the feed buffer

	size_t BufferWriteHere = 0; //where to start filling in h5 write buffer u32le_buf
	size_t BufferWriteNow = 0; //how many remaining fillable before u32le_buf is full and needs a flush

	do { //loop for as many chunks as fillable with feed buffer content
		//on the fly filling and writing back of completed chunk
		//prior to filling the very first value from feed buffer TotalWriteHere == 0
		//right after filling n-th chunk TotalWriteHere % RowsCols == 0 so buffer was full but already emptied
		//in both these cases the buffer is in a state scientifically completely unfilled
		//MK::so if we feed the results from the threadlocal buffers in the strict ascending order
		//of their threadIDs, i.e. begin with MASTER, the h5 write buffer was not yet flushed to file

		BufferWriteHere = TotalWriteHere % RowsColsPerChunk;
		BufferWriteNow = RowsColsPerChunk;
		//correct write interval end because potential feed buffer not large enough
		//correct write interval end additionally based on whether still sufficient place on buffer available
		if ( (BufferWriteHere+(LocalTotal-LocalCompleted)) < RowsColsPerChunk )
			BufferWriteNow = BufferWriteHere + LocalTotal-LocalCompleted;

cout << "1::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << "\n";
		for( size_t w = BufferWriteHere; w < BufferWriteNow; w++ ) {
			u32le_buf[w] = buffer.at(LocalCompleted);
			LocalCompleted++;
			TotalWriteHere++;
		}
cout << "2::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << "\n";

		//potentially write to H5 file
		if ( TotalWriteHere % RowsColsPerChunk == 0 ) {
cout << "3::Writing a full chunk" << "\n";
			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_STD_U32LE, dspcid, fspcid, H5P_DEFAULT, u32le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!

			//refresh buffer with dummies completely
			for ( size_t i = 0; i < RowsColsPerChunk; ++i )
				u32le_buf[i] = HDF5_U32LE_DUMMYVALUE;
		}

	} while ( LocalCompleted < LocalTotal );
	//processing of feed buffer done

cout << "--->Local done\t\t" << TotalWriteHere << "\t\t" << TotalWriteAllUsed << "\t\t" << TotalWriteAllChunk << "\n";

	//dont forget to pass potentially remaining part of the h5 write buffer to file
	if ( TotalWriteHere >= TotalWriteAllUsed ) { //everything which was planned in buffer
		//write h5 buffer out last time if not already happened
		if ( TotalWriteHere % RowsColsPerChunk != 0 ) {
cout << "4::Writing last chunk" << "\n";

			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_STD_U32LE, dspcid, fspcid, H5P_DEFAULT, u32le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!
		}
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::reset_chunked_matrix_u32le_aftercompletion()
{
cout << "Resetting u32le" << "\n";

	if ( u32le_buf != NULL ) {
		delete [] u32le_buf;
		u32le_buf = NULL;
	}

	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Sclose(fspcid);
	status = H5Pclose(cparms);
	status = H5Fclose(fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_u8le_atonce( const string h5fn, const string dsetnm,
				const size_t nr, const size_t nc, vector<unsigned char> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_STD_U8LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_STD_U8LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_u32le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<unsigned int> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_STD_U32LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_STD_U32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_u64le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<size_t> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_STD_U64LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_STD_U64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_f32le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<float> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_IEEE_F32LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_IEEE_F32LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_f64le_atonce( const string h5fn, const string dsetnm,
	const size_t nr, const size_t nc, vector<double> const & buffer )
{
	//open existing file and place data in there
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	dims[0] = nr; 		dims[1] = nc;

    dspcid = H5Screate_simple( 2, dims, NULL );
    dsetid = H5Dcreate( fileid, dsetnm.c_str(), H5T_IEEE_F64LE, dspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
    status = H5Dwrite( dsetid, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer.data() );
    status = H5Dclose (dsetid);
    status = H5Sclose (dspcid);
    status = H5Fclose (fileid);

	return WRAPPED_HDF5_SUCCESS;
}



int h5Hdl::write_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<unsigned char> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U8LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<unsigned short> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U16LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<unsigned int> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U32LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info,
		h5offsets const & offsetinfo, vector<int> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_I32LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_i64le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<long> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_I64LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_u64le_hyperslab( h5iometa const & h5info,
			h5offsets const & offsetinfo, vector<size_t> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_U64LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_i16le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, vector<short> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_STD_I16LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, vector<float> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::write_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, float* const buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	//bounds check and consistency buffer has the same length than planed
	//define offset
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dwrite( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, buffer );

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_contiguous_matrix_f64le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<double> const & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };


	//bounds check and consistency buffer has the same length than planed
	if ( buffer.size() == dimssub[0] * dimssub[1] ) {
		//define offset
		hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
		hsize_t count[2] = { dimssub[0], dimssub[1] };
		hsize_t stride[2] = { 1, 1};
		hsize_t block[2] = { 1, 1};
		fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
		dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

		mspcid = H5Screate_simple(2, dimssub, mxdims );
		dspcid = H5Dget_space( dsetid );
		status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
		status = H5Dwrite( dsetid, H5T_IEEE_F64LE, mspcid, dspcid, H5P_DEFAULT, buffer.data() );

	    status = H5Sclose(mspcid);
	    status = H5Sclose(dspcid);
	    status = H5Dclose(dsetid);
	    status = H5Fclose(fileid);
		return WRAPPED_HDF5_SUCCESS;
	}
	else {
		return WRAPPED_HDF5_INCORRECTDIMENSIONS;
	}
}


int h5Hdl::read_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<unsigned char> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	unsigned char* rbuf = NULL;
	try {
		rbuf = new unsigned char[nalloc];
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_U8LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READ_FAILED;
	}

	//##MK::pre-existent buffer! for OIM_INDEXING buffer = vector<float>( nalloc, 0.f );
	buffer = vector<unsigned char>( nalloc, 0x00 );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<unsigned short> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	unsigned short* rbuf = NULL;
	try {
		rbuf = new unsigned short[nalloc];
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_U16LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READ_FAILED;
	}

	//##MK::pre-existent buffer! for OIM_INDEXING buffer = vector<float>( nalloc, 0.f );
	buffer = vector<unsigned short>( nalloc, 0 );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<int> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	int* rbuf = NULL;
	try {
		rbuf = new int[nalloc];
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_I32LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READ_FAILED;
	}

	//##MK::pre-existent buffer! for OIM_INDEXING buffer = vector<float>( nalloc, 0.f );
	buffer = vector<int>( nalloc, 0 );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<float> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	float* rbuf = NULL;
	try {
		rbuf = new float[nalloc];
		//buffer = vector<float>( nalloc, 0.f );
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, rbuf ); //&(buffer.front()) ); //&buffer.at(0) );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READ_FAILED;
	}

	buffer = vector<float>( nalloc, 0.f );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info, h5offsets const & offsetinfo,
		vector<unsigned int> & buffer )
{
	//buffer carries implicitly stored matrices row blocks glued together along columns
	//subset dimensions planned in relation to entire dataset
	//global context of the dataset a portion of it should now be written
	hsize_t mxdims[2] = { h5info.nr, h5info.nc };

	//do the write offsets remain within mxdims?
	if ( offsetinfo.nr1 < offsetinfo.nr0 || offsetinfo.nc1 < offsetinfo.nc0 ||
			offsetinfo.nr1 > mxdims[0] || offsetinfo.nc1 > mxdims[1] ) {
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//the ni1 offsets (nr1,nc1) always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offsetinfo.nr1 - offsetinfo.nr0, offsetinfo.nc1 - offsetinfo.nc0 };

	hsize_t nalloc = dimssub[0]*dimssub[1];
	unsigned int* rbuf = NULL;
	try {
		rbuf = new unsigned int[nalloc];
		//buffer = vector<float>( nalloc, 0.f );
	}
	catch (bad_alloc &hdfcroak) {
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	//sufficent space to writebounds check and consistency buffer has the same length than planed
	hsize_t offset[2] = { offsetinfo.nr0, offsetinfo.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1};
	hsize_t block[2] = { 1, 1};
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	dsetid = H5Dopen2( fileid, h5info.dsetnm.c_str(), H5P_DEFAULT );

	mspcid = H5Screate_simple(2, dimssub, mxdims );
	dspcid = H5Dget_space( dsetid );
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	status = H5Dread( dsetid, H5T_STD_U32LE, mspcid, dspcid, H5P_DEFAULT, rbuf ); //&(buffer.front()) ); //&buffer.at(0) );
	if ( status < 0 ) {
		return WRAPPED_HDF5_READ_FAILED;
	}

	buffer = vector<unsigned int>( nalloc, 0.f );
	for( size_t i = 0; i < nalloc; ++i ) {
		buffer[i] = rbuf[i];
	}

	delete [] rbuf; rbuf = NULL;

	status = H5Sclose(mspcid);
	status = H5Sclose(dspcid);
	status = H5Dclose(dsetid);
	status = H5Fclose(fileid);
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::add_contiguous_tensor2d_u8le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<unsigned char> & buf )
{
//cout << "ifo.shape " << ifo.shape[0] << ";" << ifo.shape[1] << "\n";
	if ( offs.nr1 <= offs.nr0 || offs.nr1 > ifo.shape[0] ) {
cerr << "nc WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nc1 <= offs.nc0 || offs.nc1 > ifo.shape[1] ) {
cerr << "nr WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//mind that the ni1 offsets always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offs.nr1 - offs.nr0, offs.nc1 - offs.nc0 };
//cout << "dimssub " << dimssub[0] << ";" << dimssub[1] << "\n";
	hsize_t dims[2] = { ifo.shape[0], ifo.shape[1] };
	hsize_t mxdims[2] = { ifo.shape[0], ifo.shape[1] };
//cout << "mxdims " << mxdims[0] << ";" << mxdims[1] << "\n";

	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	mspcid = H5Screate_simple( 2, dims, mxdims );
	if ( mspcid < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	dsetid = H5Dcreate2( fileid, ifo.dsetnm.c_str(), H5T_STD_U8LE, mspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( dsetid < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	dspcid = H5Dget_space( dsetid );
	if ( dspcid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}

	hsize_t offset[2] = { offs.nr0, offs.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1 };
	hsize_t block[2] = { 1, 1 };
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_HYPERSLAB_FAILED" << "\n";
		return WRAPPED_HDF5_HYPERSLAB_FAILED;
	}
	status = H5Dwrite( dsetid, H5T_STD_U8LE, mspcid, dspcid, H5P_DEFAULT, buf.data() );
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}

	status = H5Sclose(mspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	status = H5Sclose(dspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::add_contiguous_tensor2d_u32le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<unsigned int> & buf )
{
//cout << "ifo.shape " << ifo.shape[0] << ";" << ifo.shape[1] << "\n";
	if ( offs.nr1 <= offs.nr0 || offs.nr1 > ifo.shape[0] ) {
cerr << "nc WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nc1 <= offs.nc0 || offs.nc1 > ifo.shape[1] ) {
cerr << "nr WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//mind that the ni1 offsets always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offs.nr1 - offs.nr0, offs.nc1 - offs.nc0 };
//cout << "dimssub " << dimssub[0] << ";" << dimssub[1] << "\n";
	hsize_t dims[2] = { ifo.shape[0], ifo.shape[1] };
	hsize_t mxdims[2] = { ifo.shape[0], ifo.shape[1] };
//cout << "mxdims " << mxdims[0] << ";" << mxdims[1] << "\n";

	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	mspcid = H5Screate_simple( 2, dims, mxdims );
	if ( mspcid < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	dsetid = H5Dcreate2( fileid, ifo.dsetnm.c_str(), H5T_STD_U32LE, mspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( dsetid < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	dspcid = H5Dget_space( dsetid );
	if ( dspcid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}

	hsize_t offset[2] = { offs.nr0, offs.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1 };
	hsize_t block[2] = { 1, 1 };
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_HYPERSLAB_FAILED" << "\n";
		return WRAPPED_HDF5_HYPERSLAB_FAILED;
	}
	status = H5Dwrite( dsetid, H5T_STD_U32LE, mspcid, dspcid, H5P_DEFAULT, buf.data() );
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}

	status = H5Sclose(mspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	status = H5Sclose(dspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::add_contiguous_tensor2d_i64le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<long long> & buf )
{
//cout << "ifo.shape " << ifo.shape[0] << ";" << ifo.shape[1] << "\n";
	if ( offs.nr1 <= offs.nr0 || offs.nr1 > ifo.shape[0] ) {
cerr << "nc WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nc1 <= offs.nc0 || offs.nc1 > ifo.shape[1] ) {
cerr << "nr WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//mind that the ni1 offsets always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offs.nr1 - offs.nr0, offs.nc1 - offs.nc0 };
//cout << "dimssub " << dimssub[0] << ";" << dimssub[1] << "\n";
	hsize_t dims[2] = { ifo.shape[0], ifo.shape[1] };
	hsize_t mxdims[2] = { ifo.shape[0], ifo.shape[1] };
//cout << "mxdims " << mxdims[0] << ";" << mxdims[1] << "\n";

	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	mspcid = H5Screate_simple( 2, dims, mxdims );
	if ( mspcid < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	dsetid = H5Dcreate2( fileid, ifo.dsetnm.c_str(), H5T_STD_I64LE, mspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( dsetid < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	dspcid = H5Dget_space( dsetid );
	if ( dspcid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}

	hsize_t offset[2] = { offs.nr0, offs.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1 };
	hsize_t block[2] = { 1, 1 };
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_HYPERSLAB_FAILED" << "\n";
		return WRAPPED_HDF5_HYPERSLAB_FAILED;
	}
	status = H5Dwrite( dsetid, H5T_STD_I64LE, mspcid, dspcid, H5P_DEFAULT, buf.data() );
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}

	status = H5Sclose(mspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	status = H5Sclose(dspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::add_contiguous_tensor2d_f32le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<float> & buf )
{
//cout << "ifo.shape " << ifo.shape[0] << ";" << ifo.shape[1] << "\n";
	if ( offs.nr1 <= offs.nr0 || offs.nr1 > ifo.shape[0] ) {
cerr << "nc WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nc1 <= offs.nc0 || offs.nc1 > ifo.shape[1] ) {
cerr << "nr WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//mind that the ni1 offsets always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offs.nr1 - offs.nr0, offs.nc1 - offs.nc0 };
//cout << "dimssub " << dimssub[0] << ";" << dimssub[1] << "\n";
	hsize_t dims[2] = { ifo.shape[0], ifo.shape[1] };
	hsize_t mxdims[2] = { ifo.shape[0], ifo.shape[1] };
//cout << "mxdims " << mxdims[0] << ";" << mxdims[1] << "\n";

	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	mspcid = H5Screate_simple( 2, dims, mxdims );
	if ( mspcid < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	dsetid = H5Dcreate2( fileid, ifo.dsetnm.c_str(), H5T_IEEE_F32LE, mspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( dsetid < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	dspcid = H5Dget_space( dsetid );
	if ( dspcid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}

	hsize_t offset[2] = { offs.nr0, offs.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1 };
	hsize_t block[2] = { 1, 1 };
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_HYPERSLAB_FAILED" << "\n";
		return WRAPPED_HDF5_HYPERSLAB_FAILED;
	}
	status = H5Dwrite( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, buf.data() );
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}

	status = H5Sclose(mspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	status = H5Sclose(dspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::add_contiguous_tensor2d_f64le_hyperslab( h5iometa2d const & ifo, h5offs2d const & offs, vector<double> & buf )
{
//cout << "ifo.shape " << ifo.shape[0] << ";" << ifo.shape[1] << "\n";
	if ( offs.nr1 <= offs.nr0 || offs.nr1 > ifo.shape[0] ) {
cerr << "nc WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nc1 <= offs.nc0 || offs.nc1 > ifo.shape[1] ) {
cerr << "nr WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//mind that the ni1 offsets always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[2] = { offs.nr1 - offs.nr0, offs.nc1 - offs.nc0 };
//cout << "dimssub " << dimssub[0] << ";" << dimssub[1] << "\n";
	hsize_t dims[2] = { ifo.shape[0], ifo.shape[1] };
	hsize_t mxdims[2] = { ifo.shape[0], ifo.shape[1] };
//cout << "mxdims " << mxdims[0] << ";" << mxdims[1] << "\n";

	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
	if ( fileid < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	mspcid = H5Screate_simple( 2, dims, mxdims );
	if ( dspcid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	dsetid = H5Dcreate2( fileid, ifo.dsetnm.c_str(), H5T_IEEE_F64LE, mspcid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( dsetid < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	dspcid = H5Dget_space( dsetid );
	if ( dspcid < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}

	hsize_t offset[2] = { offs.nr0, offs.nc0 };
	hsize_t count[2] = { dimssub[0], dimssub[1] };
	hsize_t stride[2] = { 1, 1 };
	hsize_t block[2] = { 1, 1 };
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_HYPERSLAB_FAILED" << "\n";
		return WRAPPED_HDF5_HYPERSLAB_FAILED;
	}
	status = H5Dwrite( dsetid, H5T_IEEE_F64LE, mspcid, dspcid, H5P_DEFAULT, buf.data() );
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}

	status = H5Sclose(mspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	status = H5Sclose(dspcid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::query_contiguous_tensor3d( const string dsetname, h5offs3d & offs )
{
	offs = h5offs3d();

	fileid = H5Fopen(h5resultsfn.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if ( fileid < 0 )
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	//status = H5Eset_auto(NULL, NULL);
	status = H5Gget_objinfo (fileid, dsetname.c_str(), 0, NULL);
	if ( status >= 0 ) {
		//group exists, so attempt to read dimensions of a possibly existent dataset
		dsetid = H5Dopen2( fileid, dsetname.c_str(), H5P_DEFAULT );
		if ( dsetid < 0 )
			return WRAPPED_HDF5_DSETACCESS_FAILED;
		dspcid = H5Dget_space(dsetid);
		if ( dspcid < 0 )
			return WRAPPED_HDF5_DSPACE_FAILED;

		const int ndims = H5Sget_simple_extent_ndims(dspcid);
		if ( ndims == 3 ) {
			hsize_t dims[ndims];
			status = H5Sget_simple_extent_dims(dspcid, dims, NULL);
			if ( status < 0 )
				return WRAPPED_HDF5_INCORRECTDIMENSIONS;

			offs = h5offs3d( 0, dims[0], 0, dims[1], 0, dims[2] );
		}
		else {
			return WRAPPED_HDF5_INCORRECTDIMENSIONS;
		}
		status = H5Sclose(dspcid);
		if ( status < 0 )
			return WRAPPED_HDF5_DSPACE_FAILED;
		status = H5Dclose(dsetid);
		if ( status < 0 )
			return WRAPPED_HDF5_DSETACCESS_FAILED;
		status = H5Fclose(fileid);
		if ( status < 0 )
			return WRAPPED_HDF5_FILEACCESS_FAILED;

		return WRAPPED_HDF5_SUCCESS;
	}
	status = H5Fclose(fileid);
	if ( status < 0 )
		return WRAPPED_HDF5_FILEACCESS_FAILED;

	return WRAPPED_HDF5_DOESNOTEXIST;
}


int h5Hdl::read_contiguous_tensor3d_u8le_hyperslab( h5iometa3d const & ifo, h5offs3d const & offs, vector<unsigned char> & buf )
{
	//buffer carries implicitly stored matrices columns data stacked along rows resulting in layers
	//cout << ifo.shape << ";" << ifo.shape[0] << ";" << ifo.shape[1] << ";" << ifo.shape[2] << "\n";
	if ( offs.nc1 <= offs.nc0 || offs.nc1 > ifo.shape[0] ) {
		cerr << "nc WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nr1 <= offs.nr0 || offs.nr1 > ifo.shape[1] ) {
		cerr << "nr WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nl1 <= offs.nl0 || offs.nl1 > ifo.shape[2] ) {
		cerr << "nl WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//mind that the ni1 offsets always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[3] = { offs.nc1 - offs.nc0, offs.nr1 - offs.nr0, offs.nl1 - offs.nl0 };
	//cout << "dimssub " << dimssub[0] << ";" << dimssub[1] << ";" << dimssub[2] << "\n";
	hsize_t mxdims[3] = { ifo.shape[0], ifo.shape[1], ifo.shape[2] };
	//cout << "mxdims " << mxdims[0] << ";" << mxdims[1] << ";" << mxdims[2] << "\n";

	hsize_t nalloc = dimssub[0] * dimssub[1] * dimssub[2];
	//cout << "nalloc " << nalloc << "\n";
	unsigned char* rbuf = NULL;
	try {
		buf = vector<unsigned char>( nalloc, 0x00 );
		rbuf = buf.data();
	}
	catch (bad_alloc &hdfcroak) {
		cerr << "WRAPPED_HDF5_RBUFALLOC_FAILED" << "\n";
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	hsize_t offset[3] = { offs.nc0, offs.nr0, offs.nl0 };
	//cout << "offset " << offset[0] << ";" << offset[1] << ";" << offset[2] << "\n";
	hsize_t count[3] = { dimssub[0], dimssub[1], dimssub[2] };
	//cout << "count " << count[0] << ";" << count[1] << ";" << count[2] << "\n";
	hsize_t stride[3] = { 1, 1, 1 };
	//cout << "stride " << stride[0] << ";" << stride[1] << ";" << stride[2] << "\n";
	hsize_t block[3] = { 1, 1, 1 };
	//cout << "block " << block[0] << ";" << block[1] << ";" << block[2] << "\n";
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT );
	if ( fileid < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	dsetid = H5Dopen2( fileid, ifo.dsetnm.c_str(), H5P_DEFAULT );
	if ( dsetid < 0 ) {
		cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	mspcid = H5Screate_simple( 3, dimssub, mxdims );
	if ( mspcid < 0 ) {
		cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	dspcid = H5Dget_space( dsetid );
	if ( dspcid < 0 ) {
		cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_HYPERSLAB_FAILED" << "\n";
		return WRAPPED_HDF5_HYPERSLAB_FAILED;
	}
	status = H5Dread( dsetid, H5T_STD_U8LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_READ_FAILED" << "\n";
		return WRAPPED_HDF5_READ_FAILED;
	}

	status = H5Sclose(mspcid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	status = H5Sclose(dspcid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::read_contiguous_tensor3d_f32le_hyperslab( h5iometa3d const & ifo, h5offs3d const & offs, vector<float> & buf )
{
	//buffer carries implicitly stored matrices columns data stacked along rows resulting in layers
	//cout << "ifo.shape " << ifo.shape[0] << ";" << ifo.shape[1] << ";" << ifo.shape[2] << "\n";
	if ( offs.nc1 <= offs.nc0 || offs.nc1 > ifo.shape[0] ) {
		cerr << "nc WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nr1 <= offs.nr0 || offs.nr1 > ifo.shape[1] ) {
		cerr << "nr WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}
	if ( offs.nl1 <= offs.nl0 || offs.nl1 > ifo.shape[2] ) {
		cerr << "nl WRAPPED_HDF5_INCORRECTOFFSETS" << "\n";
		return WRAPPED_HDF5_INCORRECTOFFSETS;
	}

	//mind that the ni1 offsets always give exclusive bounds, i.e. index the next array index beyond the last to read/write
	hsize_t dimssub[3] = { offs.nc1 - offs.nc0, offs.nr1 - offs.nr0, offs.nl1 - offs.nl0 };
	//cout << "dimssub " << dimssub[0] << ";" << dimssub[1] << ";" << dimssub[2] << "\n";
	hsize_t mxdims[3] = { ifo.shape[0], ifo.shape[1], ifo.shape[2] };
	//cout << "mxdims " << mxdims[0] << ";" << mxdims[1] << ";" << mxdims[2] << "\n";

	hsize_t nalloc = dimssub[0] * dimssub[1] * dimssub[2];
	//cout << "nalloc " << nalloc << "\n";
	float* rbuf = NULL;
	try {
		buf = vector<float>( nalloc, 0.0 );
		rbuf = buf.data();
	}
	catch (bad_alloc &hdfcroak) {
		cerr << "WRAPPED_HDF5_RBUFALLOC_FAILED" << "\n";
		return WRAPPED_HDF5_RBUFALLOC_FAILED;
	}

	hsize_t offset[3] = { offs.nc0, offs.nr0, offs.nl0 };
	//cout << "offset " << offset[0] << ";" << offset[1] << ";" << offset[2] << "\n";
	hsize_t count[3] = { dimssub[0], dimssub[1], dimssub[2] };
	//cout << "count " << count[0] << ";" << count[1] << ";" << count[2] << "\n";
	hsize_t stride[3] = { 1, 1, 1 };
	//cout << "stride " << stride[0] << ";" << stride[1] << ";" << stride[2] << "\n";
	hsize_t block[3] = { 1, 1, 1 };
	//cout << "block " << block[0] << ";" << block[1] << ";" << block[2] << "\n";
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}
	dsetid = H5Dopen2( fileid, ifo.dsetnm.c_str(), H5P_DEFAULT );
	if ( dsetid < 0 ) {
		cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	mspcid = H5Screate_simple( 3, dimssub, mxdims );
	if ( mspcid < 0 ) {
		cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	dspcid = H5Dget_space( dsetid );
	if ( dspcid < 0 ) {
		cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Sselect_hyperslab(dspcid, H5S_SELECT_SET, offset, stride, count, block);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_HYPERSLAB_FAILED" << "\n";
		return WRAPPED_HDF5_HYPERSLAB_FAILED;
	}
	status = H5Dread( dsetid, H5T_IEEE_F32LE, mspcid, dspcid, H5P_DEFAULT, rbuf );
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_READ_FAILED" << "\n";
		return WRAPPED_HDF5_READ_FAILED;
	}

	status = H5Sclose(mspcid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_MSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_MSPACE_FAILED;
	}
	status = H5Sclose(dspcid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_DSPACE_FAILED" << "\n";
		return WRAPPED_HDF5_DSPACE_FAILED;
	}
	status = H5Dclose(dsetid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_DSETACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_DSETACCESS_FAILED;
	}
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::init_chunked_matrix_f64le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc )
{
	//see additional comments for *_u32le version of these functions...
	nrows = nr; //real dataset size in number of elements on 1d implicit buffer of unsigned int is nr*nc
	ncols = nc;
	BytesPerChunk = static_cast<size_t>(ncols*1024*1024); //MK::use default chunk size 1MB, ##MK::performance study
	//##MK::value check needs to be integer multiple of sizeof(unsigned int)*ncols !
	RowsPerChunk = BytesPerChunk / (sizeof(double) * ncols);
	ColsPerChunk = ncols;
	RowsColsPerChunk = RowsPerChunk*ColsPerChunk;
	NChunks = static_cast<size_t>( ceil(static_cast<double>(nrows*ncols) / static_cast<double>(RowsColsPerChunk)) );

cout << "HDF5 RealDataRows/Cols_BytesPerChunk_Rows/Cols/Rows*ColsPerChunk_NChunksTotal\t\t" << nrows << ";" << ncols << ";" << BytesPerChunk << ";" << RowsPerChunk << ";" << ColsPerChunk << ";" << RowsColsPerChunk << ";" << NChunks << "\n";

	//allocate a read/write buffer to pass data to the H5 library call functions
	if ( f64le_buf != NULL ) {
		delete [] f64le_buf;
		f64le_buf = NULL;
	}
	else {
		try {
			f64le_buf = new double[RowsPerChunk*ColsPerChunk];
		}
		catch (bad_alloc &h5croak) {
			cerr << "Allocation error for write buffer in init_chunked_matrix_f64le HDF5" << "\n";
			return WRAPPED_HDF5_ALLOCERR;
		}
	}

	//open an existent H5 file with read/write access
	fileid = H5Fopen( h5fn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);

	//initialize in this file a chunked data set in an existent group

	//start processing chunks into buffer successively and write to HDF5 file
	dims[0] = RowsPerChunk; 		dims[1] = ColsPerChunk;
	maxdims[0] = H5S_UNLIMITED;		maxdims[1] = H5S_UNLIMITED;
	offset[0] = 0;					offset[1] = 0;
	dimsnew[0] = 0;					dimsnew[1] = ColsPerChunk;

	dspcid = H5Screate_simple(2, dims, maxdims);
	cparms = H5Pcreate(H5P_DATASET_CREATE);
	status = H5Pset_chunk( cparms, 2, dims);

	double fillval = HDF5_F64LE_DUMMYVALUE;
	status = H5Pset_fill_value(cparms, H5T_IEEE_F64LE, &fillval );
	dsetid = H5Dcreate2(fileid, dsetnm.c_str(), H5T_IEEE_F64LE, dspcid, H5P_DEFAULT, cparms, H5P_DEFAULT);

	//fill read/write buffer with dummy values which the preceeding write steps can parse
	for(size_t i = 0; i < RowsColsPerChunk; ++i) {
		f64le_buf[i] = HDF5_F64LE_DUMMYVALUE;
	}
	//and use in combination with the position offsets to always complete first individual chunks,
	//thereafter write them to file and refresh the read/write buffer with dummy values to avoid
	//having to read the file content for any preceeding thread whose data portion interval falls arbitrarily
	//on a chunk boundary
	TotalWriteHere = 0;
	TotalWritePortion = RowsColsPerChunk;
	TotalWriteAllUsed = nrows*ncols;
	TotalWriteAllChunk = NChunks*RowsColsPerChunk; //so many implicitly coded unsigned int to write in total (including potential buffer of trailing dummies)

	//do not close any resource handlers or the file, in what follows we will add data portions per thread
	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_chunked_matrix_f64le( const string h5fn, const string dsetnm, vector<double> const & buffer )
{
	//see additional comments for *_u32le version of this functions...
	size_t LocalCompleted = 0; //how much of feed buffer already processed
	size_t LocalTotal = buffer.size(); //total size of the feed buffer

	size_t BufferWriteHere = 0; //where to start filling in h5 write buffer u32le_buf
	size_t BufferWriteNow = 0; //how many remaining fillable before u32le_buf is full and needs a flush

	do { //loop for as many chunks as fillable with feed buffer content
		BufferWriteHere = TotalWriteHere % RowsColsPerChunk;
		BufferWriteNow = RowsColsPerChunk;

		if ( (BufferWriteHere+(LocalTotal-LocalCompleted)) < RowsColsPerChunk )
			BufferWriteNow = BufferWriteHere + LocalTotal-LocalCompleted;

		//fill buffer
cout << "1::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << "\n";
		for( size_t w = BufferWriteHere; w < BufferWriteNow; w++ ) {
			f64le_buf[w] = buffer.at(LocalCompleted);
			LocalCompleted++;
			TotalWriteHere++;
		}
cout << "2::BufferWriteHere;Now;LocalCompleted;TotalWriteHere\t\t" << BufferWriteHere << ";" << BufferWriteNow << ";" << LocalCompleted << ";" << TotalWriteHere << "\n";

		//potentially flush buffer into H5 file
		if ( TotalWriteHere % RowsColsPerChunk == 0 ) {
			//##MK::write#####
cout << "3::Writing a full chunk" << "\n";
			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_IEEE_F64LE, dspcid, fspcid, H5P_DEFAULT, f64le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!

			//refresh buffer with dummies completely
			for ( size_t i = 0; i < RowsColsPerChunk; ++i )
				f64le_buf[i] = HDF5_F64LE_DUMMYVALUE;
		}

	} while ( LocalCompleted < LocalTotal );
	//processing of feed buffer done

cout << "--->Local done\t\t" << TotalWriteHere << "\t\t" << TotalWriteAllUsed << "\t\t" << TotalWriteAllChunk << "\n";

	//dont forget to pass potentially remaining part of the h5 write buffer to file
	if ( TotalWriteHere >= TotalWriteAllUsed ) { //everything which was planned in buffer
		//write h5 buffer out last time if not already happened
		if ( TotalWriteHere % RowsColsPerChunk != 0 ) {
cout << "4::Writing last chunk" << "\n";

			dimsnew[0] = dimsnew[0] + RowsPerChunk;
			//second dimension of dimsnew needs to remain as is!
			//the chunk is always written completely, regardless how many values physically relevant or not!
			status = H5Dset_extent(dsetid, dimsnew);
			fspcid = H5Dget_space(dsetid);
			status = H5Sselect_hyperslab(fspcid, H5S_SELECT_SET, offset, NULL, dims, NULL);
			status = H5Dwrite(dsetid, H5T_IEEE_F64LE, dspcid, fspcid, H5P_DEFAULT, f64le_buf);
			offset[0] = offset[0] + RowsPerChunk;
			//second dimension of offset needs to remain as is!
		}
	}

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::write_attribute_string( const string dsetnm, vector<pair<string,string>> const & attr )
{
	//add a number of string attributes to an already existent but closed dataset

	//##MK::avoid I/O overhead by opening the file here

	//https://bitbucket.hdfgroup.org/projects/HDFFV/repos/hdf5-examples/browse/1_10/C/H5T/h5ex_t_stringatt.c
	for( auto it = attr.begin(); it != attr.end(); it++ ) {
	//if ( attr.size() > 0 ) {
		//##MK::write also at(>0) entries
		//string key = attr.at(0).first; //for instance "SIUnit"
		//string val = attr.at(0).second; //for instance "nm"
		string key = it->first;
		string val = it->second;

		//write only nonempty attribute strings
		if ( key.length() > 0 && val.length() > 0 ) {
cout << "Writing attribute key/val\t\t" << key << "\t\t" << val << "\n";

			//C-style ASCII conformant
			size_t SDIM = val.length()+1;
			char* cstr = new char[SDIM]; //MK::no +1 because attr are without null/newline separator;
			strcpy(cstr, val.c_str());

			fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
			dsetid = H5Dopen2( fileid, dsetnm.c_str(), H5P_DEFAULT );
			dspcid = H5Dget_space( dsetid );

			//Create file and memory datatypes. For this example we will save
			//the strings as FORTRAN strings, therefore they do not need space
			//for the null terminator in the file
			ftypid = H5Tcopy(H5T_FORTRAN_S1);
			status = H5Tset_size(ftypid, SDIM-1);
cout << "Setting attribute size " << status << "\n";
		    mtypid = H5Tcopy(H5T_C_S1);
		    status = H5Tset_size(mtypid, SDIM); //MK::no null/newline terminator -1);
cout << "Setting attribute mtypid size " << status << "\n";
		    //create the attribute and write the string data to it
		    //##MK::here exemplified using the first token only of attr
		    attrid = H5Acreate(dsetid, key.c_str(), ftypid, dspcid, H5P_DEFAULT, H5P_DEFAULT);
		    status = H5Awrite (attrid, mtypid, cstr);
cout << "Writing attribute " << status << "\n";

			delete cstr; cstr = NULL;

			//release resources
			status = H5Aclose(attrid);
			status = H5Tclose(mtypid);
			status = H5Tclose(ftypid);
			status = H5Sclose(dspcid);
			status = H5Dclose(dsetid);
			status = H5Fclose(fileid);

			//##MK::get out here, debug return WRAPPED_HDF5_SUCCESS;
		}
		else {
cout << "Skipping empty attribute key/vals" << "\n";
		}
    }

	//##MK::avoid I/O overhead by closing the file here

	return WRAPPED_HDF5_SUCCESS;
}


int h5Hdl::reset_chunked_matrix_f64le_aftercompletion()
{
cout << "Resetting f64le" << "\n";

	if ( f64le_buf != NULL ) {
		delete [] f64le_buf;
		f64le_buf = NULL;
	}

	status = H5Dclose(dsetid);
	status = H5Sclose(dspcid);
	status = H5Sclose(fspcid);
	status = H5Pclose(cparms);
	status = H5Fclose(fileid);

	return WRAPPED_HDF5_SUCCESS;
}

/*
bool h5Hdl::read_trianglehull_from_apth5( const string fn, vector<tri3d> & out )
{
	int status = 0;
	h5iometa ifo_hull = h5iometa();
	h5offsets offs_hull = h5offsets();

	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ, offs_hull ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ in the InputfileTriangleHull" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ is a " << offs_hull.nrows() << " rows " << offs_hull.ncols() << " columns matrix" << "\n";
	}

	if ( offs_hull.nrows() < 1 || offs_hull.ncols() != PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX ) {
		cerr << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::check to block-wise read
	ifo_hull = h5iometa( PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ, offs_hull.nrows(), offs_hull.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo_hull, offs_hull, f32 );
cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ on InputfileTriangleHull failed" << "\n";
		return false;
	}

	try {
		out = vector<tri3d>();
		out.reserve( offs_hull.nrows() );
//##MK::point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading triangle hull positions" << "\n";
		return false;
	}

	for( size_t i = 0; i < f32.size(); i += 9 ) {
		out.push_back( tri3d(
				f32.at(i+0), f32.at(i+1), f32.at(i+2),
				f32.at(i+3), f32.at(i+4), f32.at(i+5),
				f32.at(i+6), f32.at(i+7), f32.at(i+8) ) );
	}
	f32 = vector<float>();

	return true;
}


bool h5Hdl::read_voxelized_from_apth5( const string fn, vxlizer & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	h5resultsfn = fn;
	//read vxlbox
	out.vxlgrid = sqb();
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}
	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ is either empty and/or has unexpected size!" << "\n"; return false;
	}
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ, offs.nrows(), offs.ncols() );
	vector<unsigned int> u32;
	status = read_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ read failed " << status << "\n"; return false;
	}
	cout << "PARAPROBE_SURF_VXLBOX_META_VXL_NXYZ read " << status << "\n";

	out.vxlgrid.nx = u32.at(0);
	out.vxlgrid.ny = u32.at(1);
	out.vxlgrid.nz = u32.at(2);
	out.vxlgrid.nxy = out.vxlgrid.nx * out.vxlgrid.ny;
	out.vxlgrid.nxyz = out.vxlgrid.nx * out.vxlgrid.ny * out.vxlgrid.nz;
	u32 = vector<unsigned int>();

	//bounds mi,mx
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_META_VXL_MIMX, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_META_VXL_MIMX!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}
	if ( offs.nrows() != 3 || offs.ncols() != PARAPROBE_SURF_VXLBOX_META_VXL_MIMX_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX is either empty and/or has unexpected size!" << "\n"; return false;
	}
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_META_VXL_MIMX, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX read failed " << status << "\n"; return false;
	}
	cout << "PARAPROBE_SURF_VXLBOX_META_VXL_MIMX read " << status << "\n";

	out.vxlgrid.box = aabb3d( 	f32.at(0), f32.at(1),
								f32.at(2), f32.at(3),
								f32.at(4), f32.at(5) );
	out.vxlgrid.box.scale();
	f32 = vector<float>();

	//load width
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}
	if ( offs.nrows() != 1 || offs.ncols() != PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH is either empty and/or has unexpected size!" << "\n"; return false;
	}
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH read failed " << status << "\n"; return false;
	}
	cout << "PARAPROBE_SURF_VXLBOX_META_VXL_WIDTH read " << status << "\n";

	out.vxlgrid.width = f32.at(0);
	f32 = vector<float>();

	//load states
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_VXLBOX_RES_VXL_STATE, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_VXLBOX_RES_VXL_STATE!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_SURF_VXLBOX_RES_VXL_STATE is a " << offs.nrows() << " rows " << offs.ncols() << " columns matrix" << "\n";
	}

	if ( offs.nrows() != out.vxlgrid.nxyz || offs.nrows() < 1 || offs.ncols() != PARAPROBE_SURF_VXLBOX_RES_VXL_STATE_NCMAX ) {
		cerr << "PARAPROBE_SURF_VXLBOX_RES_VXL_STATE is either empty and/or has unexpected size!" << "\n"; return false;
	}
	//read the content and transfer to working array
	//##MK::check to block-wise read
	ifo = h5iometa( PARAPROBE_SURF_VXLBOX_RES_VXL_STATE, offs.nrows(), offs.ncols() );
	vector<unsigned char> uc8;
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_SURF_VXLBOX_RES_VXL_STATE read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURF_VXLBOX_RES_VXL_STATE read failed " << status << "\n"; return false;
	}

	if ( out.IsInside != NULL || out.IsSurface != NULL || out.IsVacuum != NULL ) {
		cerr << "out bitfields have already been allocated!" << "\n"; return false;
	}

	bool* img = NULL;
	try {
		img = new bool[out.vxlgrid.nxyz]; //allocate on heap and feedback
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during allocating out.IsInside state" << "\n"; return false;
	}

	for( size_t i = 0; i < uc8.size(); i += PARAPROBE_SURF_VXLBOX_RES_VXL_STATE_NCMAX ) {
		if ( uc8.at(i) == 0xFF ) { //IS_INSIDE
			img[i] = 0xFF;
		}
		else {
			img[i] = 0x00;
		}
	}
	uc8 = vector<unsigned char>();

	out.IsInside = img;
	//##MK::other fields not changed

	return true;
}


bool h5Hdl::read_xyz_from_apth5( const string fn, vector<p3d> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) { //".apth5" ) {
		cerr << "The InputfileReconstruction " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_RES_XYZ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the XYZ data in the InputfileReconstruction!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_RES_XYZ, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SYNTH_VOLRECON_RES_XYZ on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_SYNTH_VOLRECON_RES_XYZ read " << status << "\n";

	try {
		out = vector<p3d>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_SYNTH_VOLRECON_RES_XYZ!" << "\n"; return false;
	}

	for( size_t i = 0; i < f32.size(); i += 3 ) {
		out.push_back( p3d( f32.at(i+0), f32.at(i+1), f32.at(i+2) ) );
	}
	f32 = vector<float>();
	return true;
}


bool h5Hdl::read_detxy_from_apth5( const string fn, vector<p2dm1> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The Inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of detxy coordinates array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_TRANSCODE_DETSPACE_RES_XY, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the XY data in the Inputfile!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_TRANSCODE_DETSPACE_RES_XY is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_TRANSCODE_DETSPACE_RES_XY_NCMAX ) {
		cerr << "PARAPROBE_TRANSCODE_DETSPACE_RES_XY is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_TRANSCODE_DETSPACE_RES_XY, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_TRANSCODE_DETSPACE_RES_XY on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_TRANSCODE_DETSPACE_RES_XY read " << status << "\n";

	try {
		out = vector<p2dm1>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_TRANSCODE_DETSPACE_RES_XY!" << "\n"; return false;
	}

	unsigned int ionID = 0;
	for( size_t i = 0; i < f32.size(); i += 2 ) {
		out.push_back( p2dm1( f32.at(i+0), f32.at(i+1), ionID ) );
		ionID++;
	}
	f32 = vector<float>();
	return true;
}


bool h5Hdl::read_voltages_from_apth5( const string fn, vector<voltm1> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The Inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of Vdu data array and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the VDC data in the Inputfile!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC_NCMAX ) {
		cerr << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC read " << status << "\n";

	try {
		out = vector<voltm1>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_TRANSCODE_DETSPACE_RES_XY!" << "\n"; return false;
	}

	unsigned int ionID = 0;
	for( size_t i = 0; i < f32.size(); i++ ) {
		out.push_back( voltm1( f32.at(i+0), 0.f, i ) );
	}
	f32 = vector<float>();

	//load Vpu
	if ( query_contiguous_matrix_dims( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the VPU data in the Inputfile!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU_NCMAX ) {
		cerr << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	if ( offs.nrows() != out.size() ) {
		cerr << "VDU and VPU arrays have different size this is unexpected!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU on Inputfile failed!" << "\n"; return false;
	}
cout << "PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU read " << status << "\n";

	for( size_t i = 0; i < f32.size(); i++ ) {
		if ( out.at(i).m == static_cast<unsigned int>(i) ) {
			out.at(i).Vpu = f32.at(i);
		}
		else {
			cerr << "Interleaving of PARAPROBE_TRANSCODE_VOLTCURVES_RES_VPU with PARAPROBE_TRANSCODE_VOLTCURVES_RES_VDC failed for ion " << i << "\n";
			return false;
		}
	}
	f32 = vector<float>();
	return true;
}


bool h5Hdl::read_m2q_from_apth5( const string fn, vector<apt_real> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The InputfileReconstruction " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SYNTH_VOLRECON_RES_MQ, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the MQ data in the InputfileReconstruction!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_VOLRECON_MQ is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_MQ is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_SYNTH_VOLRECON_RES_MQ, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_SYNTH_VOLRECON_RES_MQ read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SYNTH_VOLRECON_RES_MQ on Inputfile failed!" << "\n";
		return false;
	}

	try {
		out = vector<apt_real>();
		out.reserve( offs.nrows() ); //one p3dm1 object for every ion
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_SYNTH_VOLRECON_RES_MQ!" << "\n"; return false;
	}

	for( auto it = f32.begin(); it != f32.end(); it++ ) {
		out.push_back( *it );
	}
	f32 = vector<float>();
	return true;
}



bool h5Hdl::read_iontypes_from_apth5( const string fn, vector<rangedIontype> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	//temporary container before ion types get filtered to collate 24Mg+ and 25Mg+ to a single ion type
	vector<rangedIontype> tmp_out;

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The InputfileReconstruction " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of iontype IDs array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_ID, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_ID in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_ID is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX ) {
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_ID is either empty and/or has unexpected size!" << "\n"; return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_ID, offs.nrows(), offs.ncols() );
	vector<unsigned char> uc8;
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_ID read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_ID on InputfileReconstruction failed!" << "\n"; return false;
	}

	try {
		tmp_out = vector<rangedIontype>( uc8.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, rangedIontype() ); //every row an ion
		//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_RANGER_META_TYPID_DICT!" << "\n"; return false;
	}

	for( size_t r = 0; r < offs.nrows(); r++ ) {
		tmp_out.at(r).id = static_cast<unsigned int>((int) uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX+0));
	}
	uc8 = vector<unsigned char>();
	
	//read elemental composition of these iontypes
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_WHAT, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_WHAT in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX ) {
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT is either empty and/or has unexpected size!" << "\n"; return false;
	}
	if ( offs.nrows() != tmp_out.size() ) { //every rangedIontype
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT specifies unexpectedly a different number of rangedIontypes than _DICT_ID!" << "\n"; return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_WHAT, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_WHAT on InputfileReconstruction failed!" << "\n"; return false;
	}

	//last entry for the ion type tells us how many ranges are distinguished for this type
	for( size_t r = 0; r < offs.nrows(); r++ ) {
		tmp_out.at(r).strct = evapion3( uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+0),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+1),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+2),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+3),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+4),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+5),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+6),
									uc8.at(r*PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX+7)  );
	}	
	uc8 = vector<unsigned char>();
	
	//read mass2charge ranges for those ions
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_IVAL, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_IVAL in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_WHAT is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX ) {
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_IVAL is either empty and/or has unexpected size!" << "\n"; return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_IVAL, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_IVAL read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_IVAL on InputfileReconstruction failed!" << "\n"; return false;
	}
	//how ever we also need to know to which rangedIontypes these mass2charge interval bounds belong
	//read elemental composition of these iontypes
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_META_TYPID_DICT_ASSN, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_META_TYPID_DICT_ASSN in InputfileReconstruction!" << "\n"; return false;
	}
	else {
		cout << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}
	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX ) {
			cerr << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN is either empty and/or has unexpected size!" << "\n"; return false;
	}
	if ( offs.nrows() != f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX ) { //in this case not every ival has an assign rangedIontypeID
		cerr << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN specifies unexpectedly a different number of rangedIontypes than _DICT_IVAL!" << "\n"; return false;
	}
	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_RANGER_META_TYPID_DICT_ASSN, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_META_TYPID_DICT_ASSN read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_META_TYPID_DICT_ASSN on InputfileReconstruction failed!" << "\n"; return false;
	}
	
	//assume now that every row contributes only one mqival, i.e every IDs and MQ2ID
	map<size_t, vector<mqival>> selector;
cout << "Catalogue of tmp ion types from the HDF5" << "\n";
	for( size_t r = 0; r < tmp_out.size(); r++ ) {
		//size_t hashval = z1z2z3_molecular_ion( tmp_out.at(r).strct.Z1, tmp_out.at(r).strct.Z2, tmp_out.at(r).strct.Z3 );
		size_t hashval =  	static_cast<size_t>(tmp_out.at(r).strct.Z3)*static_cast<size_t>(FIRST_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(SECOND_BYTE) +
							static_cast<size_t>(tmp_out.at(r).strct.Z2)*static_cast<size_t>(THIRD_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(FOURTH_BYTE) +
							static_cast<size_t>(tmp_out.at(r).strct.Z1)*static_cast<size_t>(FIFTH_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(SIXTH_BYTE) +
							static_cast<size_t>( 0)*static_cast<size_t>(SEVENTH_BYTE) +
							static_cast<size_t>( 0); //the seventh byte is padding and we must not multiply by the EIGTH_BYTE as it would invalidate the value
cout << "Hashval " << hashval << "\n";
		map<size_t, vector<mqival>>::iterator thisone = selector.find( hashval );
		//UNKONWN_IONTYPES are internal ion states which have been added to the rangeTable upon construction
		//and therefore must not be added again when reading it in the UNKNOWN_IONTYPE has hashval 0 so
		if ( hashval != 0 ) {
			if ( thisone != selector.end() ) { //add mqrange to an existent unique evapion3
				selector[hashval].push_back( mqival( f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0),
													 f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) ) );
	cout << "Selector add to existent key " << hashval << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0) << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) << "\n";
			}
			else { //new unique evapion3
				selector.insert( pair<size_t, vector<mqival>>( hashval, vector<mqival>() ) );
				selector[hashval].push_back( mqival( f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0),
													 f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) ) );
	cout << "Selector init new key " << hashval << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+0) << "\t\t" << f32.at(r*PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX+1) << "\n";
			}
		}
		else {
	cout << "Selector skipping UNKNOWN_IONTYPE" << "\n";
		}
	}
cout << "Selector number of unique keys " << selector.size() << "\n";

	//finally assign out
cout << "Decoded the following ion types from the HDF5" << "\n";
	//out = vector<rangedIontype>( selector.size(), rangedIontype() );
	unsigned int id = UNKNOWN_IONTYPE;
	for( auto it = selector.begin(); it != selector.end(); it++ ) {
		//##MK::will imply relabeling of ions according to what vector does
		out.push_back( rangedIontype() );
		out.back().id = static_cast<unsigned int>(id+1); //##MK::catch size_t narrowing conversion exception

			evapion3 tmp = evapion3();
			size_t hsh = it->first;
			size_t val = 0;

			val = hsh / FIRST_BYTE;		hsh = hsh - (val*FIRST_BYTE);
		//cout << "Z3.) " << val << "\n";
			tmp.Z3 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / SECOND_BYTE; 	hsh = hsh - (val*SECOND_BYTE);
		//cout << "N3.) " << val << "\n";
			tmp.N3 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / THIRD_BYTE; 	hsh = hsh - (val*THIRD_BYTE);
		//cout << "Z2.) " << val << "\n";
			tmp.Z2 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / FOURTH_BYTE; 	hsh = hsh - (val*FOURTH_BYTE);
		//cout << "N2.) " << val << "\n";
			tmp.N2 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / FIFTH_BYTE; 	hsh = hsh - (val*FIFTH_BYTE);
		//cout << "Z1.) " << val << "\n";
			tmp.Z1 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / SIXTH_BYTE; 	hsh = hsh - (val*SIXTH_BYTE);
		//cout << "N1.) " << val << "\n";
			tmp.N1 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

			val = hsh / SEVENTH_BYTE; 	hsh = hsh - (val*SEVENTH_BYTE);
		//cout << "Pa.) " << val << "\n";

			//must not divide by EIGTH_BYTE because would be division by zero!
			val = hsh;
		//cout << "Ch.) " << val << "\n";
			tmp.charge = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

cout << "Unhash " << (int) tmp.N1 << "\t" << (int) tmp.Z1 << "\t" << (int) tmp.N2 << "\t" << (int) tmp.Z2 << "\t" << (int) tmp.N3 << "\t" << (int) tmp.Z3 << "\t\t" << (int) tmp.charge << "\n";
		out.back().strct = tmp; //molecular_ion_z1z2z3( it->first );
		for( auto jt = it->second.begin(); jt != it->second.end(); jt++ ) {
			out.back().rng.push_back( *jt );
			//##MK::BEGIN DEBUG
cout << "ID, Z1Z2Z3, mqmin, mqmax " << out.back().id << "\t\t" << (int) out.back().strct.Z1 << ";" << (int) out.back().strct.Z2 << ";" << (int) out.back().strct.Z3 << "\t\t" << jt->lo << ";" << jt->hi << "\n";
			//##MK::END DEBUG
		}
		id++;
	}

	uc8 = vector<unsigned char>();
	f32 = vector<float>();
	return true;	
}


bool h5Hdl::read_ityp_from_apth5( const string fn, vector<unsigned char> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of Ion2distance value array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_RANGER_RES_TYPID, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_RANGER_RES_TYPID!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_RANGER_RES_TYPID is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_RANGER_RES_TYPID_NCMAX ) {
		cerr << "PARAPROBE_RANGER_RES_TYPID is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	vector<unsigned char> uc8;
	ifo = h5iometa( PARAPROBE_RANGER_RES_TYPID, offs.nrows(), offs.ncols() );
	status = read_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8 );
cout << "PARAPROBE_RANGER_RES_TYPID read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_RANGER_RES_TYPID failed!" << "\n";
		return false;
	}

	try {
		out = vector<unsigned char>();
		out.reserve( offs.nrows() );
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_RANGER_RES_TYPID!" << "\n"; return false;
	}

	for( auto it = uc8.begin(); it != uc8.end(); it++ ) {
		out.push_back( *it );
	}
	uc8 = vector<unsigned char>();
	return true;
}


bool h5Hdl::read_dist_from_apth5( const string fn, vector<apt_real> & out )
{
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	if ( fn.substr(fn.length()-3) != ".h5" ) {
		cerr << "The inputfile " << fn << " has not the proper .h5 file extension!" << "\n";
		return false;
	}

	//probe existence of Ion2distance value array in the input file and get dimensions
	h5resultsfn = fn;
	if ( query_contiguous_matrix_dims( PARAPROBE_SURF_DIST_RES_D, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of PARAPROBE_SURF_DIST_RES_D!" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SURF_DIST_RES_D is a " << offs.nrows() << " rows " << offs.ncols() << " matrix" << "\n";
	}

	if ( offs.nrows() < 1 || offs.ncols() != PARAPROBE_SURF_DIST_RES_D_NCMAX ) {
		cerr << "PARAPROBE_SURF_DIST_RES_D is either empty and/or has unexpected size!" << "\n";
		return false;
	}

	//read the content and transfer to working array
	//##MK::improve to read block-wise
	ifo = h5iometa( PARAPROBE_SURF_DIST_RES_D, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_SURF_DIST_RES_D read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURF_DIST_RES_D failed!" << "\n";
		return false;
	}

	try {
		out = vector<apt_real>();
		out.reserve( offs.nrows() );
//MK::here point of maximum memory consumption
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading PARAPROBE_SURF_DIST_RES_D!" << "\n"; return false;
	}

	for( auto it = f32.begin(); it != f32.end(); it++ ) {
		out.push_back( *it );
	}
	f32 = vector<float>();
	return true;	
}
*/
