//##MK::GPLV3

//specify here which specific results this tool writes into H5 files

#ifndef __FOURIER_METADATA_DEFS_H__
#define __FOURIER_METADATA_DEFS_H__


#define FOURIER											"/AmbraFourier"

#define FOURIER_META									"/AmbraFourier/Metadata"
#define FOURIER_META_HRDWR								"/AmbraFourier/Metadata/ToolEnvironment"
#define FOURIER_META_SFTWR								"/AmbraFourier/Metadata/ToolConfiguration"

//profiling
#define FOURIER_META_PROF								"/AmbraFourier/Metadata/ToolProfiling"
//one group per MPI process/rank

#define FOURIER_META_PROF_WHAT							"TaskID"										//N timings per process x 1, unsigned int
#define FOURIER_META_PROF_WHAT_NC						1
#define FOURIER_META_PROF_DT							"ElapsedTime"									//N x 1, double
#define FOURIER_META_PROF_DT_NC							1
#define FOURIER_META_PROF_VMEM							"VirtualMemory"									//N x 1, double
#define FOURIER_META_PROF_VMEM_NC						1
#define FOURIER_META_PROF_RMEM							"ResidentMemory"								//N x 1, double
#define FOURIER_META_PROF_RMEM_NC						1



#define FOURIER_META_ROIS								"/AmbraFourier/Metadata/ROIEnsemble"
#define FOURIER_META_ROIS_IDS							"/AmbraFourier/Metadata/ROIEnsemble/IDS"		//Nrois x 1, unsigned int
#define FOURIER_META_ROIS_IDS_NC						1

#define FOURIER_META_ROIS_XYZ							"/AmbraFourier/Metadata/ROIEnsemble/XYZ"		//Nrois x 3, unsigned int pixel coordinates, number of rows match with above
#define FOURIER_META_ROIS_XYZ_NC						3
#define FOURIER_META_ROIS_XYZTOPO						"/AmbraFourier/Metadata/ROIEnsemble/XYZTopo"	//3*Nrois x 1, unsigned int, utility for rendering with Paraview
#define FOURIER_META_ROIS_XYZTOPO_NC					1
#define FOURIER_META_ROIS_DIMS							"/AmbraFourier/Metadata/ROIEnsemble/NXYZ"		//Nrois x 3, dimensions nx, ny, nz, number of rows match with above
#define FOURIER_META_ROIS_DIMS_NC						3

#define FOURIER_META_FFT								"/AmbraFourier/Metadata/FFTDetails"
#define FOURIER_META_FFT_NXYZ							"/AmbraFourier/Metadata/FFTDetails/NXYZ"		//1 x 3, unsigned int matches with FOURIER_META_ROIS_DIMS currently
#define FOURIER_META_FFT_NXYZ_NC						3
#define FOURIER_META_FFT_NIJK							"/AmbraFourier/Metadata/FFTDetails/NIJK"		//1 x 3, unsigned int, how many reciprocal space points along i, j, k, mind dim k shorter, conjugate-even symmetry!
#define FOURIER_META_FFT_NIJK_NC						3

#define FOURIER_META_FFT_IJK							"/AmbraFourier/Metadata/FFTDetails/IJK"			//NI*NJ*NK x 3, unsigned int, storage-order of the results per ROI matches to storage-order FOURIER_RES_ROIS_FFTMAGN
#define FOURIER_META_FFT_IJK_NC							3
#define FOURIER_META_FFT_IJKTOPO						"/AmbraFourier/Metadata/FFTDetails/IJKTopo" 	//3*NI*NJ*NK x 1, unsigned int, utility for rendering with Paraview
#define FOURIER_META_FFT_IJKTOPO_NC						1
#define FOURIER_META_FFT_IJKCONJ						"/AmbraFourier/Metadata/FFTDetails/IJKConj"		//the conjugate positions NI*NJ*NK x 3, unsigned int, storage-order of the results per ROI matches to storage-order FOURIER_RES_ROIS_FFTMAGN
#define FOURIER_META_FFT_IJKCONJ_NC						3

#define FOURIER_META_FFTPROF							"/AmbraFourier/Metadata/FFTProfiling"
//one group per MPI process/rank
#define FOURIER_META_FFTPROF_IDS						"RoiIDs"	//1x unsigned int
#define FOURIER_META_FFTPROF_IDS_NC						1
#define FOURIER_META_FFTPROF_WHOM						"ThreadIDsDeviceIDs" //2x unsigned char
#define FOURIER_META_FFTPROF_WHOM_NC					2
#define FOURIER_META_FFTPROF_DT							"ExecAndMemcpyElapsedTimes"	//1x double
#define FOURIER_META_FFTPROF_DT_NC						1

#define FOURIER_RES										"/AmbraFourier/Results"
#define FOURIER_RES_ROIS								"/AmbraFourier/Results/ROIEnsemble"

#define FOURIER_RES_ROIS_FFTMAGN						"/AmbraFourier/Results/ROIEnsemble/FFTMagnitude"
//one sub-group per descriptor, each ROI own dataset e.g. "/AmbraFourier/Results/ROIEnsemble/FFTMagnitude/ROI0"	//MK::roiIDs matching with, for each ROI NI*NJ*NK x 1
#define FOURIER_RES_ROIS_FFTMAGN_NC						1


#endif
