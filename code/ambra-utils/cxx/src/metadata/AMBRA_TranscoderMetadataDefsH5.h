//##MK::GPLV3

//specify here which specific results this tool writes into H5 files

#ifndef __AMBRA_TRANSCODER_METADATA_DEFS_H__
#define __AMBRA_TRANSCODER_METADATA_DEFS_H__


#define TRANSCODER										"/AmbraTranscoder"

#define TRANSCODER_META									"/AmbraTranscoder/Metadata"
#define TRANSCODER_META_HRDWR							"/AmbraTranscoder/Metadata/ToolEnvironment"
#define TRANSCODER_META_SFTWR							"/AmbraTranscoder/Metadata/ToolConfiguration"

#define TRANSCODER_RES									"/AmbraTranscoder/Results"
#define TRANSCODER_RES_XYZ								"/AmbraTranscoder/Results/Imagestack"

/*
#define TRANSCODER_RES_MP_IOID							"/AmbraTranscoder/Results/MatPointIDsWithResults"	//1x int
#define TRANSCODER_RES_MP_IOID_NCMAX					1

//##MK::old version
#define TRANSCODER_RES_HKL								"/AmbraTranscoder/Results/ReciprocSpace"
#define TRANSCODER_RES_HKL_MPID							"/AmbraTranscoder/Results/ReciprocSpace/MatPointIDs"		//1x unsigned int
#define TRANSCODER_RES_HKL_MPID_NCMAX					1
#define TRANSCODER_RES_HKL_MAXIJK						"/AmbraTranscoder/Results/ReciprocSpace/MaxIJK"			//1x unsigned int
#define TRANSCODER_RES_HKL_MAXIJK_NCMAX					1
#define TRANSCODER_RES_HKL_MAXVAL						"/AmbraTranscoder/Results/ReciprocSpace/MaxCnts"			//1x float
#define TRANSCODER_RES_HKL_MAXVAL_NCMAX					1

//##MK::new version
#define TRANSCODER_RES_HKLGRID							"/AmbraTranscoder/Results/ReciprocFullGrid"
#define TRANSCODER_RES_HKLREFL							"/AmbraTranscoder/Results/ReciprocReflectors"
#define TRANSCODER_RES_HKLREFL_NCMAX					4
#define TRANSCODER_RES_HKLDBSCAN						"/AmbraTranscoder/Results/ReciprocDBScanCluster"
#define TRANSCODER_RES_HKLDBSCAN_NCMAX					5
*/


/*
#define TRANSCODER_META_MP								"/AmbraTranscoder/Metadata/MatPoints"
#define TRANSCODER_META_MP_XYZ							"/AmbraTranscoder/Metadata/MatPoints/XYZ"					//3x float
#define TRANSCODER_META_MP_XYZ_NCMAX					3
#define TRANSCODER_META_MP_TOPO							"/AmbraTranscoder/Metadata/MatPoints/Topo"					//1x unsigned int
#define TRANSCODER_META_MP_TOPO_NCMAX					1
#define TRANSCODER_META_MP_ID							"/AmbraTranscoder/Metadata/MatPoints/MatPointIDs"			//1x unsigned int
#define TRANSCODER_META_MP_ID_NCMAX						1
#define TRANSCODER_META_MP_NIONS						"/AmbraTranscoder/Metadata/MatPoints/NumberOfIons"			//1x unsigned int
#define TRANSCODER_META_MP_NIONS_NCMAX					1
#define TRANSCODER_META_MP_R							"/AmbraTranscoder/Metadata/ROIRadius"						//1x float
#define TRANSCODER_META_MP_R_NCMAX						1


#define TRANSCODER_META_PHCAND							"/AmbraTranscoder/Metadata/PhaseCandidates"
#define TRANSCODER_META_PHCAND_N						"/AmbraTranscoder/Metadata/PhaseCandidates/NumberOfCandidates"	//1x unsigned int
#define TRANSCODER_META_PHCAND_N_NCMAX					1
//+per candidate
#define TRANSCODER_META_PHCAND_ID						"IontypesID"									//1x unsigned char, [0,255] which iontype
#define TRANSCODER_META_PHCAND_ID_NCMAX					1
#define TRANSCODER_META_PHCAND_WHAT						"IontypesWhat"									//evapion3, i.e. 8x unsigned char
#define TRANSCODER_META_PHCAND_WHAT_NCMAX				8
//#define TRANSCODER_META_PHCAND_RSP					"RealspacePosition"								//1x float
//#define TRANSCODER_META_PHCAND_RSP_NCMAX				1
#define TRANSCODER_META_PHCAND_VAL						"RefIntensity"									//1x float
#define TRANSCODER_META_PHCAND_VAL_NCMAX				1

#define TRANSCODER_META_HKL								"/AmbraTranscoder/Metadata/ReciprocSpace"
#define TRANSCODER_META_HKL_NIJK						"/AmbraTranscoder/Metadata/ReciprocSpace/Discretization"	//3x unsigned int
#define TRANSCODER_META_HKL_NIJK_NCMAX					3
#define TRANSCODER_META_HKL_IVAL						"/AmbraTranscoder/Metadata/ReciprocSpace/Positions"		//1x float ##MK::??
#define TRANSCODER_META_HKL_IVAL_NCMAX					1
#define TRANSCODER_META_HKL_GRID						"/AmbraTranscoder/Metadata/ReciprocSpace/IJK"				//3x float
#define TRANSCODER_META_HKL_GRID_NCMAX					3
#define TRANSCODER_META_HKL_TOPO						"/AmbraTranscoder/Metadata/ReciprocSpace/Topo"				//1x unsigned int
#define TRANSCODER_META_HKL_TOPO_NCMAX					1
*/


#endif
