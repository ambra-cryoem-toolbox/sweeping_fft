//##MK::GPLV3

#ifndef __AMBRA_UTILS_VERBOSE_H__
#define __AMBRA_UTILS_VERBOSE_H__

#include "AMBRA_RapidXMLInterface.h"

//the GITSHA hash will be passed upon compilation using -DGITSHA

//genering global functions to report state, warnings and erros
void reporting( const int rank, const string what );
void reporting( const string what );
void complaining( const int rank, const string what );
void complaining( const string what );
void stopping( const int rank, const string what );
void stopping( const string what );

struct pparm
{
	string keyword;
	string value;
	string unit;
	string info;
	pparm() : keyword(""), value(""), unit(""), info("") {}
	pparm( const string _kyw, const string _val, const string _si, const string _ifo ) :
		keyword(_kyw), value(_val), unit(_si), info(_ifo) {}
};

ostream& operator<<(ostream& in, pparm const & val);


#endif
