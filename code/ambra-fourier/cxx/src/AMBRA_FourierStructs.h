//##MK::GPLV3

#ifndef __AMBRA_FOURIER_STRUCTS_H__
#define __AMBRA_FOURIER_STRUCTS_H__

#include "AMBRA_FourierGPUInterface.h"

struct MPI_Tensor3d_Shape
{
	//##MK::check handling of size_t or unsigned long
	unsigned int nc;
	unsigned int nr;
	unsigned int nl;
	MPI_Tensor3d_Shape() : nc(0), nr(0), nl(0) {}
	MPI_Tensor3d_Shape( const unsigned int _nc, const unsigned int _nr, const unsigned int _nl ) :
		nc(_nc), nr(_nr), nl(_nl) {}
};

ostream& operator<<(ostream& in, MPI_Tensor3d_Shape const & val);


#endif
