//##MK::GPLV3

#ifndef __AMBRA_FOURIER_IMKL_INTERFACE_H__
#define __AMBRA_FOURIER_IMKL_INTERFACE_H__

#include "AMBRA_FourierTicToc.h"

#include "mkl_dfti.h"

#define MKL_Complex8 complex<float>
#define MKL_Complex16 complex<double>

#ifdef EMPLOY_SINGLEPRECISION
	#define MKL_ComplexBioReal std::complex<float>	//equivalent to MKL_Complex8
#else
	#define MKL_ComplexBioReal std::complex<double>
#endif

/*
void mkl_debug();
*/


class fft3d_cpu_windowing
{
public:
	fft3d_cpu_windowing();
	~fft3d_cpu_windowing();

	bool hann_hamming( const bio_uint nx );

	vector<bio_real> windowing_coeff;
};


struct fftinfo
{
	MKL_LONG NX;  //##MK::initialize and check if grid is zero
	MKL_LONG NY;
	MKL_LONG NZ;
	//MKL_LONG NYZ;
	MKL_LONG NXYZ;
	MKL_LONG NI;
	MKL_LONG NJ;
	MKL_LONG NK;
	MKL_LONG NJK;
	MKL_LONG NIJK;
	MKL_LONG dimensions[3];
	MKL_LONG input_strides[4];
	MKL_LONG output_strides[4];
	bool success;
	fftinfo();
	fftinfo( const int ni_cube );
};


class fft3d_mkl_cpu
{
public:
	//out-of-place transform
	fft3d_mkl_cpu();
	~fft3d_mkl_cpu();

	void init( const int ni );
	void init( const int ni, vector<bio_real> const & img3d );

	/*
	void fill(vector<t3x3> const & F, const unsigned int c);
	*/
	void forwardFFT();
	void computeFFTMagn();
	void get_ijk_normal( vector<bio_uint> & ijknrm );
	void get_ijk_conjugate( vector<bio_uint> & ijkcnj );

	vector<char> fftTemp;	//forward transformation results carrier for real and imaginary parts
#ifdef EMPLOY_SINGLEPRECISION
	vector<float> m_input;
	vector<float> m_amplitude;
#else
	vector<double> m_input;
	vector<double> m_amplitude;
#endif


	DFTI_CONFIG_VALUE precision;
	DFTI_DESCRIPTOR_HANDLE m_f_handle;

	fftinfo ifo;

#ifdef EMPLOY_SINGLEPRECISION
	MKL_Complex8* fftTempP;
#else
	MKL_Complex16* fftTempP;
#endif


};





#endif
