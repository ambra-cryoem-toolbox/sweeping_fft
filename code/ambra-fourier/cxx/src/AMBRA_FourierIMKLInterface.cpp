//##MK::GPLV3

#include "AMBRA_FourierIMKLInterface.h"

/*
#define NX					50
#define NY 					100
*/

/*
void mkl_debug()
{
	//export MKL_NUM_THREADS=1
	//export OMP_NESTED=true
	//export OMP_PLACES=cores
	#pragma omp parallel
	{
		#pragma omp for
		for( int thr = MASTER; thr < omp_get_num_threads(); thr++ ) {
			MKL_LONG mydims[2] = {NX, NY};
			MKL_ComplexBioReal* xy = NULL;
			xy = new MKL_ComplexBioReal[NX*NY];
			for( int x = 0; x < NX; x++ ) {
				for( int y = 0; y < NY; y++ ) {
					bio_real real = static_cast<bio_real>(1+thr) * 100.0 * static_cast<bio_real>(x*NY + y);
					bio_real imag = 0.0;
					xy[x*NY+y] = complex(real, imag);
				}
			}
			#pragma omp critical
			{
				cout << "Thread " << omp_get_thread_num << " / " << thr << " create its local input " << xy.size() << "\n";
			}

			DFTI_DESCRIPTOR_HANDLE myfft = NULL;
#ifdef EMPLOY_SINGLEPRECISION
			DftiCreateDescriptor( &myfft, DFTI_SINGLE, DFTI_COMPLEX, 2, mydims );
#else
			DftiCreateDescriptor( m&myfft, DFTI_DOUBLE, DFTI_COMPLEX, 2, mydims );
#endif
			DftiCommitDescriptor( myfft );
			DftiComputeForward( myfft, xy );
			DftiFreeDescriptor( &myfft );

			#pragma omp critical
			{
				cout << "Thread " << omp_get_thread_num << " / " << thr << " executed local 2D transform " << xy.size() << "\n";
			}
		}
	}
}
*/


fft3d_cpu_windowing::fft3d_cpu_windowing()
{
	windowing_coeff = vector<bio_real>();
}


fft3d_cpu_windowing::~fft3d_cpu_windowing()
{
	windowing_coeff = vector<bio_real>();
}


bool fft3d_cpu_windowing::hann_hamming( const bio_uint nx )
{
	if ( nx < 1 ) {
		cerr << "fft3d_cpu_windowing::hann_hamming nx needs to be at least 1 !" << "\n";
		return false;
	}

	vector<bio_real> coeff1d = vector<bio_real>( nx, 1.0 );
	bio_real divisor = static_cast<bio_real>(nx - 1);
	for( bio_uint i = 0; i < nx; i++ ) {
		bio_real counter = 2.0 * MYPI * static_cast<bio_real>(i);
		coeff1d[i] = 0.5*(1.0 - cos(counter/divisor));
	}

	/*
	//##MK::BEGIN DEBUGGING
	#pragma omp master
	{
		for( auto it = coeff1d.begin(); it != coeff1d.end(); ++it ) {
			cout << "Coefficients " << it - coeff1d.begin() << "\t\t" << *it << "\n";
		}
	}
	//##MK::END DEBUGGING
	*/

	//3d outer product, weighting of 3x orthogonal 1d coefficients
	windowing_coeff = vector<bio_real>( CUBE(nx), 1.0 );
	for( bio_uint z = 0; z < nx; z++ ) {
		bio_uint zoff = z * SQR(nx);
		bio_real zmul = coeff1d[z];
		for( bio_uint y = 0; y < nx; y++ ) {
			bio_uint yzoff = y * nx + zoff;
			bio_real yzmul = coeff1d[y] * zmul;
			for( bio_uint x = 0; x < nx; x++ ) {
				windowing_coeff.at(x + yzoff) = windowing_coeff.at(x + yzoff) * coeff1d[x] * yzmul;
			}
		}
	}
	return true;
}


fft3d_mkl_cpu::fft3d_mkl_cpu()
{
	fftTempP = NULL;
}


fft3d_mkl_cpu::~fft3d_mkl_cpu()
{
	//do not delete fftTempP, only pointer to a vector!
	//DftiFreeDescriptor(&m_f_handle);
}


fftinfo::fftinfo()
{
	//##MK::tested only for cubes!
	MKL_LONG N = 0;
	this->NX = N;
	this->NY = N;
	this->NZ = N;
	this->NXYZ = this->NX * this->NY * this->NZ;
	this->NI = this->NX;
	this->NJ = this->NY;
	this->NK = static_cast<MKL_LONG>(floor(this->NZ/2)+1);
	this->NJK = this->NJ * this->NK;
	this->NIJK = this->NI * this->NJ * this->NK;
	this->dimensions[0] = this->NX;
	this->dimensions[1] = this->NY;
	this->dimensions[2] = this->NZ;
	this->input_strides[0] = 0;
	this->input_strides[1] = this->NX;
	this->input_strides[2] = this->NX * this->NY;
	this->input_strides[3] = 1;
	this->output_strides[0] = 0;
	this->output_strides[1] = this->NK;
	this->output_strides[2] = this->NJ * this->NK;
	this->output_strides[3] = 1;
	this->success = true;
	//check this one
	//https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/appendix-d-code-examples/
	//   fourier-transform-functions-code-examples/fft-code-examples.html#fft-code-examples
	//for example with cuboidal 3d transforms
	//##MK::
}


fftinfo::fftinfo( const int ni_cube )
{
	//##MK::tested only for cubes!
	MKL_LONG N = static_cast<MKL_LONG>(ni_cube);
	this->NX = N;
	this->NY = N;
	this->NZ = N;
	this->NXYZ = this->NX * this->NY * this->NZ;
	this->NI = this->NX;
	this->NJ = this->NY;
	this->NK = static_cast<MKL_LONG>(floor(this->NZ/2)+1);
	this->NJK = this->NJ * this->NK;
	this->NIJK = this->NI * this->NJ * this->NK;
	this->dimensions[0] = this->NX;
	this->dimensions[1] = this->NY;
	this->dimensions[2] = this->NZ;
	this->input_strides[0] = 0;
	this->input_strides[1] = this->NX;
	this->input_strides[2] = this->NX * this->NY;
	this->input_strides[3] = 1;
	this->output_strides[0] = 0;
	this->output_strides[1] = this->NK;
	this->output_strides[2] = this->NJ * this->NK;
	this->output_strides[3] = 1;
	this->success = true;
}


void fft3d_mkl_cpu::init( const int ni )
{
#ifdef EMPLOY_SINGLEPRECISION
	precision = DFTI_SINGLE;
#else
	precision = DFTI_DOUBLE;
#endif

	//prepare forward fft input buffer with real-valued deformation gradient data
	ifo = fftinfo( ni );

	//NX = static_cast<MKL_LONG>(ngridd[0]); //nfft
	//NY = static_cast<MKL_LONG>(ngridd[1]); //nfft
	//NZ = static_cast<MKL_LONG>(ngridd[2]); //##MK::check carefully for cuboidal geometries
	//NXYZ = NX*NY*NZ;
	//dimensions[0] = NX;
	//dimensions[1] = NY;
	//dimensions[2] = NZ;
	//input_strides[0] = 0;
	//input_strides[1] = NX;
	//input_strides[2] = NX*NY;
	//input_strides[3] = 1;

	size_t requiredSize = 	static_cast<size_t>(ifo.NX) *
							static_cast<size_t>(ifo.NY) *
							static_cast<size_t>(ifo.NZ);

#ifdef EMPLOY_SINGLEPRECISION
	m_input = vector<float>( requiredSize, 0.0 );
#else
	m_input = vector<double>( requiredSize, 0.0 );
#endif

	////prepare forward FFT output buffer of MKL_Complex16
	//NI = NX;
	//NJ = NY;
	//NK = static_cast<MKL_LONG>(floor(NZ/2)+1); //##MK::check carefully for cuboidal grids!
	//NJK = NJ * NK;
	//NIJK = NI * NJ * NK;
	//output_strides[0] = 0;
	//output_strides[1] = NK; //static_cast<MKL_LONG>(floor(NFFT/2))+1);
	//output_strides[2] = NK*NJ; //(static_cast<MKL_LONG>(floor(NFFT/2))+1)*NFFT;
	//output_strides[3] = 1;

#ifdef EMPLOY_SINGLEPRECISION
	requiredSize = 	static_cast<size_t>(ifo.NI) *
					static_cast<size_t>(ifo.NJ) *
					static_cast<size_t>(ifo.NK) * sizeof(MKL_Complex8); //##MK::sizeof necessary as carrier buffer is vector<char>
#else
	requiredSize = 	static_cast<size_t>(ifo.NI) *
					static_cast<size_t>(ifo.NJ) *
					static_cast<size_t>(ifo.NK) * sizeof(MKL_Complex16);
#endif

	fftTemp = vector<char>(requiredSize, 0x00); //on empty vector resize set new elements, i.e. all to zero

	//redefine how to interpret this carrier buffer
#ifdef EMPLOY_SINGLEPRECISION
	fftTempP = (MKL_Complex8*) &fftTemp[0];
#else
	fftTempP = (MKL_Complex16*) &fftTemp[0];
#endif

	/*
	cout << "Initialized fFFT " << "\n";
	cout << "NX/NY/NZ/NXYZ = " << ifo.NX << ";" << ifo.NY << ";" << ifo.NZ << ";" << ifo.NXYZ << "\n";
	cout << "NI/NJ/NK/NJK/NIJK = " << ifo.NI << ";" << ifo.NJ << ";" << ifo.NK << ";" << ifo.NJK << ";" << ifo.NIJK << "\n";
	*/
}


void fft3d_mkl_cpu::init( const int ni, vector<bio_real> const & img3d )
{
#ifdef EMPLOY_SINGLEPRECISION
	precision = DFTI_SINGLE;
#else
	precision = DFTI_DOUBLE;
#endif

	//prepare forward fft input buffer with real-valued deformation gradient data
	ifo = fftinfo( ni );

	size_t requiredSize = 	static_cast<size_t>(ifo.NX) *
							static_cast<size_t>(ifo.NY) *
							static_cast<size_t>(ifo.NZ);

	//m_input = img3d; //this is a copy because we have trivial types
#ifdef EMPLOY_SINGLEPRECISION
	m_input = vector<float>( requiredSize, 0.0 );
#else
	m_input = vector<double>( requiredSize, 0.0 );
#endif
	for( size_t i = 0; i < requiredSize; i++ ) {
		m_input[i] = img3d[i];
	}

#ifdef EMPLOY_SINGLEPRECISION
	requiredSize = 	static_cast<size_t>(ifo.NI) *
					static_cast<size_t>(ifo.NJ) *
					static_cast<size_t>(ifo.NK) * sizeof(MKL_Complex8);
#else
	requiredSize = 	static_cast<size_t>(ifo.NI) *
					static_cast<size_t>(ifo.NJ) *
					static_cast<size_t>(ifo.NK) * sizeof(MKL_Complex16);
#endif

	fftTemp = vector<char>(requiredSize, 0x00); //on empty vector resize set new elements, i.e. all to zero

#ifdef EMPLOY_SINGLEPRECISION
	fftTempP = (MKL_Complex8*) &fftTemp[0];
#else
	fftTempP = (MKL_Complex16*) &fftTemp[0];
#endif

	/*
	cout << "Initialized fFFT " << "\n";
	cout << "NX/NY/NZ/NXYZ = " << ifo.NX << ";" << ifo.NY << ";" << ifo.NZ << ";" << ifo.NXYZ << "\n";
	cout << "NI/NJ/NK/NJK/NIJK = " << ifo.NI << ";" << ifo.NJ << ";" << ifo.NK << ";" << ifo.NJK << ";" << ifo.NIJK << "\n";
	*/
}


void fft3d_mkl_cpu::forwardFFT()
{
	//see also https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/fourier-transform-functions/fft-functions/
	//    fft-computation-functions/configuring-and-computing-an-fft-in-c-c.html
	//for the storage order of the complex values after the forward transform the format is CCE see details here
	//https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/fourier-transform-functions/fft-functions/configuration-settings/
	//    dfti-complex-storage-dfti-real-storage-dfti-conjugate-even-storage.html#CONJUGATE_EVEN_STORAGE
	MKL_LONG status = 1;

#ifdef EMPLOY_SINGLEPRECISION
	status = DftiCreateDescriptor(&m_f_handle, DFTI_SINGLE, DFTI_REAL, 3, ifo.dimensions );
//cout << "DftiCreateDescriptor " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false; return;
	}
#else
	status = DftiCreateDescriptor(&m_f_handle, DFTI_DOUBLE, DFTI_REAL, 3, ifo.dimensions );
//cout << "DftiCreateDescriptor " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false; return;
	}
#endif
	status = DftiSetValue(m_f_handle, DFTI_PLACEMENT, DFTI_NOT_INPLACE);
//cout << "DftiSetValuePlace " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false; return;
	}
	status = DftiSetValue(m_f_handle, DFTI_CONJUGATE_EVEN_STORAGE, DFTI_COMPLEX_COMPLEX);
//cout << "DftiSetValueConj " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false; return;
	}
	status = DftiSetValue(m_f_handle, DFTI_INPUT_STRIDES, ifo.input_strides);
//cout << "DftiSetValueIStrides " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false; return;
	}
	status = DftiSetValue(m_f_handle, DFTI_OUTPUT_STRIDES, ifo.output_strides);
//cout << "DftiSetValueOStrides " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false; return;
	}

	status = DftiCommitDescriptor(m_f_handle);
//cout << "DftiSetCommitDescriptor " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false; return;
	}

	status = DftiComputeForward(m_f_handle, &m_input[0], fftTempP);
//cout << "DftiComputeForward " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false;
	}

	status = DftiFreeDescriptor(&m_f_handle);
//cout << "DftiFreeDescriptor " << DftiErrorMessage(status) << "\n";
	if ( status != 0 ) {
		ifo.success = false;
	}

	/*
	//##MK::BEGIN DEBUG
	cout << "Done, printing results" << "\n";
	//int jump = 0;
	for ( int ijk = 0; ijk < ifo.NIJK; ijk++ ) {
		cout << ijk << "\t\t" << "(" << fftTempP[ijk].real() << ", " << fftTempP[ijk].imag() << ")" << "\n";
		//jump++;
		//if ( jump != 8)
		//	continue;
		//else
		//	cout << "\n";
		//jump = 0;
	}
	//##MK::END DEBUG
	*/
}


void fft3d_mkl_cpu::computeFFTMagn()
{
	if ( ifo.success == true ) {
		m_amplitude.reserve( ifo.NIJK );
		//amplitude of FFT defined as (real^2 + imag^2)^0.5
		for ( MKL_LONG ijk = 0; ijk < ifo.NIJK; ijk++ ) {
			m_amplitude.push_back( sqrt(SQR(fftTempP[ijk].real()) + SQR(fftTempP[ijk].imag())) );
		}

		/*
		//##MK::BEGIN DEBUGGING
		//decode CCE storage order, conjugate-even symmetry to regular 3d grid
		//https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/fourier-transform-functions/fft-functions/configuration-settings/
		//   dfti-complex-storage-dfti-real-storage-dfti-conjugate-even-storage.html#CONJUGATE_EVEN_STORAGE
		for( MKL_LONG i = 0; i < ifo.NI; i++ ) {
			for( MKL_LONG j = 0; j < ifo.NJ; j++ ) {
				for ( MKL_LONG k = 0; k < ifo.NK; k++ ) {
					MKL_LONG iconj = (ifo.NI - i) % ifo.NI;
					MKL_LONG jconj = (ifo.NJ - j) % ifo.NJ;
					MKL_LONG kconj = (ifo.NK - k) % ifo.NK;
					cout << "ijk " << i << ";" << j << ";" << k << "\t\t magn " << m_amplitude.at(i + j*ifo.NI + k*ifo.NI*ifo.NJ)
							<< "\t\t ijkconj " << iconj << ";" << jconj << ";" << kconj << "\n";
					//z_k1k2k3 = conjugate( z_NI-k1_NJ-k2_NK-k3 )
				}
			}
		}
		//##MK::END DEBUGGING
		*/
	}
	else {
#ifdef EMPLOY_SINGLEPRECISION
		m_amplitude = vector<float>();
#else
		m_amplitude = vector<double>();
#endif
	}
}


void fft3d_mkl_cpu::get_ijk_normal( vector<bio_uint> & ijknrm )
{
	//decode CCE storage order, conjugate-even symmetry to regular 3d grid
	//https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/fourier-transform-functions/fft-functions/configuration-settings/
	//   dfti-complex-storage-dfti-real-storage-dfti-conjugate-even-storage.html#CONJUGATE_EVEN_STORAGE
	ijknrm = vector<bio_uint>( ifo.NIJK * FOURIER_META_FFT_IJK_NC, 0 );

	for ( MKL_LONG k = 0; k < ifo.NK; k++ ) {
		for( MKL_LONG j = 0; j < ifo.NJ; j++ ) {
			for( MKL_LONG i = 0; i < ifo.NI; i++ ) {
				MKL_LONG here = i + j*ifo.NI + k*ifo.NI*ifo.NJ;
				ijknrm[here*FOURIER_META_FFT_IJK_NC+0] = i;
				ijknrm[here*FOURIER_META_FFT_IJK_NC+1] = j;
				ijknrm[here*FOURIER_META_FFT_IJK_NC+2] = k;
			}
		}
	}
}


void fft3d_mkl_cpu::get_ijk_conjugate( vector<bio_uint> & ijkcnj )
{
	//decode CCE storage order, conjugate-even symmetry to regular 3d grid
	//https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/fourier-transform-functions/fft-functions/configuration-settings/
	//   dfti-complex-storage-dfti-real-storage-dfti-conjugate-even-storage.html#CONJUGATE_EVEN_STORAGE
	ijkcnj = vector<bio_uint>( ifo.NIJK * FOURIER_META_FFT_IJKCONJ_NC, 0 );

	for ( MKL_LONG k = 0; k < ifo.NK; k++ ) {
		MKL_LONG kconj = (ifo.NK - k) % ifo.NK;
		for( MKL_LONG j = 0; j < ifo.NJ; j++ ) {
			MKL_LONG jconj = (ifo.NJ - j) % ifo.NJ;
			for( MKL_LONG i = 0; i < ifo.NI; i++ ) {
				MKL_LONG iconj = (ifo.NI - i) % ifo.NI;
				MKL_LONG here = i + j*ifo.NI + k*ifo.NI*ifo.NJ;
				ijkcnj[here*FOURIER_META_FFT_IJK_NC+0] = iconj;
				ijkcnj[here*FOURIER_META_FFT_IJK_NC+1] = jconj;
				ijkcnj[here*FOURIER_META_FFT_IJK_NC+2] = kconj;
			}
		}
	}
}
