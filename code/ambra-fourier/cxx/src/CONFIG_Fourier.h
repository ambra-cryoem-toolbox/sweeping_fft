//##MK::GPLV3

#ifndef __AMBRA_CONFIG_FOURIER_H__
#define __AMBRA_CONFIG_FOURIER_H__

#include "../../../ambra-utils/cxx/src/CONFIG_Shared.h"
#include "../../../ambra-utils/cxx/src/AMBRA_XDMF.h"

//tool-specific definition of HDF5 meta- and dataset names
#include "../../../ambra-utils/cxx/src/metadata/AMBRA_TranscoderMetadataDefsH5.h"
#include "../../../ambra-utils/cxx/src/metadata/AMBRA_FourierMetadataDefsH5.h"


enum WINDOWING_METHOD {
	RECTANGULAR,
	HANN_HAMMING
};


enum FOURIER_METHOD {
	FOURIER_NONE,
	FOURIER_DFT_FAST,
	FOURIER_DIRECT
};


enum DIMENSIONALITY {
	ZERO_DIMENSIONS,
	ONE_DIMENSIONS,
	TWO_DIMENSIONS,
	THREE_DIMENSIONS
};


enum ROI_ENSEMBLE_METHOD {
	ENSEMBLE_NONE,
	SPECIFIC_POSITIONS,
	BUILD_A_GRID
};


enum ROI_TYPE {
	CUBOID,
	SPHERE,
	CYLINDER
};


/*
	SINGLE_POINT_CENTER,
	RANDOM_POINTS_FIXED
};
*/

struct hklgrid
{
	bio_real imi;
	bio_real imx;
	bio_real jmi;
	bio_real jmx;
	bio_real kmi;
	bio_real kmx;
	
	bio_real istep; //offset along direction i
	bio_real jstep;
	bio_real kstep;
	
	int ni;
	int nj;
	int nk;
	hklgrid() : imi(FMX), imx(FMI), jmi(FMX), jmx(FMI), kmi(FMX), kmx(FMI),
			istep(0.0), jstep(0.0), kstep(0.0), ni(0), nj(0), nk(0) {}
	hklgrid( const bio_real _imi, const bio_real _imx, const int _ni ) :
		imi(_imi), imx(_imx), jmi(_imi), jmx(_imx), kmi(_imi), kmx(_imx),
			istep((_imx-_imi)/static_cast<bio_real>(_ni)),
			jstep((_imx-_imi)/static_cast<bio_real>(_ni)),
			kstep((_imx-_imi)/static_cast<bio_real>(_ni)),
			ni(_ni), nj(_ni), nk(_ni) {}
};

ostream& operator<<(ostream& in, hklgrid const & val);


struct roi_cuboid
{
	//a cuboidal region of voxel starting at Cartesian coordinate ox,oy,oz and extending to [oi,oi+ni)
	px3d origin; //left (low nc), lower (low nr), front (low nl)
	int nx; //length, pointing to one past
	int ny;
	int nz;
	roi_cuboid() : origin(px3d(0,0,0)), nx(1), ny(1), nz(1) {}
	roi_cuboid( px3d const & _origin, const int _ni ) :
		origin(px3d(_origin.x, _origin.y, _origin.z)), nx(_ni), ny(_ni), nz(_ni) {}
	bool is_inside( const int ix, const int iy, const int iz );
	size_t nxyz();
};

ostream& operator<<(ostream& in, roi_cuboid const & val);


class ConfigFourier
{
public:
	
	static string Inputfile;
	static WINDOWING_METHOD WindowingMethod;
	static FOURIER_METHOD FourierMethod;
	static DIMENSIONALITY Dimensionality;
	static ROI_ENSEMBLE_METHOD ROIEnsemble;
	static vector<px3d> ROIUserPositions;
	static px3d ROIUserGridDimensions;
	static ROI_TYPE ROIType;
	//the template shapes which specify the shape of the ROIs with which the dataset is scanned
	//static roi_cuboid ROICuboid;

	static bool ComputeFastFourierTransform;
	static bool ComputeDirectFourierTransform;
	static bool IOStoreFFTMagnitude;
	static bool IOStoreFFTComplexValues;
	static bool IOStoreFFTProfiling;
	static bool IOStoreToolProfiling;

	static unsigned int ROIFirstIndex;
	static unsigned int ImageStackGuardWidth;
	static unsigned int ROIPaddingWidth;
	static unsigned int ROICuboidMinEdgeLength;
	static unsigned short GPUsPerProcess;
	static unsigned short GPUsPerComputingNode;
	static int MKLGetNumThreads;

	static bio_real WindowingHannHammingAlpha;
	static bio_real WindowingHannHammingBeta;
	static bio_real VoxelPhysicalEdgeLength;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
	static void reportSettings( vector<pparm> & res );
};

#endif
