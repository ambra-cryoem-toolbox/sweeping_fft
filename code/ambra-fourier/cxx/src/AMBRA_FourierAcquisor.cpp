//##MK::GPLV3

#include "AMBRA_FourierAcquisor.h"

fourier_res::fourier_res()
{
	/*
	mp = p3d(0.f, 0.f, 0.f);	
	owner = NULL;
	healthy = true;
	
	int NI = ConfigFourier::HKLGrid.ni;
	dft_real start = ConfigFourier::HKLGrid.imi;
	dft_real stop = ConfigFourier::HKLGrid.imx;

	size_t NIJK = CUBE(static_cast<size_t>(NI));
	IV = NULL;
	IV = new dft_real[NI];
	if ( IV != NULL ) {
		dft_real num = static_cast<dft_real>(NI-1);
		dft_real step = (stop-start)/num;
		for( int i = 0; i < NI; ++i ) {
			IV[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
		}

		AllPeaks = NULL;
		AllPeaks = new dft_real[NIJK];
		if ( AllPeaks != NULL ) {
			for( size_t ijk = 0; ijk < NIJK; ++ijk ) {
				AllPeaks[ijk] = 0.f;
			}
			//SignificantPeaks = vector<hklspace_res>();
		}
		else {
			healthy = false;
		}
	}
	else {
		healthy = false;
	}
	*/
	
	ifo = fftinfo();
	//tictoc = ambra_fourier_fft_prof();
}


fourier_res::~fourier_res()
{
	//##MK::do not dereference the owner, it is only a backreference!
	
	/*
	if ( IV != NULL ) {
		delete [] IV;
		IV = NULL;
	}
	if ( AllPeaks != NULL ) {
		delete [] AllPeaks;
		AllPeaks = NULL;
	}
	*/
}

/*
void fourier_res::direct_ft_cpu( vector<p3d> const & cand )
{
	double tic = omp_get_wtime();

//cout << "Entering direct_ft_cpu..." << "\n";

	if ( cand.size() > 0 && cand.size() < static_cast<size_t>(INT32MX-1) ) {
		int NI = ConfigFourier::HKLGrid.ni;
		int NIJ = SQR(NI);
		for( int k = 0; k < NI; k++ ) {
			dft_real ival_k = IV[k];
			for ( int j = 0; j < NI; j++ ) {
				dft_real ival_j = IV[j];
				int jkoffset = j*NI + k*NIJ;
				for ( int i = 0; i < NI; i++ ) {
					//reciprocal space coordinate
					dft_real ival_i = IV[i];
					dft_real fr = 0.0;
					dft_real fi = 0.0;
					for( auto it = cand.begin(); it != cand.end(); it++ ) { //most compute intensive part
						dft_real a = it->x * ival_i + it->y * ival_j + it->z * ival_k;
						fr += cos(a);
						fi += sin(a);
					}
					//sliced updating
					AllPeaks[i+jkoffset] = sqrt( fr*fr + fi*fi );
				} //i
			} //l
		} //k
	}
	else {
		healthy = false;
	}

	double toc = omp_get_wtime();
	tictoc[TICTOC_DIRECTFT] = toc-tic;

//cout << "direct_ft_cpu took " << toc-tic << "\n";
}


void fourier_res::postprocess_cpu()
{
	double tic = omp_get_wtime();
//cout << "Entering postprocess cpu ..." << "\n";
	if ( ( ConfigFourier::IOSignificantPks == true || ConfigFourier::IODBScanOnSgnPks == true ) && healthy == true ) {
		//find those position in reciprocal space with significant signal, ##MK::currently we use a unsigned char, i.e. 8-bit wide intensity value binning
		//first, get the maximum intensity in reciprocal space
		dft_real Fmax = 0.0; //negative values not possible
		int NI = ConfigFourier::HKLGrid.ni;
		int NIJ = SQR(NI);
		int NIJK = CUBE(NI);
		for( int ijk = 0; ijk < NIJK; ijk++ ) {
			if ( AllPeaks[ijk] < Fmax ) {
				continue;
			}
			else {
				Fmax = AllPeaks[ijk];
			}
		}

		if ( Fmax > EPSILON ) {
			//based on Fmax define a cut-off threshold below which intensities are counted as noise
			dft_real Fmin = ConfigFourier::SignificantPksThreshold * Fmax;
			dft_real normalizer = 1.0 / (Fmax - Fmin);

			intensity[HKLREFL_FMIN] = Fmin;
			intensity[HKLREFL_FMAX] = Fmax;
			//filter those positions in reciprocal space that are above threshold

			for( int ijk = 0; ijk < NIJK; ijk++ ) {
				if ( AllPeaks[ijk] < Fmin ) { //in good pattern and high resolution strong peaks are seldom
					continue;
				}
				else {
					int k = ijk / NIJ;
					int rem = ijk - (k*NIJ);
					int j = rem / NI;
					int i = rem - (j*NI);

					//int i = ijk % NI;
					//int j = (ijk / NIJ) % NI;
					//int k = ijk / NIJ;
					SignificantPeaks.push_back( hklspace_res( IV[i], IV[j], IV[k], (AllPeaks[ijk] - Fmin) * normalizer ) );
				}
			}
		}
		else {
			healthy = false;
		}
	}

	//earliest point in time where we can release the memory consumed for ConfigFourier:AllPeaks
	if ( ConfigFourier::IOFullGridPks == false ) {
		delete [] AllPeaks;
		AllPeaks = NULL;
	}

	//MK::IV and SignificantPeaks also must not be deleted, needed during I/O phase

	double toc = omp_get_wtime();
	tictoc[TICTOC_FINDPKS] = toc-tic;

//cout << "Findpeaks took " << toc-tic << "\n";
}


void fourier_res::dbscanpks_cpu()
{
	double tic = omp_get_wtime();
//cout << "Entering DBScan ..." << "\n";
	if ( ConfigFourier::IODBScanOnSgnPks == true && SignificantPeaks.size() > 0 && healthy == true ) {
		//perform a DBScan in continuous space on the significant peaks

		bool* visited = NULL;
		try {
			visited = new bool[SignificantPeaks.size()];
		}
		catch(bad_alloc &mecroak) {
			cerr << "Allocation failure for visited!" << "\n";
			healthy = false;
		}
		vector<unsigned int> clbl;
		try {
			clbl = vector<unsigned int>( SignificantPeaks.size(), UINT32MX );
		}
		catch(bad_alloc &mecroak) {
			cerr << "Allocation failure for cluster labels!" << "\n"; healthy = false;
		}

		vector<size_t> permutations;
		vector<p3d> uvw;
		//##MK::error management
		vector<p3dm1> thistree;
		kd_tree* tree = NULL;
		try {
			tree = new kd_tree; //this calls the default constructor
		}
		catch (bad_alloc &mecroak) {
			cerr << "Allocation failure for tree upon coarse distance approximation!" << "\n"; healthy = false;
		}

		if( healthy == true ) {
			//build KDTree of SignificantPeaks positions in reciprocal space
			for( auto it = SignificantPeaks.begin(); it != SignificantPeaks.end(); it++ ) {
				uvw.push_back( p3d( it->h, it->k, it->l ) );
			}

			tree->build( uvw, permutations, 16 );
			//cout << "Rank " << MASTER << " globally rotating direction grid tree has " << tree->nodes.size() << " nodes" << "\n";

//for(auto it = permutations.begin(); it != permutations.end(); it++ ) {
//	if ( static_cast<size_t>(*it) < SignificantPeaks.size() ) {
//		continue;
//	}
//	else {
//		cout << "--->" << static_cast<size_t>(*it) << "\t\t" << *it << "\n";
//	}
//}

			tree->pack_p3dm1( permutations, uvw, thistree );
			permutations = vector<size_t>();
			uvw = vector<p3d>();

//cout << "thistree->size() = " << thistree.size() << "\n";
//for( auto it = thistree.begin(); it != thistree.end(); it++ )
//	cout << it->x << ";" << it->y << ";" << it->z << "\n";

			for( size_t ip = 0; ip < SignificantPeaks.size(); ip++ ) {
				visited[ip] = false;
				clbl[ip] = static_cast<unsigned int>(UINT32MX); //grid nxyz defines maximum number of significant peaks < UINT32MX
			}

			unsigned int clusterid = 0;
			size_t minPts = static_cast<size_t>(ConfigFourier::DBScanMinPeaks);
			apt_real R = ConfigFourier::DBScanSearchRadius;
			apt_real RSQR = SQR(R);
			for( size_t ip = 0; ip < SignificantPeaks.size(); ++ip ) {
				if ( visited[ip] == false ) {
					visited[ip] = true;
//cout << "DBScan prepped entering ... minPts " << minPts << "\n";

					vector<unsigned int> cand;
					p3d here = p3d( SignificantPeaks[ip].h, SignificantPeaks[ip].k, SignificantPeaks[ip].l );
					tree->range_rball_noclear_nosort_p3d( here, thistree, RSQR, cand );
//cout << "ip " << ip << "\t\tcand prepped cand.size() " << cand.size() << "\n";
					if ( cand.size() < minPts ) {
						clbl[ip] = UINT32MX; //marking noise
					}
					else {
						clbl[ip] = clusterid;
						//cand includes myself
						for( size_t jt = 0; jt < cand.size(); jt++ ) {
							unsigned int ij = cand.at(jt);
							if ( static_cast<size_t>(ij) < SignificantPeaks.size() ) {
								if ( visited[ij] == false ) {
									visited[ij] = true;

									vector<unsigned int> nbcand;
									p3d there = p3d( SignificantPeaks[ij].h, SignificantPeaks[ij].k, SignificantPeaks[ij].l );
									tree->range_rball_noclear_nosort_p3d( there, thistree, RSQR, nbcand );
//cout << "ij " << ij << " nbcand.size() " << nbcand.size() << "\n";
									if ( nbcand.size() >= minPts ) { //core object of a cluster
										//MK::nboripres will now contain nborip but ipres already contains it hence ignore
										for( size_t k = 0; k < nbcand.size(); k++ ) {
											unsigned int kj = nbcand.at(k);
											if ( static_cast<size_t>(kj) < SignificantPeaks.size() ) {
												cand.push_back( kj );
											}
											else {
												cerr << "Thread " << omp_get_thread_num() << " access out of bounds kj !" << "\n";
											}

										} //having added all close bys within eps about nborposition thereby climbing ape-like through space
									}
								}
								if ( clbl[ij] == UINT32MX ) {
									clbl[ij] = clusterid;
								}
							}
							else {
								#pragma omp critical
								{
									cerr << "Thread " << omp_get_thread_num() << " access out of bounds ij !" << "\n";
								}
							}
						} //done adding if there were more than myself
					}
					clusterid++;
				} //done processing unvisited ip
			} //next ip

			delete [] visited; visited = NULL;
			delete tree; tree = NULL;
			thistree = vector<p3dm1>();
////##MK::BEGIN DEBUG
//			for( size_t i = 0; i < clbl.size(); i++ ) {
//				cout << "--->i/clid\t\t" << i << "\t\t" << clbl[i] << "\n";
//			}
////##MK::END DEBUG

			Reflectors = vector<reflector>( clusterid, reflector() );
			for( size_t i = 0; i < clbl.size(); i++ ) {
				//count intensity and get average reciprocal space position of the peak
				unsigned int clid = clbl.at(i);
				hklspace_res val = SignificantPeaks.at(i);
				if ( clid < Reflectors.size() ) {
					Reflectors[clid].u += val.h;
					Reflectors[clid].v += val.k;
					Reflectors[clid].w += val.l;
					Reflectors[clid].SUMIntensity += val.SQRIntensity;
					Reflectors[clid].PksCnts += 1.0;
				}
				else {
					#pragma omp critical
					{
						cerr << "Thread " << omp_get_thread_num() << " access out of bounds clid !" << "\n";
					}
				}
			}
			//get average position in reciprocal space
			for( auto rt = Reflectors.begin(); rt != Reflectors.end(); rt++ ) {
				rt->u = rt->u / rt->PksCnts;
				rt->v = rt->v / rt->PksCnts;
				rt->w = rt->w / rt->PksCnts;
			}
		}
	}

	double toc = omp_get_wtime();
	tictoc[TICTOC_DBSCANPKS] = toc-tic;
//cout << "DBScan took " << toc-tic << "\n";
}


#ifdef UTILIZE_GPUS
void fourier_res::direct_ft_gpu( vector<p3d> const & cand )
{
	double tic = omp_get_wtime();

	if ( cand.size() > 0 && cand.size() < static_cast<size_t>(INT32MX-1) ) {
		int ncand = static_cast<int>(cand.size());
		int n0 = 3*ncand;
		dft_real* XYZ = NULL; /// prepare asynchronous processing on GPU, use
		XYZ = new dft_real[n0];

		//acc_set_device_num(devId, acc_device_nvidia);
		int ni = ConfigFourier::HKLGrid.ni;
		int nij = SQR(ni);
		int nijk = CUBE(ni);

		dft_real* IVAL = NULL;
		IVAL = new dft_real[ni];

		dft_real* Fmagn = NULL;
		Fmagn = new float[nijk];

		if ( XYZ != NULL && IVAL != NULL && Fmagn != NULL ) {
			for( int i = 0; i < ncand; i++ ) {
				XYZ[3*i+0] = cand[i].x;
				XYZ[3*i+1] = cand[i].y;
				XYZ[3*i+2] = cand[i].z;
			}
			for( int j = 0; j < ni; j++ ) {
				IVAL[j] = IV[j];
			}

			for( int ijk = 0; ijk < nijk; ijk++ ) {
				AllPeaks[ijk] = 0.0;
				Fmagn[ijk] = 0.0; //##MK::i think this is obsolete
			}

			//transfer zero-allocated array AllPeaks to the device //##MK::in the future maybe fact that host waits during GPU execution could be utilized
			//but transform on CPU is two-orders of magnitude slower than on GPU...
			#pragma acc data copy(Fmagn[0:nijk])
			{
				int ijk, iion;
				//#pragma acc enter data copyin(Fmagn0[0:nijk]) async(1)
				#pragma acc parallel loop copyin(XYZ[0:3*ncand],IVAL[0:ni]) present(Fmagn[0:nijk]) private(iion,n0)
				for( ijk = 0; ijk < nijk; ijk++) {
					//decode reciprocal space position
					int k = ijk / nij;
					int rem = ijk - (k*nij);
					int j = rem / ni;
					int i = rem - (j*ni);
					//compute signal at that position
					dft_real fr = 0.0;
					dft_real fi = 0.0;
					for( iion = 0; iion < n0; iion += 3 ) {
						dft_real a = XYZ[iion+0]*IVAL[i] + XYZ[iion+1]*IVAL[j] + XYZ[iion+2]*IVAL[k];
						fr += cos(a);
						fi += sin(a);
					}
					Fmagn[ijk] = sqrt( fr*fr + fi*fi );
				} //AllPeaks continues to live happily on GPU
			} //end of acc data copy region explicitly syncs content from device with host

			#pragma acc wait

			for( int ijk = 0; ijk < nijk; ijk++ ) {
				AllPeaks[ijk] = Fmagn[ijk];
			}

			delete [] Fmagn; Fmagn = NULL;
			delete [] IVAL; IVAL = NULL;
			delete [] XYZ; XYZ = NULL;
			//transfer computed values from the GPU device to the host
			//pragma acc data copy allocates memory on GPU and copies data from host to GPU when entering
			//the region and copies the data to the host when exiting region
		}
		else {
			healthy = false;
		}
	}
	else {
		healthy = false;
	}

	double toc = omp_get_wtime();
	tictoc[TICTOC_DIRECTFT] = toc-tic;
}

#endif

*/


acquisor::acquisor()
{
	//cfg = binning_info();
}


acquisor::~acquisor()
{
	for( auto it = res.begin(); it != res.end(); it++ ) {
		if ( it->dat != NULL ) {
			delete it->dat;
			it->dat = NULL;
		}
	}
	res = vector<fourier_res_node>();
}

