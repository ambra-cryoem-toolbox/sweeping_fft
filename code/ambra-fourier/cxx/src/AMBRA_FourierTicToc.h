//##MK::GPLV3

#ifndef __AMBRA_FOURIER_TICTOC_H__
#define __AMBRA_FOURIER_TICTOC_H__

#include "AMBRA_FourierXDMF.h"

#include <mpi.h>
#include <omp.h>

//#include <openacc.h>

#define FOURIER_HDF_READIMAGEDIMS			1
#define FOURIER_MPI_BCASTDIMS				2
#define FOURIER_DEFINE_ROIS					3
#define FOURIER_DISTRIBUTE_ROIS				4
#define FOURIER_DEFINE_IMAGELOCAL			5
#define FOURIER_HDF_READIMAGEVAL			6
#define FOURIER_HDF_INITTARGET				7
#define FOURIER_HDF_WRITECONFIG				8
#define FOURIER_HDF_WRITEMETADATA			9
#define FOURIER_EXECUTE_ROIS				10
#define FOURIER_HDF_WRITEDATA				11


struct fourier_prof
{
	double dt;
	size_t vmem;		//virtual memory
	size_t rmem;		//residential memory
	fourier_prof() : dt(0.0), vmem(0), rmem(0) {}
	fourier_prof( const double _dt ) : dt(_dt), vmem(0), rmem(0) {}
	fourier_prof( const double _dt, const size_t _vmem, const size_t _rmem ) :
		dt(_dt), vmem(_vmem), rmem(_rmem) {}
	fourier_prof( const double _tic, const double _toc, memsnapshot const & _mm ) :
		dt(_toc-_tic), vmem(_mm.virtualmem), rmem(_mm.residentmem) {}
};


class rank_prof
{
public:
	rank_prof();
	~rank_prof();

	map<unsigned int, string> elapsedtime_dict;
	map<unsigned int, fourier_prof> elapsedtime_data;
};


//profiling related definitions
#define TICTOC_VXLINROI				0
#define TICTOC_PROCESSID			1
#define TICTOC_THREADID				2
#define TICTOC_GPUID				3
#define TICTOC_PLANFFT				4
#define TICTOC_EXECFFT				5
#define TICTOC_POSTFFT				6

struct ambra_fourier_fft_prof
{
	bio_uint nvxl;				//voxel analyzed
	bio_uint rankid;			//ID of the MPI process which processed this ROI
	bio_uint threadid;			//ID of the OpenMP thread which processed this ROI
	bio_uint deviceid;			//ID of the accelerator device which processed this ROI
	double planfft;				//elapsed time to build the plan
	double execfft; 			//elapsed time to compute the FFT
	double postfft;				//elapsed time to post-process the results of the FFT, e.g. compute magnitude or storing things
	ambra_fourier_fft_prof() : nvxl(0), 
		rankid(MASTER), threadid(MASTER), deviceid(MASTER), 
			planfft(0.0), execfft(0.0), postfft(0.0) {}
};

#endif
