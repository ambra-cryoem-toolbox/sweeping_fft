//##MK::GPLV3

#include "AMBRA_FourierGPUInterface.h"

cuda_gpu_bind_process_to_device::cuda_gpu_bind_process_to_device()
{
	ndevices = 0;
	mydevice = -1;				
	//MK::mydevice == -1 can be used as a flag indicating that no valid device was assigned (yet), this works because device IDs are >= 0
	
	devices_ifo = vector<cudaDeviceProp>();
}


cuda_gpu_bind_process_to_device::~cuda_gpu_bind_process_to_device()
{
}


bool cuda_gpu_bind_process_to_device::bind_process_to_device( const int rank )
{
	//identify how many devices are visible?? ##MK:: on the computing node
	cudaError cuError = cudaSuccess;
	cuError = cudaGetDeviceCount(&ndevices);
	if ( cuError != cudaSuccess) {
		cerr << "Rank " << rank << " cudaGetDeviceCount " << cudaGetErrorString(cuError) << "\n"; return false;
	}
	cout << "Rank " << rank << " detects " << ndevices << " devices on the computing node where the rank is running" << "\n";
	if ( ndevices == 0 ) {
		cerr << "Rank " << rank << " detected no GPU devices on the computing node, this is currently not supported by the implementation !" << "\n";
		cerr << "Rank " << rank << " note to developers implement this pure CPU fallback !" << "\n";
		return false;
	}
	if ( ndevices > 2 ) {
		cerr << "Rank " << rank << " detected there are more than 2 devices on the computing node, this is currently not supported by the implementation !" << "\n";
		cerr << "Rank " << rank << " note to developers implement this support!" << "\n";
		return false;
	}
	
	//get infos about these devices, see for example like done here https://gist.github.com/stevendborrelli/4286842
	for (int dev = 0; dev < ndevices; dev++) {
		cudaDeviceProp tmp;
		cuError = cudaGetDeviceProperties(&tmp, dev);
		if ( cuError != cudaSuccess) {
			cerr << "Rank " << rank << " cudaGetDeviceProperties error " << cudaGetErrorString(cuError) << "\n"; return false;
		}
		if (dev == 0) {
			if (tmp.major == 9999 && tmp.minor == 9999) {
				cerr << "Rank " << rank << " no CUDA GPU has been detected on the node" << "\n"; return false;
			} 
			else if (ndevices == 1) {
				cout << "Rank " << rank << " there is 1 device supporting CUDA on the node" << "\n";
			} 
			else {
				cout << "Rank " << rank << " there are " << ndevices << " supporting CUDA on the node" << "\n";
			}
		}
		devices_ifo.push_back( tmp );
		
		cout << "\t\t" << "Details for device " << dev << "\n";
		cout << "\t\t" << "Device name: " << devices_ifo.back().name << "\n";
		cout << "\t\t" << "Major revision number: " << devices_ifo.back().major << "\n";
		cout << "\t\t" << "Minor revision Number: " << devices_ifo.back().minor << "\n"; 
		cout << "\t\t" << "Total Global Memory: " << devices_ifo.back().totalGlobalMem << "\n";
		cout << "\t\t" << "Total shared mem per block: " << devices_ifo.back().sharedMemPerBlock << "\n";
		cout << "\t\t" << "Total const mem size: " << devices_ifo.back().totalConstMem << "\n";
		cout << "\t\t" << "Warp size: " << devices_ifo.back().warpSize << "\n";
		cout << "\t\t" << "Maximum block dimensions: " << devices_ifo.back().maxThreadsDim[0] << " x " 
			<< devices_ifo.back().maxThreadsDim[1] << " x " << devices_ifo.back().maxThreadsDim[2] << "\n";
		cout << "\t\t" << "Maximum grid dimensions: " << devices_ifo.back().maxGridSize[0] << " x " 
			<< devices_ifo.back().maxGridSize[1] << " x " << devices_ifo.back().maxGridSize[2] << "\n";
		cout << "\t\t" << "Clock Rate: " << devices_ifo.back().clockRate << "\n";
		cout << "\t\t" << "Number of multiprocessors: " << devices_ifo.back().multiProcessorCount << "\n";
		cout << "\n";
	}

	if ( ConfigFourier::GPUsPerComputingNode == 0 ) {
		cerr << "Rank " << rank << " I cannot use GPUs, this is currently not supported by the implementation !" << "\n";
		cerr << "Rank " << rank << " note to developers implement this pure CPU fallback !" << "\n";
		return false;
	}
	if ( ConfigFourier::GPUsPerComputingNode > 2 ) {
		cerr << "Rank " << rank << " current implementation does not support more than 2 GPUs per node !" << "\n";
		cerr << "Rank " << rank << " note to developers implement this support!" << "\n";
		return false;
	}
	
	//take a device, currently we make the following assumptions
	//at most two MPI processes per computing node are supported
	//##MK::in principle one could add support for more than 2 GPUs per node easily provided one assumes further that again each GPUs gets one MPI process
	//##MK::so e.g. on a 4 GPU node we make MPI 4 processes
	mydevice = rank % 2; //ConfigFourier::GPUsPerComputingNode;
	cout << "Rank " << rank << " ConfigFourier::GPUsPerComputingNode " << ConfigFourier::GPUsPerComputingNode << " mydevice " << mydevice << "\n";
	
	cuError = cudaSetDevice( mydevice );
	if ( cuError != cudaSuccess) {
		cerr << "Rank " << rank << " task 0 " << cudaGetErrorString(cuError) << "\n"; return false;
	}

	cuError = cudaGetDevice( &mydevice );
	if ( cuError != cudaSuccess) {
		cerr << "Rank " << rank << " cudaGetDevice error " << cudaGetErrorString(cuError) << "\n"; return false;
	}
	cout << "Rank " << rank << " initialized device, works now with device " << mydevice << "\n";

	return true;
}


fftinfo_gpu::fftinfo_gpu()
{
	//##MK::tested only for cubes!
	int N = 0;
	this->NX = N;
	this->NY = N;
	this->NZ = N;
	this->NXYZ = this->NX * this->NY * this->NZ;
	this->NI = this->NX;
	this->NJ = this->NY;
	this->NK = floor(this->NZ/2) + 1;
	this->NJK = this->NJ * this->NK;
	this->NIJK = this->NI * this->NJ * this->NK;
	this->DIMS[0] = this->NX;
	this->DIMS[1] = this->NY;
	this->DIMS[2] = this->NZ;
	/*
	this->input_strides[0] = 0;
	this->input_strides[1] = this->NX;
	this->input_strides[2] = this->NX * this->NY;
	this->input_strides[3] = 1;
	this->output_strides[0] = 0;
	this->output_strides[1] = this->NK;
	this->output_strides[2] = this->NJ * this->NK;
	this->output_strides[3] = 1;
	*/
	this->BATCH = 1;
	this->success = true;
	this->pad1 = true;
	this->pad2 = true;
	this->pad3 = true;
}


fftinfo_gpu::fftinfo_gpu( const int ni_cube )
{
	//##MK::tested only for cubes!
	this->NX = ni_cube;
	this->NY = ni_cube;
	this->NZ = ni_cube;
	this->NXYZ = this->NX * this->NY * this->NZ;
	this->NI = this->NX;
	this->NJ = this->NY;
	this->NK = floor(this->NZ/2) + 1;
	this->NJK = this->NJ * this->NK;
	this->NIJK = this->NI * this->NJ * this->NK;
	this->DIMS[0] = this->NX;
	this->DIMS[1] = this->NY;
	this->DIMS[2] = this->NZ;
	/*
	this->input_strides[0] = 0;
	this->input_strides[1] = this->NX;
	this->input_strides[2] = this->NX * this->NY;
	this->input_strides[3] = 1;
	this->output_strides[0] = 0;
	this->output_strides[1] = this->NK;
	this->output_strides[2] = this->NJ * this->NK;
	this->output_strides[3] = 1;
	*/
	this->BATCH = 1;
	this->success = true;
	this->pad1 = true;
	this->pad2 = true;
	this->pad3 = true;
}


fft3d_cuda_gpu::fft3d_cuda_gpu()
{
#ifdef EMPLOY_UNIFIED_MEMORY
	m_input = NULL;
	fftTemp = NULL;
#else
	m_input_h = NULL;
	m_input_d = NULL;
	fftTemp_h = NULL;
	fftTemp_d = NULL;
	#ifdef COMPUTE_MAGNITUDE
		fftmagn_h = NULL;
		fftmagn_d = NULL;
	#endif
#endif
	m_f_handle = UINT32MX; //##MK::cufftHandle is a cuda-internal typedeffed unsigned int
	ifo = fftinfo_gpu();
}


fft3d_cuda_gpu::~fft3d_cuda_gpu()
{
	cudaError_t status = cudaSuccess;
	cufftResult health = CUFFT_SUCCESS;
	//##MK::this creates problems when no plan was created, here I assume that cufft returns cufftHandles with values different to UINT32MX
	if ( m_f_handle != UINT32MX ) {
		health = cufftDestroy( m_f_handle );
	}
	if ( health != CUFFT_SUCCESS ) {
		cerr << "cufftDestroy failed " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	#ifdef EMPLOY_UNIFIED_MEMORY
		status = cudaFree(m_input);
		if ( status != cudaSuccess ) {
			cerr << "cudaFree m_input error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
		#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
			status = cudaDeviceSynchronize();
			if ( status != cudaSuccess ){
				cerr << "cudaDeviceSynchronize failed " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
		#endif
		status = cudaFree(fftTemp);
		if ( status != cudaSuccess ) {
			cerr << "cudaFree fftTemp error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
		#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
			status = cudaDeviceSynchronize();
			if ( status != cudaSuccess ){
				cerr << "cudaDeviceSynchronize failed " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
		#endif
	#else
		try {
			delete [] m_input_h;
			m_input_h = NULL;
		}
		catch (bad_alloc &croak) {
			cerr << "delete m_input_h failed !" << "\n"; return;
		}
		status = cudaFree(m_input_d);
		if ( status != cudaSuccess ) {
			cerr << "cudaFree m_input_d error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
		#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
			status = cudaDeviceSynchronize();
			if ( status != cudaSuccess ){
				cerr << "cudaDeviceSynchronize m_input_d failed " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
		#endif
		//##MK::add cudaDeviceSynchronize
		try {
			delete [] fftTemp_h;
			fftTemp_h = NULL;
		}
		catch (bad_alloc &croak) {
			cerr << "delete fftTemp_h failed !" << "\n";
			ifo.success = false; return;
		}
		status = cudaFree(fftTemp_d);
		if ( status != cudaSuccess ) {
			cerr << "cudaFree fftTemp_d error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
		#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
			status = cudaDeviceSynchronize();
			if ( status != cudaSuccess ){
				cerr << "ccudaDeviceSynchronize fftTemp_d failed " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
		#endif
		#ifdef COMPUTE_MAGNITUDE
			try {
				delete [] fftmagn_h;
				fftmagn_h = NULL;
			}
			catch(bad_alloc &croak) {
				cerr << "delete fftmagn_h failed !" << "\n";
				ifo.success = false; return;
			}
			status = cudaFree(fftmagn_d);
			if ( status != cudaSuccess ) {
				cerr << "cudaFree fftmagn_d error " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
			#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
				status = cudaDeviceSynchronize();
				if ( status != cudaSuccess ){
					cerr << "ccudaDeviceSynchronize fftmagn_d failed " << cudaGetErrorString(status) << "\n";
					ifo.success = false; return;
				}
			#endif
		#endif
	#endif
}


void fft3d_cuda_gpu::init( const int ni, vector<bio_real> const & img3d )
{
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
		double mytoc = 0.0;
		double ctic = 0.0;
		double ctoc = 0.0;
	#endif

	cudaError_t status = cudaSuccess;

	ifo = fftinfo_gpu( ni );

	size_t requiredSizeXYZ = ifo.NXYZ; //*ifo.BATCH;
	#ifdef EMPLOY_UNIFIED_MEMORY
		//host (CPU) and device (GPU) have same pointer, so CPU needs to sync to ensure that device finishes first before accessing data
		#ifdef EMPLOY_SINGLEPRECISION
			//##MK::for details about cudaMemAttachGlobal flag see here http://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__TYPES.html#group__CUDART__TYPES_1g4808e47eba73eb94622ec70a9f9b91ff
			status = cudaMallocManaged(&m_input, requiredSizeXYZ*sizeof(cufftReal), cudaMemAttachGlobal );
		#else
			status = cudaMallocManaged(&m_input, requiredSizeXYZ*sizeof(cufftDoubleReal), cudaMemAttachGlobal );
		#endif
	#else
		//host and device have separate copies of m_input
		#ifdef EMPLOY_SINGLEPRECISION
			m_input_h = new cufftReal[requiredSizeXYZ];
		#else
			m_input_h = new cufftDoubleReal[requiredSizeXYZ];
		#endif
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMalloc( (void**) &m_input_d, requiredSizeXYZ*sizeof(cufftReal) );
		#else
			status = cudaMalloc( (void**) &m_input_d, requiredSizeXYZ*sizeof(cufftDoubleReal) );
		#endif
	#endif
	//so far buffers in m_input_h, m_input_d contain junk data
	if ( status != cudaSuccess ) {
		cerr << "cudaMalloc m_input_d error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize m_input_d failed " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	//currently, filling
	for( size_t i = 0; i < requiredSizeXYZ; i++ ) { //##MK::make kernel launch, currently running on CPU ?
		#ifdef EMPLOY_UNIFIED_MEMORY
			//##MK::implement managed case
		#else
			//host fills first values in, then copies to device
			#ifdef EMPLOY_SINGLEPRECISION
					m_input_h[i] = (cufftReal) img3d[i];
			#else
					m_input_h[i] = (cufftDoubleReal) img3d[i];
			#endif
		#endif
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize m_input_h init " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	#ifdef DEBUG_CUDA_VERBOSE
		ctic = MPI_Wtime();
	#endif
	#ifdef EMPLOY_UNIFIED_MEMORY
		//##MK::nothing to be done, data are already on the shared memory ?
	#else
		//target, source
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMemcpy( m_input_d, m_input_h, requiredSizeXYZ*sizeof(cufftReal), cudaMemcpyHostToDevice );
		#else
			status = cudaMemcpy( m_input_d, m_input_h, requiredSizeXYZ*sizeof(cufftDoubleReal), cudaMemcpyHostToDevice );
		#endif
	#endif

	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "host2device m_input h memcpy took " << (ctoc-ctic) << " s" << "\n";
	#endif
		
	if ( status != cudaSuccess ) {
		cerr << "cudaMemcyp m_input_d/h error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize m_input_d/h error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	//prepare output buffers for the forward FFT
	size_t requiredSizeIJK = ifo.NIJK;
	#ifdef EMPLOY_UNIFIED_MEMORY
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMallocManaged(&fftTemp, requiredSizeIJK*sizeof(cufftComplex), cudaMemAttachGlobal );
		#else
			status = cudaMallocManaged(&fftTemp, requiredSizeIJK*sizeof(cufftDoubleComplex), cudaMemAttachGlobal );
		#endif
		if ( status != cudaSuccess ) {
			cerr << "cudaMallocManaged fftTemp error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#else
		#ifdef EMPLOY_SINGLEPRECISION
			fftTemp_h = new cufftComplex[requiredSizeIJK];
			status = cudaMalloc( (void**) &fftTemp_d, requiredSizeIJK*sizeof(cufftComplex) );
		#else
			fftTemp_h = new cufftDoubleComplex[requiredSizeIJK];
			//data in there are junk !
			status = cudaMalloc( (void**) &fftTemp_d, requiredSizeIJK*sizeof(cufftDoubleComplex) );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMalloc fftTemp_d error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize fftTemp_d  error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	#ifdef DEBUG_CUDA_VERBOSE
		mytoc = MPI_Wtime();
		cout << "Initializing took " << (mytoc-mytic) << " s" << "\n";
	#endif
}


void fft3d_cuda_gpu::reset_plan_batch()
{
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
		double mytoc = 0.0;
		double ctic = 0.0;
		double ctoc = 0.0;
	#endif
	cudaError_t status = cudaSuccess;

	size_t requiredSizeXYZB = ifo.NXYZ*ifo.BATCH;
	for( size_t i = 0; i < requiredSizeXYZB; i++ ) { //##MK::for m_input_d fill it on the GPU via a kernel launch
		//currently filled on CPU and copied over
		//set buffers to zero to enable spotting of numerical problems or incorrect results, somewhat hyperphobic
		#ifdef EMPLOY_UNIFIED_MEMORY
			//##MK::implement managed case
		#else
			//host fills first values in, then copies to device
			#ifdef EMPLOY_SINGLEPRECISION
					m_input_h[i] = (cufftReal) 0.0;
			#else
					m_input_h[i] = (cufftDoubleReal) 0.0;
			#endif
		#endif
	}

	#ifdef DEBUG_CUDA_VERBOSE
		ctic = MPI_Wtime();
	#endif
	#ifdef EMPLOY_UNIFIED_MEMORY
		//##MK::nothing to be done, data are already on the shared memory ?
	#else
		//arguments for cudaMemcpy are target, source
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMemcpy( m_input_d, m_input_h, requiredSizeXYZB*sizeof(cufftReal), cudaMemcpyHostToDevice );
		#else
			status = cudaMemcpy( m_input_d, m_input_h, requiredSizeXYZB*sizeof(cufftDoubleReal), cudaMemcpyHostToDevice );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMemcpy m_input_d/h error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize m_input_d/h error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif
		
	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "host2device m_input_d/h memcpy took " << (ctoc-ctic) << " s" << "\n";
	#endif

	size_t requiredSizeIJKB = ifo.NIJK*ifo.BATCH;
	for( size_t i = 0; i < requiredSizeIJKB; i++ ) { //##MK::for m_input_d fill it on the GPU via a kernel launch
		//currently filled on CPU and copied over
		//set output buffers to zero to enable spotting of numerical problems or incorrect results, somewhat hyperphobic
		#ifdef EMPLOY_UNIFIED_MEMORY
			//##MK::implement managed case
		#else
			//host fills first values in, then copies to device
			#ifdef EMPLOY_SINGLEPRECISION
					fftTemp_h[i].x = (cufftReal) 0.0;
					fftTemp_h[i].y = (cufftReal) 0.0;
					#ifdef COMPUTE_MAGNITUDE
						fftmagn_h[i] = (cufftReal) 0.0;
					#endif
			#else
					fftTemp_h[i].x = (cufftDoubleReal) 0.0;
					fftTemp_h[i].y = (cufftDoubleReal) 0.0;
					#ifdef COMPUTE_MAGNITUDE
						fftmagn_h[i] = (cufftDoubleReal) 0.0;
					#endif
			#endif
		#endif
	}

	#ifdef DEBUG_CUDA_VERBOSE
		ctic = MPI_Wtime();
	#endif
	//##MK::alternatively use cudaMemset( fftTemp_d, 0.0, requiredSizeIJK*sizeof(cufftComplex) );
	#ifdef EMPLOY_UNIFIED_MEMORY
		//##MK::nothing to be done, data are already on the shared memory ?
	#else
		//arguments for cudaMemcpy are target, source
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMemcpy( fftTemp_d, fftTemp_h, requiredSizeIJKB*sizeof(cufftComplex), cudaMemcpyHostToDevice );
		#else
			status = cudaMemcpy( fftTemp_d, fftTemp_h, requiredSizeIJKB*sizeof(cufftDoubleComplex), cudaMemcpyHostToDevice );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMemcpy fftTemp_d/h error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize fftTemp_d/h error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif
		
	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "host2device fftTemp_d/h memcpy took " << (ctoc-ctic) << " s" << "\n";
	#endif

	//MK::set amplitude buffers to zero, this is hyperphobic
	#ifdef COMPUTE_MAGNITUDE
		#ifdef DEBUG_CUDA_VERBOSE
			ctic = MPI_Wtime();
		#endif
		//##MK::alternatively use cudaMemset( fftTemp_d, 0.0, requiredSizeIJK*sizeof(cufftComplex) );
		#ifdef EMPLOY_UNIFIED_MEMORY
			//##MK::nothing to be done, data are already on the shared memory ?
		#else
			//arguments for cudaMemcpy are target, source
			#ifdef EMPLOY_SINGLEPRECISION
				status = cudaMemcpy( fftmagn_d, fftmagn_h, requiredSizeIJKB*sizeof(cufftReal), cudaMemcpyHostToDevice );
			#else
				status = cudaMemcpy( fftmagn_d, fftmagn_h, requiredSizeIJKB*sizeof(cufftDoubleReal), cudaMemcpyHostToDevice );
			#endif
		#endif
		if ( status != cudaSuccess ) {
			cerr << "cudaMemcpy fftTemp_d/h error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
		#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
			status = cudaDeviceSynchronize();
			if ( status != cudaSuccess ){
				cerr << "cudaDeviceSynchronize fftmagn_d/h error " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
		#endif
		#ifdef DEBUG_CUDA_VERBOSE
			ctoc = MPI_Wtime();
			cout << "host2device fftmagn_d/h memcpy took " << (ctoc-ctic) << " s" << "\n";
		#endif
	#endif

	#ifdef DEBUG_CUDA_VERBOSE
		mytoc = MPI_Wtime();
		cout << "fft3d_cuda_gpu::reset_plan_batch took " << (mytoc-mytic) << " s" << "\n";
		//now m_input_d, m_input_h, fftTemp_d, fftTemp_h are filled with 0.0
	#endif
}


void fft3d_cuda_gpu::init_plan_batch( const int ni, const int nbatch )
{
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
		double mytoc = 0.0;
		double ctic = 0.0;
		double ctoc = 0.0;
	#endif
	cudaError_t status = cudaSuccess;

	ifo = fftinfo_gpu( ni );
	ifo.BATCH = nbatch;

	//prepare the input buffers
	size_t requiredSizeXYZB = ifo.NXYZ*ifo.BATCH;
	#ifdef EMPLOY_UNIFIED_MEMORY
		//host (CPU) and device (GPU) have same pointer, so CPU needs to sync to ensure that device finishes first before accessing data
		//##MK::implement managed memory
	#else
		//traditional way, host and device have separate copies of m_input
		#ifdef EMPLOY_SINGLEPRECISION
			m_input_h = new cufftReal[requiredSizeXYZB];
			status = cudaMalloc( (void**) &m_input_d, requiredSizeXYZB*sizeof(cufftReal) );
		#else
			m_input_h = new cufftDoubleReal[requiredSizeXYZB];
			status = cudaMalloc( (void**) &m_input_d, requiredSizeXYZB*sizeof(cufftDoubleReal) );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMalloc m_input_d error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize after m_input_d buffer alloc " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif
	//so far buffers in m_input_h, m_input_d contain junk data

	//prepare the output buffer for the FFT
	size_t requiredSizeIJKB = ifo.NIJK*ifo.BATCH;
	#ifdef EMPLOY_UNIFIED_MEMORY
		//##MK::implement
	#else
		#ifdef EMPLOY_SINGLEPRECISION
			fftTemp_h = new cufftComplex[requiredSizeIJKB];
			status = cudaMalloc( (void**) &fftTemp_d, requiredSizeIJKB*sizeof(cufftComplex) );
		#else
			fftTemp_h = new cufftDoubleComplex[requiredSizeIJKB];
			//data in there are junk !
			status = cudaMalloc( (void**) &fftTemp_d, requiredSizeIJKB*sizeof(cufftDoubleComplex) );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMalloc fftTemp_d error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize fftTemp_d error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	//prepare amplitude computation buffers
	#ifdef COMPUTE_MAGNITUDE
		#ifdef EMPLOY_UNIFIED_MEMORY
			//##MK::implement
		#else
			#ifdef EMPLOY_SINGLEPRECISION
				fftmagn_h = new cufftReal[requiredSizeIJKB];
				status = cudaMalloc( (void**) &fftmagn_d, requiredSizeIJKB*sizeof(cufftReal) );
			#else
				fftmagn_h = new cufftDoubleReal[requiredSizeIJKB];
				//data in there are junk !
				status = cudaMalloc( (void**) &fftmagn_d, requiredSizeIJKB*sizeof(cufftDoubleReal) );
			#endif
		#endif
		if ( status != cudaSuccess ) {
			cerr << "cudaMalloc fftmagn_d error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
		#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
			status = cudaDeviceSynchronize();
			if ( status != cudaSuccess ){
				cerr << "cudaDeviceSynchronize fftmagn_d error " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
		#endif
	#endif

	//set up a plan for a batch of 3D out-of-place transforms, a plan that can be reutilized
	cufftResult health = CUFFT_SUCCESS;
	#ifdef DEBUG_CUDA_VERBOSE
		ctic = MPI_Wtime();
	#endif
	int dims[3] = { ifo.NX, ifo.NY, ifo.NZ };
	#ifdef EMPLOY_SINGLEPRECISION
		health = cufftPlanMany( &m_f_handle, 3, dims, NULL, 1, 0, NULL, 1, 0, CUFFT_R2C, ifo.BATCH );
	#else
		health = cufftPlanMany( &m_f_handle, 3, dims, NULL, 1, 0, NULL, 1, 0, CUFFT_D2Z, ifo.BATCH );
	#endif
	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "cufftPlanMany took " << (ctoc-ctic) << " s" << "\n";
	#endif
	if ( health != CUFFT_SUCCESS ) {
		cerr << "cufftPlanMany error " << health << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize cufftPlanMany error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	//characterize details of the plan, memory-wise
	size_t sizeInBytes[1] = { 0 }; //as many array elements as GPUs used by the MPI process
	#ifdef EMPLOY_SINGLEPRECISION
		health  = cufftGetSizeMany( m_f_handle, 3, dims, NULL, 1, 0, NULL, 1, 0, CUFFT_R2C, ifo.BATCH, sizeInBytes );
	#else
		health = cufftGetSizeMany( m_f_handle, 3, dims, NULL, 1, 0, NULL, 1, 0, CUFFT_D2Z, ifo.BATCH, sizeInBytes );
	#endif
	if ( health != CUFFT_SUCCESS ) {
		cerr << "cufftGetSizeMany error " << health << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize cufftGetSizeMany error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif
	#ifdef DEBUG_CUDA_VERBOSE
		cout << "fft3d_cuda_gpu::current FFT plan takes " << (double) sizeInBytes[0] / (double) (1024*1024*1024) << " gigabytes, i.e. "
			<< (double) sizeInBytes[0] / (double) (ifo.NXYZ * ifo.BATCH) << " bytes per matpoint"<< "\n";
		//##MK::cache memory requirements in ifo
	
		mytoc = MPI_Wtime();
		cout << "fft3d_cuda_gpu::init_plan_batch took " << (mytoc-mytic) << " s" << "\n";
	#endif
}


void fft3d_cuda_gpu::fill_data_batch( vector<bio_real> const & img3d, const int batchid )
{
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
	#endif

	size_t requiredSizeXYZ = ifo.NXYZ;
	//MK::all transforms have to be of the same size!
	//if ( img3d.size() == requiredSizeXYZ ) {
		size_t woffset = batchid * requiredSizeXYZ;
		for( size_t i = 0; i < requiredSizeXYZ; i++ ) {
			//##MK::make kernel launch, currently running on CPU ?
			//set buffers to zero to enable stopping of numerical problems
			#ifdef EMPLOY_UNIFIED_MEMORY
				//##MK::implement managed case
			#else
				//host fills first values in, then copies to device
				#ifdef EMPLOY_SINGLEPRECISION
						m_input_h[woffset+i] = (cufftReal) img3d[i];
				#else
						m_input_h[woffset+i] = (cufftDoubleReal) img3d[i];
				#endif
			#endif
		}
	//}
	//else {
	//	cerr << "fft3d_cuda_gpu::fill_data_batch batchid " << batchid << " failed because img3d.size() != " << requiredSizeXYZ << "\n";
	//	return;
	//}
	#ifdef DEBUG_CUDA_VERBOSE
		double mytoc = MPI_Wtime();
		cout << "fft3d_cuda_gpu::fill_data_batch for a single fft took " << (mytoc-mytic) << " s" << "\n";
	#endif
}


void fft3d_cuda_gpu::move_plan_batch()
{
	//copy host data to device for all ffts of the batch
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
	#endif

	cudaError_t status = cudaSuccess;

	size_t requiredSizeXYZB = ifo.NXYZ*ifo.BATCH;
	#ifdef EMPLOY_UNIFIED_MEMORY
		//##MK::nothing to be done, data are already on the shared memory ?
	#else
		//arguments for cudaMemcpy are target, source
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMemcpy( m_input_d, m_input_h, requiredSizeXYZB*sizeof(cufftReal), cudaMemcpyHostToDevice );
		#else
			status = cudaMemcpy( m_input_d, m_input_h, requiredSizeXYZB*sizeof(cufftDoubleReal), cudaMemcpyHostToDevice );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMemcpy m_input_d/h error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize m_input_d/h error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	#ifdef DEBUG_CUDA_VERBOSE
		double mytoc = MPI_Wtime();
		cout << "fft3d_cuda_gpu::move_plan_batch for all ffts took " << (mytoc-mytic) << " s" << "\n";
	#endif
}


void fft3d_cuda_gpu::forwardFFT()
{
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
		double mytoc = 0.0;
		double ctic = 0.0;
		double ctoc = 0.0;
	#endif
	cudaError_t status = cudaSuccess;
	cufftResult health = CUFFT_SUCCESS;

	#ifdef DEBUG_CUDA_VERBOSE
		ctic = MPI_Wtime();
	#endif
	#ifdef EMPLOY_SINGLEPRECISION
		health = cufftPlan3d( &m_f_handle, ifo.NX, ifo.NY, ifo.NZ, CUFFT_R2C );
	#else
		health = cufftPlan3d( &m_f_handle, ifo.NX, ifo.NY, ifo.NZ, CUFFT_R2C );
	#endif
	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "cufftPlan3d took " << (ctoc-ctic) << " s" << "\n";
	#endif
	if ( health != CUFFT_SUCCESS ) {
		cerr << "cufftPlan3d error " << health << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize cufftPlan3d error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	#ifdef DEBUG_CUDA_VERBOSE
		ctic = MPI_Wtime();
	#endif
	#ifdef EMPLOY_UNIFIED_MEMORY
		#ifdef EMPLOY_SINGLEPRECISION
			health = cufftExecR2C( m_f_handle, m_input, fftTemp ); //implicitly CUFFT_FORWARD
		#else
			health = cufftExecD2Z( m_f_handle, m_input, fftTemp );
		#endif
	#else
		#ifdef EMPLOY_SINGLEPRECISION
			health = cufftExecR2C( m_f_handle, m_input_d, fftTemp_d );
		#else
			health = cufftExecD2Z( m_f_handle, m_input_d, fftTemp_d );
		#endif
	#endif
	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "cufftExec took " << (ctoc-ctic) << " s" << "\n";
	#endif
	if ( health != CUFFT_SUCCESS ) {
		cerr << "cufftExec error " << health << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize cufftExec error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif
	#ifdef EMPLOY_UNIFIED_MEMORY
		//##MK::nothing to be done, results coherent and in memory of host ?
	#else
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMemcpy( fftTemp_h, fftTemp_d, ifo.NIJK*sizeof(cufftComplex), cudaMemcpyDeviceToHost );
		#else
			status = cudaMemcpy( fftTemp_h, fftTemp_d, ifo.NIJK*sizeof(cufftDoubleComplex), cudaMemcpyDeviceToHost );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMemcpy error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}

	health = cufftDestroy( m_f_handle );
	if ( health != CUFFT_SUCCESS ) {
		cerr << "cufftDestroy error " << health << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize cufftDestroy error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif

	/*
	//##after a cudaDeviceSynchronize the host should have the results of the FFT in its fftTemp_h array
	//##MK::BEGIN DEBUG
	cout << "Done, printing results" << "\n";
	//int jump = 0;
	for ( int ijk = 0; ijk < ifo.NIJK; ijk++ ) {
		cout << ijk << "\t\t" << "(" << fftTempP[ijk].real() << ", " << fftTempP[ijk].imag() << ")" << "\n";
		//jump++;
		//if ( jump != 8)
		//	continue;
		//else
		//	cout << "\n";
		//jump = 0;
	}
	//##MK::END DEBUG
	*/

	#ifdef DEBUG_CUDA_VERBOSE
		mytoc = MPI_Wtime();
		cout << "fft3d_cuda_gpu::forwardFFT took " << (mytoc-mytic) << " s" << "\n";
		//##MK::fftTemp is filled with junk data so far
	#endif
}


void fft3d_cuda_gpu::exec_plan_batch()
{
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
		double mytoc = 0.0;
		double ctic = 0.0;
		double ctoc = 0.0;
	#endif
	cudaError_t status = cudaSuccess;
	cufftResult health = CUFFT_SUCCESS;
	
	#ifdef DEBUG_CUDA_VERBOSE
		ctic = MPI_Wtime();
	#endif
	#ifdef EMPLOY_UNIFIED_MEMORY
		#ifdef EMPLOY_SINGLEPRECISION
			health = cufftExecR2C( m_f_handle, m_input, fftTemp ); //implicitly CUFFT_FORWARD
		#else
			health = cufftExecD2Z( m_f_handle, m_input, fftTemp );
		#endif
	#else
		#ifdef EMPLOY_SINGLEPRECISION
			health = cufftExecR2C( m_f_handle, m_input_d, fftTemp_d ); //implicitly CUFFT_FORWARD because real to complex
		#else
			health = cufftExecD2Z( m_f_handle, m_input_d, fftTemp_d );
		#endif
	#endif
	if ( health != CUFFT_SUCCESS ) {
		cerr << "cuFFTExec error " << health << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize cufftExec error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif
	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "cufftExec took " << (ctoc-ctic) << " s" << "\n";
	#endif

	//FFT results are still on the device only

	//ctic = MPI_Wtime();
	#ifdef EMPLOY_UNIFIED_MEMORY
		//##MK::nothing to be done, results coherent and in memory of host ?
	#else
		#ifdef EMPLOY_SINGLEPRECISION
			status = cudaMemcpy( fftTemp_h, fftTemp_d, ifo.NIJK*sizeof(cufftComplex), cudaMemcpyDeviceToHost );
		#else
			status = cudaMemcpy( fftTemp_h, fftTemp_d, ifo.NIJK*sizeof(cufftDoubleComplex), cudaMemcpyDeviceToHost );
		#endif
	#endif
	if ( status != cudaSuccess ) {
		cerr << "cudaMemcpy fftTemp_h/d error " << cudaGetErrorString(status) << "\n";
		ifo.success = false; return;
	}
	#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
		status = cudaDeviceSynchronize();
		if ( status != cudaSuccess ){
			cerr << "cudaDeviceSynchronize cudaMemcpy error " << cudaGetErrorString(status) << "\n";
			ifo.success = false; return;
		}
	#endif
	#ifdef DEBUG_CUDA_VERBOSE
		ctoc = MPI_Wtime();
		cout << "cudaMemcpy fftTemp_h/d took " << (ctoc-ctic) << " s" << "\n";
	#endif

	/*
	//##after a cudaDeviceSynchronize the host should have the results of the FFT in its fftTemp_h array
	//##MK::BEGIN DEBUG
	cout << "Done, printing results" << "\n";
	//int jump = 0;
	for ( int ijk = 0; ijk < ifo.NIJK; ijk++ ) {
		cout << ijk << "\t\t" << "(" << fftTempP[ijk].real() << ", " << fftTempP[ijk].imag() << ")" << "\n";
		//jump++;
		//if ( jump != 8)
		//	continue;
		//else
		//	cout << "\n";
		//jump = 0;
	}
	//##MK::END DEBUG
	*/
	#ifdef DEBUG_CUDA_VERBOSE
		mytoc = MPI_Wtime();
		cout << "fft3d_cuda_gpu::exec_plan_batch took " << (mytoc-mytic) << " s" << "\n";
	#endif
}


#ifdef COMPUTE_MAGNITUDE
	#include <math.h>
	__global__ void amplitude_f32( int nijkb, cufftComplex *in, cufftReal *out )
	{
		//for each complex FFT result we get one amplitude, out uses the same storage order as for the in
		/*
		int index = threadIdx.x;
		int stride = blockDim.x;
		*/
		int index = blockIdx.x * blockDim.x + threadIdx.x;
		int stride = blockDim.x * gridDim.x;
		for (int i = index; i < nijkb; i += stride) {
			out[i] = sqrt( in[i].x*in[i].x + in[i].y*in[i].y );
		}
	}


	__global__ void amplitude_f64( int nijkb, cufftDoubleComplex *in, cufftDoubleReal *out )
	{
		//for each complex FFT result we get one amplitude, out uses the same storage order as for the in
		/*
		int index = threadIdx.x;
		int stride = blockDim.x;
		*/
		int index = blockIdx.x * blockDim.x + threadIdx.x;
		int stride = blockDim.x * gridDim.x;
		for (int i = index; i < nijkb; i += stride) {
			out[i] = sqrt( in[i].x*in[i].x + in[i].y*in[i].y );
		}
	}
#endif


void fft3d_cuda_gpu::exec_magn_batch()
{
	#ifdef DEBUG_CUDA_VERBOSE
		double mytic = MPI_Wtime();
		double mytoc = 0.0;
	#endif
		
	#ifdef COMPUTE_MAGNITUDE
		#ifdef DEBUG_CUDA_VERBOSE
			double ctic = 0.0;
			double ctoc = 0.0;
		#endif
		cudaError_t status = cudaSuccess;

		//see tutorial on how to work with simple kernels like this one https://developer.nvidia.com/blog/even-easier-introduction-cuda/
		if ( ifo.NIJK*ifo.BATCH < static_cast<size_t>(INT32MX) ) {
			#ifdef DEBUG_CUDA_VERBOSE
				ctic = MPI_Wtime();
			#endif
			//compute the amplitude as sqrt(real^2+imag^2)
			int requiredSizeIJKB = (int) ifo.NIJK * (int) ifo.BATCH;

			//pure sequential execution on the GPU
			int blockSize = 1;
			int numBlocks = 1;
			//multithreaded execution on the GPU
			blockSize = 64; //needs to be a multiple of 32 !
			numBlocks = (requiredSizeIJKB + blockSize - 1) / blockSize;
			#ifdef DEBUG_CUDA_VERBOSE
				cout << "cuda amplitude kernel requiredSizeIJKB " << requiredSizeIJKB << " blockSize " << 64 << " numBlocks " << numBlocks << "\n";
			#endif
			#ifdef EMPLOY_SINGLEPRECISION
				amplitude_f32<<<numBlocks, blockSize>>>( requiredSizeIJKB, fftTemp_d, fftmagn_d );
			#else
				amplitude_f64<<<numBlocks, blockSize>>>( requiredSizeIJKB, fftTemp_d, fftmagn_d );
			#endif
			#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
				status = cudaDeviceSynchronize();
				if ( status != cudaSuccess ){
					cerr << "cudaDeviceSynchronize amplitude kernel error " << cudaGetErrorString(status) << "\n";
					ifo.success = false; return;
				}
			#endif
			#ifdef DEBUG_CUDA_VERBOSE
				ctoc = MPI_Wtime();
				cout << "cuda amplitude kernel took " << (ctoc-ctic) << " s" << "\n";
			#endif

			//mem copy results to host
			#ifdef DEBUG_CUDA_VERBOSE
				ctic = MPI_Wtime();
			#endif
			#ifdef EMPLOY_UNIFIED_MEMORY
				//##MK::implement
			#else
				#ifdef EMPLOY_SINGLEPRECISION
					status = cudaMemcpy( fftmagn_h, fftmagn_d, ifo.NIJK*ifo.BATCH*sizeof(cufftReal), cudaMemcpyDeviceToHost );
				#else
					status = cudaMemcpy( fftmagn_h, fftmagn_d, ifo.NIJK*ifo.BATCH*sizeof(cufftDoubleReal), cudaMemcpyDeviceToHost );
				#endif
			#endif
			if ( status != cudaSuccess ) {
				cerr << "cudaMemcpy fftmagn_h/d error " << cudaGetErrorString(status) << "\n";
				ifo.success = false; return;
			}
			#ifdef DEBUG_WITH_CUDA_DEVICE_SYNCS
				status = cudaDeviceSynchronize();
				if ( status != cudaSuccess ){
					cerr << "cudaDeviceSynchronize fftmagn_h/d cudaMemcpy error " << cudaGetErrorString(status) << "\n";
					ifo.success = false; return;
				}
			#endif
			#ifdef DEBUG_CUDA_VERBOSE
				ctoc = MPI_Wtime();
				cout << "cudaMemcpy fftmagn_h/d took " << (ctoc-ctic) << " s" << "\n";
			#endif
		}
		else {
			cerr << "fft3d_cuda_gpu::computeFFTMagn current implementation does not support the case ifo.NIJK * ifo.BATCH >= " << INT32MX << "\n";
			ifo.success = false; return;
		}
	#endif
		
	#ifdef DEBUG_CUDA_VERBOSE
		mytoc = MPI_Wtime();
		cout << "fft3d_cuda_gpu::computeFFTMagn took " << (mytoc-mytic) << " s" << "\n";
	#endif
}


/*
void fft3d_cuda_gpu::get_ijk_normal( vector<bio_uint> & ijknrm )
{
}


void fft3d_cuda_gpu::get_ijk_conjugate( vector<bio_uint> & ijkcnj )
{
}
*/
