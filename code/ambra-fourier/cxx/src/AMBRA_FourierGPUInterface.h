//##MK::GPLV3

#ifndef __AMBRA_FOURIER_GPU_INTERFACE_H__
#define __AMBRA_FOURIER_GPU_INTERFACE_H__

#include "AMBRA_FourierCPUInterface.h"

//#define SINGLE_GPU_WORKLOAD					10

//MK::(de/)activate GPUs for easier development purposes, easier because will not force to have MPI+OMP+OPENACC capable compiler
#define UTILIZE_GPUS
#ifdef UTILIZE_GPUS

	//using CUDA and calls to the CUDA cuFFT library
	//#include <openacc.h>
	//#include <accelmath.h>

	//load general headers for CUDA runtime libraries
	#include <cuda.h>
	#include <cuda_runtime.h>
	//load specific CUDA toolkit headers, here for the cuFFT library
	#include <cufft.h>
	#include <cufftXt.h>

	//##MK::comment out to switch off all device syncs, DONT AT THE MOMENT, because some syncs are required ! ##MK::need to check here carefully!
	#define DEBUG_WITH_CUDA_DEVICE_SYNCS
	//#define EMPLOY_UNIFIED_MEMORY
	//#define EMPLOY_SINGLEPRECISION
	#define COMPUTE_MAGNITUDE
	//#define DEBUG_CUDA_VERBOSE
	//define the above to get more detailed timing for cuda calls


	struct fftinfo_gpu
	{
		int NX;
		int NY;
		int NZ;
		//int NYZ;
		int NXYZ;
		int NI;
		int NJ;
		int NK;
		int NJK;
		int NIJK;
		int DIMS[3];
		//int input_strides[4];
		//int output_strides[4];
		int BATCH;
		bool success;
		bool pad1;
		bool pad2;
		bool pad3;
		fftinfo_gpu();
		fftinfo_gpu( const int ni_cube );
	};


	class cuda_gpu_bind_process_to_device
	{
	public:
		cuda_gpu_bind_process_to_device();
		~cuda_gpu_bind_process_to_device();

		bool bind_process_to_device( const int rank );

		int ndevices;
		int mydevice;
		vector<cudaDeviceProp> devices_ifo;
	};


	class fft3d_cuda_gpu
	{
	public:
		//out-of-place transform
		fft3d_cuda_gpu();
		~fft3d_cuda_gpu();

		void init( const int ni, vector<bio_real> const & img3d );
		//clear buffers prior issuing the next batched transform that reuses the plan
		void reset_plan_batch();
		//advanced functionality for batched 3d transforms with plan reutilization
		void init_plan_batch( const int ni, const int nbatch );
		//fill the input buffers for the batched FFT
		void fill_data_batch( vector<bio_real> const & img3d, const int batchid );
		//copy entire input buffer from host prior plan execution
		void move_plan_batch();

		void forwardFFT();
		void exec_plan_batch();
		void exec_magn_batch();
		/*
		void get_ijk_normal( vector<bio_uint> & ijknrm );
		void get_ijk_conjugate( vector<bio_uint> & ijkcnj );
		*/

	#ifdef EMPLOY_UNIFIED_MEMORY
		#ifdef EMPLOY_SINGLEPRECISION
			//out-of-place input buffer
			cufftReal* m_input;
			//cufftReal* m_amplitude;
			//forward transformation results buffer
			cufftComplex* fftTemp;
		#else
			cufftDoubleReal* m_input;
			//cufftDoubleReal* m_amplitude;
			cufftDoubleComplex* fftTemp;
		#endif
	#else
		#ifdef EMPLOY_SINGLEPRECISION
			cufftReal* m_input_h;
			cufftReal* m_input_d;
			cufftComplex* fftTemp_h;
			cufftComplex* fftTemp_d;
			#ifdef COMPUTE_MAGNITUDE
				cufftReal* fftmagn_h;
				cufftReal* fftmagn_d;
			#endif
		#else
			cufftDoubleReal* m_input_h;
			cufftDoubleReal* m_input_d;
			cufftDoubleComplex* fftTemp_h;
			cufftDoubleComplex* fftTemp_d;
			#ifdef COMPUTE_MAGNITUDE
				cufftDoubleReal* fftmagn_h;
				cufftDoubleReal* fftmagn_d;
			#endif
		#endif
	#endif

		cufftHandle m_f_handle;
		fftinfo_gpu ifo;
	};

	//references
	//http://gpu.cs.uct.ac.za/Slides/unified-and-3D-memory.pdf
	//best practices
	//http://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html


	#endif

#endif
