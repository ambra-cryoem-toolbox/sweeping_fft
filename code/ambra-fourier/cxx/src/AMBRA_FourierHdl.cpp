//##MK::GPLV3

#include "AMBRA_FourierHdl.h"

fourierHdl::fourierHdl()
{
	myrank = MASTER;
	nranks = 1;
	/*
	maximum_iontype_uc = 0;
	*/
}


fourierHdl::~fourierHdl()
{
	for( size_t i = 0; i < workers.size(); i++ ) {
		if ( workers.at(i) != NULL ) {
			delete workers.at(i);
			workers.at(i) = NULL;
		}
	}
}


bool fourierHdl::read_imgstack_dims_from_h5()
{
	double tic = MPI_Wtime();
	bool success = true;

	if ( get_myrank() == MASTER ) {
		cout << "Reading image stack dimensions from " << inputH5Hdl.h5resultsfn << " ..." << "\n";

		h5offs3d offs = h5offs3d();
		if ( inputH5Hdl.query_contiguous_tensor3d( TRANSCODER_RES_XYZ, offs ) == true ) {
		//if ( inputH5Hdl.query_contiguous_tensor3d( "/AmbraTranscoder/Results/Vf", info ) == true ) {

cout << "offs = " << offs << "\n";
			//evaluate constraints wrt to maximum dimensions of the image stack
			if ( (offs.nc1 - offs.nc0) >= static_cast<size_t>(INT32MX) )
				return false;
			if ( (offs.nr1 - offs.nr0) >= static_cast<size_t>(INT32MX) )
				return false;
			if ( (offs.nl1 - offs.nl0) >= static_cast<size_t>(INT32MX) )
				return false;

			//define physical length which the voxel edges cover
			bio_real Lphys[3] = { ConfigFourier::VoxelPhysicalEdgeLength, ConfigFourier::VoxelPhysicalEdgeLength, ConfigFourier::VoxelPhysicalEdgeLength };

			//define physical dimensions of material volume probed by the entire (global) image stack
			bio_real L[3] = { 	Lphys[0] * static_cast<bio_real>(offs.nc1 - offs.nc0),
								Lphys[1] * static_cast<bio_real>(offs.nr1 - offs.nr0),
								Lphys[2] * static_cast<bio_real>(offs.nl1 - offs.nl0)   };

			imgstack.global.bounded_volume = aabb_p3d(	0.0, L[0], 0.0, L[1], 0.0, L[2] );
			/*
			imgstack_global.guarded_volume = aabb_p3d(  0.0 - ConfigFourier::ImageStackGuardWidth * Lphys[0], L[0] + ConfigFourier::ImageStackGuardWidth * Lphys[0],
														0.0 - ConfigFourier::ImageStackGuardWidth * Lphys[1], L[1] + ConfigFourier::ImageStackGuardWidth * Lphys[1],
														0.0 - ConfigFourier::ImageStackGuardWidth * Lphys[2], L[2] + ConfigFourier::ImageStackGuardWidth * Lphys[2] );
			*/

			imgstack.global.bounded_nx = static_cast<int>( offs.nc1 - offs.nc0 );
			imgstack.global.bounded_ny = static_cast<int>( offs.nr1 - offs.nr0 );
			imgstack.global.bounded_nz = static_cast<int>( offs.nl1 - offs.nl0 );
			/*
			imgstack_global.guarded_nx = ConfigFourier::ImageStackGuardWidth + static_cast<bio_uint>( info.nc1 - info.nc0 ) + ConfigFourier::ImageStackGuardWidth;
			imgstack_global.guarded_ny = ConfigFourier::ImageStackGuardWidth + static_cast<bio_uint>( info.nr1 - info.nr0 ) + ConfigFourier::ImageStackGuardWidth;
			imgstack_global.guarded_nz = ConfigFourier::ImageStackGuardWidth + static_cast<bio_uint>( info.nl1 - info.nl0 ) + ConfigFourier::ImageStackGuardWidth;
			*/
			imgstack.global.inside_xmi = 0;
			imgstack.global.inside_xmx = 0 + static_cast<int>( offs.nc1 - offs.nc0 );
			imgstack.global.inside_ymi = 0;
			imgstack.global.inside_ymx = 0 + static_cast<int>( offs.nr1 - offs.nr0 );
			imgstack.global.inside_zmi = 0;
			imgstack.global.inside_zmx = 0 + static_cast<int>( offs.nl1 - offs.nl0 );
			/*
			imgstack_global.halo_xmi = imgstack_global.inside_xmi - static_cast<int>(ConfigFourier::ImageStackGuardWidth); //##MK::+1 ? currently not used
			imgstack_global.halo_xmx = imgstack_global.inside_xmx + static_cast<int>(ConfigFourier::ImageStackGuardWidth);
			imgstack_global.halo_ymi = imgstack_global.inside_ymi - static_cast<int>(ConfigFourier::ImageStackGuardWidth);
			imgstack_global.halo_ymx = imgstack_global.inside_ymx + static_cast<int>(ConfigFourier::ImageStackGuardWidth);
			imgstack_global.halo_zmi = imgstack_global.inside_zmi - static_cast<int>(ConfigFourier::ImageStackGuardWidth);
			imgstack_global.halo_zmx = imgstack_global.inside_zmx + static_cast<int>(ConfigFourier::ImageStackGuardWidth);
			*/

cout << imgstack.global << "\n";
		}
		else {
			cerr << ConfigFourier::Inputfile << " read failed !" << "\n";
			success = false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_HDF_READIMAGEDIMS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "HDF5::ReadImagestackDims", XXX, SEQUENTIAL, mm, tic, toc);
	return success;
}


bool fourierHdl::broadcast_imgstack_dims()
{
	double tic = MPI_Wtime();

	//initially significant only at MASTER
	if ( get_myrank() == MASTER )
		cout << "Broadcasting image stack dimensions ..." << "\n";

	MPI_aabb_px3d sndrcv = MPI_aabb_px3d( imgstack.global );
	int status = MPI_Bcast( &sndrcv, 1, MPI_aabb_px3d_Type, MASTER, MPI_COMM_WORLD );

	if ( status != MPI_SUCCESS )
		return false;

	if ( get_myrank() != MASTER ) {
		imgstack.global.bounded_volume = aabb_p3d(
				sndrcv.bvxmi, sndrcv.bvxmx,
				sndrcv.bvymi, sndrcv.bvymx,
				sndrcv.bvzmi, sndrcv.bvzmx );
		/*
		imgstack_global.guarded_volume = aabb_p3d(
				sndrcv.gvxmi, sndrcv.gvxmx,
				sndrcv.gvymi, sndrcv.gvymx,
				sndrcv.gvzmi, sndrcv.gvzmx );
		*/
		imgstack.global.bounded_nx = sndrcv.bounded_nx;
		imgstack.global.bounded_ny = sndrcv.bounded_ny;
		imgstack.global.bounded_nz = sndrcv.bounded_nz;
		/*
		imgstack_global.guarded_nx = sndrcv.guarded_nx;
		imgstack_global.guarded_ny = sndrcv.guarded_ny;
		imgstack_global.guarded_nz = sndrcv.guarded_nz;
		*/
		imgstack.global.inside_xmi = sndrcv.inside_xmi;
		imgstack.global.inside_xmx = sndrcv.inside_xmx;
		imgstack.global.inside_ymi = sndrcv.inside_ymi;
		imgstack.global.inside_ymx = sndrcv.inside_ymx;
		imgstack.global.inside_zmi = sndrcv.inside_zmi;
		imgstack.global.inside_zmx = sndrcv.inside_zmx;
		/*
		imgstack_global.halo_xmi = sndrcv.halo_xmi;
		imgstack_global.halo_xmx = sndrcv.halo_xmx;
		imgstack_global.halo_ymi = sndrcv.halo_ymi;
		imgstack_global.halo_ymx = sndrcv.halo_ymx;
		imgstack_global.halo_zmi = sndrcv.halo_zmi;
		imgstack_global.halo_zmx = sndrcv.halo_zmx;
		*/
	}

cout << "Rank " << get_myrank() << " imgstack.global " << imgstack.global << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_MPI_BCASTDIMS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "MPI::BroadcastImagestackDims", XXX, SEQUENTIAL, mm, tic, toc);

	return true;
}


bool fourierHdl::define_roi_ensemble()
{
	double tic = MPI_Wtime();

	int myrank = get_myrank();
	if ( myrank == MASTER ) {
		cout << "Defining the ROI ensemble ..." << "\n";
	}

	//##MK::to begin with work on the entire image stack, later define how to filter windows on the image stack
	aabb_px3d window = imgstack.global;
	//cout << "Rank " << get_myrank() << " window " << window << "\n";

	if (ConfigFourier::ROIEnsemble == SPECIFIC_POSITIONS ) {
		//cout << "---------->" << ConfigFourier::ROIUserPositions.size() << "\n";
		for ( auto it = ConfigFourier::ROIUserPositions.begin(); it != ConfigFourier::ROIUserPositions.end(); it++ ) {
			if ( it->x >= imgstack.global.inside_xmi && it->x < imgstack.global.inside_xmx &&
					it->y >= imgstack.global.inside_ymi && it->y < imgstack.global.inside_ymx &&
						it->z >= imgstack.global.inside_zmi && it->z < imgstack.global.inside_zmx ) {
				px3d here = px3d( it->x, it->y, it->z );
				if ( myrank == MASTER ) {
					cout << "ROIEnsemble specific positions added here " << here.x << ";" << here.y << ";" << here.z << "\n";
				}

				rois.push_back( roi_cuboid( here, ConfigFourier::ROIUserGridDimensions.x ) );
			}
			else {
				cerr << "Rank " << get_myrank() << " ROIUserPosition " << *it << " lays not inside the global imagestack !" << "\n";
				return false;
			}
		}
	}

	if (ConfigFourier::ROIEnsemble == BUILD_A_GRID ) {
		//cout << "-------->Grid dimensions" << "\n";
		//##MK::naive first shot like proposed in Luca's SweepingFFT slide
		if ( ConfigFourier::ROIUserGridDimensions.x >= imgstack.global.bounded_nx ||
				ConfigFourier::ROIUserGridDimensions.y >= imgstack.global.bounded_ny ||
					ConfigFourier::ROIUserGridDimensions.z >= imgstack.global.bounded_nz ) {
			cerr << "Rank " << get_myrank() << " ROIUserGridDimensions exceed respective edge lengths of the global imagestack !" << "\n";
			return false;
		}

		int nrois[3] = { 	window.bounded_nx / ConfigFourier::ROIUserGridDimensions.x,
							window.bounded_ny / ConfigFourier::ROIUserGridDimensions.y,
							window.bounded_nz / ConfigFourier::ROIUserGridDimensions.z  };

		for( int i = 0; i < 3; i++ ) {
			if ( myrank == MASTER ) {
				cout << "ROIEnsemble grid with number of ROIs in dimensions i = " << i << " is " << nrois[i] << "\n";
			}
			if ( nrois[i] >= static_cast<int>(INT16MX) )
				return false;
		}

		for( int iz = 0; iz < nrois[2]; iz++ ) {
			for ( int iy = 0; iy < nrois[1]; iy++ ) {
				for ( int ix = 0; ix < nrois[0]; ix++ ) {
					//propose where to place the lower-front-left origin of a cuboidal ROI
					px3d here = px3d( 	imgstack.global.inside_xmi + ix * ConfigFourier::ROIUserGridDimensions.x,
										imgstack.global.inside_ymi + iy * ConfigFourier::ROIUserGridDimensions.y,
										imgstack.global.inside_zmi + iz * ConfigFourier::ROIUserGridDimensions.z   );
					//check if within inside volume
					if ( 	here.x >= imgstack.global.inside_xmi && (here.x + ConfigFourier::ROIUserGridDimensions.x - 1) < imgstack.global.inside_xmx &&
							here.y >= imgstack.global.inside_ymi && (here.y + ConfigFourier::ROIUserGridDimensions.y - 1) < imgstack.global.inside_ymx &&
							here.z >= imgstack.global.inside_zmi && (here.z + ConfigFourier::ROIUserGridDimensions.z - 1) < imgstack.global.inside_zmx  ) {
						if ( myrank == MASTER ) {
							cout << "ROIEnsemble grid member added here " << here.x << ";" << here.y << ";" << here.z << "\n";
						}
						rois.push_back( roi_cuboid( here, ConfigFourier::ROIUserGridDimensions.x ) );
					}
					else {
						cerr << "Rank " << get_myrank() << " ROIEnsemble grid member discarded here " << here.x << ";" << here.y << ";" << here.z << "\n";
					}
				} //next x
			} //next y
		} //next z
	}

	cout << "Rank " << get_myrank() << " the total number of ROIs is " << rois.size() << "\n";

	/*
	//##MK::BEGIN DEBUGGING
	for( auto it = rois.begin(); it != rois.end(); it++ ) {
		cout << "roiID " << ConfigFourier::ROIFirstIndex + (it - rois.begin()) << " xyz " << it->origin.x << ";" << it->origin.y << ";" << it->origin.z << " nxnynz " << it->nx << ";" << it->ny << ";" << it->nz << "\n";
	}
	//##MK::END DEBUGGING
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_DEFINE_ROIS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "DefineROIEnsemble", XXX, SEQUENTIAL, mm, tic, toc);

	return true;
}


bool fourierHdl::distribute_roi_ensemble_on_processes()
{
	//ROIs are stacked along nl, FFTs all have the same size so we can assume that the workload per ROI is reasonable similar,
	//so we can give the MASTER rank the nROIs/nranks ROIs, the second the same, ... and the remaining ROIs to the last for instance
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER )
		cout << "Distributing ROI ensemble on processes ..." << "\n";

	if ( rois.size() >= static_cast<size_t>(UINT32MX) ) {
		cerr << "A too large number of ROIs should be distributed which is currently not supported !" << "\n";
		return false;
	}

	bio_uint NumberOfROIs = static_cast<bio_uint>(rois.size());
	bio_uint NumberOfWorkers = static_cast<bio_uint>(get_nranks());
	bio_uint NaiveROIsPerWorker = ( NumberOfROIs / NumberOfWorkers >= 1 ) ? NumberOfROIs / NumberOfWorkers : NumberOfROIs / NumberOfWorkers + 1;
	bio_uint roiID = static_cast<bio_uint>(ConfigFourier::ROIFirstIndex);
	bio_uint NumberOfROIsAssgnd = 0;
	bio_uint myWorkload = 0; //##MK::DEBUGGING

	//cout << "Initial roiID " << roiID << "\n";
	for( int rk = MASTER; rk < get_nranks(); rk++ ) {
		if ( (NumberOfROIsAssgnd + NaiveROIsPerWorker) < NumberOfROIs ) { //most likely case
			for( bio_uint i = 0; i < NaiveROIsPerWorker; i++ ) {
				bio_uint roiIDnext = roiID + i;
				rois2rk[static_cast<unsigned int>(roiIDnext)] = rk;
				if ( rk == get_myrank() ) {
					myroiIDs.push_back( roiIDnext );
				}
			}
			NumberOfROIsAssgnd += NaiveROIsPerWorker;
			roiID += NaiveROIsPerWorker;
			if ( get_myrank() == rk )
				myWorkload += NaiveROIsPerWorker;
		}
		else { //distribute remainder
			for( bio_uint i = 0; i < (NumberOfROIs - NumberOfROIsAssgnd); i++ ) {
				bio_uint roiIDnext = roiID + i;
				rois2rk[static_cast<unsigned int>(roiIDnext)] = rk;
				if ( rk == get_myrank() ) {
					myroiIDs.push_back( roiIDnext );
				}
			}
			roiID += (NumberOfROIs - NumberOfROIsAssgnd);
			if ( get_myrank() == rk )
				myWorkload += (NumberOfROIs - NumberOfROIsAssgnd);
		}
		//cout << "Rank " << rk << " roiID " << roiID << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_DISTRIBUTE_ROIS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "DistributeMatPointsOnMPIProcesses", XXX, SEQUENTIAL, mm, tic, toc);

	cout << "Rank " << get_myrank() << " myWorkload " << myWorkload << " myroiIDs.size() " << myroiIDs.size() << " of a total of " << rois2rk.size() << " ROIs" << "\n";

	return true;
}


bool fourierHdl::define_imgstack_local()
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER )
		cout << "Defining imagestack local ..." << "\n";

	/*
	//naive way, will cause that all processes load all data
	imgstack_local = imgstack_global; //##MK::DEBUGGING
	*/

	//#################need to change reading of local slab from HDF5 and accessing of imgstack_local keeping offset in mind
	//more advanced way, load only imagestack slabs needed locally
	//check which voxel the ROIs of the process touches, provided the load balancing algo groups nearby ROIs into a slab of images and assigns this slab to the same process,
	//the overlap_region of ROIs of get_myrank() cover a volume that is essentially a slab, all other regions of the global imagestack are irrelevant for this rank
	//so we can use this and load only a portion of NX*NY slabs from the global imagestack
	//has two benefits: shorter I/O per process, fewer main memory per process
	//##MK::further refinement is possible by taking into account the 3d shape of the overlap region
	//##MK::but this is much more complicated, and hence error prone, so therefore here the simpler compromise
	aabb_px3d overlap_region = aabb_px3d();
	//cout << "Detecting local region ..." << "\n";
	//cout << "Iter 0" << "\t\t" << overlap_region.inside_xmi << ";" << overlap_region.inside_xmx << "\n";
	//cout << "Iter 0" << "\t\t" << overlap_region.inside_ymi << ";" << overlap_region.inside_ymx << "\n";
	//cout << "Iter 0" << "\t\t" << overlap_region.inside_zmi << ";" << overlap_region.inside_zmx << "\n";
	for( auto it = myroiIDs.begin(); it != myroiIDs.end(); it++ ) {
		bio_uint roiID = static_cast<bio_uint>(*it);
		bio_uint roiIdx = roiID - static_cast<bio_uint>(ConfigFourier::ROIFirstIndex);
		roi_cuboid myroi = rois.at(roiIdx);
		//cout << myroi << "\n";
		if ( myroi.origin.x < overlap_region.inside_xmi ) {
			overlap_region.inside_xmi = myroi.origin.x;
		}
		if ( (myroi.origin.x + myroi.nx) >= overlap_region.inside_xmx ) {
			overlap_region.inside_xmx = myroi.origin.x + myroi.nx;
		}
		if ( myroi.origin.y < overlap_region.inside_ymi ) {
			overlap_region.inside_ymi = myroi.origin.y;
		}
		if ( (myroi.origin.y + myroi.ny) >= overlap_region.inside_ymx ) {
			overlap_region.inside_ymx = myroi.origin.y + myroi.ny;
		}
		if ( myroi.origin.z < overlap_region.inside_zmi ) {
			overlap_region.inside_zmi = myroi.origin.z;
		}
		if ( (myroi.origin.z + myroi.nz) >= overlap_region.inside_zmx ) {
			overlap_region.inside_zmx = myroi.origin.z + myroi.nz;
		}
		//cout << "Iter " << it - myroiIDs.begin() << "\t\t" << overlap_region.inside_xmi << ";" << overlap_region.inside_xmx << "\n";
		//cout << "Iter " << it - myroiIDs.begin() << "\t\t" << overlap_region.inside_ymi << ";" << overlap_region.inside_ymx << "\n";
		//cout << "Iter " << it - myroiIDs.begin() << "\t\t" << overlap_region.inside_zmi << ";" << overlap_region.inside_zmx << "\n";
	}

	//define offset between origin of the global and the local image stack
	imgstack.local2global = px3d( overlap_region.inside_xmi, overlap_region.inside_ymi, overlap_region.inside_zmi );
	cout << "Rank " << get_myrank() << " imgstack.local2global " << imgstack.local2global << "\n";

	//define physical length which the voxel edges cover
	bio_real Lphys[3] = { ConfigFourier::VoxelPhysicalEdgeLength, ConfigFourier::VoxelPhysicalEdgeLength, ConfigFourier::VoxelPhysicalEdgeLength };

	overlap_region.bounded_volume = aabb_p3d( 	Lphys[0] * static_cast<bio_real>(overlap_region.inside_xmi), Lphys[0] * static_cast<bio_real>(overlap_region.inside_xmx),
												Lphys[1] * static_cast<bio_real>(overlap_region.inside_ymi), Lphys[1] * static_cast<bio_real>(overlap_region.inside_ymx),
												Lphys[2] * static_cast<bio_real>(overlap_region.inside_zmi), Lphys[2] * static_cast<bio_real>(overlap_region.inside_zmx)  ); //##MK::currently not used
	/*
	overlap_region.guarded_volume = aabb3_p3d( 	Lphys[0] * static_cast<bio_real>(overlap_region.inside_xmi - ConfigFourier::ImageStackGuardWidth),
												Lphys[0] * static_cast<bio_real>(overlap_region.inside_xmx+1 + ConfigFourier::ImageStackGuardWidth),
												Lphys[1] * static_cast<bio_real>(overlap_region.inside_ymi - ConfigFourier::ImageStackGuardWidth),
												Lphys[1] * static_cast<bio_real>(overlap_region.inside_ymx+1 + ConfigFourier::ImageStackGuardWidth),
												Lphys[2] * static_cast<bio_real>(overlap_region.inside_zmi - ConfigFourier::ImageStackGuardWidth),
												Lphys[2] * static_cast<bio_real>(overlap_region.inside_zmx+1 + ConfigFourier::ImageStackGuardWidth)  ); //##MK::currently not used
	*/
	overlap_region.bounded_volume.scale();
	/*
	overlap_region.guarded_volume.scale();
	*/
	overlap_region.bounded_nx = overlap_region.inside_xmx - overlap_region.inside_xmi;
	overlap_region.bounded_ny = overlap_region.inside_ymx - overlap_region.inside_ymi;
	overlap_region.bounded_nz = overlap_region.inside_zmx - overlap_region.inside_zmi;
	/*
	overlap_region.guarded_nx = ConfigFourier::ImageStackGuardWidth + overlap_region.inside_xmx - overlap_region.inside_xmi + 1 + ConfigFourier::ImageStackGuardWidth; //##MK::currently not used
	overlap_region.guarded_ny = ConfigFourier::ImageStackGuardWidth + overlap_region.inside_ymx - overlap_region.inside_ymi + 1 + ConfigFourier::ImageStackGuardWidth;
	overlap_region.guarded_nz = ConfigFourier::ImageStackGuardWidth + overlap_region.inside_zmx - overlap_region.inside_zmi + 1 + ConfigFourier::ImageStackGuardWidth;
	overlap_region.halo_xmi = 0; //##MK::not used
	overlap_region.halo_xmx = 1; //##MK::not used
	overlap_region.halo_ymi = 0; //##MK::not used
	overlap_region.halo_ymx = 1; //##MK::not used
	overlap_region.halo_zmi = 0; //##MK::not used
	overlap_region.halo_zmx = 1; //##MK::not used
	*/

	imgstack.local = overlap_region;
	cout << "Rank " << get_myrank() << " imgstack.local " << imgstack.local << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_DEFINE_IMAGELOCAL] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "DefineImageStackLocal", XXX, SEQUENTIAL, mm, tic, toc);

	return true;
}


bool fourierHdl::read_imgstack_values_from_h5()
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER )
		cout << "Processing reading values of their assigned local imagestack ..." << "\n";

	MPI_Barrier( MPI_COMM_WORLD );

	//##MK::for the moment naive loading of the data portions sequentially, but process load only a portion of the data
	bool success_local = true;
	for( int rk = MASTER; rk < get_nranks(); rk++ ) {
		if ( get_myrank() == rk ) {

			double mytic = MPI_Wtime();

			/*
			//##MK::DEBUGGING WITH HARDCODED VALUES, later parse from computation
			h5iometa3d ifo_global = h5iometa3d( TRANSCODER_RES_XYZ, U8, 1000, 1500, 1000 );
			h5offs3d offs_local = h5offs3d( 0, 1000, 0, 1500, 0, 1000 );
			*/

			//###########need to modify execute_local_workpackage handling and offset between imgstack_local and imgstack_global
			//##MK::new values
			h5iometa3d ifo_global = h5iometa3d( TRANSCODER_RES_XYZ, U8,
													imgstack.global.bounded_nx,
														imgstack.global.bounded_ny,
															imgstack.global.bounded_nz );

			h5iometa3d ifo_local = h5iometa3d( TRANSCODER_RES_XYZ, U8,
																imgstack.local.bounded_nx,
																	imgstack.local.bounded_ny,
																		imgstack.local.bounded_nz );

			h5offs3d offs_local = h5offs3d( imgstack.local.inside_xmi, imgstack.local.inside_xmx,
											imgstack.local.inside_ymi, imgstack.local.inside_ymx,
											imgstack.local.inside_zmi, imgstack.local.inside_zmx  );

			vector<unsigned char> u8_local;

			//##MK::2 x 4 x 3 debugging example
			/*
			h5iometa3d ifo_local = h5iometa3d( "/AmbraTranscoder/Results/Vf", F32, 2, 4, 3 );
			h5offs3d offs_local = h5offs3d( 0, 2, 0, 4, 0, 3 );
			*/
			//##MK::4 x 30 x 20 proc_50/vf debugging example for 0000, 0001, 0002, 0004
			/*
			h5iometa3d ifo_local = h5iometa3d( "/AmbraTranscoder/Results/Vf", F32, 4, 30, 20 );
			h5offs3d offs_local = h5offs3d( 0, 4, 0, 30, 0, 20 );
			vector<float> f32_local;
			*/

			if ( inputH5Hdl.read_contiguous_tensor3d_u8le_hyperslab( ifo_global, offs_local, u8_local ) == WRAPPED_HDF5_SUCCESS ) {
				//cout << "Rank " << rk << " successfully loaded its portion of the image stack" << "\n";

				img_local = vector<bio_real>();
				img_local.reserve( ifo_local.npx3d() );
				//cout << "Rank " << rk << " ifo_local.npx3d() " << ifo_local.npx3d() << "\n";
				for( auto it = u8_local.begin(); it != u8_local.end(); it++ ) { //decode u8 color values to floating point values
					//##MK:::DEBUGGING currently Luca delivered binarized data
					if ( *it == 0x00 )
						img_local.push_back( static_cast<bio_real>(0.0) );
					else
						img_local.push_back( static_cast<bio_real>(1.0) );
				}

				//cout << "Rank " << rk << " u8_local.size() " << u8_local.size() << "\n";
				//cout << "Rank " << rk << " img_local.size() " << img_local.size() << "\n";
				/*
				//##MK::BEGIN DEBUGGING
				size_t z = 100;
				size_t y = 100;
				size_t yzoff = 100 * imgstack_local.bounded_nx + 100 * imgstack_local.bounded_nx * imgstack_local.bounded_ny;
				for( size_t x = 0; x < 100; x++ ) {
					cout << x << "\t" << y << "\t" << z << "\t\t" << img_local.at(x + yzoff) << "\n";
				}
				//##MK::END DEBUGGING
				*/
			}
			/*
			if ( inputH5Hdl.read_contiguous_tensor3d_f32le_hyperslab( ifo_local, offs_local, f32_local ) == WRAPPED_HDF5_SUCCESS ) {
				cout << "Rank " << rk << " successfully loaded its portion of the image stack" << "\n";

				img_local = vector<bio_real>();
				img_local.reserve( ifo_local.npx3d() );
				for( auto it = f32_local.begin(); it != f32_local.end(); it++ ) {
					img_local.push_back( *it );
					//cout << it - f32_local.begin() << "\t\t" << *it << "\n";
				}

				cout << "Rank " << rk << " f32_local.size() " << f32_local.size() << "\n";
				cout << "Rank " << rk << " img_local.size() " << img_local.size() << "\n";
			}*/
			else {
				cerr << "Rank " << rk << " failed to load its portion of the image stack" << "\n"; success_local = false;
			}

			double mytoc = MPI_Wtime();
			cout << "Rank " << get_myrank() << " loading of imgstack.local took " << (mytoc-mytic) << " seconds" << "\n";
		}
		MPI_Barrier( MPI_COMM_WORLD );
	}

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	h5tictoc.elapsedtime_data[FOURIER_DEFINE_IMAGELOCAL] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "HDF5::LoadImageStackLocal", XXX, SEQUENTIAL, mm, tic, toc);

	return success_local;
}


/*
bool fourierHdl::broadcast_input()
{
	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) {
			cerr << "Current implementation does not handle the case of larger than 2 billion ion dataset transfer!" << "\n"; localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the dataset!" << "\n";
		return false;
	}

	//dataset on the master is okay, but do we need to broadcast at all to others, i.e. are there slaves?
	if ( get_nranks() < 2 ) {
		cout << "Skipping broadcast_reconxyz because single process execution" << "\n";
		return true;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<size_t>(xyz.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_IonPositions* buf = NULL;
	try {
		buf = new MPI_IonPositions[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < xyz.size(); ++i ) {
			buf[i] = MPI_IonPositions( xyz[i] );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_IonPositions_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			xyz.reserve( static_cast<size_t>(nevt) );
			//dist2hull.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				xyz.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
				//dist2hull.push_back( buf1[i].d ); //##MK::fill with dummy values here!
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

	//cout << "Rank " << get_myrank() << " listened to MASTER xyz.size() " << xyz.size() << "\n";
	//cout << "Rank " << get_myrank() << " listened to MASTER dist2hull.size() " << dist2hull.size() << "\n";
	//cout << "Rank " << get_myrank() << " listened to MASTER triangle_hull.size() " << triangle_hull.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastReconXYZ", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}
*/


/*
bool fourierHdl::define_phase_candidates()
{
	double tic = MPI_Wtime();

	size_t max_memory_per_node = fourier_tictoc.get_memory_max_on_node();
	size_t max_memory_for_buf = 0.1 * max_memory_per_node;

	cout << "Rank " << get_myrank() << " max_memory_per_node " << max_memory_per_node << " B" << "\n";
	cout << "Rank " << get_myrank() << " max_memory_for_buf " << max_memory_for_buf << " B" << "\n";

	//firstly, we transform the human-readable icombis targ/nbor strings pairs into evapion3 objects
	vector<Evapion3Combi> tmp;
	for( auto it = itsk.icombis.begin(); it != itsk.icombis.end(); it++ ) {
		tmp.push_back( Evapion3Combi() );

		rng.add_evapion3( it->targets, tmp.back().targets );


//#ifdef EMPLOY_SINGLEPRECISION
//		apt_real parse_realspace = stof( it->realspacestr );
//#else
//		apt_real parse_realspace = stod( it->realspacestr );
//#endif
//		if ( parse_realspace < EPSILON || parse_realspace > ConfigFourier::ROIRadiusMax ) {
//			cerr << "Rank " << get_myrank() << " attempt to use an invalid realspace value in a an evapion3 ityp combi, combi will be ignored!" << "\n";
//		}
//		else {
//			tmp.back().realspace = parse_realspace;
//		}

//		//##BEGIN DEBUG
//		cout << "Rank " << get_myrank() << " targets __" << it->targets << "__ ---->" << "\n";
//		for( auto jt = tmp.back().targets.begin(); jt != tmp.back().targets.end(); jt++ ) {
//			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
//		}
//		cout << "Rank " << get_myrank() << " realspace __" << it->realspacestr << "__ ---->" << "\n";
//		cout << tmp.back().realspace << "\n";
//		//##END DEBUG
	}

	//secondly, we have to understand that these user-combinations may not at all be present in the ranging data, i.e.
	//are possibly not all evapion3 types that we identified in the analyzed dataset as ranging data,
	//sure, in most cases user will make correct input but in every case we need to make sure
	map<size_t,size_t>::iterator does_not_exist = rng.iontypes_dict.end();
	for( auto jt = tmp.begin(); jt != tmp.end(); jt++ ) {
		UC8Combi valid;
		for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
			size_t hashval = rng.hash_molecular_ion( tg->Z1, tg->Z2, tg->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = rng.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
//cout << " Adding an identified ion from the ranging process as target\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.targets.push_back( vt->second );
			}
		}
		//valid.realspace = jt->realspace;

		itsk.combinations.push_back( UC8Combi() );
		for( auto tgs = valid.targets.begin(); tgs != valid.targets.end(); tgs++ ) { //explicit deep copy
			itsk.combinations.back().targets.push_back( *tgs );
		}
		//itsk.combinations.back().realspace = valid.realspace;
	}

	//get rid of temporaries
	tmp = vector<Evapion3Combi>();

	//thirdly, and now having the IontypeCombinations we plan the actual PhaseCandidates or tasks
	//the key idea here is that we conduct eventually multiple Araullo Peters analyses per ROI
	//think like indexing in Electron Backscatter Diffraction (EBSD): we need to know crystal unit cells of possible candidates
	//with which to run the analysis a priori. for araullo it is the same, we probe the local spatial arrangement of
	//specific iontypes and for each such ityp candidate we probe intensity of a specific Fourier spectrum magnitude bin
	//as we know that for a pristine crystal of this phase and sub-lattice we would expect lattice planes in the unrotated
	//configuration, we can work in the unrotated configuration because we probe along different direction
	//one direction will be strong and aligned with the crystal [uvw] regardless the orientation if the grid of SDMs is
	//probed accurately


	unsigned int tskid = 0; //scientific analysis taskIDs start at 0
	size_t max_memory_expected = 0; //in bytes

	if ( itsk.combinations.size() < 1 ) {
		cerr << "Rank " << get_myrank() << " combinations were specified but not parsable after having evaluated InputfileReconstruction ranging data and settings!" << "\n";
		return false;
	}

	for( auto it = itsk.combinations.begin(); it != itsk.combinations.end(); it++ ) {
		if ( it->targets.size() <= TARGET_LIST_SIZE ) {

			itsk.iphcands.push_back( PhaseCandidate() );
			size_t i = 0;
			for(       ; i < it->targets.size(); i++ ) {
				itsk.iphcands.back().targets[i] = ( static_cast<int>(it->targets.at(i)) < static_cast<int>(UCHARMX) ) ?
						it->targets.at(i) : 0xFF;
			}
			for(       ; i < TARGET_LIST_SIZE; i++ ) {
				itsk.iphcands.back().targets[i] = 0xFF;
			}
			//itsk.iphcands.back().realspace = it->realspace;
			itsk.iphcands.back().candid = static_cast<unsigned int>(it - itsk.combinations.begin() );

			//memory consumption per iph
			//max_memory_expected += 0;
		}
		else {
			cerr << "Rank " << get_myrank() << " skipping a PhaseCandidate with too many target iontypes maximum is " << TARGET_LIST_SIZE << "\n";
		}
	}

	if ( itsk.iphcands.size() < 1 ) {
		cerr << "Rank " << get_myrank() << " no valid PhaseCandidate(s) were identifiable!" << "\n";
		return false;
	}

//	//final check would all these buffers fit in memory?
//	if ( ( max_memory_expected * static_cast<size_t>(1 + omp_get_max_threads()) ) >= max_memory_per_node ) { //+1 because there is the process-local copy to decouple critical regions of non-master threads and master thread
//		cerr << "With " << max_memory_expected << " * " << omp_get_max_threads() << " threads we would exceed the max_memory_per_node!" << "\n"; return false;
//	}
//	else {
//		cout << "Rank expected memory demands for buffering intermediate results to be " << max_memory_expected << " B" << "\n";
//	}

	//##BEGIN DEBUG
	for( int rk = MASTER; rk < get_nranks(); rk++ ) {
		if ( rk == get_myrank() ) {
		cout << "Rank " << rk << " prepared all tasks now verifying successful deep copies of values..." << "\n";
			for( auto ic = itsk.icombis.begin(); ic != itsk.icombis.end(); ic++ ) {
				cout << ic->targets << "\n"; //"\t\t" << ic->realspacestr << "\n";
			}
			for( auto jt = itsk.combinations.begin(); jt != itsk.combinations.end(); jt++ ) {
				cout << "Rank " << rk << " itsk.combinations " << jt - itsk.combinations.begin() << "\n";
				for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
					cout << "Rank " << rk << " tg = " << (int) *tg << "\n";
				}
				//cout << "Rank " << rk << " rsp = " << jt->realspace << "\n";
			}
			for( auto tsk = itsk.iphcands.begin(); tsk != itsk.iphcands.end(); tsk++ ) {
				cout << "Rank " << rk << " isk.iphcands" << "\n";

				for( unsigned int uc = 0; uc < TARGET_LIST_SIZE; uc++ ) {
					cout << "Target\t\t" << (int) tsk->targets[uc] << "\n";
				}
				//cout << "realspc = " << tsk->realspace << "\n";
				cout << "candid = " << tsk->candid << "\n";
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	//##END DEBUG

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "DefinePhaseCandidates", APT_XX, APT_IS_PAR, mm, tic, toc );

	return true;
}
*/

/*
void fourierHdl::spatial_decomposition()
{
	double tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	//get maximum iontype
	unsigned int maximum_ityp = 0;
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		if ( it->id < static_cast<unsigned char>(UCHARMX) ) {
			if ( it->id > maximum_ityp ) {
				maximum_ityp = it->id;
			}
		}
		else {
			cerr << "Detected an invalid iontype during itype_sensitive_spatial_decomposition!" << "\n"; return;
		}
	}

	maximum_iontype_uc = static_cast<unsigned char>(maximum_ityp); //MK::not the number of disjoint types but the maximum ityp value!
cout << "Rank " << get_myrank() << " maximum itype identified is " << (int) maximum_iontype_uc << "\n";

	sp.loadpartitioning_subkdtree_per_ityp( xyz, dist2hull, ityp_org, ityp_org, maximum_iontype_uc ); //random labels will not be used!

	//first point were ityp_org and ityp_rnd are no longer necessary and could in principle be deleted

	toc = MPI_Wtime();
	mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}
*/


/*
gpu_cpu_max_workload fourierHdl::plan_max_per_epoch( const int ngpu, const int nthr, const int nmp )
{
	gpu_cpu_max_workload res = gpu_cpu_max_workload();
	if ( ngpu == 1 ) {
		if ( nthr > 1 ) { //master delegates work to GPUs, only if additional threads CPU also participates in FT
			if ( nmp <= nthr-1 ) { //##MK::small workload only on the GPU
				res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
				res.chunk_max_gpu = nmp;
			}
			else { //large workload distribute on GPUs and CPUs
				res.chunk_max_cpu = nthr-1;
				res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload; //##MK::needs benchmarking, empirical!, per GPU!, assume multiple of the same accelerator e.g. 2x NVIDIA V100 as on TALOS
			}
		}
		else { //only a master which delegates work to GPUs but no own FT work scheduled to this master CPU !
			res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
			res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload;
		}
	}
	else if ( ngpu == 0 ) { //no GPUs, everything on threads/CPU
		res.chunk_max_gpu = 0;
		res.chunk_max_cpu = nthr; //every thread gets an own FT to work on to maximize independent workload as such minimize sync
		//##MK::might be problematic for large NN, as memory consumption is at least nt*(NN^3)*3*8B, <=256 should no problem even for nt=40 for TALOS
	}
	else {
		cerr << "Rank " << get_myrank() << " attempt to use more than a single GPU per process is  invalid!" << "\n";
	}

	return res;
}


gpu_cpu_now_workload fourierHdl::plan_now_per_epoch(
		const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl )
{
	gpu_cpu_now_workload res = gpu_cpu_now_workload();
	int rem = nmp - mp;

	if ( ngpu == 1 ) { //if we can use accelerators
		if ( nthr > 1 ) { //master delegates work to GPUs surplus additional threads CPUs also computing FTs
			if ( rem >= (mxl.chunk_max_gpu + mxl.chunk_max_cpu) ) { //still enough work for GPUs and CPUs
				res.chunk_now_gpu = mxl.chunk_max_gpu;
				res.chunk_now_cpu = mxl.chunk_max_cpu;
			}
			else if ( rem >= mxl.chunk_max_cpu ) { //more work than CPUs available, so GPUs assist
				res.chunk_now_gpu = rem - mxl.chunk_max_cpu; //GPUs assist
				res.chunk_now_cpu = mxl.chunk_max_cpu; //CPUs remainder
			}
			else {
				res.chunk_now_gpu = 0; //not even enough work to keep all CPUs busy, ##MK::can be improved...
				res.chunk_now_cpu = rem;
			}
		}
		else { //only master, so master delegates potentially to GPUs
			if ( rem >= mxl.chunk_max_gpu ) {
				res.chunk_now_gpu = mxl.chunk_max_gpu; //enough work to make a full gpu epoch
				res.chunk_now_cpu = 0;
			}
			else {
				res.chunk_now_gpu = rem; //not enough work for a complete gpu epoch but delegate then this less work still to the gpu
				res.chunk_now_cpu = 0;
			}
		}
	}
	else if ( ngpu == 0 ) { //we have no accelerators
		res.chunk_now_gpu = 0;
		if ( rem >= mxl.chunk_max_cpu ) { //more work than CPUs available all process full epoch
			res.chunk_now_cpu = mxl.chunk_max_cpu;
		}
		else { //not even enough work to keep all CPUs busy
			res.chunk_now_cpu = rem;
		}
	}
	else {
		cerr << "Rank " << get_myrank() << " attempt to use more than a single GPU per process is  invalid!" << "\n";
	}

	return res;
}
*/

/*
void fourierHdl::generate_debug_roi( mt19937 & mydice, vector<dft_real> * out )
{
	//generate a pseudo lattice workload for testing purposes of the algorithm
	uniform_real_distribution<dft_real> unifrnd(0.f, 1.f); //no MT warmup
	int imi = -5;
	int imx = +5;
	dft_real a = 0.404; //lattice constant
	dft_real da = 0.10 * a; //some simple squared scatter about ideal lattice position
	for( int z = imi; z <= imx; z++ ) {
		for( int y = imi; y <= imx; y++ ) {
			for( int x = imi; x <= imx; x++ ) {
				//create dummy sc lattice
				dft_real xx = static_cast<dft_real>(x) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real yy = static_cast<dft_real>(y) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real zz = static_cast<dft_real>(z) * a * (2*unifrnd(mydice)-1.0) * 0.5 * da;
				//if ( unifrnd(mydice) < 1.0 ) { //random thinning
				out->push_back( xx );
				out->push_back( yy );
				out->push_back( zz );
				//}
			}
		}
	}
	//##MK::BEGIN OF DEBUGGING
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " generated a dummy dataset with " << out->size() << " coordinates" << "\n";
	}
	//##MK::END OF DEBUGGING
}
*/

/*
void fourierHdl::query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out )
{
	//CALLED FROM WITHIN PARALLEL REGION
	apt_xyz SearchRadiusSQR = SQR(roicenter.R);
	p3dm1 probesite = p3dm1( roicenter.x, roicenter.y, roicenter.z, DEFAULTTYPE );

	vector<p3dm1> neighbors3dm1;
	for( size_t thr = MASTER; thr < sp.db.size(); thr++ ) { //scan all regions O(lgN) to find neighboring candidates
		threadmemory* thisregion = sp.db.at(thr);
		if ( thisregion != NULL ) {
			vector<p3dm1> const & theseions = thisregion->ionpp3_kdtree;
			kd_tree* curr_kauri = thisregion->threadtree;
			if ( curr_kauri != NULL ) {
				curr_kauri->range_rball_noclear_nosort_p3d( probesite, theseions, SearchRadiusSQR, neighbors3dm1 );
			}
		}
	}

	//##MK::implement possible iontype filtering here!

	for( auto it = neighbors3dm1.begin(); it != neighbors3dm1.end(); it++ ) { //un-pack and layout into de-interleaved array of xyz distance differences
		out->push_back( it->x - probesite.x );
		out->push_back( it->y - probesite.y );
		out->push_back( it->z - probesite.z );
	}
}


void fourierHdl::debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing )
{
	
//	cout << "DEBUG reporting how threads spent time in the epochs" << "\n";
//	cout << "ThreadID;NThreads;Epoch;WallClock(s);NChkuns;Ionsum" << "\n";
//	for( auto it = in.begin(); it != in.end(); it++ ) {
//		cout << *it << "\n";
//	}

	//generate table with epochs as rows and columns detailing what the threads were doing
	//demands that number of threads did not change
	if ( in.size() < 1 ) {
		cerr << "Epoch diary is empty!" << "\n"; return;
	}
	size_t expected_nt = static_cast<size_t>(in.at(0).nthreads);
	if ( (in.size() % expected_nt) != 0 ) {
		cerr << in.size() << "\n";
		cerr << expected_nt << "\n";
		cerr << (in.size() % expected_nt) << "\n";
		cerr << "Total number of logs is not an integer multiple of the expected number of threads so data are faulty or data are missing!" << "\n"; return;
	}
	for( auto it = in.begin(); it != in.end(); it++ ) {
		if ( static_cast<size_t>(it->nthreads) == expected_nt ) {
			continue;
		}
		else {
			cerr << "The total number of threads was not the same for all epochs" << "\n"; return;
		}
	}
	size_t nepochs = in.size() / expected_nt;
	if ( timing.size() != nepochs ) {
		cerr << "The number of epoch_prof timing steps is inconsistent with the thread profiling data!" << "\n"; return;
	}

	//##MK::suboptimal... one file per rank
	string fn = "PARAPROBE.Fourier.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".EpochThreadProfiling.csv";

	ofstream csvlog;
	csvlog.open(fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "Epoch;MPITotalWallclock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPWallClock";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPIonTotal";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPChunkTotal";
		csvlog << "\n";
		csvlog << "Epoch;MPITotalWallClock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		csvlog << "\n";

		for( size_t i = 0; i < in.size(); i += expected_nt ) {
			csvlog << in.at(i).epoch << ";" << timing.at(i/expected_nt).dt;
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).dt;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).ionsum;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).nchunks;
			csvlog << "\n";
		}

		csvlog.flush();
		csvlog.close();
		cout << "Rank " << get_myrank() << " successful generation of epoch profiling protocol file" << "\n";
	}
	else {
		cerr << "Unable to write process-local epoch profiling protocol file" << "\n";
	}
}


void fourierHdl::execute_local_workpackage1()
{
	cout << "Rank " << get_myrank() << " performing OpenMP/OpenACC direct Fourier transforms..." << "\n";

	double global_tic = MPI_Wtime();

	//initialize one GPUthe two NVIDIA GPUs on the TALOS node on which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int n_mygpus = 0; //either 0, or 1, rank either has a gpu and can use it or not
	int devId = -1;
	int devCount = 0;

#ifdef UTILIZE_GPUS
	if ( ConfigFourier::GPUsPerNode > 0 ) {
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		if ( devCount > 0  ) {
			n_gpus = devCount;
			n_mygpus = 1;
			cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		}
		else {
			n_mygpus = 0;
			cout << "Rank " << get_myrank() << " no accelerators existent " << devCount << "\n";
		}
	}
	else

	//##MK::not necessarily portable, here system-specific accelerator initialization exemplified
	//for the MPCDF system TALOS, specifically its computing nodes with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
	if ( n_gpus > 0 ) {
		//MK::assume user has instructed model with two processes per node each getting one GPUs
		devId = get_myrank() % ConfigFourier::GPUsPerNode;
		//cout << "Rank " << get_myrank() << " my GPU devID is " << devId << "\n";
		// creates a context on the GPUs
		//excludes initialization time from computations
		acc_set_device_num(devId, acc_device_nvidia);
		acc_init(acc_device_nvidia);
cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " will use multi-threaded CPU fallback n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";
	}
#endif
	//##cout <<  report use fallback multithreaded CPU only
	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing... n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";

	//the passing of individual ROIs for each process onto the CPU/GPUs works as follows:
	//the CPU OpenMP thread team identifies the ions within each ROI
	//ROIs are group into packets (epoch), a packet to work on for the CPU and a packet to work for the GPUs
	//the CPUs process their package in parallel, each thread one material point, i.e. one DFT
	//the GPUs process the GPU packet
	//the GPU get a heuristic fraction of ROIs: when a CPU processes 1 ROI the GPU processes ConfigFourier::GPUWorkload ROIs at the same time
	//##MK::to get more fine tuning in the future allow CPUs to have more than one package
	//we work through the pairs of CPU/GPU ROI packages until all ROIs of the MPI process are completed

	int local_nmp = myworkload.size();
	cout << "Rank " << get_myrank() << " myworkload.size() " << local_nmp << "\n";

	//we need to initialize buffers first to store the ion positions from all ROIs per epoch
	vector<vector<dft_real>*> buffer_gpu;
	vector<vector<dft_real>*> buffer_cpu;
	vector<int> buffer_local_mpid_gpu; //buffer to store which specific ROIs are currently processed, this is handled by the CPU
	vector<int> buffer_local_mpid_cpu; //MK::do not jumble up these local ROI IDs with the global material point/ROI ids!
	//the local IDs live on the ID interval [0, myworkload.size() )] BUT
	//the global IDs live on the ID interval [ 0, mp2rk.size() ) !

	//we go parallel with the threads but orchestrate/guide them through their workplan
	//we need a parallel region to have the allocation of the buffers in the master
	//##MK::TO DO performance benchmark, if the master thread gets pinned to core 0 it has on average faster access
	//to the PCI control busses, therefore we let the master thread delegate and execute work on the CPUs, the master therefore
	//does not contribute in solving own Fourier transforms ##MK::TO BE DISCUSSED but dont underestimate might get  tricky to get orchestrated
	//otherwise because we have here heterogeneous parallelism with device (GPU) and host (CPU) + asynchronous execution on the device
	//we call the gpu for now the device and the cpu the host

	gpu_cpu_max_workload wl = gpu_cpu_max_workload();
	//cout << "Rank " << get_myrank() << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

	#pragma omp parallel shared(n_gpus,n_mygpus,local_nmp,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,wl)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		#pragma omp master
		{
			//there is always a master thread
			//MK::idea is if GPUS+CPUs are used, in each iteration nt-1 FT are solved on the CPU, and an empirically identified number per GPU
			//this number, ##MK here just chosen as 4, will minimize CPU vs GPU async wait time if GPU runtime per epoch is about equal as per thread
			wl = plan_max_per_epoch( n_mygpus, nt, local_nmp );
			cout << "Rank " << get_myrank() << " MASTER " << mt << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

			//thread team shared global address space buffers get populated by master
			//the reason for using a vector of vector of pointer rather than a vector of coordinates
			//is to collate raw pointers to different addresses, specifically pointer to different thread-local mem virtual mem sections
			if ( wl.chunk_max_gpu > 0 ) {
				buffer_gpu = vector<vector<dft_real>*>( wl.chunk_max_gpu, NULL );
				buffer_local_mpid_gpu = vector<int>( wl.chunk_max_gpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
			if ( wl.chunk_max_cpu > 0 ) {
				buffer_cpu = vector<vector<dft_real>*>( wl.chunk_max_cpu, NULL );
				buffer_local_mpid_cpu = vector<int>( wl.chunk_max_cpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
		} //master initialized buffer memory
	} //end of parallel region

	//start processing epochs
	vector<epoch_log> epoch_diary;
	vector<epoch_prof> epoch_wallclock;
	int epoch = 0;
	int mympid = 0;
	int ni = static_cast<int>(ConfigFourier::HKLGrid.ni);

	while ( mympid < local_nmp )
	{
		double tic = MPI_Wtime();
		gpu_cpu_now_workload wn = gpu_cpu_now_workload();

		//we define an epoch as a single chunk of FT for ROIs processed, the epoch contains at most chunk_plan_gpu+chunk_plan_cpu material points
		#pragma omp parallel shared(n_gpus,n_mygpus,epoch_diary,epoch,mympid,local_nmp,wl,wn,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,ni)
		{
			int mt = omp_get_thread_num();
			int nt = omp_get_num_threads();
			//we keep a thread-local buffer of results to avoid having to synchronize after each ROI transform a write-back of
			//results to the globally shared container which would choke parallel execution performance
			vector<hklval> myresults_buffer;
			double mytic = omp_get_wtime();
			epoch_log mystatus = epoch_log( static_cast<unsigned int>(mt), static_cast<unsigned int>(nt) );

			//buffer keeps pointers to ion position data for ROI of material points
			//we store the ROIs for the FT on the GPUs at [0,chunk_plan_gpu+chunk_plan_cpu-2] and
			//the ROI for the FT on the CPUs at [chunk_plan_gpu+chunk_plan_cpu-1]

			//once initialized the thread team processes through its material points using the GPU as accelerators,
			//meanwhile other processes do the same but on different material points/ROIs,
			//thereby using triple parallelism (hybrid MPI/OpenMP + OpenACC)
			#pragma omp single
			{
				//only a single thread must compute this, hence implicitly pragma omp critically writing on wn
				wn = plan_now_per_epoch( n_mygpus, nt, local_nmp, mympid, wl );

				int id = mympid; //at which ID on ID interval [0, myworkload.size() ) where we started the current epoch
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					buffer_local_mpid_gpu.at(chk_gpu) = id + chk_gpu;
				}
				id = mympid + wn.chunk_now_gpu;
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					buffer_local_mpid_cpu.at(chk_cpu) = id + chk_cpu;
				}
				//now we know which global material point IDs are processed by the CPU/GPU team

			}
			//we need a barrier to assure that all threads see change of wn
			#pragma omp barrier

			#pragma omp master
			{
				cout << "Current epoch " << epoch << " on thread " << mt << " chunk_now_gpu " << wn.chunk_now_gpu << " chunk_now_cpu " << wn.chunk_now_cpu << "\n";
			}

			//firstly, we let the threads (all of the team), populate the buffers for GPU and CPUs
			//the concept of epochs is useful to avoid a very high memory consumption for storing ROI population with ions
			//e.g. lets assume 1000 mat points per process, each ROI having 5000 ions, this would require storing 5k * 1k points,
			//using epochs we need to buffer only 5k times as many as ROIs as processed per epoch which is typically << local_nmp
			for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
				int whom = chk_gpu % nt;
				if ( whom == mt ) { //threads share the allocation and querying load
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_gpu.at(chk_gpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						//##MK::false sharing?
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across the thread team!
						//#pragma omp critical
						//{
						//	cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
						//			<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						//}
						buffer_gpu.at(chk_gpu) = mybuf; //no pragma omp critical necessary, enforced collision free through whom == mt
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for GPU mybuf!" << "\n";
						}
					}
				} //mt is done
			} //done with filling in GPU work

			//next, the thread team populates the CPU buffer
			for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
				int whom = chk_cpu % nt;
				if ( whom == mt ) {
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_cpu.at(chk_cpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across thread team!
						//#pragma omp critical
						//{
						//	cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
						//			<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						//}
						buffer_cpu.at(chk_cpu) = mybuf;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for CPU mybuf!" << "\n";
						}
					}
				} //mt done
			} //done with filling CPU buffers

			//process the MPI-process local ROI material point IDs critical and master only, threads get to know values because ID fields are shared across team
			//barrier is necessary, because all buffer (cpus, gpus) have to be filled, before asynchronous processing on cpu/gpus starts...
			#pragma omp barrier

			////##MK::BEGIN DEBUGGING, can later be deleted
			//#pragma omp master
			//{
			//	cout << "Buffer for all chunks of current epoch " << epoch << " populated" << "\n";
			//	size_t ntotal = 0;
			//	size_t nactive = 0;
			//	for(size_t ii = 0; ii < buffer_gpu.size(); ii++) {
			//		if ( buffer_gpu.at(ii) != NULL ) { //allocated
			//			if ( buffer_gpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
			//				nactive++;
			//				ntotal += buffer_gpu.at(ii)->size();
			//			}
			//		}
			//	}
			//	for(size_t ii = 0; ii < buffer_cpu.size(); ii++) {
			//		if ( buffer_cpu.at(ii) != NULL ) { //allocated
			//			if ( buffer_cpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
			//				nactive++;
			//				ntotal += buffer_cpu.at(ii)->size();
			//			}
			//		}
			//	}
			//	cout << "Average number of ions per point " << ntotal / (3*nactive) << "\n"; //3 because three coordinates values per ion
			//} //no implicit barrier
			//#pragma omp barrier
			////##MK::END DEBUGGING

			//handle heterogeneously and possibly asynchronously executed direct Fourier transforms of current epoch on the GPUs through MASTER OMP_NESTED=true
			if ( n_mygpus > 0 ) { //if there are accelerators...
#ifdef UTILIZE_GPUS
				if ( mt == MASTER ) { //...the master thread always schedules work to GPUs only
					//we have only one GPU per process
					//double mmytic = omp_get_wtime();
					//cout << "Current epoch " << epoch << " on thread MASTER processing GPU part" << "\n";

					//define 1d Fourier grid positions on [-2.0,+2.0]
					int nij = SQR(ni);
					int nijk = CUBE(ni);
					//dft_real* IV = (dft_real*) malloc( ni*sizeof(dft_real) ); //restrict ... (dft_real*)
					dft_real* IV = new dft_real[ni];
					//dft_real* const IV = malloc( ni*sizeof(dft_real) );

					dft_real start = ConfigFourier::HKLGrid.imi; //-1.0;
					dft_real stop = ConfigFourier::HKLGrid.imx; //+1.0;
					dft_real num = static_cast<dft_real>(ni-1);
					dft_real step = (stop-start)/num;
					for( int i = 0; i < ni; i++ ) {
						IV[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
					}

					for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu;         ) {
						//prepare processing on the process's GPU devId

						//efficiently solving the max, index search is involved, for discussion see here
						//https://www.pgroup.com/userforum/viewtopic.php?t=5096
						dft_real* A0 = NULL; ///restrict
						dft_real* Fmagn0 = NULL;
						//https://www.pgroup.com/resources/docs/18.7/x86/pgi-user-guide/index.htm

						//prepare asynchronous processing on GPU0
						int n0 = 0;
						bool gpuvalid = false;
						if ( buffer_gpu.at(chk_gpu) != NULL ) {
							if ( buffer_gpu.at(chk_gpu)->size() >= 3 ) { //at least one ion within ROI!
								gpuvalid = true;
							}
						}

						//master delegates work on the GPUs, the master itself does not process work but does host interaction
						if ( gpuvalid == true ) {
							n0 = buffer_gpu.at(chk_gpu)->size();
							//A0 = (dft_real*) malloc( n0 * sizeof(dft_real) );
							A0 = new dft_real[n0];
							//dft_real* const A0 = malloc( n0 * sizeof(dft_real) );
							//Fmagn0 = (dft_real*) calloc( nijk*1, sizeof(dft_real) );
							Fmagn0 = new dft_real[nijk];
							for( int i = 0; i < nijk; i++ ) {
								Fmagn0[i] = 0.f;
							}
							//dft_real* const Fmagn0 = calloc( nijk, sizeof(dft_real) );
							vector<dft_real> & here0 = *(buffer_gpu[chk_gpu]);
							for( int i = 0; i < n0; i++ ) { //fill buffer for the device first with values on the host
								A0[i] = here0[i];
							}

							//acc_set_device_num(devId, acc_device_nvidia);
							//do all on GPU0 ASYNCHRONOUSLY, use the default async queue because we use only one GPU
							//create a Fmagn0 on the device
							#pragma acc data copy(Fmagn0[0:nijk]) async
							{
								int ijk, iion;
								//#pragma acc enter data copyin(Fmagn0[0:nijk]) async(1)
								#pragma acc parallel loop copyin(A0[0:n0],IV[0:ni]) present(Fmagn0[0:nijk]) async private(iion,n0)
								for( ijk = 0; ijk < nijk; ijk++) {
									int i = ijk % ni;
									int j = (ijk / nij) % ni;
									int k = ijk / nij;
									dft_real fr = 0.0;
									dft_real fi = 0.0;
									for( iion = 0; iion < n0; iion += 3 ) {
										dft_real a = A0[iion+0]*IV[i] + A0[iion+1]*IV[j] + A0[iion+2]*IV[k];
										fr += cos(a);
										fi += sin(a);
									}
									Fmagn0[ijk] = sqrt( fr*fr + fi*fi );
								} //Fmagn0 continues to live happily on GPU0
								//#pragma acc update host(Fmagn0[0:nijk]) async(1)
								//#pragma acc exit data delete(Fmagn0[0:nijk]) async(1)
							}
							//destroyed Fmagn0 on the device
							//pragma acc data copy allocates memory on GPU and copies data from host to GPU when entering
							//the region and copies the data to the host when exiting region
						}

						//necessary, because once the host has dispatched work to the GPU
						//it could do own work ##MK:: host needs to wait for the GPU to complete all asynchronously executed tasks on the default async queue
						#pragma acc wait
						//now that the host/master threads knows that the GPUs has populated Fmagn0 with the results we can search for the maximum value on Fmagn0

						hklval max0 = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						if ( gpuvalid == true ) {
							for( int ijk = 0; ijk < nijk; ijk++ ) {
								if ( Fmagn0[ijk] < max0.val )
									continue;
								else
									max0 = hklval( WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND, ijk, Fmagn0[ijk] );
							}
							max0.mpid = buffer_local_mpid_gpu.at(chk_gpu); //process-local material point ID, not global Material point ID!
							//free(A0);
							delete [] A0; A0 = NULL;
							//free(Fmagn0);
							delete [] Fmagn0; Fmagn0 = NULL;
						}
						//this is a process-local material point ID !, i.e. there are possibly as many 0-th local material points as there are MPI processes
						//use myworkload.at(max0.mpid).mpid to resolve the global material point ID
						//if max0.mpid is flagged with NORESULTS_FOR_THIS_MATERIALPOINT we can filter this out!
						myresults_buffer.push_back( max0 );

						////find those position in reciprocal space with significant signal, ##MK::currently we use a unsigned char, i.e. 8-bit wide intensity value binning
						//if ( gpuvalid == true ) {
						//	//get maximum intensity in reciprocal space
						//	dft_real Fmax = F32MI;
						//	for( int ijk = 0; ijk < nijk; ijk++ ) {
						//		if ( Fmagn0[ijk] < Fmax ) {
						//			continue;
						//		}
						//		else {
						//			Fmax = Fmagn0[ijk];
						//		}
						//	}
						//	//based on Fmax define a cut-off threshold below which intensities are counted as noise
						//	dft_real Fmin = (1.0/256.0)*Fmax;
						//	//filter those positions in reciprocal space that are above threshold
						//	vector<hklspace_res> filtered;
						//	for( int ijk = 0; ijk < nijk; ijk++ ) { //strong peaks are seldom
						//		if ( Fmagn0[ijk] < Fmin ) {
						//			continue;
						//		}
						//		else {
						//			int i = ijk % ni;
						//			int j = (ijk / nij) % ni;
						//			int k = ijk / nij;
						//			filtered.push_back( hklspace_res( IV[i], IV[j], IV[k], Fmagn0[ijk] ) );
						//		}
						//	}
						//	max0.mpid = buffer_local_mpid_gpu.at(chk_gpu); //process-local material point ID, not global Material point ID!
						//	//free(A0);
						//	delete [] A0; A0 = NULL;
						//	//free(Fmagn0);
						//	delete [] Fmagn0; Fmagn0 = NULL;
						//}
						
						////##MK::BEGIN DEBUG
						//#pragma omp critical
						//{
						//	cout << "Current epoch " << epoch << " on thread MASTER after pragma acc wait" << "\n";
						//	cout << "max0 " << max0 << "\n";
						//	if ( useboth == true )
						//		cout << "max1 " << max1 << "\n";
						//}
						//double gtoc = omp_get_wtime();
						//#pragma omp critical
						//{
						//	if ( useboth == true )
						//		cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << chk_gpu+1 << " two FT took " << (gtoc-gtic) << " seconds" << "\n";
						//	else
						//		cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << "xxxx" << " one FT took " << (gtoc-gtic) << " seconds" << "\n";
						//}
						////##MK::END DEBUG

						chk_gpu++;

						//bookkeeping
						mystatus.nchunks++;
						mystatus.ionsum = mystatus.ionsum + n0/3;

					} //next chk_gpu

					//free(IV);
					delete [] IV; IV = NULL;

					//double mmytoc = omp_get_wtime();
					//#pragma omp critical
					//{
					//	cout << "Current epoch " << epoch << " on thread MASTER async with " << wn.chunk_now_gpu << " on GPUs took " << (mmytoc-mmytic) << " seconds" << "\n";
					//}
				} //master done with its work
				else { //remainder non-master threads work asychronously on own direct Fourier transform
					//double mmytic = omp_get_wtime();
					//##MK::for now we can with this model execute only one chk on the CPUs per epoch
					//because as master execution path is different than of non-master threads we cannot invoke a #pragma omp barrier here
					//MK::we could dispatch as chks for each accelerator and proceed with OMP threading but typically accelerators are
					//faster and hence could process multiple chks while the thread team hasnt even complete one
					if ( (mt-1) < wn.chunk_now_cpu ) {
						//MK::the construct mt-1 works because the MASTER thread never enters this section so mt is always >= 1
						//MK::mt-1 possible because this section is only executed by non-master thread if such exist

						hklval cpumt1res = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						size_t thisone = mt-1; //all threads contribute, if enough points left
						if ( buffer_cpu.at(thisone) != NULL ) {
							if ( buffer_cpu.at(thisone)->size() > 0 ) {

								dft_cpu solver = dft_cpu( ni );

								cpumt1res = solver.execute( *(buffer_cpu.at(thisone)) );

								cpumt1res.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

								mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
								mystatus.nchunks++;
							}
						}
						myresults_buffer.push_back( cpumt1res );

						//double mmytoc = omp_get_wtime();
						//#pragma omp critical
						//{
						//	cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
						//}
					} //non-master threads done with processing the only cpu part of the current epoch
				} //non-master thread part
#endif
			} //handled case of asynchronous GPU + CPU parallel execution
			else {
				//handle multithreaded fallback in case there are no accelerators
				//#pragma omp critical
				//{
				//	cout << "Thread " << mt << " participates in multi-threaded fallback current epoch chunk_now_cpu " << wn.chunk_now_cpu << "\n";
				//}
				double mmytic = omp_get_wtime();

				if ( mt < wn.chunk_now_cpu ) { //possibly all threads contribute, if enough points left
					hklval cpumtres = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
					size_t thisone = mt; //all threads contribute, if enough points left
					if ( buffer_cpu.at(thisone) != NULL ) { //if the ROI was queried
						if ( buffer_cpu.at(thisone)->size() > 0 ) { //and it contains coordinate values

							dft_cpu solver = dft_cpu( ni );

							cpumtres = solver.execute( *(buffer_cpu.at(thisone)) );

							cpumtres.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

							mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
							mystatus.nchunks++;
						}
					}
					myresults_buffer.push_back( cpumtres );
				}

				double mmytoc = omp_get_wtime();
				#pragma omp critical
				{
					cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
				}
			} //done for the CPU only multi-threaded fallback

			//threads dump their results now resulting in only as many criticals as there are threads
			//rather than as many individual criticals as FT transforms per epoch
			#pragma omp critical
			{
				for( auto kyvl = myresults_buffer.begin(); kyvl != myresults_buffer.end(); kyvl++ ) {
					if ( kyvl->mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
						myhklval.at(kyvl->mpid) = *kyvl;
					}
				}
				//release thread-local buffer of collected results
				myresults_buffer = vector<hklval>();
			}

			mystatus.epoch = epoch;
			double mytoc = omp_get_wtime();
			mystatus.dt = mytoc-mytic;
			#pragma omp critical
			{
				epoch_diary.push_back( mystatus );
			}

			//a barrier is necessary, because we need to avoid that masters starts release buffers already while threads may still accumulate results
			#pragma omp barrier

			//clear the buffer and reinitialize for next use
			#pragma omp master
			{
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					if ( buffer_gpu.at(chk_gpu) != NULL ) {
						delete buffer_gpu.at(chk_gpu);
						buffer_gpu.at(chk_gpu) = NULL; //necessary to prepare for reusage
					}
				}
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					if ( buffer_cpu.at(chk_cpu) != NULL ) {
						delete buffer_cpu.at(chk_cpu);
						buffer_cpu.at(chk_cpu) = NULL; //necessary to prepare for reusage
					}
				}
			} //no implicit barrier as were about to exit parallel region with implicit barrier anyway

		} //end of parallel region within the while loop for the current epoch

		mympid = mympid + wn.chunk_now_gpu + wn.chunk_now_cpu; //advance to begin a new epoch

		double toc = MPI_Wtime();
		epoch_wallclock.push_back( epoch_prof( epoch, toc-tic ) );

		cout << "Current epoch " << epoch << " with a total of " << wn.chunk_now_gpu + wn.chunk_now_cpu << " completed took " << (toc-tic) << " seconds" << "\n";
		epoch++;
	} //end of the while loop

	double global_toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "QueryROIsAndPerformDirectFourierTransforms", APT_XX, APT_IS_PAR, mm, global_tic, global_toc);

	cout << "Rank " << get_myrank() << " the direct FT took " << (global_toc-global_tic) << " seconds" << "\n";

	sort( epoch_diary.begin(), epoch_diary.end(), SortEpochLogAsc );

	debug_epoch_profiling( epoch_diary, epoch_wallclock );
}


void fourierHdl::execute_local_workpackage2()
{
	double global_tic = MPI_Wtime();

	if ( mp2me.size() == 0) {
		cout << "WARNING:: Rank " << get_myrank() << " has no work to do!" << "\n"; return;
	}

	//initialize one GPU, ##MK::for TALOS the two NVIDIA V100 GPUs per node which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int n_mygpus = 0; //either 0, or 1, because a rank either has an assisting gpu and can use it or uses a multi-threaded fallback
	int devId = -1;
	int devCount = 0;

#ifdef UTILIZE_GPUS
	if ( ConfigFourier::GPUsPerNode > 0 ) {
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		if ( devCount > 0 ) {
			n_gpus = devCount;
			n_mygpus = 1;
			cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		}
		else {
			n_mygpus = 0;
			cout << "Rank " << get_myrank() << " no accelerators existent " << devCount << "\n";
		}
	}
	//##MK::not necessarily portable, here system-specific accelerator initialization exemplified
	//for the MPCDF system TALOS, specifically its computing nodes with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
	if ( n_gpus > 0 ) {
		//MK::assume user has instructed model with two processes per node each getting one GPUs
		devId = get_myrank() % ConfigFourier::GPUsPerNode;
		//cout << "Rank " << get_myrank() << " my GPU devID is " << devId << "\n";
		// creates a context on the GPUs
		//excludes initialization time from computations
		acc_set_device_num(devId, acc_device_nvidia);
		acc_init(acc_device_nvidia);
cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " will use multi-threaded CPU fallback n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";
	}
#endif
	//##cout <<  report use fallback multithreaded CPU only
	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing... n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";

	//identify which ion types we need to probe at all as they are targets in a PhaseCandidate
	//all other ions are immediately ignored thereby improving on ityp_specific ion querying
	//important is that material point positions in contrast to ions in paraprobe-spatstat mostly, except
	//for some seldom by chance events lay exactly at the positions of ions, therefore we only should query
	//external position in the point cloud, i.e. not atoms but real space positions wrt to the iontype specific kd sub-trees

	#pragma omp parallel shared(n_gpus,n_mygpus,devId)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		//mkl_set_num_threads_local(1);

		#pragma omp master
		{
			workers = vector<acquisor*>( nt, NULL );
		}
		//necessary as otherwise write back conflict on workers from threads
		#pragma omp barrier

		//setup thread-local worker and use thread-local memory
		acquisor* thrhdl = NULL;
		try {
			thrhdl = new acquisor;
		}
		catch (bad_alloc &mecroak) {
			#pragma omp critical
			{
				cerr << "Rank " << get_myrank() << " thread " << mt << " unable to allocate an acquisor instance!" << "\n";
			}
		}

		//now workers backreference holders can safely be modified by current thread
		//MK::pointer will point to thread-local memory
		#pragma omp critical
		{
			workers.at(mt) = thrhdl;
		}

		//thrhdl->configure();

		apt_real R = ConfigFourier::ROIRadiusMax;
		apt_real RSQR = SQR(R);
		//##MK::how is it assured that workers is always !NULL ?

		//make thread-local memory copies of the phases to reduce cache traffic
		vector<PhaseCandidate> mytsk;
		for( auto tsk = itsk.iphcands.begin(); tsk != itsk.iphcands.end(); tsk++ ) {
			mytsk.push_back( PhaseCandidate() );
			for( unsigned int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
				mytsk.back().targets[tg] = tsk->targets[tg];
			}
			//mytsk.back().realspace = tsk->realspace;
			mytsk.back().candid = tsk->candid;
		}

		//identify which iontype kd-subtrees we should at all attempt to probe as targets across all regions
		//we need to probe only the _org trees
		set<int> relevant_trees;
		for( int thr = MASTER; thr < nt; thr++ ) {
			threadmemory* thisone = sp.db.at(thr);
			int nitypes = thisone->ionpp1_kdtree_org.size(); //int suffices, only at most 256 itypes!
			for( int ii = 0; ii < nitypes; ii++ ) {
				if ( thisone->ionpp1_kdtree_org.at(ii) != NULL ) { //for a sub-tree to be relevant it first of all needs to contain ions
					//but also it should handle an iontype which is part of an analysis task
					for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
						for( int jj = 0; jj < TARGET_LIST_SIZE; jj++ ) {
							unsigned char current_ityp = mtsk->targets[jj];
							if ( current_ityp != 0xFF ) {
								if ( static_cast<int>(current_ityp) != ii ) {
									continue;
								}
								relevant_trees.insert( ii );
								break; //we break because we scan for ityp ii only!
							}
						} //next target to test if of ityp ii
					} //next phase candidate to test delivering potentially different itypes to query for
				}
			} //next type to test
		} //next thread region to test

		//actual ion scanning
		size_t nivals = 1+static_cast<size_t>((int) maximum_iontype_uc); //MK::do forget that we have also the zero-th type so we need 1+nival accessors for subkdtrees

//##MK::BEGIN OF DEBUG
#pragma omp critical
{
	cout << "Rank " << get_myrank() << " thread " << mt << " nivals " << nivals << "\n";
}
//##MK::END OF DEBUG

		//barrier here is necessary because all threads have to be initialized and their settings configured
		#pragma omp barrier

		//now execute cooperative processing of material points by thread team
		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < mp2me.size(); i++ ) { //threads distribute materials point dynamically
			int mpid = mp2me.at(i); //i is a process-local material point ID, mpid is a world-global material point ID !
			mp3d thisone = mp.matpoints.at(mpid);
			apt_real rlargestsqr = SQR(thisone.R);

			p3d here = p3d( thisone.x, thisone.y, thisone.z ); //a material point, i.e. location in the volume, NOT an ion!
			//p3dm1 probesite = p3dm1( thisone.x, thisone.y, thisone.z, 0 );

//threaded-searches for ions within spherical ROI and iontypes referred to in relevant_trees
			vector<p3d> ions_xyz;
			vector<iontype_ival> ions_ityp_block = vector<iontype_ival>( nivals, iontype_ival(0, 0) ); //build an array of neighbors within largest ROI
			size_t prev = 0;
			size_t next = 0;

			for( auto tt = relevant_trees.begin(); tt != relevant_trees.end(); tt++ ) { //its a set so implicitly sorted in ascending order!
//cout << "\t\t\t\t\ttt " << *tt << "\n";
				//scan all the relevant sub-trees
				prev = ions_xyz.size();

				//query all ions of ityp tt within a ROI at position here using ityp-subtree O(lgN_ityp)
				for( int thr = MASTER; thr < nt; thr++ ) {
					threadmemory* currentregion = sp.db.at(thr);
					if ( currentregion != NULL ) {
						kd_tree* thistree = currentregion->ityp_kdtree_org.at(*tt);
						//always external queries because matpoint against ions, not ions against other ions like in paraprobe-spatstat
						if ( thistree != NULL ) {
							vector<p3d> const & ppp = *(currentregion->ionpp1_kdtree_org.at(*tt));
							thistree->range_rball_append_external( here, ppp, rlargestsqr, ions_xyz );
						}
					}
				} //next threadregion

				next = ions_xyz.size();
				ions_ityp_block.at(*tt) = iontype_ival( prev, next );
			} //query next relevant itype, leave all others ityp_block values at 0, 0 indicating nothing to read from ions_xyz for these ityps

//thread-local processing of neighbors
			//walk through all PhaseCandidates with the querying results
			for( auto mtsk = mytsk.begin(); mtsk != mytsk.end(); mtsk++ ) {
				//do we have within the current ROI at all ions of at least one target ityp for the task?

				vector<p3d> myneighbors;
				for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
					if ( mtsk->targets[tg] != 0xFF ) {
						int curr_ityp = static_cast<int>(mtsk->targets[tg]);
						for( size_t jj = ions_ityp_block.at(curr_ityp).incl_left; jj < ions_ityp_block.at(curr_ityp).excl_right; jj++ ) {
							myneighbors.push_back( ions_xyz.at(jj) );
						}
					}
				}
				if ( myneighbors.size() > 0 ) { //allocate a thread-local buffer for the temporaries and work on current matpoint
					fourier_res* hdl = NULL;
					try {
						hdl = new fourier_res;
					}
					catch( bad_alloc &mecroak ) {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " failed to allocate a fourier_res solver!" << "\n";
						}
					}
					if ( hdl != NULL ) {
						//#pragma omp critical
						//{
						//	cout << "Rank " << get_myrank() << " thread " << mt << " processing hdl " << "\n";
						//}
						hdl->owner = thrhdl; //MK::pointer to thrhdl not pointer to this class instance!!
						hdl->mp = here;

						//origin shift as proposed by F. Vurpillot
						for( size_t ii = 0; ii < myneighbors.size(); ii++ ) {
							myneighbors[ii].x = myneighbors[ii].x - here.x;
							myneighbors[ii].y = myneighbors[ii].y - here.y;
							myneighbors[ii].z = myneighbors[ii].z - here.z;
						}

						hdl->tictoc[TICTOC_IONSINROI] = static_cast<double>(myneighbors.size());
						hdl->tictoc[TICTOC_THREADID] = static_cast<double>(mt);

#ifdef UTILIZE_GPUS
						if ( (n_mygpus == 1 && mt != MASTER) || (n_mygpus == 0) ) {
							//in-case there is a GPU but I am slave thread, I do regular processing
							//or in case there are no thread and therefore use a multi-thread fallback

							hdl->direct_ft_cpu( myneighbors );

							hdl->postprocess_cpu();

							hdl->dbscanpks_cpu();
						}
						else { //will only be entered if n_mygpus != 0 ##MK::and provided above setting of n_mygpus to 1 when mt == MASTER

							hdl->direct_ft_gpu( myneighbors );

							hdl->postprocess_cpu(); //##MK::parallelize GPU and switch to findpks_gpu

							hdl->dbscanpks_cpu();
						}
#else

						hdl->direct_ft_cpu( myneighbors );

						hdl->postprocess_cpu();

						hdl->dbscanpks_cpu();
#endif

						thrhdl->res.push_back( fourier_res_node() );
						thrhdl->res.back().dat = hdl;
						thrhdl->res.back().mpid = mpid;
						thrhdl->res.back().phcandid = mtsk->candid;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " mpid " << mpid << " was unable to allocate araullo_res object, hdl == NULL!" << "\n";
						}
						//no results are stored for unallocatable guys
					}
				}
				//else{} no results are stored for unallocateable guys
			} //next task at the point
		} //next sampling point

		//#pragma omp barrier
		//#pragma omp critical
		//{
		//	cout << "Rank " << myrk << " Thread " << mt << " finished atom probe crystallography took " << (mytoc-mytic) << " seconds" << endl;
		//}
	} //end of parallel region

	double global_toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "QueryROIsAndPerformDirectFourierTransforms", APT_XX, APT_IS_PAR, mm, global_tic, global_toc);

	//##MK::report timings

cout << "Rank " << get_myrank() << " the direct FT took " << (global_toc-global_tic) << " seconds" << "\n";
}
*/


bool fourierHdl::init_target_file()
{
	//##MK::generate group hierarchy for the HDF5-based results file of the ambra-fourier tool
	double tic = MPI_Wtime();

	//MK::only one rank must instantiate the groups, here we let the MASTER do this
	if ( get_myrank() == MASTER ) {
		cout << "Rank " << MASTER << " initializes the target file " << outputH5Hdl.h5resultsfn << " ..." << "\n";

		outputH5Hdl.create_fourier_h5( outputH5Hdl.h5resultsfn, get_nranks() );

/*
		if ( outputH5Hdl.create_fourier2_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
			//instantiate containers for PhaseCandidate specific results
			string fwslash = "/";
			string grpnm = "";
			int status = WRAPPED_HDF5_SUCCESS;
			if ( ConfigFourier::IOFullGridPks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_FOURIER_RES_HKLGRID + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			if ( ConfigFourier::IOSignificantPks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_FOURIER_RES_HKLREFL + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			if ( ConfigFourier::IODBScanOnSgnPks == true ) {
				for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
					grpnm = PARAPROBE_FOURIER_RES_HKLDBSCAN + fwslash + "PH" + to_string(mtsk->candid);
					status = debugh5Hdl.create_group( grpnm );
					if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
					cout << grpnm << " create " << status << "\n";
				}
			}
			//for the overview list for which material point and we have results for phase ph
			grpnm = PARAPROBE_FOURIER_RES_MP_IOID;
			status = debugh5Hdl.create_group( grpnm );
			if ( status != WRAPPED_HDF5_SUCCESS ) { cerr << grpnm << " create failed! " << status << "\n"; return false; }
			cout << grpnm << " create " << status << "\n";
*/
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_HDF_INITTARGET] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "HDF5::InitOutputFile", XXX, SEQUENTIAL, mm, tic, toc);
	return true;
}


bool fourierHdl::write_cpu_environment_and_settings()
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {
		cout << "Rank " << MASTER << " reports tool environment and configuration to target file ..." << "\n";

		vector<pparm> hardware = fourier_tictoc.report_machine();
		string prfx = FOURIER_META_HRDWR;
		for( auto it = hardware.begin(); it != hardware.end(); it++ ) {
			//cout << it->keyword << "__" << it->value << "__" << it->unit << "__" << it->info << "\n";
			if ( outputH5Hdl.write_settings_entry( prfx, *it ) == WRAPPED_HDF5_SUCCESS ) {
				continue;
			}
			else {
				cerr << "Writing hardware setting " << it->keyword << " failed!" << "\n";
				return false;
			}
		}
		cout << "Rank " << MASTER << " hardware settings written to H5" << "\n";

		vector<pparm> software;
		ConfigFourier::reportSettings( software );
		prfx = FOURIER_META_SFTWR;
		for( auto jt = software.begin(); jt != software.end(); jt++ ) {
			//cout << jt->keyword << "__" << jt->value << "__" << jt->unit << "__" << jt->info << "\n";
			if ( outputH5Hdl.write_settings_entry( prfx, *jt ) == WRAPPED_HDF5_SUCCESS ) {
				continue;
			}
			else {
				cerr << "Writing software tool setting " << jt->keyword << " failed!" << "\n";
				return false;
			}
		}
		cout << "Rank " << MASTER << " software settings written to H5" << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_HDF_WRITECONFIG] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "HDF5::WriteToolEnvAndConfig", XXX, SEQUENTIAL, mm, tic, toc);

	return true;
}


bool fourierHdl::write_metadata()
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {
		cout << "Rank " << MASTER << " writes metadata to target file ..." << "\n";

		string fwslash = "/";
		int status = 0;
		h5iometa2d ifo = h5iometa2d();
		h5offs2d offs = h5offs2d();
		string dsnm = "";

		//regions-of-interest IDs and origin in the dataset
		//ROI indices (aka "names"/"identifiers")
		dsnm = FOURIER_META_ROIS_IDS;
		vector<unsigned int> u32 = vector<unsigned int>( rois.size() * FOURIER_META_ROIS_IDS_NC, 0 );
		for( auto it = rois.begin(); it != rois.end(); it++ ) {
			size_t roiIdx = it - rois.begin();
			u32[roiIdx] = ConfigFourier::ROIFirstIndex + roiIdx;
		}
		ifo = h5iometa2d( dsnm, U32, rois.size(), FOURIER_META_ROIS_IDS_NC );
		offs = h5offs2d( 0, rois.size(), 0, FOURIER_META_ROIS_IDS_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_ROIS_IDS" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_ROIS_IDS" << status << "\n"; return false;
		}
		u32 = vector<unsigned int>();

		//x, y, z
		dsnm = FOURIER_META_ROIS_XYZ;
		u32 = vector<unsigned int>( rois.size() * FOURIER_META_ROIS_XYZ_NC, 0 );
		for( auto it = rois.begin(); it != rois.end(); it++ ) {
			size_t roiIdx = it - rois.begin();
			u32[roiIdx*FOURIER_META_ROIS_XYZ_NC+0] = it->origin.x;
			u32[roiIdx*FOURIER_META_ROIS_XYZ_NC+1] = it->origin.y;
			u32[roiIdx*FOURIER_META_ROIS_XYZ_NC+2] = it->origin.z;
		}
		ifo = h5iometa2d( dsnm, U32, rois.size(), FOURIER_META_ROIS_XYZ_NC );
		offs = h5offs2d( 0, rois.size(), 0, FOURIER_META_ROIS_XYZ_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_ROIS_XYZ" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_ROIS_XYZ metadata !" << status << "\n"; return false;
		}
		u32 = vector<unsigned int>();

		dsnm = FOURIER_META_ROIS_XYZTOPO;
		u32 = vector<unsigned int>( 3*rois.size() * FOURIER_META_ROIS_XYZTOPO_NC, 1 ); //XDMF one vertex 1, XDMF point/vertex geometric primitive type
		for( size_t i = 0; i < rois.size(); i++ ) {
			u32[3*i+2] = i;
		}
		ifo = h5iometa2d( dsnm, U32, 3*rois.size(), FOURIER_META_ROIS_XYZTOPO_NC );
		offs = h5offs2d( 0, 3*rois.size(), 0, FOURIER_META_ROIS_XYZTOPO_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_ROIS_XYZTOPO" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_ROIS_XYZTOPO metadata !" << status << "\n"; return false;
		}
		u32 = vector<unsigned int>();

		dsnm = FOURIER_META_ROIS_DIMS;
		u32 = vector<unsigned int>( rois.size() * FOURIER_META_ROIS_DIMS_NC, 0 );
		for( auto it = rois.begin(); it != rois.end(); it++ ) {
			size_t roiIdx = it - rois.begin();
			u32[roiIdx*FOURIER_META_ROIS_DIMS_NC+0] = it->nx;
			u32[roiIdx*FOURIER_META_ROIS_DIMS_NC+1] = it->ny;
			u32[roiIdx*FOURIER_META_ROIS_DIMS_NC+2] = it->nz;
		}
		ifo = h5iometa2d( dsnm, U32, rois.size(), FOURIER_META_ROIS_DIMS_NC );
		offs = h5offs2d( 0, rois.size(), 0, FOURIER_META_ROIS_DIMS_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_ROIS_DIMS" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_ROIS_DIMS metadata !" << status << "\n"; return false;
		}
		u32 = vector<unsigned int>();

		//##MK::for now all ROIs have the same dimensions so the

		fft3d_mkl_cpu mkl_debug;
		mkl_debug.init( ConfigFourier::ROIUserGridDimensions.x );
		vector<bio_uint> u32ijknrm;
		vector<bio_uint> u32ijkcnj;
		mkl_debug.get_ijk_normal( u32ijknrm );
		mkl_debug.get_ijk_conjugate( u32ijkcnj );

		dsnm = FOURIER_META_FFT_IJK;
		ifo = h5iometa2d( dsnm, U32, u32ijknrm.size()/FOURIER_META_FFT_IJK_NC, FOURIER_META_FFT_IJK_NC );
		offs = h5offs2d( 0, u32ijknrm.size()/FOURIER_META_FFT_IJK_NC, 0, FOURIER_META_FFT_IJK_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32ijknrm );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_FFT_IJK" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_FFT_IJK metadata !" << status << "\n"; return false;
		}
		u32ijknrm = vector<unsigned int>();

		dsnm = FOURIER_META_FFT_IJKTOPO;
		u32 = vector<unsigned int>( 3*static_cast<size_t>(mkl_debug.ifo.NIJK), 1 ); //XDMF one vertex 1, XDMF point/vertex geometric primitive type
		for( size_t i = 0; i < static_cast<size_t>(mkl_debug.ifo.NIJK); i++ ) {
			u32[3*i+2] = i;
		}
		ifo = h5iometa2d( dsnm, U32, 3*static_cast<size_t>(mkl_debug.ifo.NIJK), FOURIER_META_FFT_IJKTOPO_NC );
		offs = h5offs2d( 0, 3*static_cast<size_t>(mkl_debug.ifo.NIJK), 0, FOURIER_META_FFT_IJKTOPO_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_FFT_IJKTOPO" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_FFT_IJKTOPO metadata !" << status << "\n"; return false;
		}
		u32 = vector<unsigned int>();

		/*
		dsnm = FOURIER_META_FFT_IJKCONJ;
		ifo = h5iometa2d( dsnm, U32, u32ijkcnj.size()/FOURIER_META_FFT_IJKCONJ_NC, FOURIER_META_FFT_IJKCONJ_NC );
		offs = h5offs2d( 0, u32ijkcnj.size()/FOURIER_META_FFT_IJKCONJ_NC, 0, FOURIER_META_FFT_IJKCONJ_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32ijkcnj );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_FFT_IJKCONJ" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_FFT_IJKCONJ metadata !" << status << "\n"; return false;
		}
		u32ijkcnj = vector<unsigned int>();
		*/

		dsnm = FOURIER_META_FFT_NXYZ;
		vector<long long> i64 = { mkl_debug.ifo.NX, mkl_debug.ifo.NY, mkl_debug.ifo.NZ };
		ifo = h5iometa2d( dsnm, I64, 1, FOURIER_META_FFT_NXYZ_NC );
		offs = h5offs2d( 0, 1, 0, FOURIER_META_FFT_NXYZ_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_i64le_hyperslab( ifo, offs, i64 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_FFT_NXYZ" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_FFT_NXYZ metadata !" << status << "\n"; return false;
		}
		i64 = vector<long long>();

		dsnm = FOURIER_META_FFT_NIJK;
		i64 = { mkl_debug.ifo.NI, mkl_debug.ifo.NJ, mkl_debug.ifo.NK };
		ifo = h5iometa2d( dsnm, I64, 1, FOURIER_META_FFT_NIJK_NC );
		offs = h5offs2d( 0, 1, 0, FOURIER_META_FFT_NIJK_NC );
		status = outputH5Hdl.add_contiguous_tensor2d_i64le_hyperslab( ifo, offs, i64 );
		if ( status == WRAPPED_HDF5_SUCCESS ) {
			cout << "Rank " << get_myrank() << " added tensor2d FOURIER_META_FFT_NIJK" << "\n";
		}
		else {
			cerr << "Rank " << get_myrank() << " failed to add FOURIER_META_FFT_NIJK metadata !" << status << "\n"; return false;
		}
		i64 = vector<long long>();

		cout << "Rank " << MASTER << " metadata written to H5" << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_HDF_WRITEMETADATA] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "HDF5::WriteMetadata", XXX, SEQUENTIAL, mm, tic, toc);

	return true;
}


bool fourierHdl::write_gpu_environment()
{
	return true;
}


bool fourierHdl::write_data()
{
	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa2d ifo = h5iometa2d();
	h5offs2d offs = h5offs2d();
	string dsnm = "";

	cout << "Rank " << get_myrank() << " writing data to " << outputH5Hdl.h5resultsfn << " ..." << "\n";

	bool perform_io = ConfigFourier::IOStoreFFTMagnitude;

	for( size_t thr = MASTER; thr < workers.size(); thr++ ) {
		acquisor* myresults = workers.at(thr);
		if ( myresults != NULL ) {
			for( auto it = myresults->res.begin(); it != myresults->res.end(); it++ ) {
				dsnm = FOURIER_RES_ROIS_FFTMAGN + fwslash + it->roiname; //to_string(it->roiid);
				fourier_res* thisone = it->dat;
				if ( thisone != NULL ) {
					status = WRAPPED_HDF5_SUCCESS;
					if ( perform_io == true ) {
						//cout << "Rank " << get_myrank() << " attempting to write " << dsnm << "\n";
						#ifdef EMPLOY_SINGLEPRECISION
							MYHDF5_DATATYPES bio_real_dtyp = F32;
						#else
							MYHDF5_DATATYPES bio_real_dtyp = F64;
						#endif
						ifo = h5iometa2d( dsnm, bio_real_dtyp, thisone->magn.size(), FOURIER_RES_ROIS_FFTMAGN_NC );
						offs = h5offs2d( 0, thisone->magn.size(), 0, FOURIER_RES_ROIS_FFTMAGN_NC );
						#ifdef EMPLOY_SINGLEPRECISION
							status = outputH5Hdl.add_contiguous_tensor2d_f32le_hyperslab( ifo, offs, thisone->magn );
						#else
							status = outputH5Hdl.add_contiguous_tensor2d_f64le_hyperslab( ifo, offs, thisone->magn );
						#endif
					}
					if ( status == WRAPPED_HDF5_SUCCESS ) {
						thisone->magn = vector<bio_real>();
						//cout << "Rank " << get_myrank() << " added tensor2d " << dsnm << "\n";
					}
					else {
						thisone->magn = vector<bio_real>();
						cerr << "Rank " << get_myrank() << " failed to add data for " << dsnm << " status " << status << "\n"; return false;
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " unexpected situation at the fourier_res_node level !" << "\n";
				}
			} //next thread-local ROI
		} //next thread
		else {
			cerr << "Rank " << get_myrank() << " unexpected situation at the workers level !" << "\n";
		}
	}


	double toc = MPI_Wtime();
	cout << "Rank " << get_myrank() << " writing results heavy data to  H5 took " << (toc-tic) << " seconds" << "\n";
	memsnapshot mm = memsnapshot();
	h5tictoc.elapsedtime_data[FOURIER_HDF_WRITEDATA] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "HDF5::WriteData", XXX, SEQUENTIAL, mm, tic, toc);

	return true;
}


bool fourierHdl::write_profiling()
{
	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa2d ifo = h5iometa2d();
	h5offs2d offs = h5offs2d();
	string grpnm = FOURIER_META_FFTPROF + fwslash + to_string(get_myrank());
	string dsnm = "";

	if ( ConfigFourier::IOStoreFFTProfiling == true ) {
		//collect all metadata at once, this is 1x double, 2x unsigned char, 1x int per ROI so even for a billion ROIs (unlikely sitting on one process) 16B*10^9, 16GB in memory cached
		//typically a few hundred thousand ROIs per process so a few MB cache required
		vector<unsigned int> u32;
		vector<unsigned char> u8;
		vector<double> f64;

		cout << "Rank " << get_myrank() << " collecting profiling data ..." << "\n";
		for( size_t thr = MASTER; thr < workers.size(); thr++ ) {
			acquisor* myresults = workers.at(thr);
			if ( myresults != NULL ) {
				for( auto it = myresults->res.begin(); it != myresults->res.end(); it++ ) {
					fourier_res* thisone = it->dat;
					if ( thisone != NULL ) {
						u32.push_back( it->roiid );
						u8.push_back( it->ifo.threadID );
						u8.push_back( it->ifo.devID );
						f64.push_back( it->ifo.elapsed );
					}
				}
			}
		}

		if ( u32.size()/FOURIER_META_FFTPROF_IDS_NC == u8.size()/FOURIER_META_FFTPROF_WHOM_NC && u32.size()/FOURIER_META_FFTPROF_IDS_NC == f64.size()/FOURIER_META_FFTPROF_DT_NC ) {

			dsnm = grpnm + fwslash + FOURIER_META_FFTPROF_IDS;
			ifo = h5iometa2d( dsnm, U32, u32.size()/FOURIER_META_FFTPROF_IDS_NC, FOURIER_META_FFTPROF_IDS_NC );
			offs = h5offs2d( 0, u32.size()/FOURIER_META_FFTPROF_IDS_NC, 0, FOURIER_META_FFTPROF_IDS_NC );
			status = outputH5Hdl.add_contiguous_tensor2d_u32le_hyperslab( ifo, offs, u32 );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
				//cout << "Rank " << get_myrank() << " added tensor2d " << dsnm << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to add data for " << dsnm << " status " << status << "\n"; return false;
			}

			dsnm = grpnm + fwslash + FOURIER_META_FFTPROF_WHOM;
			ifo = h5iometa2d( dsnm, U8, u8.size()/FOURIER_META_FFTPROF_WHOM_NC, FOURIER_META_FFTPROF_WHOM_NC );
			offs = h5offs2d( 0, u8.size()/FOURIER_META_FFTPROF_WHOM_NC, 0, FOURIER_META_FFTPROF_WHOM_NC );
			status = outputH5Hdl.add_contiguous_tensor2d_u8le_hyperslab( ifo, offs, u8 );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
				//cout << "Rank " << get_myrank() << " added tensor2d " << dsnm << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to add data for " << dsnm << " status " << status << "\n"; return false;
			}

			dsnm = grpnm + fwslash + FOURIER_META_FFTPROF_DT;
			ifo = h5iometa2d( dsnm, F64, f64.size()/FOURIER_META_FFTPROF_DT_NC, FOURIER_META_FFTPROF_DT_NC );
			offs = h5offs2d( 0, f64.size()/FOURIER_META_FFTPROF_DT_NC, 0, FOURIER_META_FFTPROF_DT_NC );
			status = outputH5Hdl.add_contiguous_tensor2d_f64le_hyperslab( ifo, offs, f64 );
			if ( status == WRAPPED_HDF5_SUCCESS ) {
				//cout << "Rank " << get_myrank() << " added tensor2d " << dsnm << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to add data for " << dsnm << " status " << status << "\n"; return false;
			}
		}
		else {
			cerr << "Rank " << get_myrank() << " profiling data are inconsistent wrt to the length of the output arrays" << "\n";
			return false;
		}
		u32 = vector<unsigned int>();
		u8 = vector<unsigned char>();
		f64 = vector<double>();
	}

	/*
	if ( ConfigFourier::IOStoreToolProfiling == true ) {
		//##MK::there is currently an issue with an HDF5 lock error when calling the program with two processes
		//##MK::therefore I do not write profiling data out right now
		//##############################################
		//##############################################
		//I assume the error is in the writing of the attributes likely a file that was opened by the previous process but not closed creating a file lock situation
		//##############################################
		grpnm = FOURIER_META_PROF + fwslash + to_string(get_myrank());
		for( map<unsigned int, fourier_prof>::iterator it = h5tictoc.elapsedtime_data.begin(); it != h5tictoc.elapsedtime_data.end(); it++ ) {
			unsigned whatID = it->first;
			map<unsigned int, string>::iterator thisone = h5tictoc.elapsedtime_dict.find( whatID );
			if ( thisone != h5tictoc.elapsedtime_dict.end() ) {
				string what = thisone->second;
				dsnm = grpnm + fwslash + FOURIER_META_PROF_DT + fwslash + what; //+ "::" + "ElapsedTime";

				ifo = h5iometa2d( dsnm, F64, 1, 1 );
				offs = h5offs2d( 0, 1, 0, 1 );
				vector<double> f64 = vector<double>( 1, it->second.dt );
				status = outputH5Hdl.add_contiguous_tensor2d_f64le_hyperslab( ifo, offs, f64 );
				if ( status != WRAPPED_HDF5_SUCCESS ) {
					cerr << "Rank " << get_myrank() << " failed to add data for " << dsnm << " status " << status << "\n"; return false;
				}
				status = outputH5Hdl.add_attribute( dsnm, "seconds", "elapsed time for that task" );
				if ( status != WRAPPED_HDF5_SUCCESS ) {
					cerr << "Rank " << get_myrank() << " write attributes for " << dsnm << " failed !" << "\n"; return false;
				}

				dsnm = grpnm + fwslash + FOURIER_META_PROF_VMEM + fwslash + what; //"::" + "VirtualMemory";
				ifo = h5iometa2d( dsnm, F64, 1, 1 );
				offs = h5offs2d( 0, 1, 0, 1 );
				f64 = vector<double>( 1, (it->second.vmem == -1 ) ? 0.0 : it->second.vmem );
				status = outputH5Hdl.add_contiguous_tensor2d_f64le_hyperslab( ifo, offs, f64 );
				if ( status != WRAPPED_HDF5_SUCCESS ) {
					cerr << "Rank " << get_myrank() << " failed to add data for " << dsnm << " status " << status << "\n"; return false;
				}
				status = outputH5Hdl.add_attribute( dsnm, "byte", "virtual memory snapshot from /proc/self/stat" );
				if ( status != WRAPPED_HDF5_SUCCESS ) {
					cerr << "Rank " << get_myrank() << " write attributes for " << dsnm << " failed !" << "\n"; return false;
				}

				dsnm = grpnm + fwslash + FOURIER_META_PROF_RMEM + fwslash + what; //"::" + "ResidentMemory";
				ifo = h5iometa2d( dsnm, F64, 1, 1 );
				offs = h5offs2d( 0, 1, 0, 1 );
				f64 = vector<double>( 1, (it->second.rmem == -1 ) ? 0.0 : it->second.rmem );
				status = outputH5Hdl.add_contiguous_tensor2d_f64le_hyperslab( ifo, offs, f64 );
				if ( status != WRAPPED_HDF5_SUCCESS ) {
					cerr << "Rank " << get_myrank() << " failed to add data for " << dsnm << " status " << status << "\n"; return false;
				}
				status = outputH5Hdl.add_attribute( dsnm, "byte", "resident memory snapshot from /proc/self/stat" );
				if ( status != WRAPPED_HDF5_SUCCESS ) {
					cerr << "Rank " << get_myrank() << " write attributes for " << dsnm << " failed !" << "\n"; return false;
				}
			}
			else {
				cerr << "Programming error, trying to add an unknown profiling event !" << "\n";
				return false;
			}
		}
	}
	*/

	double toc = MPI_Wtime();
	cout << "Rank " << get_myrank() << " profiling and writing these metadata to H5 took " << (toc-tic) << " seconds" << "\n";

	//memsnapshot mm = memsnapshot();
	//h5tictoc.elapsedtime_data[FOURIER_HDF_WRITEDATA] = fourier_prof( tic, toc, mm );
	//fourier_tictoc.prof_elpsdtime_and_mem( "HDF5::WriteData", XXX, SEQUENTIAL, mm, tic, toc);

	return true;
}


void fourierHdl::execute_local_workpackage_cpu()
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER )
		cout << "Executing local workpackages in parallel ..." << "\n";

	#pragma omp parallel
	{
		//allocate thread-local acquisor
		#pragma omp master
		{
			workers = vector<acquisor*>( omp_get_num_threads(), NULL );
		}
		//##MK::required, because master construct has no implicit one
		#pragma omp barrier

		//MK::heap memory allocated by local thread, but will be freed by master!
		acquisor* myresults = NULL;
		myresults = new acquisor;
		//MK::myresults is the thread-local bookkeeper, it MUST NOT be released before all results of this process (rank) have been stored !

		//##MK::currently all ROIs are cubes, so pre-compute the windowing coefficients only once and make these thread-local copies
		bio_uint nx = ConfigFourier::ROIUserGridDimensions.x;
		vector<bio_real> coeff1d = vector<bio_real>( nx, 1.0 );
		if ( ConfigFourier::WindowingMethod == HANN_HAMMING ) {
			bio_real divisor = static_cast<bio_real>(nx - 1);
			for( bio_uint i = 0; i < nx; i++ ) {
				bio_real counter = 2.0 * MYPI * static_cast<bio_real>(i);
				coeff1d[i] = 0.5*(1.0 - cos(counter/divisor));
			}
		}

		/*
		//##MK::BEGIN DEBUGGING
		#pragma omp master
		{
			for( auto it = coeff1d.begin(); it != coeff1d.end(); ++it ) {
				cout << "Coefficients " << it - coeff1d.begin() << "\t\t" << *it << "\n";
			}
		}
		//##MK::END DEBUGGING
		*/

		//3d outer product, weighting of 3x orthogonal 1d coefficients
		vector<bio_real> windowing_coeff = vector<bio_real>( CUBE(nx), 1.0 );
		for( bio_uint z = 0; z < nx; z++ ) {
			bio_uint zoff = z * SQR(nx);
			bio_real zmul = coeff1d[z];
			for( bio_uint y = 0; y < nx; y++ ) {
				bio_uint yzoff = y * nx + zoff;
				bio_real yzmul = coeff1d[y] * zmul;
				for( bio_uint x = 0; x < nx; x++ ) {
					windowing_coeff.at(x + yzoff) = windowing_coeff.at(x + yzoff) * coeff1d[x] * yzmul;
				}
			}
		}

		#pragma omp for
		for( auto it = myroiIDs.begin(); it != myroiIDs.end(); it++ ) {
		//for( map<unsigned int, int>::iterator rt = rois2rk.begin(); rt != rois2rk.end(); rt++ ) {
			//if ( rt->second == get_myrank() ) { //##MK::make own list for the local ROIs to avoid these ifs

			//double mytic = omp_get_wtime();

			fft3d_mkl_cpu my_mkl_debug;

			//hand over the data
			bio_uint roiID = static_cast<bio_uint>(*it);
			bio_uint roiIdx = roiID - static_cast<bio_uint>(ConfigFourier::ROIFirstIndex);
			roi_cuboid myroi = rois.at(roiIdx);

			vector<bio_real> imgpart;
			imgpart.reserve( myroi.nxyz() );
			/*
			//read out ROI from imgstack_global
			for( bio_uint z = myroi.origin.z; z < (myroi.origin.z + myroi.nx); z++ ) { //##MK::cubic ROIs
				size_t zoff_global = z * imgstack_global.bounded_nx * imgstack_global.bounded_ny;
				size_t zoff_roi = (z - myroi.origin.z) * SQR(nx);
				for( bio_uint y = myroi.origin.y; y < (myroi.origin.y + myroi.nx); y++ ) { //##MK::cubic ROIs
					size_t yzoff_global = y * imgstack_global.bounded_nx + zoff_global;
					size_t yzoff_roi = (y - myroi.origin.y) * nx + zoff_roi;
					for( bio_uint x = myroi.origin.x; x < (myroi.origin.x + myroi.nx); x++ ) { //x,y,z are voxel addresses in imgstack_global
						//multiply with windowing coefficients
						imgpart.push_back( windowing_coeff.at( (x - myroi.origin.x) + yzoff_roi ) * img_local.at( x + yzoff_global ) );
					}
				}
			}
			*/

			//read out ROI from imgstack_local
			for( int zglobal = myroi.origin.z; zglobal < (myroi.origin.z + myroi.nx); zglobal++ ) { //##MK::cubic ROIs, z, y, and x are coordinates in the global image stack
				int zlocal = zglobal - imgstack.local2global.z;
				size_t zoff_local = zlocal * imgstack.local.bounded_nx * imgstack.local.bounded_ny;
				size_t zoff_roi = (zglobal - myroi.origin.z) * SQR(nx);
				for( int yglobal = myroi.origin.y; yglobal < (myroi.origin.y + myroi.nx); yglobal++ ) { //##MK::cubic ROIs
					int ylocal = yglobal - imgstack.local2global.y;
					size_t yzoff_local = ylocal * imgstack.local.bounded_nx + zoff_local;
					size_t yzoff_roi = (yglobal - myroi.origin.y) * nx + zoff_roi;
					for( int xglobal = myroi.origin.x; xglobal < (myroi.origin.x + myroi.nx); xglobal++ ) { //x,y,z are voxel addresses in imgstack_global
						//multiply with windowing coefficients
						bio_real wdw_coeff = windowing_coeff.at( (xglobal - myroi.origin.x) + yzoff_roi );
						bio_real img_value = img_local.at( (xglobal - imgstack.local2global.x) + yzoff_local );
						imgpart.push_back( wdw_coeff * img_value );
					}
				}
			}

			my_mkl_debug.init( nx, imgpart ); //##MK:: !! be careful, make adaptive at some point when ROIs can have different dimensions, substantiating need for plan recreation
			my_mkl_debug.forwardFFT();
			my_mkl_debug.computeFFTMagn();

			if ( my_mkl_debug.ifo.success == true ) {
				fourier_res* tmp = new fourier_res;
				if ( tmp != NULL ) {
					myresults->res.push_back( fourier_res_node() );
					tmp->ifo = my_mkl_debug.ifo;
					tmp->magn = my_mkl_debug.m_amplitude; //copying over a trivial type the results before the next ROI is processed and thereby the temporary my_mkl_debug gets deleted
					//communicate to the thread-local worker bookkeeping where the pointers to the results vectors are stored
					myresults->res.back().dat = tmp;
					myresults->res.back().roiid = roiID;
				}
				else {
					myresults->res.push_back( fourier_res_node() );
					myresults->res.back().dat = NULL;
					myresults->res.back().roiid = BIMX;
				}
			}

			//double mytoc = omp_get_wtime();
			//#pragma omp critical
			//{
			//	cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " solved roiID " << roiID << " in " << (mytoc-mytic) << "\n";
			//}
		}
		//##MK::really necessary?
		#pragma omp barrier

		#pragma omp critical
		{
			workers.at(omp_get_thread_num()) = myresults; //MK::this "remembers" the pointers to the thread-local bookeepers and thus their individual ROI results
			//##MK::currently this memory is freed at the end of the program execution, for intermediate I/O cycling we need to free earlier, needs to be implemented though
		}
	} //end of parallel region

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	h5tictoc.elapsedtime_data[FOURIER_EXECUTE_ROIS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "ExecuteLocalWorkpackageCPU", XXX, PARALLEL, mm, tic, toc);

	cout << "Rank " << get_myrank() << " completed its local workpackage CPU in " << (toc-tic) << " seconds" << "\n";
}


void fourierHdl::execute_local_workpackage_cpugpu_debug()
{
	//this is a test implementation, its main purpose is debugging/verifying of the core FFT tasks
	//the idea is that we execute here each ROI of the process on the GPU and each thread to have a mechanism for checking whether the results of the FFT and the storage scheme is
	//the same between results from MKL and cuFFT
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER )
		cout << "Executing local workpackages in parallel debug ..." << "\n";

	//int nthreads = omp_get_max_threads();

	#pragma omp parallel
	{
		//which thread am I ?
		int mythreadid = omp_get_thread_num();
		bool mybooster = false;

		//allocate thread-local acquisor
		#pragma omp master
		{
			workers = vector<acquisor*>( omp_get_num_threads(), NULL );
		}
		//##MK::barrier is required because the pragma omp master construct has no implicit barrier
		#pragma omp barrier

		//MK::heap memory allocated by local thread, but will be freed by master!
		acquisor* myresults = NULL;
		myresults = new acquisor;
		//MK::myresults is the thread-local bookkeeper, it MUST NOT be released before all results of this process (rank) have been stored !

		//##MK::currently all ROIs are cubes, so pre-compute the windowing coefficients only once and make these thread-local copies
		fft3d_cpu_windowing mywindower = fft3d_cpu_windowing();

		bio_uint nx = ConfigFourier::ROIUserGridDimensions.x;
		bool ompError = mywindower.hann_hamming( nx ); //##MK::do not evaluate for now eventual ompErrors here

		//##MK::ONLY BECAUSE the current implementation demands that all ROIs have the same dimensions we can reutilize FFT plans
		//##MK::therefore we can initialize these plans prior the actual computations, especially on GPUs this is essential to compensate for the high plan creation costs of the cuFFT library
		//on MAWS30 with a RTX2080Ti for instance the plan creation takes as long as it takes to solve a few thousand 100^3 3D FFTs !!
		fft3d_cuda_gpu my_cuda_debug = fft3d_cuda_gpu();

		if ( mythreadid == MASTER ) {
			if ( gpubinder.mydevice != -1 ) {
				//yeah, I have a device boosting my performance
				mybooster = true;

				//init GPU batched 3d FFT plans for reuse
				my_cuda_debug.init_plan_batch( ConfigFourier::ROIUserGridDimensions.x, 1 );
				my_cuda_debug.reset_plan_batch();
				//cout << "Rank " << get_myrank() << " thread " << MASTER << " mybooster " << mybooster << "\n";

				//##MK::init CPU batch 3d FFT plans for reuse
			}
		}
		#pragma omp barrier

		//start processing the ROI ensemble using OpenMP threads and GPGPU, remember each process uses always either none or only one device at a time
		//#pragma omp for
		//#######MK::with commenting the line above out every ROI is visited by every thread
		for( auto it = myroiIDs.begin(); it != myroiIDs.end(); it++ ) {

			//MK::we want to distribute now ROIs to OpenMP threads and GPUs, there are multiple options how to distribute the work
			//MK::the general case is quite involved, as detailed in https://arxiv.org/abs/1702.00629 the elapsed time depends on the size and dimensions of the transform
			//there are four general cases when we have an ensemble of FFTs to compute for ROIs with the same size and same dimensions and a number of Nbatch such FFTs fitting in the memory of a single CPU and GPU
			//CPU core can solve the single FFT faster than the GPU, not the common case, because demands that FFT fits in low-level core caches where CPU can play its advantages of faster operations but few "threads"
			//CPU core solves a single FFT in about the same time it takes the GPU to solve the batch of Nbatch, the usual case for FFT sizes in the order of 32^3 - 100^3 ish, here GPUs play their advantage of having many but slower threads
			//CPU core solves a single FFT much slower than the GPU for the same single FFT, this is the case for larger and especially very large FFTS, e.g. >100^3
			//rest are difficult to control details, such as scheduling, system usage exclusivity, memory sub system, etc.

			//to begin with ... and debug... we implement here the case that the core executing the master OpenMP thread delegates and cooperates with the GPU using batched cuFFT library
			//hand over the data, ##MK::currently only one ROI at a time!
			double mytic = omp_get_wtime();

			bio_uint roiID = static_cast<bio_uint>(*it);
			bio_uint roiIdx = roiID - static_cast<bio_uint>(ConfigFourier::ROIFirstIndex);
			roi_cuboid myroi = rois.at(roiIdx);

			//image stack portion extraction on the CPU
			vector<bio_real> imgpart;
			imgpart.reserve( myroi.nxyz() );
			//read out ROI data from imgstack_local
			for( int zglobal = myroi.origin.z; zglobal < (myroi.origin.z + myroi.nx); zglobal++ ) { //##MK::cubic ROIs, z, y, and x are coordinates in the global image stack
				int zlocal = zglobal - imgstack.local2global.z;
				size_t zoff_local = zlocal * imgstack.local.bounded_nx * imgstack.local.bounded_ny;
				size_t zoff_roi = (zglobal - myroi.origin.z) * SQR(nx);
				for( int yglobal = myroi.origin.y; yglobal < (myroi.origin.y + myroi.nx); yglobal++ ) { //##MK::cubic ROIs
					int ylocal = yglobal - imgstack.local2global.y;
					size_t yzoff_local = ylocal * imgstack.local.bounded_nx + zoff_local;
					size_t yzoff_roi = (yglobal - myroi.origin.y) * nx + zoff_roi;
					for( int xglobal = myroi.origin.x; xglobal < (myroi.origin.x + myroi.nx); xglobal++ ) { //x,y,z are voxel addresses in imgstack_global
						//multiply with windowing coefficients
						bio_real wdw_coeff = mywindower.windowing_coeff.at( (xglobal - myroi.origin.x) + yzoff_roi );
						bio_real img_value = img_local.at( (xglobal - imgstack.local2global.x) + yzoff_local );
						imgpart.push_back( wdw_coeff * img_value );
					}
				}
			}

			//FFT solving on CPU or CPU/GPU
			if ( mythreadid == MASTER && mybooster == true ) {
				//####delegate work to GPU reusing the already existent FFT plan
				for( int bid = 0; bid < 1; bid++ ) {
					my_cuda_debug.fill_data_batch( imgpart, bid );
				}
				my_cuda_debug.move_plan_batch();
				my_cuda_debug.exec_plan_batch();

				my_cuda_debug.exec_magn_batch();

				//transfer the magnitude results to the results container to prep them for I/O
				if ( my_cuda_debug.ifo.success == true ) {
					fourier_res* tmp = new fourier_res;
					if ( tmp != NULL ) {
						myresults->res.push_back( fourier_res_node() );
						tmp->ifo = fftinfo( ConfigFourier::ROIUserGridDimensions.x );
						size_t nijk = my_cuda_debug.ifo.NIJK*my_cuda_debug.ifo.BATCH;
						if ( tmp->ifo.NIJK * 1 == nijk ) {
							tmp->magn.reserve( nijk );
							for( size_t ijk = 0; ijk < nijk; ijk++ ) {
								tmp->magn.push_back( my_cuda_debug.fftmagn_h[ijk] );
							}
						}
						else {
							cerr << "my_cuda_debug.ifo.NIJKB does not match with tmp->ifo !" << "\n";
						}
						//communicate to the thread-local worker bookkeeping where the pointers to the results vectors are stored
						myresults->res.back().dat = tmp;
						myresults->res.back().roiname = "rank_" + to_string(get_myrank()) + "_thr_" + to_string(mythreadid) + "_gpu_" + to_string(gpubinder.mydevice) + "_roi_" + to_string(roiID);
						myresults->res.back().roiid = roiID;
						double mytoc = omp_get_wtime();
						myresults->res.back().ifo = fourier_res_prof( mytoc-mytic, get_myrank(), MASTER, gpubinder.mydevice );
					}
					else {
						myresults->res.push_back( fourier_res_node() );
						myresults->res.back().dat = NULL;
						myresults->res.back().roiname = "";
						myresults->res.back().roiid = BIMX;
						myresults->res.back().ifo = fourier_res_prof();
					}
				}

				//prepare the plan for reuse
				my_cuda_debug.reset_plan_batch();
			}
			else { //non-master threads or master thread (including cases with only one OpenMP thread, i.e. the master thread only) without a GPU, we use the CPU fallback

				//currently no plan reuse implemented, ##MK::but the IMKL supports this very similar to cuFFT
				fft3d_mkl_cpu my_mkl_debug = fft3d_mkl_cpu();
				my_mkl_debug.init( nx, imgpart ); //##MK:: !! be careful, make adaptive at some point when ROIs can have different dimensions, substantiating the need for plan recreation
				my_mkl_debug.forwardFFT();

				my_mkl_debug.computeFFTMagn();

				//transfer the magnitude results to the results container to prep them for I/O
				if ( my_mkl_debug.ifo.success == true ) {
					fourier_res* tmp = new fourier_res;
					if ( tmp != NULL ) {
						myresults->res.push_back( fourier_res_node() );
						tmp->ifo = my_mkl_debug.ifo;
						tmp->magn = my_mkl_debug.m_amplitude; //copying over a trivial type the results before the next ROI is processed and thereby the temporary my_mkl_debug gets deleted
						//communicate to the thread-local worker bookkeeping where the pointers to the results vectors are stored
						myresults->res.back().dat = tmp;
						myresults->res.back().roiname = "rank_" + to_string(get_myrank()) + "_thr_" + to_string(mythreadid) + "_cpu_" + to_string(mythreadid) + "_roi_" + to_string(roiID);
						myresults->res.back().roiid = roiID;
						double mytoc = omp_get_wtime();
						myresults->res.back().ifo = fourier_res_prof( mytoc-mytic, get_myrank(), mythreadid );
					}
					else {
						myresults->res.push_back( fourier_res_node() );
						myresults->res.back().dat = NULL;
						myresults->res.back().roiname = "";
						myresults->res.back().roiid = BIMX;
						myresults->res.back().ifo = fourier_res_prof();
					}
				}
			}

			//double mytic = omp_get_wtime();
			//double mytoc = omp_get_wtime();
			//#pragma omp critical
			//{
			//	cout << "Rank " << get_myrank() << " thread " << omp_get_thread_num() << " solved roiID " << roiID << " in " << (mytoc-mytic) << "\n";
			//}
		}
		//##MK::really necessary?
		#pragma omp barrier

		#pragma omp critical
		{
			workers.at(omp_get_thread_num()) = myresults; //MK::this "remembers" the pointers to the thread-local bookeepers and thus their individual ROI results
			//##MK::currently this memory is freed at the end of the program execution, for intermediate I/O cycling we need to free earlier, needs to be implemented though
		}
	} //end of parallel region

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	h5tictoc.elapsedtime_data[FOURIER_EXECUTE_ROIS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "ExecuteLocalWorkpackageCPUGPUDebug", XXX, PARALLEL, mm, tic, toc);

	cout << "Rank " << get_myrank() << " completed its local workpackage CPUGPUDebug in " << (toc-tic) << " seconds" << "\n";
}


void fourierHdl::execute_local_workpackage_cpugpu_batched_no()
{
	//this is a (theoretical) less performant version of batched_yes the main purpose is running production jobs for the core FFT tasks
	//the idea is that we distribute the ROI ensemble of the process in packets of one ROI on either GPU or any of the non-MASTER threads
	//computations on non-master OpenMP threads happen to run parallel to computations of the master OpenMP thread which offloads work to the device of the rank

	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER )
		cout << "Executing local workpackages in parallel batching no ..." << "\n";

	#pragma omp parallel
	{
		//which thread am I ?
		int mythreadid = omp_get_thread_num();
		bool mybooster = false;

		//allocate thread-local acquisor
		#pragma omp master
		{
			workers = vector<acquisor*>( omp_get_num_threads(), NULL );
		}
		//##MK::barrier is required because the pragma omp master construct has no implicit barrier
		#pragma omp barrier

		//MK::heap memory allocated by local thread, but will be freed by master!
		acquisor* myresults = NULL;
		myresults = new acquisor;
		//MK::myresults is the thread-local bookkeeper, it MUST NOT be released before all results of this process (rank) have been stored !

		//##MK::currently all ROIs are cubes, so pre-compute the windowing coefficients only once and make these thread-local copies
		fft3d_cpu_windowing mywindower = fft3d_cpu_windowing();

		bio_uint nx = ConfigFourier::ROIUserGridDimensions.x;
		bool ompError = mywindower.hann_hamming( nx ); //##MK::do not evaluate for now eventual ompErrors here

		//##MK::ONLY BECAUSE the current implementation demands that all ROIs have the same dimensions we can reutilize FFT plans
		//##MK::therefore we can initialize these plans prior the actual computations, especially on GPUs this is essential to compensate for the high plan creation costs of the cuFFT library
		//on MAWS30 with a RTX2080Ti for instance the plan creation takes as long as it takes to solve a few thousand 100^3 3D FFTs !!
		fft3d_cuda_gpu my_cuda_debug = fft3d_cuda_gpu();

		if ( mythreadid == MASTER ) {
			if ( gpubinder.mydevice != -1 ) {
				//yeah, I have a device boosting my performance
				mybooster = true;

				//init GPU batched 3d FFT plans for reuse
				my_cuda_debug.init_plan_batch( ConfigFourier::ROIUserGridDimensions.x, 1 );
				my_cuda_debug.reset_plan_batch();

				//cout << "Rank " << get_myrank() << " thread " << MASTER << " mybooster " << mybooster << "\n";

				//##MK::init CPU batch 3d FFT plans for reuse
			}
		}
		#pragma omp barrier

		//start processing the ROI ensemble using OpenMP threads and GPGPU, remember each process uses always either none or only one device at a time
		#pragma omp for
		for( auto it = myroiIDs.begin(); it != myroiIDs.end(); it++ ) { //threads work on disjoint iterators that point to disjoint ROIs !

			//MK::we want to distribute now ROIs to OpenMP threads and GPUs, there are multiple options how to distribute the work
			//MK::the general case is quite involved, as detailed in https://arxiv.org/abs/1702.00629 the elapsed time depends on the size and dimensions of the transform
			//there are four general cases when we have an ensemble of FFTs to compute for ROIs with the same size and same dimensions and a number of Nbatch such FFTs fitting in the memory of a single CPU and GPU
			//CPU core can solve the single FFT faster than the GPU, not the common case, because demands that FFT fits in low-level core caches where CPU can play its advantages of faster operations but few "threads"
			//CPU core solves a single FFT in about the same time it takes the GPU to solve the batch of Nbatch, the usual case for FFT sizes in the order of 32^3 - 100^3 ish, here GPUs play their advantage of having many but slower threads
			//CPU core solves a single FFT much slower than the GPU for the same single FFT, this is the case for larger and especially very large FFTS, e.g. >100^3
			//rest are difficult to control details, such as scheduling, system usage exclusivity, memory sub system, etc.

			//to begin with ... and debug... we implement here the case that the core executing the master OpenMP thread delegates and cooperates with the GPU using batched cuFFT library
			//hand over the data, ##MK::currently only one ROI at a time!
			double mytic = omp_get_wtime();

			bio_uint roiID = static_cast<bio_uint>(*it);
			bio_uint roiIdx = roiID - static_cast<bio_uint>(ConfigFourier::ROIFirstIndex);
			roi_cuboid myroi = rois.at(roiIdx);

			//image stack portion extraction on the CPU
			vector<bio_real> imgpart;
			imgpart.reserve( myroi.nxyz() );
			//read out ROI data from imgstack_local
			for( int zglobal = myroi.origin.z; zglobal < (myroi.origin.z + myroi.nx); zglobal++ ) { //##MK::cubic ROIs, z, y, and x are coordinates in the global image stack
				int zlocal = zglobal - imgstack.local2global.z;
				size_t zoff_local = zlocal * imgstack.local.bounded_nx * imgstack.local.bounded_ny;
				size_t zoff_roi = (zglobal - myroi.origin.z) * SQR(nx);
				for( int yglobal = myroi.origin.y; yglobal < (myroi.origin.y + myroi.nx); yglobal++ ) { //##MK::cubic ROIs
					int ylocal = yglobal - imgstack.local2global.y;
					size_t yzoff_local = ylocal * imgstack.local.bounded_nx + zoff_local;
					size_t yzoff_roi = (yglobal - myroi.origin.y) * nx + zoff_roi;
					for( int xglobal = myroi.origin.x; xglobal < (myroi.origin.x + myroi.nx); xglobal++ ) { //x,y,z are voxel addresses in imgstack_global
						//multiply with windowing coefficients
						bio_real wdw_coeff = mywindower.windowing_coeff.at( (xglobal - myroi.origin.x) + yzoff_roi );
						bio_real img_value = img_local.at( (xglobal - imgstack.local2global.x) + yzoff_local );
						imgpart.push_back( wdw_coeff * img_value );
					}
				}
			}

			//FFT solving on CPU or CPU/GPU
			if ( mythreadid == MASTER && mybooster == true ) {
				//####delegate work to GPU reusing the already existent FFT plan
				for( int bid = 0; bid < 1; bid++ ) {
					my_cuda_debug.fill_data_batch( imgpart, bid );
				}
				my_cuda_debug.move_plan_batch();
				my_cuda_debug.exec_plan_batch();

				my_cuda_debug.exec_magn_batch();

				//transfer the magnitude results to the results container to prep them for I/O
				if ( my_cuda_debug.ifo.success == true ) {
					fourier_res* tmp = new fourier_res;
					if ( tmp != NULL ) {
						myresults->res.push_back( fourier_res_node() );
						tmp->ifo = fftinfo( ConfigFourier::ROIUserGridDimensions.x );
						size_t nijk = my_cuda_debug.ifo.NIJK*my_cuda_debug.ifo.BATCH;
						if ( tmp->ifo.NIJK * 1 == nijk ) {
							tmp->magn.reserve( nijk );
							for( size_t ijk = 0; ijk < nijk; ijk++ ) {
								tmp->magn.push_back( my_cuda_debug.fftmagn_h[ijk] );
							}
						}
						else {
							cerr << "my_cuda_debug.ifo.NIJKB does not match with tmp->ifo !" << "\n";
						}
						//communicate to the thread-local worker bookkeeping where the pointers to the results vectors are stored
						myresults->res.back().dat = tmp;
						myresults->res.back().roiname = to_string(roiID);
						myresults->res.back().roiid = roiID;
						double mytoc = omp_get_wtime();
						myresults->res.back().ifo = fourier_res_prof( mytoc-mytic, get_myrank(), MASTER, gpubinder.mydevice );
					}
					else {
						myresults->res.push_back( fourier_res_node() );
						myresults->res.back().dat = NULL;
						myresults->res.back().roiname = "";
						myresults->res.back().roiid = BIMX;
						myresults->res.back().ifo = fourier_res_prof();
					}
				}

				//prepare the plan for reuse
				my_cuda_debug.reset_plan_batch();
			}
			else { //non-master threads or master thread (including cases with only one OpenMP thread, i.e. the master thread only) without a GPU, we use the CPU fallback

				//currently no plan reuse implemented, ##MK::but the IMKL supports this very similar to cuFFT
				fft3d_mkl_cpu my_mkl_debug = fft3d_mkl_cpu();
				my_mkl_debug.init( nx, imgpart ); //##MK:: !! be careful, make adaptive at some point when ROIs can have different dimensions, substantiating the need for plan recreation
				my_mkl_debug.forwardFFT();

				my_mkl_debug.computeFFTMagn();

				//transfer the magnitude results to the results container to prep them for I/O
				if ( my_mkl_debug.ifo.success == true ) {
					fourier_res* tmp = new fourier_res;
					if ( tmp != NULL ) {
						myresults->res.push_back( fourier_res_node() );
						tmp->ifo = my_mkl_debug.ifo;
						tmp->magn = my_mkl_debug.m_amplitude; //copying over a trivial type the results before the next ROI is processed and thereby the temporary my_mkl_debug gets deleted
						//communicate to the thread-local worker bookkeeping where the pointers to the results vectors are stored
						myresults->res.back().dat = tmp;
						myresults->res.back().roiname = to_string(roiID); //"rank_" + to_string(get_myrank()) + "_thr_" + to_string(mythreadid) + "_cpu_" + to_string(mythreadid) + "_roi_" + to_string(roiID);
						myresults->res.back().roiid = roiID;
						double mytoc = omp_get_wtime();
						myresults->res.back().ifo = fourier_res_prof( mytoc-mytic, get_myrank(), mythreadid );
					}
					else {
						myresults->res.push_back( fourier_res_node() );
						myresults->res.back().dat = NULL;
						myresults->res.back().roiname = "";
						myresults->res.back().roiid = BIMX;
						myresults->res.back().ifo = fourier_res_prof();
					}
				}
			}
		}
		//##MK::really necessary?
		#pragma omp barrier

		#pragma omp critical
		{
			workers.at(omp_get_thread_num()) = myresults; //MK::this "remembers" the pointers to the thread-local bookeepers and thus their individual ROI results
			//##MK::currently this memory is freed at the end of the program execution, for intermediate I/O cycling we need to free earlier, needs to be implemented though
		}
	} //end of parallel region

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	h5tictoc.elapsedtime_data[FOURIER_EXECUTE_ROIS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "ExecuteLocalWorkpackageCPUGPUBatchedNo", XXX, PARALLEL, mm, tic, toc);

	cout << "Rank " << get_myrank() << " completed its local workpackage CPUGPU batched no in " << (toc-tic) << " seconds" << "\n";
}


void fourierHdl::execute_local_workpackage_cpugpu_batched_yes()
{
	double tic = MPI_Wtime();

	cerr << "That function is not yet implemented, the following pieces are missing :" << "\n";
	cerr << "Implement distributing of myroiIDs per rank to GPUs in batches with Nbatch > 1 but Nbatch == 1 per thread" << "\n";
	cerr << "Implement batching using the MKL batched execution" << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	h5tictoc.elapsedtime_data[FOURIER_EXECUTE_ROIS] = fourier_prof( tic, toc, mm );
	fourier_tictoc.prof_elpsdtime_and_mem( "ExecuteLocalWorkpackageCPUGPUBatchedYes", XXX, PARALLEL, mm, tic, toc);

	cout << "Rank " << get_myrank() << " completed its local workpackage CPUGPU batched yes in " << (toc-tic) << " seconds" << "\n";
}


/*
bool fourierHdl::collect_results_on_masterprocess()
{
	double tic = MPI_Wtime();

	//then we have two options, the simplest, enforce sequential loop of individual slave sends, master receives
	//or leaner a MPI_Gatherv on the master
	//##MK::we opt first for the simpler strategy, because computing time for the fourier transform >> communication time

	//all processes first identify how many results they have to sent
	int localhealth = 1;
	int globalhealth = 0;
	if ( myhklval.size() >= UINT32MX ) {
		cerr << "Rank " << get_myrank() << " has too many results to send them using the currently implemented communcation protocol!";
		localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all processes are able to contribute results!" << "\n";
		return false;
	}

	unsigned int n_localresults = static_cast<unsigned int>(myhklval.size()); //we send also results on material points with no ions
	vector<unsigned int> rk2nres = vector<unsigned int>( get_nranks(), 0 );
	unsigned int* buf = NULL;
	if ( myrank == MASTER ) {
		buf = rk2nres.data(); //raw pointer to array ie &rk2nres[0]
	}
	MPI_Gather( &n_localresults, 1, MPI_UNSIGNED, buf, 1, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD );

	size_t nrestotal = 0;
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < rk2nres.size(); i++ ) {
			cout << "Rank " << i << " contributes " << rk2nres[i] << " results" << "\n";
		}
		for( auto it = rk2nres.begin(); it != rk2nres.end(); it++ ) {
			nrestotal += static_cast<size_t>(*it);
		}
		cout << "Rank MASTER has counted " << nrestotal << " results and expects " << mp.matpoints.size() << "\n";

		//is this consistent with expectation?
		if ( nrestotal != mp.matpoints.size() ) {
			localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that the reported number of results is different from expectation!" << "\n";
		return false;
	}

	//no differences, so let master allocate sufficiently sized buffer
	//MK::we enforce here, at the cost of performance, and explicit writing of the result in order, such that regardless the number of processes used
	//the order of the results arrays in the HDF5 file is always the same, i.e. deterministic
	vector<MPI_Fourier_ROI> roisend;
	vector<MPI_Fourier_ROI> roirecv;
	vector<MPI_Fourier_HKLValue> hklsend;
	vector<MPI_Fourier_HKLValue> hklrecv;

	if ( get_myrank() == MASTER ) {
		//master allocates global results field, see above comments on strategy
		mproi = vector<MPI_Fourier_ROI>( nrestotal, MPI_Fourier_ROI() );
		mpres = vector<MPI_Fourier_HKLValue>( nrestotal, MPI_Fourier_HKLValue() );

		//master fills in all values
		//infos about the master's ROIs
		for( auto it = myworkload.begin(); it != myworkload.end(); it++ ) { //myworkload handling global material point IDs
			unsigned int global_mpid = it->mpid;
			mproi.at(global_mpid) = MPI_Fourier_ROI( it->x, it->y, it->z, it->R, it->mpid, it->nions ); //number of ions not yet known
		}
		//master also fills in all its actual results
		for( auto it = myhklval.begin(); it != myhklval.end(); it++ ) { //myhklval handling local material point IDs
			unsigned int local_mpid = it->mpid; //process-local ID
			if ( local_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
				unsigned int global_mpid = myworkload.at(local_mpid).mpid;
				mpres.at(global_mpid) = MPI_Fourier_HKLValue( global_mpid, it->ijk, it->val ); //from explicit global_mpid to implicit IDs
				//##MPI_Fourier_HKLValue change mpid from int to unsigned int
			}
			//if no result leave MPI_Fourier_HKLValue with the default datatype constructor value
		}
	}
	else { //meanwhile the slaves prepare their MPI send buffers
		//infos about the slaves's ROIs
		roisend.reserve( myworkload.size() );
		for( auto it = myworkload.begin(); it != myworkload.end(); it++ ) { //myworkload handling global material point IDs
			roisend.push_back( MPI_Fourier_ROI(it->x, it->y, it->z, it->R, it->mpid, it->nions) );
		}
		//the actual results of the slave
		hklsend.reserve( myhklval.size() );
		for( auto it = myhklval.begin(); it != myhklval.end(); it++ ) { //myhklval handling local material point IDs
			unsigned int local_mpid = it->mpid; //process-local ID
			if ( local_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) { //only if this is resolvable there is a result, else the ROI has no ion support
				unsigned int global_mpid = myworkload.at(local_mpid).mpid;
				hklsend.push_back( MPI_Fourier_HKLValue( global_mpid, it->ijk, it->val ) ); //from explicit global_mpid to implicit IDs
			}
			else {
				hklsend.push_back( MPI_Fourier_HKLValue( NORESULTS_FOR_THIS_MATERIALPOINT, INT32MX, 0.f ) );
			}
		}
	}

	//agglomeration stage, master collects results from slaves
	for( int rank = MASTER + 1; rank < get_nranks(); rank++ ) { //+1 because we have already the results from the master
		if ( get_myrank() == MASTER ) {

			roirecv = vector<MPI_Fourier_ROI>( rk2nres.at(rank), MPI_Fourier_ROI() );
			MPI_Recv( roirecv.data(), rk2nres.at(rank), MPI_Fourier_ROI_Type, rank, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

			hklrecv = vector<MPI_Fourier_HKLValue>( rk2nres.at(rank), MPI_Fourier_HKLValue() );
			MPI_Recv( hklrecv.data(), rk2nres.at(rank), MPI_Fourier_HKLValue_Type, rank, 1000*rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			//once results are received fill them in
			for( auto it = roirecv.begin(); it != roirecv.end(); it++ ) {
				unsigned int global_mpid = it->mpid;
				mproi.at(global_mpid) = *it;
			}
			for( auto jt = hklrecv.begin(); jt != hklrecv.end(); jt++ ) {
				unsigned int global_mpid = jt->mpid;
				if ( global_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) { //reset only those results values for which results exist
					mpres.at(global_mpid) = *jt;
				}
			}
		}
		else { //I am a slave to I have to send to MASTER
			if ( get_myrank() == rank ) {
				MPI_Send( roisend.data(), roisend.size(), MPI_Fourier_ROI_Type, MASTER, rank, MPI_COMM_WORLD );
				roisend = vector<MPI_Fourier_ROI>();

				MPI_Send( hklsend.data(), hklsend.size(), MPI_Fourier_HKLValue_Type, MASTER, 1000*rank, MPI_COMM_WORLD );
				hklsend = vector<MPI_Fourier_HKLValue>();
			}
			//else {} //nothing to do for me, I neither do I/O nor have the data for rank
		}
		MPI_Barrier(MPI_COMM_WORLD); //##MK::hyperphobic
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "CollectResultsOnMasterProcess", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " participated successfully in results collection stage took " << (toc-tic) << " seconds" << "\n";
	return true;
}
*/


/*
bool fourierHdl::write_roi_results_to_h5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return false;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return false;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";

	//HKLGrid voxel grid


	//material point grid xyz
	vector<float> f32 = vector<float>( PARAPROBE_FOURIER_META_MP_XYZ_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_FOURIER_META_MP_XYZ;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_FOURIER_META_MP_XYZ_NCMAX+0) = mp.matpoints[i].x;
		f32.at(i*PARAPROBE_FOURIER_META_MP_XYZ_NCMAX+1) = mp.matpoints[i].y;
		f32.at(i*PARAPROBE_FOURIER_META_MP_XYZ_NCMAX+2) = mp.matpoints[i].z;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_XYZ metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, 0, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX,
			f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_XYZ metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>( PARAPROBE_FOURIER_META_MP_R_NCMAX*mp.matpoints.size(), 0.f ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_FOURIER_META_MP_R;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		f32.at(i*PARAPROBE_FOURIER_META_MP_R_NCMAX+0) = mp.matpoints[i].R;
	}
	ifo = h5iometa( dsnm, f32.size()/PARAPROBE_FOURIER_META_MP_R_NCMAX, PARAPROBE_FOURIER_META_MP_R_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_R metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_MP_R_NCMAX, 0, PARAPROBE_FOURIER_META_MP_R_NCMAX,
			f32.size()/PARAPROBE_FOURIER_META_MP_R_NCMAX, PARAPROBE_FOURIER_META_MP_R_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_R metadata! " << status << "\n"; return false;
	}
	f32 = vector<float>();

	//matpoint IDs
	vector<unsigned int> u32 = vector<unsigned int>( PARAPROBE_FOURIER_META_MP_ID_NCMAX*mp.matpoints.size(), 0 );
	dsnm = PARAPROBE_FOURIER_META_MP_ID;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(i*PARAPROBE_FOURIER_META_MP_ID_NCMAX+0) = mp.matpoints[i].mpid;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, PARAPROBE_FOURIER_META_MP_ID_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_ID metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, 0, PARAPROBE_FOURIER_META_MP_ID_NCMAX,
			u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, PARAPROBE_FOURIER_META_MP_ID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_ID metadata! " << status << "\n"; return false;
	}

	//XDMF topo
	u32 = vector<unsigned int>( 3*mp.matpoints.size(), 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	dsnm = PARAPROBE_FOURIER_META_MP_TOPO;
	for ( size_t i = 0; i < mp.matpoints.size(); i++ ) {
		u32.at(3*i+2) = i;
	}
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to create MP_TOPO metadata! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, 0, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX,
			u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " failed to store MP_TOPO metadata! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	//details about the reciprocal space grid
	//details about the grid used
	u32 = vector<unsigned int>( 3, static_cast<unsigned int>(ConfigFourier::HKLGrid.ni) );
	ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_NIJK, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
			PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_NIJK create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_NIJK create " << status << "\n";
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
			u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_NIJK write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_NIJK write " << status << "\n";
	u32 = vector<unsigned int>();

	//##MK::make small function which computes this mapping taking only FourierGridResolution as input
	f32 = vector<float>();
	dft_real start = ConfigFourier::HKLGrid.imi;
	dft_real stop = ConfigFourier::HKLGrid.imx;
	dft_real num = static_cast<dft_real>(ConfigFourier::HKLGrid.ni-1);
	dft_real step = (stop-start)/num;
	for( int i = 0; i < ConfigFourier::HKLGrid.ni; ++i ) {
		dft_real v = TWO_PI * (start + static_cast<dft_real>(i)*step);
		f32.push_back( v );
	}
	ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_IVAL, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
			PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_IVAL create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_IVAL create " << status << "\n";
	offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
			f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_FOURIER_META_HKL_IVAL write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_FOURIER_META_HKL_IVAL write " << status << "\n";

	if ( ConfigFourier::IOFullGridPks == true ) {
		vector<float> f32xyz;
		int ni = ConfigFourier::HKLGrid.ni;
		f32xyz.reserve( PARAPROBE_FOURIER_META_HKL_GRID_NCMAX * CUBE(ni) );
		for( int k = 0; k < ni; k++ ) {
			float ival_k = f32[k];
			for( int j = 0; j < ni; j++ ) {
				float ival_j = f32[j];
				for( int i = 0; i < ni; i++ ) {
					float ival_i = f32[i];
					f32xyz.push_back( ival_i );
					f32xyz.push_back( ival_j );
					f32xyz.push_back( ival_k );
				}
			}
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_GRID, f32xyz.size()/PARAPROBE_FOURIER_META_HKL_GRID_NCMAX,
					PARAPROBE_FOURIER_META_HKL_GRID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_GRID create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_GRID create " << status << "\n";
		offs = h5offsets( 0, f32xyz.size()/PARAPROBE_FOURIER_META_HKL_GRID_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_GRID_NCMAX,
				f32xyz.size()/PARAPROBE_FOURIER_META_HKL_GRID_NCMAX, PARAPROBE_FOURIER_META_HKL_GRID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32xyz );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_GRID write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_GRID write " << status << "\n";

		//topology
		unsigned int NIJK = CUBE(static_cast<unsigned int>(ni));
		vector<unsigned int> u32topo = vector<unsigned int>( 3*NIJK, 1 ); //XDMF keyword and point type
		for ( unsigned int i = 0; i < NIJK; i++ ) {
			u32topo.at(3*i+2) = i;
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_TOPO, u32topo.size()/PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX,
					PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_TOPO create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_TOPO create " << status << "\n";
		offs = h5offsets( 0, u32topo.size()/PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX,
				u32topo.size()/PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX, PARAPROBE_FOURIER_META_HKL_TOPO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32topo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_TOPO write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_TOPO write " << status << "\n";

		f32xyz = vector<float>();
	}
	//IV inside f32 now no longer required
	f32 = vector<float>();

	//phases
	u32.push_back( itsk.iphcands.size() );
	dsnm = PARAPROBE_FOURIER_META_PHCAND_N;
	ifo = h5iometa( dsnm, u32.size()/PARAPROBE_FOURIER_META_PHCAND_N_NCMAX, PARAPROBE_FOURIER_META_PHCAND_N_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_FOURIER_META_PHCAND_N create failed! " << status << "\n"; return false;
	}
	offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_PHCAND_N_NCMAX, 0, PARAPROBE_FOURIER_META_PHCAND_N_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_PHCAND_N_NCMAX, PARAPROBE_FOURIER_META_PHCAND_N_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status < 0 ) {
		cerr << "Rank " << get_myrank() << " PARAPROBE_FOURIER_META_PHCAND_N write failed! " << status << "\n"; return false;
	}
	u32 = vector<unsigned int>();

	for( auto mtsk = itsk.iphcands.begin(); mtsk != itsk.iphcands.end(); mtsk++ ) {
		string grpnm = PARAPROBE_FOURIER_META_PHCAND + fwslash + "PH" + to_string(mtsk->candid);
		status = debugh5Hdl.create_group( grpnm );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "Rank " << get_myrank() << " grpnm " << grpnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " grpnm " << grpnm << " create " << status << "\n";

		vector<unsigned char> u8_id;
		vector<unsigned char> u8_wh;
		for( int tg = 0; tg < TARGET_LIST_SIZE; tg++ ) {
			unsigned char current_ityp = mtsk->targets[tg];
			if ( current_ityp != 0xFF ) {
				u8_id.push_back( current_ityp );
				evapion3 tmp = rng.iontypes.at(static_cast<int>(current_ityp)).strct;
				u8_wh.push_back( tmp.Z1 );
				u8_wh.push_back( tmp.N1 );
				u8_wh.push_back( tmp.Z2 );
				u8_wh.push_back( tmp.N2 );
				u8_wh.push_back( tmp.Z3 );
				u8_wh.push_back( tmp.N3 );
				u8_wh.push_back( tmp.sign );
				u8_wh.push_back( tmp.charge );
			}
		}

		dsnm = grpnm + fwslash + PARAPROBE_FOURIER_META_PHCAND_ID;
		ifo = h5iometa( dsnm, u8_id.size()/PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX, PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_id.size()/PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX, 0, PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX,
						u8_id.size()/PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX, PARAPROBE_FOURIER_META_PHCAND_ID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_id );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_id = vector<unsigned char>();

		dsnm = grpnm + fwslash + PARAPROBE_FOURIER_META_PHCAND_WHAT;
		ifo = h5iometa( dsnm, u8_wh.size()/PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX, PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " create failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " create " << status << "\n";
		offs = h5offsets( 0, u8_wh.size()/PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX, 0, PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX,
						u8_wh.size()/PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX, PARAPROBE_FOURIER_META_PHCAND_WHAT_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, u8_wh );
		if ( status < 0 ) {
			cerr << "Rank " << get_myrank() << " dsnm " << dsnm << " write failed! " << status << "\n"; return false;
		}
		cout << "Rank " << get_myrank() << " dsnm " << dsnm << " write " << status << "\n";
		u8_wh = vector<unsigned char>();
		//instantiate groups for phase-specific results were instantiated at init_targetfile
	}

	string xdmffn = "PARAPROBE.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".MatPoints.xdmf";
	debugxdmf.create_materialpoint_file( xdmffn, mp.matpoints.size(), debugh5Hdl.h5resultsfn );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "WriteMatPointsResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}
*/


/*
bool fourierHdl::write_results2_to_apth5()
{
	double tic = MPI_Wtime();

	if ( workers.size() == 0 ) {
		cerr << "Rank " << get_myrank() << " workers is empty!" << "\n"; return true;
	}
	if ( workers.at(0) == NULL ) {
		cerr << "Rank " << get_myrank() << " MASTER worker does not exist!" << "\n"; return true;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	size_t roffset = 0;

	//##MK::DEBUG simple OpenMP profiling per material point
	string tictoc_fn = "PARAPROBE.Fourier.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".DetailedProfiling.csv";
	ofstream csvlog;
	csvlog.open(tictoc_fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "MPID;PHCANDID;IonsInROI;Rank;ThreadID;DirectFT;FindPks;Fmin;Fmax;DBScanPks\n";
		csvlog << ";;;;;s;s;;;s\n";
		csvlog << "MPID;PHCANDID;IonsInROI;Rank;ThreadID;DirectFT;FindPks;Fmin;Fmax;DBScanPks\n";
	}
	else {
		cerr << "Unable to write process-local profiling files" << "\n";
	}
	int rank = get_myrank();
	//##MK::DEBUG

	for( size_t thr = 0; thr < workers.size(); thr++ ) {
		acquisor* thrhdl = workers.at(thr);
		if ( thrhdl != NULL ) {
			for( auto it = thrhdl->res.begin(); it != thrhdl->res.end(); it++ ) {

				int mpid = it->mpid;
				int phcid = it->phcandid;
				fourier_res* thisone = it->dat;

//cout << "Rank " << get_myrank() << " mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\n";

				//to separate or not the results of different images or use aggregated hyperslabs?
				//++ separation is much more intuitive to read and check for implementation errors, in particular for non experts
				//++ allows for explicit visualization of single material point results
				//-- is less performant because more datasets to create, in particular when doing so using PHDF5
				//##MK::here I opted to use the separated approach
				if ( thisone != NULL ) {

					if ( ConfigFourier::IOFullGridPks == true && thisone->AllPeaks != NULL ) {
//cout << "Rank " << get_myrank() << " IOFullGridPks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->SpecificPeaksTbl.size() << "\n";

						dsnm = PARAPROBE_FOURIER_RES_HKLGRID + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						size_t NIJK = CUBE(static_cast<size_t>(ConfigFourier::HKLGrid.ni));
						ifo = h5iometa( dsnm, NIJK/1, 1 );
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, NIJK/1, 0, 1, NIJK/1, 1);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, thisone->AllPeaks );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
								delete [] thisone->AllPeaks; thisone->AllPeaks = NULL;
//cout << "Rank " << get_myrank() << " wrote SpecificPeak " << dsnm << " status " << status << "\n";
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}
					if ( ConfigFourier::IOSignificantPks == true && thisone->SignificantPeaks.size() > 0 ) {
//cout << "Rank " << get_myrank() << " IOSSignificantPks mpid/phcid/thisone " << mpid << ";" << phcid << ";" << thisone << "\t\t" << thisone->ThreePeaksTbl.size() << "\n";

						dsnm = PARAPROBE_FOURIER_RES_HKLREFL + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//in-place writing of thisone->ThreePeaksTbl
						//##MK::replace struct by
						vector<float> f32val = vector<float>( PARAPROBE_FOURIER_RES_HKLREFL_NCMAX*thisone->SignificantPeaks.size(), 0.f);
						size_t ii = 0;
						for( auto it = thisone->SignificantPeaks.begin(); it != thisone->SignificantPeaks.end(); it++ ) {
							f32val.at(ii) = it->h; ii++;
							f32val.at(ii) = it->k; ii++;
							f32val.at(ii) = it->l; ii++;
							f32val.at(ii) = it->SQRIntensity; ii++;
						}
						thisone->SignificantPeaks = vector<hklspace_res>();
						ifo = h5iometa( dsnm, f32val.size()/PARAPROBE_FOURIER_RES_HKLREFL_NCMAX, PARAPROBE_FOURIER_RES_HKLREFL_NCMAX);
						status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, f32val.size()/PARAPROBE_FOURIER_RES_HKLREFL_NCMAX,
										0, PARAPROBE_FOURIER_RES_HKLREFL_NCMAX,
										f32val.size()/PARAPROBE_FOURIER_RES_HKLREFL_NCMAX, PARAPROBE_FOURIER_RES_HKLREFL_NCMAX);
							status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32val );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote SignificantPeaks " << dsnm << " status " << status << "\n";
								f32val = vector<float>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( ConfigFourier::IODBScanOnSgnPks == true && thisone->Reflectors.size() > 0 ) {
						dsnm = PARAPROBE_FOURIER_RES_HKLDBSCAN + fwslash + "PH" + to_string(phcid) + fwslash + to_string(mpid);
						//##MK::replace struct by
						vector<double> f64 = vector<double>( PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX*thisone->Reflectors.size(), 0.0);
						size_t ii = 0;
						for( auto it = thisone->Reflectors.begin(); it != thisone->Reflectors.end(); it++ ) {
							f64.at(ii) = it->u; ii++;
							f64.at(ii) = it->v; ii++;
							f64.at(ii) = it->w; ii++;
							f64.at(ii) = it->SUMIntensity; ii++;
							f64.at(ii) = it->PksCnts; ii++;
						}
						thisone->Reflectors = vector<reflector>();
						ifo = h5iometa( dsnm, f64.size()/PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX,
								PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX);
						status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
						if ( status == WRAPPED_HDF5_SUCCESS ) {
							offs = h5offsets( 0, f64.size()/PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX,
										0, PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX,
										f64.size()/PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX, PARAPROBE_FOURIER_RES_HKLDBSCAN_NCMAX);
							status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
							if ( status == WRAPPED_HDF5_SUCCESS ) {
//cout << "Rank " << get_myrank() << " wrote Reflector " << dsnm << " status " << status << "\n";
								f64 = vector<double>();
							}
							else {
								cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to store " << dsnm << " status " << status << "\n"; return false;
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << thr << " mpid " << mpid << " failed to create " << dsnm << " status " << status << "\n"; return false;
						}
					}

					if ( csvlog.is_open() == true ) {
						csvlog << mpid << ";" << phcid << ";" <<
								thisone->tictoc[TICTOC_IONSINROI] << ";" << rank << ";" << thisone->tictoc[TICTOC_THREADID] <<
								";" << thisone->tictoc[TICTOC_DIRECTFT] << ";" << thisone->tictoc[TICTOC_FINDPKS] << ";" <<
								thisone->intensity[HKLREFL_FMIN] << ";" << thisone->intensity[HKLREFL_FMAX] << ";" <<
								thisone->tictoc[TICTOC_DBSCANPKS] << "\n";
					}
					//MK::it->dat remains != NULL to indicate we have results for this one
				} //done writing all desired and populated fields for a specific material point and phase
				//no else because if no results then there were no results achieved for the material point maybe because there were no or not sufficient ions to support the analysis
			} //next result from this thread
		}
	} //next thread of this rank

	if ( csvlog.is_open() == true ) {
		csvlog.flush();
		csvlog.close();
	}

	//strict I/O policy, all matpoints with their results need to end up in file, otherwise we already went out with a false
	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "WriteResultsH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}
*/


bool fourierHdl::initialize_hdl()
{
	return true;
}


int fourierHdl::get_myrank()
{
	return this->myrank;
}


int fourierHdl::get_nranks()
{
	return this->nranks;
}


void fourierHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void fourierHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void fourierHdl::init_mpidatatypes()
{

	MPI_Type_contiguous(3, MPI_UNSIGNED, &MPI_Tensor3d_Shape_Type);
	MPI_Type_commit(&MPI_Tensor3d_Shape_Type);

	int cnts[2] = {6*1, 9}; //6, 6*2};
#ifdef EMPLOY_SINGLEPRECISION
	MPI_Aint dsplc[2] = {0, 6*1*MPIIO_OFFSET_INCR_F32 }; //6*2*MPIIO_OFFSET_INCR_F32 + 6*MPIIO_OFFSET_INCR_U32};
	MPI_Datatype otypes[2] = {MPI_FLOAT, MPI_INT}; //MPI_UNSIGNED, MPI_INT};
#else
	MPI_Aint dsplc[2] = {0, 6*1*MPIIO_OFFSET_INCR_F64 }; //6*2*MPIIO_OFFSET_INCR_F64 + 6*MPIIO_OFFSET_INCR_U32};
	MPI_Datatype otypes[2] = {MPI_DOUBLE, MPI_INT}; //UNSIGNED, MPI_INT};
#endif
	MPI_Type_create_struct(2, cnts, dsplc, otypes, &MPI_aabb_px3d_Type);
	MPI_Type_commit(&MPI_aabb_px3d_Type);
}
