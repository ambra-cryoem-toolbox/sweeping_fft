//##MK::GPLV3

#ifndef __AMBRA_FOURIER_ACQUISOR_H__
#define __AMBRA_FOURIER_ACQUISOR_H__

#include "AMBRA_FourierPhaseCandHdl.h"

struct fourier_res_prof
{
	double elapsed;				//time in seconds, ##MK::including memory transfers and computing
	int processID;				//which MPI process solved that one
	unsigned char threadID;		//which thread solved it, 0xfe encodes error
	unsigned char devID;		//which GPU solved it, 0xfe encodes error, 0xFF encodes none
	unsigned char pad1;
	unsigned char pad2;
	fourier_res_prof() : elapsed(0.0), processID(0x00), threadID(0xfe), devID(0xfe), pad1(0x00), pad2(0x00) {}
	fourier_res_prof( const double _dt, const int _rk, const int _thrid ) :
			elapsed(_dt), processID(_rk),
			threadID((_thrid >= 0 && _thrid < (UCHARMX-1) ) ? (unsigned char) _thrid : 0xfe),
			devID(0xff),
			pad1(0x00), pad2(0x00) {}
	fourier_res_prof( const double _dt, const int _rk, const int _thrid, const int _devid ) :
		elapsed(_dt), processID(_rk),
		threadID((_thrid >= 0 && _thrid < (int) (UCHARMX-1) ) ? (unsigned char) _thrid : 0xfe),
		devID((_devid >= 0 && _devid < (int) (UCHARMX-1) ) ? (unsigned char) _devid : 0xfe),
		pad1(0x00), pad2(0x00) {}
};

class acquisor;

class fourier_res;


struct fourier_res_node
{
	fourier_res* dat;			//an individually allocatable storage container for results specific for ROI with ID roiid
	string roiname;				//a specific name to use as a dataset name when storing this result in the HDF5 file
	bio_uint roiid;
	fourier_res_prof ifo;
	
	fourier_res_node() : dat(NULL), roiname(""), roiid(BIMX) {}
	fourier_res_node( const int _roiid ) :
		 dat(NULL), roiname(to_string(_roiid)), roiid(_roiid) {}
};


class fourier_res
{
public:
	fourier_res();
	~fourier_res();
	
/*
	acquisor* owner;
	p3d mp;
	bool healthy;
	void direct_ft_cpu( vector<p3d> const & cand );
	void postprocess_cpu();
	void dbscanpks_cpu();

#ifdef UTILIZE_GPUS
	void direct_ft_gpu( vector<p3d> const & cand );
#endif

	//implicit results arrays per material point
	dft_real* IV;
	dft_real* AllPeaks;
	//##MK::we use C-style arrays here to pass directly to GPUs
	vector<hklspace_res> SignificantPeaks;
	vector<reflector> Reflectors;
	
	dft_real intensity[2];
	*/

	vector<bio_real> magn;
	fftinfo ifo;
	//ambra_fourier_fft_prof tictoc;
};


class acquisor
{
public:
	acquisor();
	~acquisor();

	vector<fourier_res_node> res;
};

#endif
