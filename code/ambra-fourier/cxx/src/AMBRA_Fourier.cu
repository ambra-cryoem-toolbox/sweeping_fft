//##MK::GPLV3

#include "AMBRA_FourierHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2
//optional arguments for debugging purposes
#define CUFFT_MYGRIDRESU						3	//which cube size to take
#define CUFFT_MYBATCHSIZE						4	//how many FFTs to batch per run

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g

#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								0
#endif
//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									str(s)
#define str(s) 									#s


void hello( void )
{
	cout << "ambra-fourier" << "\n";
	cout << xstr(GITSHA) << "\n";
	cout << "Collecting " << CiteMeFourier::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteMeFourier::Citations.begin(); it != CiteMeFourier::Citations.end(); it++ ) {
		cout << it->author << "\n";
		cout << it->title << "\n";
		cout << it->journal << ", " << it->year << ", " << it->vol << ", pp" << it->pages << "\n";
		cout << it->doi << "\n";
	}
}


bool init( int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigFourier::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigFourier::checkUserInput() == false ) {
		cerr << "\n" << "Settings and parameters failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void process_many_local_rois_in_parallel( const int r, const int nr, char** pargv  )
{
	//allocate process-level instance of a fourierHdl. The instance handles all process-internal processing
	//the instances synchronize across and communicate with each other upon executing
	fourierHdl* fourier = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		fourier = new fourierHdl;
		//fourier->initialize_hdl();
		fourier->set_myrank(r);
		fourier->set_nranks(nr);
		fourier->init_mpidatatypes();
		fourier->inputH5Hdl.h5resultsfn = ConfigFourier::Inputfile;
		fourier->outputH5Hdl.h5resultsfn = "AMBRA.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " unable to allocate a fourierHdl instance" << "\n"; localhealth = 0;
	}

	//##MK::add reading of additional data

	//MASTER is always on board, so determine dataset dimensions
	if ( fourier->get_myrank() == MASTER ) {

		if ( fourier->read_imgstack_dims_from_h5() == true ) {
			cout << "Rank MASTER read successfully the dimensions of the image stack" << "\n";
			if ( fourier->broadcast_imgstack_dims() == true ) {
				cout << "Rank MASTER broadcast image stack data successfully" << "\n";
			}
			else {
				cerr << "Rank MASTER failed to broadcast image stack data !" << "\n"; localhealth = 0;
			}
		}
		else {
			cerr << "Rank MASTER was unable to read the dimensions of the image stack !" << "\n"; localhealth = 0;
		}
	}
	else {
		if ( fourier->broadcast_imgstack_dims() == true ) {
			cout << "Rank " << r << " received image stack dimensions successfully" << "\n";
		}
		else {
			cerr << "Rank " << r << " failed to receive image stack dimensions !" << "\n"; localhealth = 0;
		}
	}

	//are we all there and on the same page wrt to dimensions of the image stack?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//decide about the ROI ensemble
	if ( fourier->define_roi_ensemble() == true ) {
		cout << "Rank " << r << " defined the ROI ensemble" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to define the ROI ensemble !" << "\n"; localhealth = 0;
	}

	//and how to distribute it across the ranks
	if ( fourier->distribute_roi_ensemble_on_processes() == true ) {
		cout << "Rank " << r << " collected its workload of ROIs" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to collect its workload of ROIs !" << "\n"; localhealth = 0;
	}

	//we define I/O model 1 as follows: ranks identify which portion of the data from the image stack they need and load these individually
	//instead of the naive all ranks load the entire dataset into main memory
	if ( fourier->define_imgstack_local() == true ) {
		cout << "Rank " << r << " defined the portion of the image stack that this rank is going to process" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to define the portion of the image stack !" << "\n"; localhealth = 0;
	}

	//decide which data the ranks need to load
	//##MK::for now lets load all data here, later ##MK::load only relevant data
	if ( fourier->read_imgstack_values_from_h5() == true ) {
		cout << "Rank " << r << " successfully read its data portion" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to loaded its data portion from the image stack !" << "\n"; localhealth = 0;
	}

	//are we all still good to go and have our image stack data?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//prepare results file, before we might need to dump data on-the-fly
	//we need to create the results file as so many data might be stored that we have to dump some intermediatedly
	if ( fourier->init_target_file() == true ) {
		cout << "Rank " << r << " successfully initialized H5 results file" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to initialize results H5 file !" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete fourier; fourier = NULL; return;
	}

	if ( fourier->write_cpu_environment_and_settings() == true ) {
		cout << "Rank " << r << " wrote details about the hardware environment and tool configuration" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to write hardware and/or tool configuration details !" << "\n"; localhealth = 0;
	}
	if ( fourier->write_metadata() == true ) {
		cout << "Rank " << r << " successfully wrote metadata to H5" << "\n";
	}
	else {
		cerr << "Rank " << r << " was unable to write metadata to H5 !" << "\n"; localhealth = 0;
	}
	
	//bind GPGPU devices to the processes
	#ifdef UTILIZE_GPUS
		//##MK::binding is here implemented at the level of AMBRA_Fourier.cu to avoid that AMBRA_FourierHdl has to call cuda code
		cuda_gpu_bind_process_to_device gpubinder;
		if ( gpubinder.bind_process_to_device( r ) == true ) {
			//MK::we need a handshaking between gpubinder and the fourierHdl class object instance with its own gpubinder
			fourier->gpubinder.ndevices = gpubinder.ndevices;
			fourier->gpubinder.mydevice = gpubinder.mydevice;
			cout << "Rank " << r << " running on device " << fourier->gpubinder.mydevice << " of a total of " << fourier->gpubinder.ndevices << " devices on our computing node !" << "\n";
		}
		else {
			cerr << "Rank " << r << " unable to bind to device" << "\n"; localhealth = 0;
		}
		if ( fourier->write_gpu_environment() == true ) {
			cout << "Rank " << r << " wrote details about its GPU hardware environment" << "\n";
		}
		else {
			cerr << "Rank " << r << " was unable to write details about its GPU hardware !" << "\n"; localhealth = 0;
		}
	#endif

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//not really necessary but use it for now to have all processes starting at the same time
	//##MK::barrier will be necessary as soon as one outputs intermediate results while executing local workpackages
	MPI_Barrier( MPI_COMM_WORLD );

	#ifdef UTILIZE_GPUS	
		//fourier->execute_local_workpackage_cpugpu_debug();
		fourier->execute_local_workpackage_cpugpu_batched_no();
		//fourier->execute_local_workpackage_cpugpu_batched_yes();
	#else
		fourier->execute_local_workpackage_cpu();
	#endif


	cout << "Rank " << r << " has completed its workpackage" << "\n";
	MPI_Barrier(MPI_COMM_WORLD);

	//##MK::naive sequential writing of results
	for( int rk = MASTER; rk < nr; rk++ ) {
		if ( rk == r ) {
			if ( fourier->write_data() == true ) {
				cout << "Rank " << rk << " successfully wrote all its ROI results to H5 file" << "\n";
			}
			else {
				cerr << "Rank " << rk << " encountered errors while writing ROI results to H5 !" << "\n"; localhealth = 0;
			}
			
			if ( fourier->write_profiling() == true ) {
				cout << "Rank " << rk << " successfully wrote all its ROI results to H5 file" << "\n";
			}
			else {
				cerr << "Rank " << rk << " encountered errors while writing ROI results to H5 !" << "\n"; localhealth = 0;
			}
		}
		MPI_Barrier( MPI_COMM_WORLD );
	}

	//check if all process have successfully been able to write their results to the HDF5 file
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes were successful in writing their results to the H5 file" << "\n";
		delete fourier; fourier = NULL; return;
	}

	//fourier->fourier_tictoc.spit_profiling( "Fourier", ConfigShared::SimID, fourier->get_myrank() );

	//release resources
	delete fourier; fourier = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	hello();

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//for this MPI/OMP/CUDA/OpenACC, i.e. process/thread/accelerator heterogeneous program not every combination of mpiexec -n, OMP_NUM_THREADS and GPUs is admissible
	//##MK::if we dont want to use GPUs at all possible
	//##MK::current implementation initiates one MPI process per GPU on the node
	//##MK::problem is that the number of MPI processes per computing node is typically decided with the configuration of the job run script within slurm

//EXECUTE SPECIFIC TASK
	/*
	//test IntelMKL 3D FFT
	#pragma omp parallel
	{
		#pragma omp for
		for( int thr = MASTER; thr < omp_get_num_threads(); thr++ ) {
			double mytic = omp_get_wtime();

			fft3d_mkl_cpu my_mkl_debug;
			my_mkl_debug.init( ConfigShared::SimID );
			my_mkl_debug.forwardFFT();

			double myto c = omp_get_wtime();
			#pragma omp critical
			{
				cout << "Rank " << r << " thread " << thr << " did an own FFT3D using IntelMKL in " << (mytoc-mytic) << " !" << "\n";
			}
		}
	}
	*/

	/*
	//test CUDA cuFFT 3D FFT call
	double mytic = 0.0;
	double mytoc = 0.0;
	int mpiError = 0;
	cudaError_t cuError = cudaSuccess;
	
	int n_cuda_debug_gridresu = 32;
	int n_cuda_debug_batchsize = 1;
	if(argc > 2) {
		n_cuda_debug_gridresu = atoi(argv[CUFFT_MYGRIDRESU]);
	}
	if (argc > 3) {
		n_cuda_debug_batchsize = atoi(argv[CUFFT_MYBATCHSIZE]);
	}

	cuError = cudaGetLastError();
	if (cuError != cudaSuccess) {
		cerr << "Rank " << r << " task 0 " << cudaGetErrorString(cuError) << "\n";
		MPI_Abort(MPI_COMM_WORLD, mpiError); return 1;
	}
	
	//get device infos and bind device to process
	cuda_gpu_bind_process_to_device gpubinder;
	if ( gpubinder.bind_process_to_device( r ) == true ) {
		cout << "Rank " << r << " running on device " << gpubinder.mydevice << "\n";
	}
	else {
		cerr << "Rank " << r << " unable to bind to device" << "\n";
		MPI_Abort(MPI_COMM_WORLD, mpiError); return 1;
	}

	//run a small cuFFT 3D test kernel
	cout << "Rank " << r << " starting the cuFFT test program... " << "\n";
	*/
	/*
	//example for a single FFT without batching
	vector<bio_real> mydata = vector<bio_real>( n*n*n, 0.0 );
	cout << "Rank " << r << " computing a out-of-place forward FFT real to complex for n = " << n << " mydata.size() " << mydata.size() << "\n";
	//fft3d_cuda_gpu* fft_gpu = new fft3d_cuda_gpu();
	fft3d_cuda_gpu fft_gpu = fft3d_cuda_gpu();
	fft_gpu.init( n, mydata );
	fft_gpu.forwardFFT();
	//delete fft_gpu;
	*/		
	
	//example for two batched FFTs that reutilize the same plan, so only one time we pay the plan creation costs
	/*
	mytic = MPI_Wtime();
	cout << "Rank " << r << " computing two batches of out-of-place forward FFTs real to complex for n = " << n_cuda_debug_gridresu << " b = " << n_cuda_debug_batchsize << "\n";
	
	fft3d_cuda_gpu fft_gpu = fft3d_cuda_gpu();
	
	fft_gpu.init_plan_batch( n_cuda_debug_gridresu, n_cuda_debug_batchsize );
	fft_gpu.reset_plan_batch();
	
	mytoc = MPI_Wtime();
	cout << "Rank " << r << " completed the preparation of the batched FFT plan " << (mytoc-mytic) << " s" << "\n";
	
	mytic = MPI_Wtime();
	vector<bio_real> mydata1 = vector<bio_real>( CUBE(n_cuda_debug_gridresu)*n_cuda_debug_batchsize, 1.0 );
	cout << "Rank " << r << " mydata1.size() " << mydata1.size() << "\n";
	for( int bid = 0; bid < n_cuda_debug_batchsize; bid++ ) {
		fft_gpu.fill_data_batch( mydata1, bid );
	}
	fft_gpu.move_plan_batch();
	fft_gpu.exec_plan_batch();
	//do something with the results
	fft_gpu.exec_magn_batch();
	
	fft_gpu.reset_plan_batch();
	mydata1 = vector<bio_real>();
	mytoc = MPI_Wtime();
	cout << "Rank " << r << " completed the cuFFT part 1 took " << (mytoc-mytic) << " s" << "\n";
	
	//reuse the plan while doing a second batch
	mytic = MPI_Wtime();
	vector<bio_real> mydata2 = vector<bio_real>( CUBE(n_cuda_debug_gridresu)*n_cuda_debug_batchsize, 2.0 );
	cout << "Rank " << r << " mydata2.size() " << mydata2.size() << "\n";
	for( int bid = 0; bid < n_cuda_debug_batchsize; bid++ ) {
		fft_gpu.fill_data_batch( mydata2, bid );
	}
	fft_gpu.move_plan_batch();
	fft_gpu.exec_plan_batch();
	//do something with the results
	
	fft_gpu.reset_plan_batch();
	mydata2 = vector<bio_real>();
	mytoc = MPI_Wtime();
	cout << "Rank " << r << " completed the cuFFT part 2 took " << (mytoc-mytic) << " s" << "\n";
	*/
	
	//do the actual computations

	process_many_local_rois_in_parallel( r, nr, argv );

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	double toc = omp_get_wtime();
	cout << "ambra-fourier took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}
