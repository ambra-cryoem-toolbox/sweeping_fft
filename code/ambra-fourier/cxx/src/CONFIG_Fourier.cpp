//##MK::GPLV3

#include "CONFIG_Fourier.h"


ostream& operator<<(ostream& in, hklgrid const & val)
{
	in << "imin / istep / imax / ni = " << val.imi << ";" << val.istep << ";" << val.imx << ";" << val.ni <<  "\n";
	in << "jmin / jstep / jmax / nj = " << val.jmi << ";" << val.jstep << ";" << val.jmx << ";" << val.nj <<  "\n";
	in << "kmin / kstep / kmax / nk = " << val.kmi << ";" << val.kstep << ";" << val.kmx << ";" << val.nk <<  "\n";
	return in;
}


bool roi_cuboid::is_inside( const int ix, const int iy, const int iz )
{
	if ( ix < this->origin.x )
		return false;
	if ( ix >= this->origin.x + this->nx )
		return false;
	if ( iy < this->origin.y )
		return false;
	if ( iy >= this->origin.y + this->ny )
		return false;
	if ( iz < this->origin.z )
		return false;
	if ( iz >= this->origin.z + this->nz )
		return false;
	return true;
}


size_t roi_cuboid::nxyz()
{
	return static_cast<size_t>(this->nx) * static_cast<size_t>(this->ny) * static_cast<size_t>(this->nz);
}


ostream& operator<<(ostream& in, roi_cuboid const & val)
{
	in << "px3d origin x,y,z = " << val.origin.x << ";" << val.origin.y << ";" << val.origin.z << "\n";
	in << "px3d length nx, ny, nz = " << val.nx << ";" << val.ny << ";" << val.nz << "\n";
	return in;
}	

string ConfigFourier::Inputfile = "";
WINDOWING_METHOD ConfigFourier::WindowingMethod = HANN_HAMMING; //RECTANGULAR;
bio_real ConfigFourier::WindowingHannHammingAlpha = 25.0 / 46.0; //##MK::https://ccrma.stanford.edu/~jos/sasp/Hamming_Window.html
bio_real ConfigFourier::WindowingHannHammingBeta = 21.0 / 46.0;
bio_real ConfigFourier::VoxelPhysicalEdgeLength = 1.0;
FOURIER_METHOD ConfigFourier::FourierMethod = FOURIER_DFT_FAST;
DIMENSIONALITY ConfigFourier::Dimensionality = THREE_DIMENSIONS;
ROI_ENSEMBLE_METHOD ConfigFourier::ROIEnsemble = SPECIFIC_POSITIONS;
vector<px3d> ConfigFourier::ROIUserPositions = vector<px3d>();
px3d ConfigFourier::ROIUserGridDimensions = px3d( 0, 0, 0 );
ROI_TYPE ConfigFourier::ROIType = CUBOID;
//roi_cuboid ConfigFourier::ROICuboid = roi_cuboid();

bool ConfigFourier::ComputeFastFourierTransform = false;
bool ConfigFourier::ComputeDirectFourierTransform = false;
bool ConfigFourier::IOStoreFFTMagnitude = false;
bool ConfigFourier::IOStoreFFTComplexValues = false;
bool ConfigFourier::IOStoreFFTProfiling = false;
bool ConfigFourier::IOStoreToolProfiling = false;

unsigned int ConfigFourier::ROIFirstIndex = 0;
unsigned int ConfigFourier::ImageStackGuardWidth = 1; //culling the image stack by a pixel shell
unsigned int ConfigFourier::ROIPaddingWidth = 1; //culling the ROI image stack sub-set by a pixel shell for zero padding prior FFT
unsigned int ConfigFourier::ROICuboidMinEdgeLength = 10;

unsigned short ConfigFourier::GPUsPerProcess = 1; //##MK::currently only 1 supported by the implementation
unsigned short ConfigFourier::GPUsPerComputingNode = 0; //0, no GPUs available, currently only GPUs supported
int ConfigFourier::MKLGetNumThreads = 1; //MK::needs to be 1 because OpenMP threads solve always only one FFT at a time, disjoint ROIs!


bool ConfigFourier::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("AMBRA.Fourier.SimID.0.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigFourier")) {
		cerr << "Undefined settings and parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	
	Inputfile = read_xml_attribute_string( rootNode, "Inputfile" );
	VoxelPhysicalEdgeLength = read_xml_attribute_real( rootNode, "VoxelPhysicalEdgeLength" );

	WindowingMethod = HANN_HAMMING;
	arg = read_xml_attribute_uint32( rootNode, "FourierMethod" );
	switch (arg)
	{
		case FOURIER_DFT_FAST:
			FourierMethod = FOURIER_DFT_FAST; break;
		default:
			FourierMethod = FOURIER_NONE;
			cerr << "Choose a valid FourierMethod, currently only option 1, fast fourier transform supported !" << "\n";
			return false;
	}
	
	arg = read_xml_attribute_uint32( rootNode, "Dimensionality" );
	switch (arg)
	{
		case 3:
			Dimensionality = THREE_DIMENSIONS; break;
		default:
			Dimensionality = ZERO_DIMENSIONS;
			cerr << "Choose a valid Dimensionality, currently only option 3, three-dimensional transforms supported !" << "\n";
			return false;
	}

	//##MK::parse ROIType models and length
	arg = read_xml_attribute_uint32( rootNode, "ROIEnsemble" );
	switch (arg)
	{
		case 1:
			ROIEnsemble = SPECIFIC_POSITIONS; break;
		case 2:
			ROIEnsemble = BUILD_A_GRID; break;
		default:
			cerr << "Choose a valid ROIEnsemble, currently only 1 (give positions) and 2 (build grid) supported !" << "\n";
			return false;
	}
	if ( ROIEnsemble == SPECIFIC_POSITIONS ) {
		xml_node<>* comb_node = rootNode->first_node("ROIUserLocations");
		ROIUserPositions = vector<px3d>();
		for (xml_node<> * entry_node = comb_node->first_node("entry"); entry_node; entry_node = entry_node->next_sibling() ) {
			unsigned long xx = stoul(entry_node->first_attribute("posx")->value());
			unsigned long yy = stoul(entry_node->first_attribute("posy")->value());
			unsigned long zz = stoul(entry_node->first_attribute("posz")->value());
			//##MK::add adaptive lengthx, y, z
			if ( xx >= 0 && xx < static_cast<unsigned long>(INT32MX) &&
					yy >= 0 && yy < static_cast<unsigned long>(INT32MX) &&
						zz >= 0 && zz < static_cast<unsigned long>(INT32MX) ) {
				ROIUserPositions.push_back( px3d( static_cast<int>(xx), static_cast<int>(yy), static_cast<int>(zz) ) );
cout << "Adding a ROI at " << xx << ";" << yy << ";" << zz << "\n";
			}
			else {
				cerr << "ROIUserPosition " << xx << ";" << yy << ";" << zz << " is invalid input, choose smaller and positive coordinate values !" << "\n";
				return false;
			}
			//##MK::add user input checks
		}
	}

	/*
	stringstream parsethis;
	parsethis << ConfigFourier::SamplingPosition;
	string datapiece;
	getline( parsethis, datapiece, ';');	here.x = stof( datapiece );
	getline( parsethis, datapiece, ';');	here.y = stof( datapiece );
	getline( parsethis, datapiece, ';');	here.z = stof( datapiece );
	*/

	ROIType = CUBOID;
	xml_node<>* comb_node = rootNode->first_node("ROIUserGrid");
	for (xml_node<> * entry_node = comb_node->first_node("entry"); entry_node; entry_node = entry_node->next_sibling() ) {
		unsigned long nx = stoul(entry_node->first_attribute("lengthx")->value());
		unsigned long ny = stoul(entry_node->first_attribute("lengthy")->value());
		unsigned long nz = stoul(entry_node->first_attribute("lengthz")->value());
		if ( nx >= ConfigFourier::ROICuboidMinEdgeLength && nx < static_cast<unsigned long>(INT32MX) &&
				ny >= ConfigFourier::ROICuboidMinEdgeLength && ny < static_cast<unsigned long>(INT32MX) &&
					nz >= ConfigFourier::ROICuboidMinEdgeLength && nz < static_cast<unsigned long>(INT32MX) ) {
			ROIUserGridDimensions = px3d( static_cast<int>(nx), static_cast<int>(ny), static_cast<int>(nz) );
cout << "Adding user grid dimensions " << ROIUserGridDimensions << "\n";
		}
		else {
			cerr << "ROIUserGridDimensions " << nx << ";" << nz << ";" << nz << " is invalid input, choose at least 10 but not more than the respective edge lengths of the image stack !" << "\n";
			return false;
		}
	}
	/*
	//##MK::deprecated as of 2020-11-05
	px3d org = px3d( 50, 50, 50 );
	ROICuboid = roi_cuboid( org, 100 );
	*/
	
	if ( FourierMethod == FOURIER_DFT_FAST ) {
		ComputeFastFourierTransform = true;
		ComputeDirectFourierTransform = false;
	}
	IOStoreFFTMagnitude = read_xml_attribute_bool( rootNode, "IOStoreFFTMagnitude" );
	IOStoreFFTComplexValues = read_xml_attribute_bool( rootNode, "IOStoreFFTComplexValues" );
	IOStoreFFTProfiling = read_xml_attribute_bool( rootNode, "IOStoreFFTProfiling" );
	IOStoreToolProfiling = read_xml_attribute_bool( rootNode, "IOStoreToolProfiling" );
	ImageStackGuardWidth = 1;
	ROIPaddingWidth = 1;
	GPUsPerComputingNode = read_xml_attribute_uint16( rootNode, "GPUsPerComputingNode" );
		
	return true;
}


bool ConfigFourier::checkUserInput()
{
	cout << "ConfigFourier::" << "\n";
	cout << "Inputfile " << Inputfile << "\n";
	if ( VoxelPhysicalEdgeLength < EPSILON ) {
		cerr << "VoxelPhysicalEdgeLength should be at least " << EPSILON << "\n";
	}
	cout << "VoxelPhysicalEdgeLength " << VoxelPhysicalEdgeLength << "\n";
	cout << "WindowingMethod " << WindowingMethod << "\n";
	//switch(WindowingMethod) {
	if ( WindowingMethod == HANN_HAMMING ) {
	//	case HANN_HAMMING:
			cout << "WindowingHannHammingAlpha " << WindowingHannHammingAlpha << "\n";
			cout << "WindowingHannHammingBeta " << WindowingHannHammingBeta << "\n";
	//		break;
	}
	cout << "FourierMethod " << FourierMethod << "\n";
	cout << "Dimensionality " << Dimensionality << "\n";
	cout << "ROIEnsemble " << ROIEnsemble << "\n";
	cout << "ROIType " << ROIType << "\n";
	//##MK::add locations of the ROIs and user grid dimensions
	//cout << "ROICuboid " << ROICuboid << "\n";
	cout << "ComputeFastFourierTransform " << ComputeFastFourierTransform << "\n";
	cout << "ComputeDirectFourierTransform " << ComputeDirectFourierTransform << "\n";
	cout << "IOStoreFFTMagnitude " << IOStoreFFTMagnitude << "\n";
	cout << "IOStoreFFTComplexValues " << IOStoreFFTComplexValues << "\n";
	cout << "IOStoreFFTProfiling " << IOStoreFFTProfiling << "\n";
	cout << "IOStoreToolProfiling " << IOStoreToolProfiling << "\n";
	cout << "ROIFirstIndex " << ROIFirstIndex << "\n";
	cout << "ImageStackGuardWidth " << ImageStackGuardWidth << "\n";
	cout << "ROIPaddingWidth " << ROIPaddingWidth << "\n";
	if ( GPUsPerProcess != 1 ) {
		cerr << "User mentioned that a different number of GPUs than 1 is assigned to 1 MPI Process, but this is not supported currently !" << "\n";
		return false;
	}
	cout << "GPUsPerProcess " << GPUsPerProcess << "\n";
	if ( GPUsPerComputingNode == 0 ) {
		cerr << "User mentioned that there are no GPUs on the computing node, but this is not supported currently !" << "\n";
		return false;
	}
	if ( GPUsPerComputingNode > 2 ) {
		cerr << "User mentioned that there are more than two GPUs on the computing node, but this is not supported currently !" << "\n";
		return false;
	}
	cout << "GPUsPerComputingNode " << GPUsPerComputingNode << "\n";
	if ( MKLGetNumThreads != 1 ) {
		cerr << "MKLGetNumThreads needs to be 1 because currently we do not allow for nested parallel solving of single FFTs via the IMKL library !" << "\n";
		return false;
	}
	cout << "MKLGetNumThreads " << MKLGetNumThreads << "\n";
	cout << "\n";

	return true;
}


void ConfigFourier::reportSettings( vector<pparm> & res )
{
	res.push_back( pparm( "Inputfile", Inputfile, "", "image dataset for which local Fourier transformations are performed" ) );
	res.push_back( pparm( "VoxelPhysicalEdgeLength", real2str(VoxelPhysicalEdgeLength), "", "which physical material length does the edge of a single voxel probe" ) );
	res.push_back( pparm( "WindowingMethod", sizet2str(WindowingMethod), "", "which type of windowing is used" ) );
	if ( WindowingMethod == HANN_HAMMING ) {
		res.push_back( pparm( "WindowingHannHammingAlpha", real2str(WindowingHannHammingAlpha), "", "scaling constant alpha for the Hann-Hamming window" ) );
		res.push_back( pparm( "WindowingHannHammingBeta", real2str(WindowingHannHammingBeta), "", "scaling constant beta for the Hann-Hamming window" ) );
	}
	res.push_back( pparm( "FourierMethod", sizet2str(FourierMethod), "", "type of transformations" ) );
	res.push_back( pparm( "Dimensionality", sizet2str(Dimensionality), "", "dimensionality of the transformations" ) );
	res.push_back( pparm( "ROIEnsemble", sizet2str(ROIEnsemble), "", "how are ROI(s) chosen" ) );
	res.push_back( pparm( "ROIType", sizet2str(ROIType), "", "what shape of ROI(s) are used" ) );
	//##MK::add details about the ROI, or better in the HDF5 output
	res.push_back( pparm( "ComputeFastFourierTransform", bool2str(ComputeFastFourierTransform), "bool", "where fast Fourier transformations computed" ) );
	res.push_back( pparm( "ComputeDirectFourierTransform", bool2str(ComputeDirectFourierTransform), "bool", "where direct Fourier transformations computed" ) );
	res.push_back( pparm( "IOStoreFFTMagnitude", bool2str(IOStoreFFTMagnitude), "bool", "reporting magnitude results per ROI" ) );
	res.push_back( pparm( "IOStoreFFTComplexValues", bool2str(IOStoreFFTComplexValues), "bool", "reporting complex value results per ROI" ) );
	res.push_back( pparm( "IOStoreFFTProfiling", bool2str(IOStoreFFTProfiling), "bool", "reporting FFT profiling for each ROI resolved per MPI process (rank)" ) );
	res.push_back( pparm( "IOStoreToolProfiling", bool2str(IOStoreToolProfiling), "bool", "reporting tool profiling resolved per MPI process (rank)" ) );

	res.push_back( pparm( "ROIFirstIndex", uint2str(ROIFirstIndex), "", "with which integer do we start naming the ROIs" ) );
	res.push_back( pparm( "ImageStackGuardWidth", uint2str(ImageStackGuardWidth), "px", "how many pixel guard about image stack" ) );
	res.push_back( pparm( "ROIPaddingWidth", uint2str(ROIPaddingWidth), "px", "how many pixel zero padding culling the ROI" ) );
	res.push_back( pparm( "ROICuboidMinEdgeLength", uint2str(ROICuboidMinEdgeLength), "px", "how many pixel edge length does each cuboidal ROI need to have at least" ) );
	res.push_back( pparm( "GPUsPerProcess", uint2str(GPUsPerProcess), "1", "how many GPUs each MPI process controls" ) );
	res.push_back( pparm( "GPUsPerComputingNode", uint2str(GPUsPerComputingNode), "1", "how many GPUs there are on each computing node" ) );
	res.push_back( pparm( "MKLGetNumThreads", int2str(MKLGetNumThreads), "", "how many threads were use for the Intel Math Kernel library" ) );
	//res.push_back( pparm( "GPUWorkload", uint2str(GPUWorkload), "", "how many ROIs per GPU and epoch" ) );
}

