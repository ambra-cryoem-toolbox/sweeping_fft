//##MK::GPLV3

#ifndef __AMBRA_FOURIER_CPU_INTERFACE_H__
#define __AMBRA_FOURIER_CPU_INTERFACE_H__

#include "AMBRA_FourierIMKLInterface.h"

/*
class dft_cpu
{
	//the class is a utility tool it does the actual direct Fourier transform on the CPU
public:
	dft_cpu();
	dft_cpu( const size_t n );
	~dft_cpu();
	
	hklval execute( vector<dft_real> const & p );
	void reset();
		
private:
	dft_real* Fmagn;				//magnitude of the Fourier space intensity
	dft_real* ival;					//Fourier space grid position 1d of a 3d cube hkl <=> ijk
	int NI;							//Fourier space number of voxel 1d
	int NIJ;
	int NIJK;	
};
*/

#endif
