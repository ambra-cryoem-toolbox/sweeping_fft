//##MK::GPLV3

#ifndef __AMBRA_FOURIER_HDL_H__
#define __AMBRA_FOURIER_HDL_H__

#include "AMBRA_FourierAcquisor.h"


class fourierHdl
{
	//process-level class which implements the worker instance at the level of a single MPI process
	//internally using OpenMP and CUDA or OpenACC i.e. CPU and GPUs

public:
	fourierHdl();
	~fourierHdl();
	
	bool read_imgstack_dims_from_h5();
	bool broadcast_imgstack_dims();
	
	bool define_roi_ensemble();
	bool distribute_roi_ensemble_on_processes();

	bool define_imgstack_local();
	bool read_imgstack_values_from_h5();

	/*
	bool broadcast_input();
	bool define_phase_candidates();
	void spatial_decomposition();
	*/

	/*
	gpu_cpu_max_workload plan_max_per_epoch( const int ngpu, const int nthr, const int nmp );
	gpu_cpu_now_workload plan_now_per_epoch( const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl );
	*/
	
	//void generate_debug_roi( mt19937 & mydice, vector<dft_real> * out );
	/*
	void query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out );
	void debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing );
	void execute_local_workpackage1();
	void execute_local_workpackage2();
	*/
	void execute_local_workpackage_cpu();
	#ifdef UTILIZE_GPUS
		void execute_local_workpackage_cpugpu_debug();
		void execute_local_workpackage_cpugpu_batched_no();
		void execute_local_workpackage_cpugpu_batched_yes();
	#endif
	
	bool init_target_file();
	bool write_cpu_environment_and_settings();
	bool write_metadata();
	bool write_gpu_environment();
	bool write_data();
	bool write_profiling();

	/*
	bool write_roi_results_to_h5();
	*/
	
	bool initialize_hdl();
	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	/*
	rangeTable rng;
	itypeCombiHdl itsk;
	vector<PhaseCandidate> cands;
	unsigned char maximum_iontype_uc;
	*/
	
	/*
	decompositor sp;
	volsampler mp;
	vector<int> mp2rk;								
	vector<int> mp2me;
	*/
	vector<roi_cuboid> rois;						//location of the ROIs, ROIs in principle could overlap
	vector<size_t> myroiIDs;						//IDs on rois this rank has to solve
	map<unsigned int, int> rois2rk;					//##MK::bio_uint, mapping of ROIs to ranks

	//xdmfHdl debugxdmfHdl;
	h5Hdl inputH5Hdl;
	fourier_h5 outputH5Hdl;
	fourier_xdmf debugxdmf;

	//data and metadata of the image stack which the tool processes
	discretized_volume imgstack;
	//aabb_px3d imgstack_global;
	//aabb_px3d imgstack_local;
	//##MK::mapping between the two
	vector<bio_real> img_local;

	/*
	vector<unsigned char> ityp_org;					//iontypes ranged, original
	//vector<p3dm1> xyz_ityp;						//##MK::temporarily until proper ion type handling implemented
	vxlizer ca;
	vector<mp3d> myworkload;						//the material points of the process
	vector<hklval> myhklval;						//##MK::DEBUG for now the actual data of interest for science
	vector<MPI_Fourier_ROI> mproi;					//only significant for MASTER to organize results writing
	vector<MPI_Fourier_HKLValue> mpres;				//only significant for MASTER to organize results writing
	*/

	vector<acquisor*> workers;						//every OpenMP thread keeps an own to improve memory locality
													//(results from ROIs will be stored preferentially in memory close to the core that processed these ROIs)

	rank_prof h5tictoc;
	profiler fourier_tictoc;
	cuda_gpu_bind_process_to_device gpubinder;		//CUDA callee getting information about available devices at runtime and bind MPI process to specific device

private:
	//MPI related
	int myrank;										//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	MPI_Datatype MPI_Tensor3d_Shape_Type;
	MPI_Datatype MPI_aabb_px3d_Type;
};


#endif

