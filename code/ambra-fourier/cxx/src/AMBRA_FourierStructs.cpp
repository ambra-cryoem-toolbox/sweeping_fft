//##MK::GPLV3

#include "AMBRA_FourierStructs.h"


ostream& operator<<(ostream& in, MPI_Tensor3d_Shape const & val)
{
	in << val.nc << ";" << val.nr << ";" << val.nl << "\n";
	return in;
}
