//##MK::GPLV3

#include "AMBRA_FourierTicToc.h"

rank_prof::rank_prof()
{
	elapsedtime_dict = {
		{FOURIER_HDF_READIMAGEDIMS, "HDF5::ReadImagestackDimensions"},
		{FOURIER_MPI_BCASTDIMS, "BroadcastImagestackDimensions"},
		{FOURIER_DEFINE_ROIS, "DefineROIEnsemble"},
		{FOURIER_DISTRIBUTE_ROIS, "DistributeROIEnsemble"},
		{FOURIER_DEFINE_IMAGELOCAL, "DefineImagestackLocal"},
		{FOURIER_HDF_READIMAGEVAL, "HDF5::ReadImagestackValues"},
		{FOURIER_HDF_INITTARGET, "HDF5::InitTargetFile"},
		{FOURIER_HDF_WRITECONFIG, "HDF5::WriteEnvAndConfig"},
		{FOURIER_HDF_WRITEMETADATA, "HDF5::WriteMetadata"},
		{FOURIER_EXECUTE_ROIS, "ExecuteLocalWorkpackage"},
		{FOURIER_HDF_WRITEDATA, "HDF5::WriteData"}  };
		//{12, "Total"}   };
}


rank_prof::~rank_prof()
{
}
