//##MK::GPLV3

#include "AMBRA_FourierHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


fourier_h5::fourier_h5()
{
}


fourier_h5::~fourier_h5()
{
}


int fourier_h5::create_fourier_h5( const string h5fn, const int nranks )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Create fourier h5 file creation failed!" << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	vector<string> grp_dict{ FOURIER, FOURIER_META,
		                     FOURIER_META_HRDWR, FOURIER_META_SFTWR, FOURIER_META_PROF,
							 FOURIER_META_ROIS, FOURIER_META_FFT, FOURIER_META_FFTPROF,
							 FOURIER_RES, FOURIER_RES_ROIS, FOURIER_RES_ROIS_FFTMAGN };
	//order is relevant
	//because upon H5 group creation we need to follow the group hierarchy strictly to the lower and lower levels, this is different compared to h5py

	//generate an AMBRA HDF5 file for results
	//domain specific HDF5 keywords and data fields
	string fwslash = "/";

	for( auto it = grp_dict.begin(); it != grp_dict.end(); it++ ) {
		groupid = H5Gcreate2(fileid, it->c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group " << *it << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group " << *it << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
	}
	
	//add one group per rank for profiling data how long general sections of the entire tool took under FOURIER_META_PROF
	for( int rk = MASTER; rk < nranks; rk++ ) {
		string grpnm = FOURIER_META_PROF + fwslash + to_string(rk);
		groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group " << grpnm << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group " << grpnm << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
		vector<string> subgrps_dict{ FOURIER_META_PROF_DT, FOURIER_META_PROF_VMEM, FOURIER_META_PROF_RMEM };
		for( auto it = subgrps_dict.begin(); it != subgrps_dict.end(); it++ ) {
			grpnm = FOURIER_META_PROF + fwslash + to_string(rk) + fwslash + *it;
			groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			if ( groupid < 0 ) {
				cerr << "Create group " << grpnm << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
			}
			status = H5Gclose(groupid);
			if ( status < 0 ) {
				cerr << "Close group " << grpnm << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
			}
		}

		if ( ConfigFourier::IOStoreFFTProfiling == true ) {
			//add one group per rank for profiling data how long solving the FFTs took under FOURIER_META_FFTPROF
			grpnm = FOURIER_META_FFTPROF + fwslash + to_string(rk);
			groupid = H5Gcreate2(fileid, grpnm.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
			if ( groupid < 0 ) {
				cerr << "Create group " << grpnm << " failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
			}
			status = H5Gclose(groupid);
			if ( status < 0 ) {
				cerr << "Close group " << grpnm << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
			}
		}
	}

	status = H5Fclose(fileid);
	if ( status < 0 ) {
cerr << "WRAPPED_HDF5_FILEACCESS_FAILED" << "\n";
		return WRAPPED_HDF5_FILEACCESS_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}
