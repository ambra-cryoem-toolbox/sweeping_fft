#!/bin/bash -l
#SBATCH -o ./AMBRA.Fourier.SimID.1.STDOUT.%j
#SBATCH -e ./AMBRA.Fourier.SimID.1.STDERR.%j
#SBATCH -D ./
#SBATCH -J ambra_fourier

#SBATCH --partition=p.talos
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=20
#SBATCH --gres=gpu:2
#SBATCH --mail-user=m.kuehbach@mpie.de
#SBATCH --time=00:15:00

##use the s.talos queue if you need only a fraction of a node (single gpu/few cores)
##use the p.talos queue if you need full node, this is always exclusive usage 
###SBATCH --exclusive
##how many MPI processes control via if assuming -ntasks-per-node means one MPI process per node
##-nodes=80 e.g. use all 80 nodes of TALOS with one MPI process per node
##how many OpenMP threads at most per node
##-cpus-per-task=40 e.g. use all 40 threads i.e. one per HT core pair, using in total 80x40 cores
##how many GPUs
##--gres=gpu:2

module purge
module load cmake/3.18
module load cuda
module load intel
module load impi
module load mkl
echo "Current modules loaded"
module list

echo "OpenMP NUM THREADS env option"
if [ ! -z $SLURM_CPUS_PER_TASK ] ; then
	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
else
	export OMP_NUM_THREADS=1
fi
echo $OMP_NUM_THREADS
echo "OpenMP PLACES env option"
export OMP_PLACES=cores
echo $OMP_PLACES

###echo "OpenMP NESTED env option"
###export OMP_NESTED=true
###echo $OMP_NESTED

echo "MKL NUM THREADS env option"
export MKL_NUM_THREADS=1
echo $MKL_NUM_THREADS

echo "CUDA VISIBLE DEVICES env option"
export CUDA_VISIBLE_DEVICES=0,1
echo $CUDA_VISIBLE_DEVICES

##set unlimited stack memory
ulimit -s unlimited
echo "ulimit env option"
##set no core files
##ulimit -S -c 0
##display ulimit settings
ulimit

##list allocated TALOS resources nodes
echo $SLURM_JOB_NODELIST
##echo $SLURM_NODELIST
echo $SLURM_JOB_NUM_NODES
##echo $SLURM_NNODES

echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"


srun ambra_fourier 1 AMBRA.Fourier.SimID.1.xml











#########OBSOLETE
####for pgi/18
##export PGI_CURR_CUDA_HOME=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10/linux86-64/2018/cuda/10.0
##echo $PGI_CURR_CUDA_HOME
##echo "PGI home folder"
##export PGI=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10
##echo $PGI
##export MKL_NUM_THREADS=1
##echo $MKL_NUM_THREADS
##echo $OMP_PLACES
##export LIBRARY_PATH=/mpcdf/soft/SLE_15/packages/x86_64/intel_parallel_studio/2018.4/mkl/lib/intel64_lin/
##echo $LIBRARY_PATH

