cmake_minimum_required(VERSION 3.18.4 FATAL_ERROR)

################################################################################################################################
##DEVELOPER SECTION#############################################################################################################
##in this section software developers need to make changes when debugging#######################################################
################################################################################################################################ 

#please name your project accordingly
set(MYPROJECTNAME "ambra_fourier")

#set build type to release by default for those cases in which the user 
#neither specifies debug nor release build mode
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

##pull general information about external libraries and paths related to ambra
#message([STATUS] "MYGITSHA: ${MYGITSHA}")
#string(REGEX REPLACE "-" "_" MYCLEANGITSHA ${MYGITSHA}) #'-' is an invalid character in a C preprocessor macro
#message([STATUS] "MYCLEANGITSHA: ${MYCLEANGITSHA}")
##add_definitions( -DGITSHA=${MYGITSHA} )
#add_definitions( -DGITSHA=${MYCLEANGITSHA} )

include("../../AMBRA.ExternalLibraries.cmake")
if(USE_MPI30)
	message([STATUS] "We are working on MAWS30")
endif()
if(USE_TALOS)
	message([STATUS] "We are compiling on TALOS")
endif()
message([STATUS] "MYPROJECTPATH: ${MYPROJECTPATH}")
message([STATUS] "MYUTILSPATH: ${MYUTILSPATH}")
message([STATUS] "MYHDF5PATH: ${MYHDFPATH}")

#compose a tool-specific path
set(MYTOOLPATH "${MYPROJECTPATH}/ambra-fourier/")
message([STATUS] "MYTOOLPATH: ${MYTOOLPATH}")

#identify which compiler to use
message([STATUS] "MYCCC_COMPILER: __${CMAKE_C_COMPILER}__")
message([STATUS] "MYCXX_COMPILER: __${CMAKE_CXX_COMPILER}__")
#if(${CMAKE_C_COMPILER} STREQUAL "mpiicc" AND ${CMAKE_CXX_COMPILER} STREQUAL "mpiicpc")
	message([STATUS] "We compile with the GNU C/C++ and the Nvidia CUDA nvcc compilers")
	project( ${MYPROJECTNAME} LANGUAGES CXX CUDA )
#endif()

#define which library dependencies exist
set(EMPLOY_MYHDF5 ON)
set(EMPLOY_MYIMKL ON)
set(EMPLOY_MYCUDA ON)

#define which parallelization layers are used
set(EMPLOY_PARALLELISM_MPI ON)
set(EMPLOY_PARALLELISM_OMP ON)

################################################################################################################################
##END OF INTERACTION FOR NON PRO USERS##########################################################################################
##here advanced users might want/need to make modifications if they use non default places for thirdparty libraries#############
################################################################################################################################ 
#HDF5 local installation for advanced I/O, collecting metadata and bundle analysis results together
if(EMPLOY_MYHDF5)
	message([STATUS] "We use the HDF5 library for advanced I/O")
	include_directories("${MYHDFPATH}/include")
	link_directories("${MYHDFPATH}/lib")
	##link against static libraries
	set(MYHDFLINKFLAGS "-L${MYHDFPATH}/lib/ ${MYHDFPATH}/lib/libhdf5_hl.a ${MYHDFPATH}/lib/libhdf5.a ${MYHDFPATH}/lib/libz.a ${MYHDFPATH}/lib/libszip.a -ldl")
endif()
if(EMPLOY_MYIMKL)
	message([STATUS] "We use the Intel Math Kernel library for threaded FFTs")
	add_definitions( -DMKL_ILP64 )
	message([STATUS] "${MYMKLROOT}")
	include_directories("${MYMKLROOT}/include")
	set(MYMKLCOMP "-I${MYMKLROOT}/include")
	message([STATUS] "${MYMKLCOMP}")	
	#set(MYMKLLINK "-L${MYMKLROOT}/lib/intel64 ${MYMKLROOT}/lib/intel64/lmkl_intel_ilp64 ${MYMKLROOT}/lib/intel64/lmkl_intel_thread ${MYMKLROOT}/lib/intel64/lmkl_core -liomp5 -lpthread -lm -ldl") ### -Wl,-rpath,${MYMKLROOT}/lib/intel64")
	#set(MYMKLLINK "-L${MYMKLROOT}/lib/intel64 -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl") ### -Wl,-rpath,${MYMKLROOT}/lib/intel64")
	set(MYMKLLINK "-Wl,--start-group ${MYMKLROOT}/lib/intel64/libmkl_intel_ilp64.a ${MYMKLROOT}/lib/intel64/libmkl_intel_thread.a ${MYMKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -liomp5 -lpthread -lm -ldl")
	message([STATUS] "${MYMKLLINK}")
endif()
if(EMPLOY_MYCUDA)
	#include("../../AMBRA.GPUSupport.cmake")
	#message([STATUS] "MYCUDAHOME: ${MYCUDAHOME}")
	#message([STATUS] "MYCUFFTLINKFLAGS: ${MYCUFFTLINKFLAGS}")
	message([STATUS] "We compile with CUDA GPU support")
	find_package(CUDAToolkit REQUIRED)
	set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -Xcompiler -fopenmp -m64")
else()
	message([FATAL_ERROR] "We need to compilcompile without CUDA GPU support, instead we use the CPU-only but multithreaded fallback!")
endif()

#automatically assign project name and compiler flags
set(CMAKE_BUILD_DIR "build")

#setting up compiler-specifics
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")

message([STATUS] "Projectname is ${MYPROJECTNAME}")
message([STATUS] "We utilize optimization level ${MYOPTLEVEL}")

#parallelization - MPI process-level
#query location of MPI library
if(EMPLOY_PARALLELISM_MPI)
	find_package(MPI REQUIRED)
	include_directories(${MPI_INCLUDE_PATH})
endif()

#specific paths of dependencies for this tool
set(MYTOOLSRCPATH "${MYTOOLPATH}/cxx/src/")

#list firstly the precompiled shared aka utils, secondly the tool-specific components, lastly the tool-specific main
add_executable(${MYPROJECTNAME}
	${MYUTILSPATH}/AMBRA_Verbose.cpp.o
	${MYUTILSPATH}/AMBRA_Profiling.cpp.o
	${MYUTILSPATH}/AMBRA_Datatypes.cpp.o
	${MYUTILSPATH}/AMBRA_Math.cpp.o
	${MYUTILSPATH}/CONFIG_Shared.cpp.o
	${MYUTILSPATH}/AMBRA_HDF5IO.cpp.o
	${MYUTILSPATH}/AMBRA_XDMF.cpp.o
	
	${MYTOOLSRCPATH}/CONFIG_Fourier.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierCiteMe.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierIMKLInterface.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierCPUInterface.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierGPUInterface.cu
	${MYTOOLSRCPATH}/AMBRA_FourierStructs.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierPhaseCandHdl.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierAcquisor.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierHDF5.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierXDMF.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierTicToc.cpp
	${MYTOOLSRCPATH}/AMBRA_FourierHdl.cpp

	#${MYTOOLSRCPATH}/AMBRA_Fourier.cpp
	${MYTOOLSRCPATH}/AMBRA_Fourier.cu	
)

# We need to explicitly state that we need all CUDA files in the particle
# library to be built with -dc as the member functions could be called by
# other libraries and executables
set_target_properties( ${MYPROJECTNAME} PROPERTIES CUDA_SEPARABLE_COMPILATION ON )
set_target_properties( ${MYPROJECTNAME} PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}" )
set_target_properties( ${MYPROJECTNAME} PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}" )
#set_property(TARGET ${MYPROJECTNAME} PROPERTY CUDA_ARCHITECTURES 35 50 72)
set_target_properties( ${MYPROJECTNAME} PROPERTIES CUDA_ARCHITECTURES 35 50 72)

# Request that particles be built with -std=c++11
# As this is a public compile feature anything that links to particles
# will also build with -std=c++11
target_compile_features( ${MYPROJECTNAME} PUBLIC cxx_std_11 )

#linking process, the target link libraries command is specific for each tool of the toolbox
target_link_libraries(${MYPROJECTNAME} ${MYOMP} ${MPI_LIBRARIES} ${MYHDFLINKFLAGS} ${MYMKLLINK} CUDA::cudart CUDA::cufft)
