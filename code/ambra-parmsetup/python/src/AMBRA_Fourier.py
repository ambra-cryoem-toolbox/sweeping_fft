# -*- coding: utf-8 -*-
"""
created 2020/11/09, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating XML and shell config files for ambra-fourier
"""

import glob
from pathlib import Path
import numpy as np

from AMBRA_XML import *

config = { 'Inputfile': '',
           'VoxelPhysicalEdgeLength': 1.0,
           'FourierMethod': 1,
           'Dimensionality': 3,
           'ROIEnsemble':  2,
           'IOStoreFFTMagnitude': 1,
           'IOStoreFFTComplexValues': 0,
           'IOStoreFFTProfiling': 0,
           'IOStoreToolProfiling': 1,
           'GPUsPerComputingNode': 1  }

#we should inform on particular config keys, does not need inform for every config key
info = { 'VoxelPhysicalEdgeLength': 'which physical material length does the edge of a single voxel probe',
         'IOStoreFullGridPeaks': 'all IO options are active when 1, else inactive',
         'FourierMethod': 'currently only 1, Fast Fourier Transform',
         'Dimensionality': 'currently only 3, three-dimensional transforms',
         'ROIEnsemble': '1 user locations according to the list below, 2 grid-based',
         'IOStoreFFTMagnitude': 'set to 1 if you want to report the FFT magnitudes for each ROI',
         'IOStoreFFTComplexValues': 'set to 1 if you also want the complex values, hermitian symmetry applies',
         'IOStoreFFTProfiling': 'set to 1 if you want to get details on which MPI, OMP, GPU the FFT the ROIs were solved and how long this took',
         'IOStoreToolProfiling': 'set to 1 if you want to get details on general elapsed time of the tool at the level of the individual MPI processes',
         'GPUsPerComputingNode': 'one GPU per MPI process, on talos max two MPI processes per node' }

class ambra_fourier():

    def __init__(self, simid, imgstack, *args, **kwargs):
        self.simid = simid
        self.toolname = 'Fourier'
        self.config = config
        self.help = info
        self.roi_user_pos = [] #N x 3 position np array
        self.roi_user_grid = [] #1 x 3 length array
        
        if not imgstack.endswith('.h5'):
            raise ValueError('The filename pointing to the inputfile is not an H5 file !')
        else:
            self.config['Inputfile'] = imgstack
    
    def set_roi_ensemble_grid(self):
        self.config['ROIEnsemble'] = 2

    def set_roi_ensemble_positions( self, pos ):
        if pos != []:
            if pos.shape[0] > 0 and pos.shape[1] == 3:
                self.roi_user_pos = []
                for i in np.arange(0, pos.shape[0]):
                    self.roi_user_pos.append( '<entry type="cuboid" posx="' + str(np.uint32(pos[i,0])) + '" posy="' + str(np.uint32(pos[i,1])) + '" posz="' + str(np.uint32(pos[i,2])) + '"/>')
                self.config['ROIEnsemble'] = 1
            else:
                raise ValueError('Argument pos needs to be a >=1 x 3 numpy array !')
        else:
            raise valueError('Argument pos is an empty array !')

    def set_roi_grid(self, nxyz):
        if nxyz != []:
            if nxyz.shape[0] == 1 and nxyz.shape[1] == 3:
                self.roi_user_grid = '<entry type="cuboid" lengthx="' + str(np.uint32(nxyz[0,0])) + '" lengthy="' + str(np.uint32(nxyz[0,1])) + '" lengthz="' + str(np.uint32(nxyz[0,2])) + '"/>'
            else:
                raise ValueError('Argument nxyz needs to be a 1 x 3 numpy array !')
        else:
            raise ValueError('Argument nxyz is an empty array !')

    def run(self, nmpi = 1):
        #let XML generator class create the XML file
        #needs an XML top-level group name, here 'ConfigSurfacer' because we want sth specific for surfacer tool
        #and a dictionary with the settings
        if self.config['ROIEnsemble'] == 1:
            self.xml = ambra_xml( 'Fourier', self.simid, 'ConfigFourier', self.config, self.help, roi_positions = self.roi_user_pos, roi_gridxyz = self.roi_user_grid )
        else:
            self.xml = ambra_xml( 'Fourier', self.simid, 'ConfigFourier', self.config, self.help, roi_gridxyz = self.roi_user_grid )
        #sh = 'paraprobe_bash()'
        
        #create the specific bash command to run this analysis
        if nmpi >= 1:
            sid = str(np.uint32(self.simid))
            prefix = 'AMBRA.Fourier.SimID.' + sid
            execnm = 'ambra_fourier'
            #sh = 'mpiexec -n ' + str(np.uint32(nmpi)) + ' ' + execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            sh = execnm + ' ' + sid + ' ' + prefix + '.xml' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
            return sh
        else:
            raise ValueError('We need to use at least one process when working with the Message Passing Interface !')     

# =============================================================================
#task = ambra_fourier( 1, 'AMBRA.Transcoder.SimID.1.h5' )
#task.set_roi_grid( np.array([100, 100, 100]) )
#task.set_roi_ensemble_grid()
##or
##define mypositions myps
#task.set_roi_ensemble_positions( mypos )
#task.run()
##or giving the numbers of processes explicitly
#task.run( nmpi = 10 )
#del task
# =============================================================================
