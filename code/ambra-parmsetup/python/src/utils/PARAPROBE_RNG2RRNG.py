# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks as a utility to convert range files from RNG to RRNG format
"""

import glob
from pathlib import Path
import numpy as np

class paraprobe_rng2rrng():
    
    def __init__(self, rng_filename, *args, **kwargs ):
        if not rng_filename.endswith('.RNG') and not rng_filename.endswith('.rng'):
            raise ValueError(rng_filename + ' is not a valid RNG format range file !')
        else:
            #read the ASCII textfile RNG format and interpret line by line
            self.rng_filename = rng_filename #rng_filename.split('.', 1)[0] + '.rng'
            self.rrng_filename = rng_filename.split('.', 1)[0] + '.rrng'
            #all non self variables are temporaries
            self.nions = np.uint32(0)
            self.nranges = np.uint32(0)
            self.ions = {}
            ionid = np.uint32(1)
            self.ranges = {}
            rangeid = np.uint32(1)
            self.mq = {}
            #read and parse rng file
            f = open( self.rng_filename, 'r')
            lineid = np.uint32(1) #Fortran line reading for clarity so that beginners can follow easier   
            for line in f:
                if lineid == np.uint32(1):
                    tmp = line.split(' ')
                    if not len(tmp) == 2:
                        raise ValueError('RNG file inconsistency, we did found NumberOfIons and/or NumberOfRanges !')
                    else:
                        self.nions = np.uint32(tmp[0])
                        self.nranges = np.uint32(tmp[1])
                        #print(str(self.nions))
                        #print(str(self.nranges))
                        lineid += np.uint32(1)
                else:
                    #ion!
                    if lineid > 1 and lineid < 1+2*self.nions: #we are only interested in the element shortlabels not the color these are in even lines
                        if lineid % 2 == 0:
                            self.ions[ionid] = line.replace('\n', '').replace(',', '.') #check that element shortlabels are not corrupted or there are trailing data
                            ionid += np.uint32(1)
                        #always go to next line, skip odd ones with the colors
                        lineid += np.uint32(1)
                    else: #the ranges
                        #a valid RNG file has a single line following immediately after the ions
                        if not lineid == 1+2*self.nions+1 and lineid > 1+2*self.nions+1 and lineid <= 1+2*self.nions+1+self.nranges: #ignore range header line for the keys table
                            tmp = line.replace('\n', '').replace(',', '.').split() #replace eventual commas for dots then splitting for special delimiter none, ie one and multiple whitespaces
                            #a valid range line needs to have 3 + nions columns, first is . second is lower mq, third is higher mq, rest are boolean flags indicating which ion the range maps to
                            ncols = len(tmp)
                            if not ncols == 3+self.nions:
                                raise ValueError('RNG file inconsistency, the range line does not contain the expected number of columns so the file is faulty !')
                            else:
                                #we parse of only single ions, i.e. ions with a single boolean flag in the range line set to '1'
                                boolean_flags = []
                                for i in range(0,self.nions):
                                    boolean_flags.append(np.uint32(tmp[3+i]))
                                if not sum(boolean_flags) == 1:
                                    raise ValueError('paraprobe-rng2rrng can currently not parse molecular ions !')
                                else: #parse out this valid range
                                    #print(tmp)                                  
                                    for i in range(0,self.nions):
                                        if boolean_flags[i] == 1:
                                            self.mq[rangeid] = np.array([np.around(np.float32(tmp[1]),decimals=4), np.around(np.float32(tmp[2]),decimals=4)] )
                                            self.ranges[rangeid] = i+1 #Cstyle column index to Fortran index
                                            #use that a valid range file sorts the column names according to the ions 
                                            rangeid += np.uint32(1)
                                            break
                        lineid += np.uint32(1)
            #debug
            #for key in self.ions.keys():
            #    print('__' + self.ions[key] + '__')
            #for key in self.mq.keys():
            #    print('__' + str(self.mq[key][0]) + ', ' + str(self.mq[key][1]) + '___')
            #for key in self.ranges.keys():
            #    print('__' + str(self.ranges[key]) + '__')
        
    def write_rrng(self):
        #compose the slurm script, each slurm scripts needs a magic first line (shebang)
        rrng = '[Ions]' + '\n'
        rrng += 'Number=' + str(self.nions) + '\n'
        for i in np.arange(1,self.nions+1):
            rrng += 'Ion' + str(i) + '=' + self.ions[i] + '\n'
        rrng += '[Ranges]' + '\n'
        rrng += 'Number=' + str(self.nranges) + '\n'
        for i in np.arange(1,self.nranges+1):
            #Range1=13.3804 13.8029 Vol:0.01661 Al:1 Color:33FFFF
            elemid = self.ranges[i]
            element_lbl = str(self.ions[elemid])
            rrng += 'Range' + str(i) + '=' + ' ' + str(self.mq[i][0]) + ' ' + str(self.mq[i][1]) + ' Vol:0.0000 ' + element_lbl + ':1 Color:FFFFFF' + '\n'
        with open(self.rrng_filename,'w') as f:
            f.write(rrng)
            
    def run(self): #pure Python script, nmpi = 1): #*args, **kwargs):
        #create the specific bash command to run this analysis
        #if nmpi >= 1:
        #sid = str(np.uint32(self.simid))
        #prefix = 'PARAPROBE.Spatstat.SimID.' + sid
        #execnm = 'paraprobe_spatstat'
        sh = 'python3.7 PARAPROBE_RNG2RRNG.py ' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
        return sh
        #else:
        #    raise ValueError('We need to use at least one process when working with the Message Passing Interface !') 
             
    def get_rrng_filename(self):
        return self.rrng_filename
             
# =============================================================================
# #usage example
# #task = paraprobe_rng2rrng( 'R76-26865-PA.rng' )
# #task.write_rrng()
# #print(task.get_rrng_filename())
# =============================================================================
