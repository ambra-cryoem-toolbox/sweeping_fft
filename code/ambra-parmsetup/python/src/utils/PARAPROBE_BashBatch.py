# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks, utility for submitting multiple Slurm scripts at once for convenience
"""

import glob
from pathlib import Path
import numpy as np

class paraprobe_bash_batch():
    
    def __init__(self, batchnm, slmdict, bshdict, *arg, **kwargs):
        #convenience script for slurm to submit all jobs at once
        self.slm = '#!/bin/bash' + '\n' #shebang
        #transform eventual Windows line endings to Unix line endings
        self.slm += 'dos2unix *.xml' + '\n'  #for xml files
        self.slm += 'dos2unix *.sh' + '\n'
        self.slm += 'chmod +x *.sh' + '\n' #make the scripts executable
        self.slm += '\n'
        for key in slmdict.keys():
            self.slm += 'sbatch ' + slmdict[key] + '\n'
        
        #def write_bash_script(self):
        self.filename = 'SLURM.BatchQueue.' + batchnm + '.sh'
        with open(self.filename,'w') as f:
            f.write(self.slm)
            
        #convenience script for bash to run jobs one after another
        self.sh = '#!/bin/bash' + '\n' #shebang
        #transform eventual Windows line endings to Unix line endings
        self.sh += 'dos2unix *.xml' + '\n'  #for xml files
        self.sh += 'dos2unix *.sh' + '\n'
        self.sh += 'chmod +x *.sh' + '\n' #make the scripts executable
        self.sh += '\n'
        for key in bshdict.keys():
            self.sh += './' + bshdict[key] + '\n'        
        #def write_bash_script(self):
        self.filename = 'BASH.BatchQueue.' + batchnm + '.sh'
        with open(self.filename,'w') as f:
            f.write(self.sh)        

#usage example
#bsh = paraprobe_bash_batch( 'HuanZhaoScriptMat2018', { '1': '1.sh', '2': '2.sh' } )

