# -*- coding: utf-8 -*-
"""
created 2020/11/09, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for writing specifically configured two-level XML files for paraprobe tools
"""

import glob
from pathlib import Path
import numpy as np

#from PARAPROBE_Shell.py import *

class ambra_xml():
    
    def __init__(self, toolnm, simid, grpnm, dic, hlp, *args, **kwargs):
        self.filename = 'AMBRA.' + toolnm + '.SimID.' + str(np.uint32(simid)) + '.xml'
        #create the XML formatted configuration file, using the dictionary ensures all elements exist
        self.xml = '<?xml version="1.0" encoding="utf-8"?>' + '\n'
        self.xml += '<' + grpnm + '>' + '\n' 
        
        #handle settings and parameter
        for key, val in dic.items():
            self.add_xml_entry(key, val)
            #check if for this key(word) there is help like and explanation for the setting/parameter available
            if key in hlp.keys():
                self.add_xml_info( hlp[key] )
                
        #are there optional target-only ion type combinations
        xyz = kwargs.get('roi_positions', None)
        if not xyz == None:
            if not xyz == []:
                self.xml += '\t' + '<ROIUserLocations>' + '\n'
                for val in xyz:
                    #for every combination of a trg
                    #print('__' + str(val) + '__') 
                    self.xml += '\t\t' + val + '\n'
                self.xml += '\t' + '</ROIUserLocations>' + '\n'
            else:
                raise ValueError('We need at least one entry for xyz !')
        #else:
        #    raise ValueError('targets is empty!')
        
        #are there optional matrix lattice?
        nxyz = kwargs.get('roi_gridxyz', None)
        if not nxyz == None:
            if not nxyz == []:
                self.xml += '\t' + '<ROIUserGrid>' + '\n'
                self.xml += '\t\t' + nxyz + '\n'
                self.xml += '\t' + '</ROIUserGrid>' + '\n'
                
        self.xml += '</ConfigFourier>' + '\n'

        #write the file
        with open(self.filename, 'w') as f:
             f.write(self.xml)
    
    def add_xml_entry(self, tag, val): 
        self.xml += '\t<' + tag + '>' + str(val) + '</' + tag + '>' + '\n'
        
    def add_xml_info(self, val):
        self.xml += '\t<!--' + val + '-->' + '\n'
