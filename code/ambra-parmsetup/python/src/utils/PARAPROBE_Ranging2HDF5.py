# -*- coding: utf-8 -*-
"""
created 2020/05/26, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks as a utility to convert range files from RNG to RRNG format
"""
import sys
import glob
from pathlib import Path
import re
import numpy as np
try:
    import periodictable as pse
except ImportError as e:
   raise ValueError('Install periodictable Python package via e.g pip install periodictable !')
try:
    import h5py
except ImportError as e:
   raise ValueError('Install h5py Python package via e.g pip install h5py !')
try:
    import warnings
except ImportError as e:
    raise ValueError('Install warnings !')
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle

from PARAPROBE_MetadataRanging import *

#from PARAPROBE_Parmsetup_EncodeIontypeHash import *

#https://stackoverflow.com/questions/3663450/remove-substring-only-at-the-end-of-string
def rchop(s, suffix):
    if suffix and s.endswith(suffix):
        return s[:-len(suffix)]
    return s

MQ_EPSILON = 1.0e-4

class molecularion():
    def __init__(self, *args, **kwargs):
        self.id = np.uint8('0')
        self.z1 = np.uint8('0')
        self.n1 = np.uint8('0')
        self.z2 = np.uint8('0')
        self.n2 = np.uint8('0')
        self.z3 = np.uint8('0')
        self.n3 = np.uint8('0')
        self.sgn = np.uint8('0')
        self.charge = np.uint('0') #int
        self.mq = [] #Da
        #self.mqmi = 0.0 #Da
        #self.mqmx = 0.0 #Da
    
    def set_id(self, id):
        if id >= 0 and id < 256:
            self.id = np.uint8(id)
        else:
            raise ValueError('ID needs to be on [0, 255] !')

    def set_z1(self, z):
        if z >= 0 and z < 118:
            self.z1 = np.uint8(z)
        else:
            raise ValueError('Z1 needs to be on [0, 118] !')

    def set_z2(self, z):
        if z >= 0 and z < 118:
            self.z2 = np.uint8(z)
        else:
            raise ValueError('Z2 needs to be on [0, 118] !')

    def set_z3(self, z):
        if z >= 0 and z < 118:
            self.z3 = np.uint8(z)
        else:
            raise ValueError('Z3 needs to be on [0, 118] !')

    def add_mq(self, mi, mx):
        mqmi = np.around(np.float32(mi), decimals=4)
        mqmx = np.around(np.float32(mx), decimals=4)
        #print(str(mqmi) + ' ' + str(mqmx))
        if mqmi >= np.float32(0.0) and mqmx >= mqmi:
            new = True
            #check if range does not already exist if it does ignore
            for iv in self.mq:
                if abs(iv[0]-mqmi) > MQ_EPSILON or abs(iv[1]-mqmx) > MQ_EPSILON:
                    continue
                else:
                    new = False        
            if new == True:
                self.mq.append( [mqmi, mqmx] )
                
    def check_mq(self):
        #check that mq are not overlapping
        for iv in self.mq:
            for jv in self.mq: #all against all
                if iv[1] + MQ_EPSILON <= jv[0] or jv[1] <= MQ_EPSILON + iv[0]:
                    continue
                else:
                    raise ValueError('A mass-to-charge range overlaps with another!')
        return True
    
    def get_paraprobe_name(self):
        nm = ''
        if not self.z1 == np.uint8('0'):
            symb = pse.elements[self.z1].symbol
            if len(symb) == 2:
                nm += symb
            else:
                nm += symb + ':' #single character element key separator in paraprobe
        if not self.z2 == np.uint8('0'):
            symb = pse.elements[self.z2].symbol
            if len(symb) == 2:
                nm += symb
            else:
                nm += symb + ':'
        if not self.z3 == np.uint8('0'):
            symb = pse.elements[self.z3].symbol
            if len(symb) == 2:
                nm += symb
            else:
                nm += symb + ':'
        return nm
    
    def get_paraprobe_code(self):
        what = np.zeros( [PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX], np.uint8 )
        what[0] = self.z1
        what[2] = self.z2
        what[4] = self.z3
        return what

class ranging():
    def __init__(self, *args, **kwargs):
        self.ionnames = {}
        self.itypes = {}       
        #dictionaries resolving how a given molecular ion type ID matches to a particular molecular ion and its components
        #e.g. tell me which ions from the dataset with mq values on interval [ranging['0'].mqmin, ranging['0'].mqmax] match to ion type '0'
        
    def read_rng_file(self, fnm):
        #read the ASCII textfile RNG format and interpret it
        #self.paraprobe_hashvals = {}
        #fnm = 'R76-26110-0.5h.Huan.rng'
        with open( fnm, 'r' ) as rngfile:
            txt = rngfile.read() #lines()
            
        #replace eventual comma decimal points by dots
        txt = txt.replace(',', '.')
        
        #for a RNG range file only the ------ line is relevant which is the header of a table
        #which details the element combination per molecular ion for each mass-to-charge mi/mx

        #strip empty lines
        txt_stripped = [line for line in txt.split('\n') if line.strip() != '']
        
        #parse out all lines with keyword string 'Ion[0-9]+='
        ions = {}
        for line in txt_stripped:
            tmp = re.findall(r'----', line)
            if tmp == []:
                continue
            else:
                #found the table, parse out the tokens to get the disjoint element names
                tmp = re.split(r'\s+', line)
                if ( tmp[0].find('----') == -1):
                    raise ValueError('The RNG file is corrupted because the molecular ion table is not of expected format !')
                else:
                    #for a RNG file the table header contains only a '-----' and space-separated disjoint element names
                    N = len(tmp)
                    elements = {}
                    col2elnumber = {}
                    for i in np.arange(1,N):
                        element = tmp[i]
                        #element = ['Al', 'XX']
                        for el in pse.elements:
                            if not el.symbol == element:
                                continue
                            else:
                                ions[element] = element
                                elements[el.number] = element
                                col2elnumber[i-1] = el.number
                                break
                #break we found the table header
                break
        if len(ions.keys()) == 0:
            raise ValueError('No valid element was found !')
        self.ionnames = ions
        #now parse out the mqmi and mqmx and corresponding combinations
        #first add default ion type
        ityp_cand = {}
        z = [0, 0, 0]
        zkey = '0;0;0'             
        unknown = molecularion()
        unknown.set_z1( z[0] )
        unknown.set_z2( z[1] )
        unknown.set_z3( z[2] )        
        unknown.add_mq( 0.0, 0.001 )        
        ityp_cand[zkey] = unknown
        #parse out all lines with keyword string 'Range[0-9]+='
        for line in txt_stripped:
            #line = '. 26.0830 31.5450  1  0  1  1'
            #in an rng file a range line is introduced by a leading '.'
            tmp = re.split(r'\s+', line)
            if tmp[0] == '.':
                print(tmp)
                ###MK::users seem to make custom modifications by adding user-types like so U01
                ###MK::this is okay but not an element currently handled by PARAPROBE
                ###MK::we allow for this but here currently only when these user types come after the pse elements!
                if len(tmp) >= (1+1+1+len(col2elnumber.keys())):
                    if len(tmp) != (1+1+1+len(col2elnumber.keys())):
                        warnings.warn('It seems that user-defined types have been added, they are currently ignored and only allowed to the end of the range table!')
                    #found a range line
                    #tmp[1] is the mqmin
                    #tmp[2] is the mqmax
                    flags_all = np.uint32(tmp[3:len(tmp)]) #these are multiplicity vectors
                    flags_sum = np.sum(flags_all)
                    #check that at least one flag is set
                    if flags_sum == 0 :
                        raise ValueError('Range line corrupted no flag is set !')
                    elif flags_sum > 3:
                        raise ValueError('Range line okay, but such molecular ions not support in paraprobe, contact support !')
                    else:
                        #paraprobe supports molecular ions with at most three components, the components are Z's 
                        #molecular ions with more than one isotope of the same element appear as duplicates, a hypothetical H2 would be [1, 1, 0]            
                        z = [0, 0, 0]
                        j = 0
                        for i in np.arange(0,len(col2elnumber.keys())):
                            if flags_all[i] == 0:
                                continue
                            else:
                                for mult in np.arange(0,flags_all[i]):
                                    #get the element number Z, as many times as multiplicity
                                    z[j] = col2elnumber[i]
                                    j += 1
                        z.sort( reverse=True) #paraprobe molecular ions are sorted by decreasing Z internally, though,
                        #paraprobe sorts more complicated including the isotope but a range file like rrng does not care for isotopes...
                        cand = molecularion()
                        cand.set_z1( z[0] )
                        cand.set_z2( z[1] )
                        cand.set_z3( z[2] )
                        cand.add_mq( tmp[1], tmp[2] )
                        zkey = str(z[0]) + ';' + str(z[1]) + ';' + str(z[2])
                        if not zkey in ityp_cand.keys():
                            #add a new key
                            ityp_cand[zkey] = cand
                        else:
                            ityp_cand[zkey].add_mq( tmp[1], tmp[2] )
                else:
                    raise ValueError('The RNG file is corrupted because a line with the prefix of range has unexpected number of tokens !')
        if len(ityp_cand.keys()) == 0:
            raise ValueError('No valid iontype was found !')
        #summarize how many iontypes we have
        self.itypes = ityp_cand
        #how many iontypes?
        self.nitypes = len(self.itypes.keys())
        #how many disjoint ion names?
        self.nionnames = len(self.ionnames.keys())
    
    def read_rrng_file(self, fnm):
        #read the ASCII textfile RRNG format and interpret it
        #self.paraprobe_hashvals = {}
        #fnm = 'R76_30140-v01.rrng'
        with open( fnm, 'r' ) as rrngfile:
            txt = rrngfile.read() #lines()
        
        #replace eventual comma decimal points by dots
        txt = txt.replace(',', '.')

        #strip empty lines
        txt_stripped = [line for line in txt.split('\n') if line.strip() != '']
        
        #parse out all lines with keyword string 'Ion[0-9]+='
        ions = {}
        for line in txt_stripped:
            if not re.findall(r'^Ion[0-9]+=', line) == []:
                tmp = re.split(r'[\s=]+', line)
                if not tmp[1] in ions.keys(): #make the ion element name the key and the string Ion with preceeding uint32 the value
                    ions[tmp[1]] = tmp[1]
        if len(ions.keys()) == 0:
            raise ValueError('No valid element was found !')
        self.ionnames = ions
        #parse out all lines with keyword string 'Range[0-9]+='
        ityp_cand = {}
        #add default type
        z = [0, 0, 0]
        zkey = '0;0;0'             
        unknown = molecularion()
        unknown.set_z1( z[0] )
        unknown.set_z2( z[1] )
        unknown.set_z3( z[2] )        
        unknown.add_mq( 0.0, 0.001 )        
        ityp_cand[zkey] = unknown
        for line in txt_stripped:
            if not re.findall(r'^Range[0-9]+=', line) == []:
                tmp = re.split(r'[\s=]+', line)
                print(tmp)
                #minimum and maximum required number of tokens
                len_mi = 6 #range, mi, mx, vol, comp1, color
                len_mx = 8 #range, mi, mx, vol, comp1, comp2, comp3, color
                len_lim = len(tmp) #one past last token to get interpreted as element name
                #is the last token 'Color'?
                clr = tmp[-1].split(':')
                if not clr[0] == 'Color':
                    len_mi -= 1
                    len_mx -= 1
                    len_lim -= 1
                if len(tmp) > len_mx:
                    raise ValueError('The range line seems to contain molecular ions with more than three components, this is not supported!')
                if len(tmp) < len_mi:
                    raise ValueError('The range line seems to contain too few tokens to be significant!')
                #use regular expression to parse out components of range line more robustly even if whitespace have been added
                vol = tmp[3].split(':')
                if not vol[0] == 'Vol':
                    raise ValueError(str(tmp[2]) + ' is not of required format Vol:float32 !')
                #paraprobe supports molecular ions with at most three components, the components are Z's 
                #molecular ions with more than one isotope of the same element appear as duplicates, a hypothetical H2 would be [1, 1, 0]            
                z = [0, 0, 0]
                for i in np.arange(4,len_lim): #6
                    element = tmp[i].split(':')
                    #element = ['Al', 'XX']
                    for el in pse.elements:
                        if not el.symbol == element[0]:
                            continue
                        else:
                            #print(el)
                            z[i-3] = el.number
                            break
                z.sort( reverse=True) #paraprobe molecular ions are sorted by decreasing Z internally, though,
                #paraprobe sorts more complicated including the isotope but a range file like rrng does not care for isotopes...
                cand = molecularion()
                cand.set_z1( z[0] )
                cand.set_z2( z[1] )
                cand.set_z3( z[2] )
                cand.add_mq( tmp[1], tmp[2] )
                zkey = str(z[0]) + ';' + str(z[1]) + ';' + str(z[2])
                if not zkey in ityp_cand.keys():
                    #add a new key
                    ityp_cand[zkey] = cand
                else:
                    ityp_cand[zkey].add_mq( tmp[1], tmp[2] )
        if len(ityp_cand.keys()) == 0:
            raise ValueError('No valid iontype was found !')
        #summarize how many iontypes we have
        self.itypes = ityp_cand
        #how many iontypes?
        self.nitypes = len(self.itypes.keys())
        #how many disjoint ion names?
        self.nionnames = len(self.ionnames.keys())
        
#https://matplotlib.org/3.1.1/gallery/statistics/errorbars_and_boxes.html#sphx-glr-gallery-statistics-errorbars-and-boxes-py

class paraprobe_ranging2hdf5():
    
    def __init__(self, rngfnm, *args, **kwargs ):
        self.rngTbl = ranging()
        self.ifn = ''
        
        if not rngfnm.endswith('.RNG') and not rngfnm.endswith('.rng') and not rngfnm.endswith('.RRNG') and not rngfnm.endswith('.rrng'):
            raise ValueError(rngfnm + ' is neither a RNG nor a RRNG format range file !')
        else:
            if rngfnm.endswith('.RNG'):
                self.rngTbl.read_rng_file( rngfnm )
                #self.ifn = rchop(rngfnm, '.RNG'):
                self.ifn = rngfnm
            if rngfnm.endswith('.rng'):
                self.rngTbl.read_rng_file( rngfnm )
                #self.ifn = rchop(rngfnm, '.rng')
                self.ifn = rngfnm
            if rngfnm.endswith('.RRNG'):
                self.rngTbl.read_rrng_file( rngfnm )
                #self.ifn = rchop(rngfnm, '.RRNG')
                self.ifn = rngfnm
            if rngfnm.endswith('.rrng'):
                self.rngTbl.read_rrng_file( rngfnm )
                #self.ifn = rchop(rngfnm, '.rrng')
                self.ifn = rngfnm
                
    def write_h5_rangefile(self, *args, **kwargs):
        #h5fn = 'Test2.rrng' + '.h5'
        h5fn = self.ifn + '.h5'        
        f = h5py.File( h5fn, 'w' )
        #write first the ion IDs
        N = len(self.rngTbl.itypes)
        ionids = np.linspace( 0, N-1, N, endpoint=True, dtype=np.uint8() )
        dset = f.create_dataset( PARAPROBE_RANGER_META_TYPID_DICT_ID, (len(self.rngTbl.itypes),PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX), dtype=np.uint8(), data=ionids )
        #write mqival to ID mapping
        mq2ionid = []
        N = 0
        #'0;0;0' needs to be always the first!
        M = 0
        for key in self.rngTbl.itypes.keys():
            for mq in self.rngTbl.itypes[key].mq:
                mq2ionid.append( N )
                M += 1
            N += 1
        mq2ids = np.uint8( mq2ionid )
        del mq2ionid
        dset = f.create_dataset( PARAPROBE_RANGER_META_TYPID_DICT_ASSN, (M, PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX), dtype=np.uint8(), data=mq2ids )
        #write mqivals
        mimx = np.zeros( [M, PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX], np.float32 )
        M = 0
        for key in self.rngTbl.itypes.keys():
            for mq in self.rngTbl.itypes[key].mq:
                mimx[M,:] = np.array(mq)
                M += 1
        dset = f.create_dataset( PARAPROBE_RANGER_META_TYPID_DICT_IVAL, (M,2), dtype=np.float32(), data=mimx )
        #write what
        what = np.zeros( [N, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX], np.uint8 )
        N = 0
        for key in self.rngTbl.itypes.keys():
            what[N,:] = self.rngTbl.itypes[key].get_paraprobe_code()
            N += 1
        dset = f.create_dataset( PARAPROBE_RANGER_META_TYPID_DICT_WHAT, (N, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX), dtype=np.uint8(), data=what )
        f.close()


# =============================================================================
# #example usage read in RNG/RRNG range files and transcode to HDF5
# #task = paraprobe_ranging2hdf5( 'R76-26110-0.5h.Huan.rng' )
# task = paraprobe_ranging2hdf5( 'R76_30140-v01.rrng' )
# #generate a generic HDF5 file with the ranging data
# task.write_h5_rangefile()
# =============================================================================


def plot_rangefile_assessment( ds, fn, mqleft, mqright, figwidth, figheight ):
    """
    plot a barcode profile of multiple range file rangings to compare them heads up, ds is a np.array of dataset names
    """
    #here is an example how to generate a range file genome to quickly compare many rangefiles
    #for overlap and similarity from the ranges
    if mqleft < 0.0 or mqright <= mqleft:
        raise ValueError('Error on the mass-to-charge interval bounds 0.0 <= mqleft < mqright !')
   
    #fig, ax = plt.subplots( figsize=(16.53, 11.69) ) #A3 11.69 x 16.53
    fig, ax = plt.subplots( figsize=(figwidth, figheight) ) #A3 11.69 x 16.53
    
    xmi = 1.0e36
    xmx = -1.0e36
    ymi = 1.0e36
    ymx = -1.0e36
    for i in np.arange(0,len(ds)):
        task = paraprobe_ranging2hdf5( ds[i] )
        print(ds[i])
        offset = (i+1)*10.0
        height = 5.0
        #rangeboxes = []
        for key in task.rngTbl.itypes.keys():
            niv = len(task.rngTbl.itypes[key].mq)
            for iv in np.arange(0,niv):
                mqmi = task.rngTbl.itypes[key].mq[iv][0]
                mqmx = task.rngTbl.itypes[key].mq[iv][1]
                if mqmi <= xmi:
                    xmi = mqmi
                if mqmx >= xmx:
                    xmx = mqmx
                width = mqmx - mqmi
                #print(str(width))
                x = mqmi
                y = offset - 0.5*height #offset
                if y-0.5*height <= ymi:
                    ymi = y-0.5*height
                if y+0.5*height >= ymx:
                    ymx = y+0.5*height
                ax.add_patch( plt.Rectangle( (x, y), width, height, facecolor='black', alpha=0.25 ) )
                #plt.xlim([0.95*xmi, +1.05*xmx])
        del task
        ax.text( mqleft - 8.0, offset, ds[i], fontsize=6 ) #, fontdict=font)
    plt.xlim([mqleft, mqright])
    #plt.xlim([65.0, 75.0])
    #plt.xlim([-10.0, 140.0])
    #plt.ylim([-5.0, +5.0])
    plt.ylim([5.0, offset+5.0])
    plt.yticks([])
    plt.xlabel('Mass-to-charge (Da)')
    #plt.set_title('Range file assessment')
    #plt.legend( [lgnd], loc='upper right')
    #plt.xlabel('Distance (nm)')
    #plt.ylabel(r'Radial distribution function')
    #xy.vlines([xmx], 0, 1, transform=xy.get_xaxis_transform(), colors=MYPARULA[2], linestyles='dotted') #'solid') #'dotted')
    #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
    #plt.yscale('log')
    figfn = fn + '.pdf' #'.png'
    figfn = fn + '.png'
    #https://stackoverflow.com/questions/32185411/break-in-x-axis-of-matplotlib
    #fig.savefig( figfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
    #                    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
    fig.savefig( figfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                    transparent=False, bbox_inches='tight', pad_inches=0.1, metadata=None) #frameon=None,
    fig.clf()

# =============================================================================
# #example usage
# #define the range files you want to compare heads up
# ds = np.array([ 'R76_27632-v01.rrng',
#                 'R76_29974-v03.rrng',
#                 'R76_30139-v03.rrng',
#                 'R76_30140-v01.rrng',
#                 'R76_30168-v01.rrng'  ])
# 
# #give a filename for the figure
# fn = 'RangeFileAssessment'
# 
# plot_rangefile_assessment( ds, fn, 60.0, 80.0, 10.0, 2.0 )
# =============================================================================




# =============================================================================
#     def write_rrng(self):
#         #compose the slurm script, each slurm scripts needs a magic first line (shebang)
#         rrng = '[Ions]' + '\n'
#         rrng += 'Number=' + str(self.nions) + '\n'
#         for i in np.arange(1,self.nions+1):
#             rrng += 'Ion' + str(i) + '=' + self.ions[i] + '\n'
#         rrng += '[Ranges]' + '\n'
#         rrng += 'Number=' + str(self.nranges) + '\n'
#         for i in np.arange(1,self.nranges+1):
#             #Range1=13.3804 13.8029 Vol:0.01661 Al:1 Color:33FFFF
#             elemid = self.ranges[i]
#             element_lbl = str(self.ions[elemid])
#             rrng += 'Range' + str(i) + '=' + ' ' + str(self.mq[i][0]) + ' ' + str(self.mq[i][1]) + ' Vol:0.0000 ' + element_lbl + ':1 Color:FFFFFF' + '\n'
#         with open(self.rrng_filename,'w') as f:
#             f.write(rrng)
#             
#     def run(self): #pure Python script, nmpi = 1): #*args, **kwargs):
#         #create the specific bash command to run this analysis
#         #if nmpi >= 1:
#         #sid = str(np.uint32(self.simid))
#         #prefix = 'PARAPROBE.Spatstat.SimID.' + sid
#         #execnm = 'paraprobe_spatstat'
#         sh = 'python3.7 PARAPROBE_RNG2RRNG.py ' + ' 1>' + prefix + '.STDOUT.txt' + ' 2>' + prefix + '.STDERR.txt'
#         return sh
#         #else:
#         #    raise ValueError('We need to use at least one process when working with the Message Passing Interface !') 
#              
#     def get_rrng_filename(self):
#         return self.rrng_filename
#
# #usage example
# #task = paraprobe_rng2rrng( 'R76-26865-PA.rng' )
# #task.write_rrng()
# #print(task.get_rrng_filename())
# =============================================================================
