# -*- coding: utf-8 -*-
"""
created 2020/05/28, Markus K\"uhbach, m.kuehbach@mpie.de
low-level utility function for decoding the paraprobes internal representation for ions into human-readable form
"""

import os, sys, glob, subprocess
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
try:
    import periodictable as pse
except ImportError as e:
    raise ValueError('Install periodictable Python package via e.g. pip install periodictable !')
try:
    import h5py
except ImportError as e:
    raise ValueError('Install h5py Python package via e.g pip install h5py !')

def decode_molecularion(hashval):
    """
    low-level function specific for paraprobe-ranger which extracts a human-readable
    molecular ion string description from an np.uint8 type and shaped (8,) numpy array using periodictable
    """
    #first six column values give Z/N pair of first, second, and third isotope component of the molecular ion
    #seventh column gives sign and eigth the charge of the molecular ion
    #first one is either 0 indicating UNKNOWN_IONTYPE, i.e. unkwown, or single isotope ion
    if hashval[0] == 0:
        print(str('unknown'))
        return 'unknown'
    else:
        #hashval = ityp_hashvals[1,:]
        #tmp = '['  #use later for r'''$\textnormal{[}{\textnormal{Al}}_2\textnormal{O}]^{3+}$''', unlikely example though...
        tmp = ''
        if hashval[1] == 0:
            tmp += str(pse.elements[hashval[0]])
        else:
            tmp += '^{'+ str(hashval[1]) + '}' + str(pse.elements[hashval[0]])
            #can use latex notation, no fuss any more with formatting :), see the beauty here
        #tmp += ']'
        ###MK::add the charges
        #print(str(tmp))
        return tmp

# =============================================================================
#example usage, tell me how many ions of a certain type
#molecularion_str = decode_molecularion(np.array([13, 13, 12, 1, 44, 77, 1, 12]))
# =============================================================================
