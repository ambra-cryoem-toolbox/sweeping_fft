# -*- coding: utf-8 -*-
"""
created 2020/11/10, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating Bash scripts for local workstation
"""

import os
import sys
import glob
#from pathlib import Path
import numpy as np

class ambra_bash_single():
    
    def __init__(self, ifo, tools, *args, **kwargs ):
        #and configure how many computing nodes and processes you want to use
        if not 'JobName' in ifo.keys():
            raise ValueError('JobName is unknown in the ifo dictionary !')
        if not 'SimulationID' in ifo.keys():
            raise ValueError('SimulationID is unknown in the ifo dictionary !')
        if not 'Compiler' in ifo.keys():
            raise ValueError('Compiler is unknown in the ifo dictionary !')
        if not 'NodesToUse' in ifo.keys():
            raise ValueError('NodesToUse is unknown in the ifo dictionary !')
        if not 'ProcessesPerNode' in ifo.keys():
            raise ValueError('ProcessesPerNode is unknown in the ifo dictionary !')
        if not 'ThreadsPerProcess' in ifo.keys():
            raise ValueError('ThreadsPerProcess is unknown in the ifo dictionary !')
            
        if not 'AmbraFourier' in tools.keys():
            raise ValueError('AmbraFourier is not part of the tools dictionary !')
            
        if ifo['NodesToUse'] < 1:
            raise ValueError('NodesToUse needs to be >= 1 !')
        if ifo['Compiler'] != 'ITL':
            raise ValueError('Compiler needs to be ITL !')
        if ifo ['ProcessesPerNode'] < 1:
            raise ValueError('ProcessesPerNode needs to be >= 1 !')
        if ifo['ThreadsPerProcess'] < 1:
            raise ValueError('ThreadsPerProcess needs to be >= 1 !')
                        
        self.nmpi = str(np.uint32(ifo['NodesToUse'] * ifo['ProcessesPerNode']))    
        self.simid = str(np.uint32(ifo['SimulationID']))
        self.filename = 'BASH.AMBRA.Workflow.SimID.' + self.simid + '.sh'
        self.stdout = 'BASH.AMBRA.Workflow.SimID.' + self.simid + '.STDOUT.txt'
        self.stderr = 'BASH.AMBRA.Workflow.SimID.' + self.simid + '.STDERR.txt'
        #self.wkdir = './'
        self.jbnm = ifo['JobName'] + self.simid
        
        #typically no queue on local workstations like MPI30

        #which module environment to use?
        self.modls = ''
        #if machine == 'MPI30':
        self.modls += 'module purge' + '\n'
            #if compiler == 'ITL':
        self.modls += 'module load Compilers/Intel/Intel2018' + '\n'
        self.modls += 'module load MPI/Intel/IntelMPI2018' + '\n'
        self.modls += 'module load Libraries/IntelMKL2018' + '\n'
                #self.modls += 'module load impi' + '\n'
                #self.modls += 'module load mkl' + '\n'
                #self.modls += 'module load boost' + '\n'
            #if compiler == 'GNU':
            #    self.modls += 'module load Compilers/GCC/GCC-7.5.0' + '\n'
            #    self.modls += 'module load MPI/GCC/MPICH-GCC-7.5.0' + '\n'
            #    #self.modls += 'module load mpich' + '\n'
            #    #self.modls += 'module load mkl' + '\n'
            #    #self.modls += 'module load boost' + '\n'
            #if compiler == 'PGI':
            #    raise ValueError('Bash scripts for PGI compiler currently not implemented !')
            #    self.modls += 'module load cmake/3.10' + '\n'
            #    self.modls += 'module load cuda/10.0' + '\n'
            #    self.modls += 'module load pgi/19' + '\n'
            #    self.modls += 'module load impi/2018.4' + '\n'
        self.modls += 'module list' + '\n'
        #self.modls += 'if [ ! -z $SLURM_CPUS_PER_TASK ] ; then' + '\n'
        #if machine == 'MPI30':
        self.modls += 'export OMP_NUM_THREADS=' + str(ifo['ThreadsPerProcess']) + '\n'
        self.modls += 'export OMP_PLACES=cores' + '\n'
        #self.modls += 'export OMP_NESTED=true'
        self.modls += 'export MKL_NUM_THREADS=1' + '\n'
        self.modls += 'echo $OMP_NUM_THREADS' + '\n'
        self.modls += 'echo $OMP_PLACES' + '\n'
        #self.modls += 'echo $OMP_NESTED'
        self.modls += 'echo $MKL_NUM_THREADS' + '\n'
        #if compiler == 'PGI':
        #    self.modls += 'export CUDA_VISIBLE_DEVICES=0,1' + '\n'
        #    self.modls += 'echo $CUDA_VISIBLE_DEVICES' + '\n'
        #    self.modls += 'echo $PGI_CURR_CUDA_HOME' + '\n'
        #    self.modls += 'echo $PGI' + '\n'
        #echo "MKL NUM THREADS env option"
        #echo $MKL_NUM_THREADS + '\n'
        #export LIBRARY_PATH=/mpcdf/soft/SLE_15/packages/x86_64/intel_parallel_studio/2018.4/mkl/lib/intel64_lin/
        #echo $LIBRARY_PATH
        #echo "CUDA VISIBLE DEVICES env option"
        #export CUDA_VISIBLE_DEVICES=0,1
        #echo $CUDA_VISIBLE_DEVICES
        #echo "PGI CURR CUDA env option"
        ####for pgi/18
        ##export PGI_CURR_CUDA_HOME=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10/linux86-64/2018/cuda/10.0
        ##echo $PGI_CURR_CUDA_HOME
        ##echo "PGI home folder"
        ##export PGI=/talos/u/system/soft/SLE_15/packages/skylake/pgi/18.10
        ##echo $PGI
        ##for pgi/19
        ##export PGI_CURR_CUDA_HOME=/talos/u/system/soft/SLE_15/packages/skylake/pgi/linux86-64-llvm/2019/cuda
        ##echo $PGI_CURR_CUDA_HOME
        ##echo "PGI home folder"
        ##export PGI=/talos/u/system/soft/SLE_15/packages/skylake/pgi/19.3
        ##echo $PGI
        self.modls += 'ulimit -s unlimited' + '\n'
        self.modls += 'echo "ulimit env option"' + '\n'
        self.modls += 'ulimit' + '\n'
        ##list allocated TALOS resources nodes
        #self.modls += 'echo $SLURM_JOB_NODELIST' + '\n'
        ###echo $SLURM_NODELIST
        #self.modls += 'echo $SLURM_JOB_NUM_NODES' + '\n'
        ###echo $SLURM_NNODES
        #self.modls += 'echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"' + '\n'
        ##end of TALOS specific stuff
        
        ##describe the workflow, i.e. which commands to execute one after another?
        self.what = ''
        for key in tools.keys():
            self.what += 'mpiexec -n' + ' ' + self.nmpi + ' ' + './' + tools[key] + ' 1>' + self.stdout + ' 2>' + self.stderr + '\n'
            #print('__' + str(key) + '__' + taskdict[key] + '__') #+ '\n')
                
    def write_bash_script(self):
        #compose the slurm script, each slurm scripts needs a magic first line (shebang)
        self.sh = '#!/bin/bash' + '\n'
        #no queing system
        self.sh += '\n'
        #next we have the compiler specific modifications to load the correct environment
        self.sh += self.modls + '\n'
        
        self.sh += '\n'
        #finally the actual paraprobe jobs
        self.sh += '### ' + ' ambra tool workflow SimID ' + str(self.simid) + '\n'
        self.sh += '\n'
        self.sh += self.what + '\n'

        with open(self.filename,'w') as f:
             f.write(self.sh)
             
    def get_bash_script_filename(self):
        return self.filename
             
#usage example
#bsh = ambra_bash_single( '', '' )
#bsh.write_bash_script()
#print(bsh.get_bash_script_filename())
