# -*- coding: utf-8 -*-
"""
created 2020/12/07, Markus K\"uhbach, m.kuehbach@mpie.de
Python class to be used in Jupyter notebooks for creating Slurm scripts for computer cluster like TALOS
using the configuration of talos as of 2020/12/07
"""

import os
import sys
import glob
#from pathlib import Path
import numpy as np

class ambra_slurm_single():
    
    def __init__(self, ifo, tools, *args, **kwargs ):
        #and configure how many computing nodes and processes you want to use
        if not 'JobName' in ifo.keys():
            raise ValueError('JobName is unknown in the ifo dictionary !')
        if not 'SimulationID' in ifo.keys():
            raise ValueError('SimulationID is unknown in the ifo dictionary !')
        if not 'Compiler' in ifo.keys():
            raise ValueError('Compiler is unknown in the ifo dictionary !')
        if not 'NodesToUse' in ifo.keys():
            raise ValueError('NodesToUse is unknown in the ifo dictionary !')
        if not 'ProcessesPerNode' in ifo.keys():
            raise ValueError('ProcessesPerNode is unknown in the ifo dictionary !')
        if not 'ThreadsPerProcess' in ifo.keys():
            raise ValueError('ThreadsPerProcess is unknown in the ifo dictionary !')
            
        if not 'AmbraFourier' in tools.keys():
            raise ValueError('AmbraFourier is not part of the tools dictionary !')
        if ifo['SimulationID'] < 0:
            raise ValueError('Simulation IDs need to be positive !')
        if ifo['NodesToUse'] < 1 or ifo['NodesToUse'] > 80:
            raise ValueError('NodesToUse needs to be on [1, 80] !')
        if ifo['Compiler'] != 'ITL':
            raise ValueError('Compiler needs to be ITL !')
        if ifo['ProcessesPerNode'] < 1 or ifo['ProcessesPerNode'] > 2:
            raise ValueError('ProcessesPerNode needs to be on [1,2] !')
        if ifo['ProcessesPerNode'] == 1:
            if ifo['ThreadsPerProcess'] < 1 or ifo['ThreadsPerProcess'] > 40:
                raise ValueError('ThreadsPerProcess needs to be on [1, 40] when using one MPI process !')
        else:
            if ifo['ThreadsPerProcess'] < 1 or ifo['ThreadsPerProcess'] > 20:
                raise ValueError('ThreadsPerProcess needs to be on [1, 20] when using two MPI processes !')
        if ifo['GPUsToUsePerProcess'] < 0 or ifo['GPUsToUsePerProcess'] > 1:
            raise ValueError('GPUsToUsePerProcess needs to be either 0 or 1 !')       
        
        self.simid = str(np.uint32(ifo['SimulationID']))
        self.wkdir = './'
        self.stdout = 'AMBRA.Fourier.SimID.' + self.simid + '.STDOUT.txt'
        self.stderr = 'AMBRA.Fourier.SimID.' + self.simid + '.STDERR.txt'
        self.jbnm = 'ambra_fourier'
        self.filename = 'SLURM.AMBRA.Workflow.SimID.' + self.simid + '.sh'
        self.xml = ''
        
        ##this example is specific for TALOS only as of 2020/12/07
        #queing settings, how many cores, how many processes and threads, how much time, which queue?
        self.partition = '--partition=p.talos'
        self.nodes = '--nodes=' + str(np.uint32(ifo['NodesToUse'])) #how many computing nodes to use?
        self.ntasks = '--ntasks-per-node=' + str(np.uint32(ifo['ProcessesPerNode'])) #how many mpi processes, 1 for normal jobs, 2 for GPU jobs to delegate work per V100
        self.ncpus = '--cpus-per-task=' + str(np.uint32(ifo['ThreadsPerProcess'])) #how many threads per process, no hyperthreading!
        self.ngpus = ''
        if ifo['GPUsToUsePerProcess'] == 1:
            if ifo['ProcessesPerNode'] == 1:
                self.ngpus = '--gres=gpu:1'
            if ifo['ProcessesPerNode'] == 2:
                self.ngpus = '--gres=gpu:2'
        #self.mail = '--mail-user=m.kuehbach@mpie.de'
        self.time = '--time=24:00:00' #set a time limit
        
        #which module environment to use?
        self.modls = ''
        self.modls += 'module load cmake/3.18' + '\n'
        self.modls += 'module load cuda' + '\n'
        self.modls += 'module load intel' + '\n'
        self.modls += 'module load impi' + '\n'
        self.modls += 'module load mkl' + '\n'
        
        self.modls += 'echo "Current modules loaded"' + '\n'
        self.modls += 'module list' + '\n'

        self.modls += 'echo "OpenMP NUM THREADS env option"' + '\n'
        self.modls += 'if [ ! -z $SLURM_CPUS_PER_TASK ] ; then' + '\n'
        self.modls += '\t' + 'export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK' + '\n'
        self.modls += 'else' + '\n'
        self.modls += '\t' + 'export OMP_NUM_THREADS=1' + '\n'
        self.modls += 'fi' + '\n'
        self.modls += 'echo $OMP_NUM_THREADS' + '\n'
        self.modls += 'echo "OpenMP PLACES env option"' + '\n'
        self.modls += 'export OMP_PLACES=cores' + '\n'
        self.modls += 'echo $OMP_PLACES' + '\n'
        self.modls += '###echo "OpenMP NESTED env option"' + '\n'
        self.modls += '###export OMP_NESTED=true' + '\n'
        self.modls += '###echo $OMP_NESTED' + '\n'
        self.modls += 'echo "MKL NUM THREADS env option"' + '\n'
        self.modls += 'export MKL_NUM_THREADS=1' + '\n'
        self.modls += 'echo $MKL_NUM_THREADS' + '\n'
        self.modls += 'echo "CUDA VISIBLE DEVICES env option"' + '\n'
        if ifo['GPUsToUsePerProcess'] == 1:
            if ifo['ProcessesPerNode'] == 1:
                self.modls += 'export CUDA_VISIBLE_DEVICES=0' + '\n'
            if ifo['ProcessesPerNode'] == 2:
                self.modls += 'export CUDA_VISIBLE_DEVICES=0,1' + '\n'
        self.modls += 'echo $CUDA_VISIBLE_DEVICES' + '\n'
        self.modls += '##set unlimited stack memory' + '\n'
        self.modls += 'ulimit -s unlimited' + '\n'
        self.modls += 'echo "ulimit env option"' + '\n'
        self.modls += '##set no core files' + '\n'
        self.modls += '##ulimit -S -c 0' + '\n'
        self.modls += '##display ulimit settings' + '\n'
        self.modls += 'ulimit' + '\n'
        self.modls += '##list allocated TALOS resources nodes' + '\n'
        self.modls += 'echo $SLURM_JOB_NODELIST' + '\n'
        self.modls += '##echo $SLURM_NODELIST' + '\n'
        self.modls += 'echo $SLURM_JOB_NUM_NODES' + '\n'
        self.modls += '##echo $SLURM_NNODES' + '\n'
        self.modls += 'echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"' + '\n'
               
        ##describe the workflow, i.e. which commands to execute one after another?
        self.what = ''
        for key in tools.keys():
            self.what += 'srun ' + ' ' + tools[key] + ' ' + self.simid + ' ' + 'AMBRA.Fourier.SimID.' + self.simid + '.xml' + '\n'
    
    def set_timeplan(self, val):
        self.time = val;

    def write_slurm_script(self):
        #compose the slurm script, each slurm scripts needs a magic first line (shebang)
        self.sh = '#!/bin/bash -l' + '\n'
        #parameter and settings for the Slurm batch system need to come first!
        self.sh += '#SBATCH -o ' + self.wkdir + self.stdout + '.%j' + '\n'
        self.sh += '#SBATCH -e ' + self.wkdir + self.stderr + '.%j' + '\n'
        self.sh += '#SBATCH -D ' + self.wkdir + '\n'
        self.sh += '#SBATCH -J ' + self.jbnm + '\n'
        self.sh += '#SBATCH ' + self.partition + '\n'
        self.sh += '#SBATCH ' + self.nodes + '\n'
        self.sh += '#SBATCH ' + self.ntasks + '\n'
        self.sh += '#SBATCH ' + self.ncpus + '\n'
        self.sh += '#SBATCH ' + self.ngpus + '\n'
        #self.sh += '#SBATCH ' + self.mail + '\n'
        self.sh += '#SBATCH ' + self.time + '\n'
        
        self.sh += '\n'
        #next we have the compiler specific modifications to load the correct environment
        self.sh += self.modls + '\n'
        
        self.sh += '\n'
        #finally the actual ambra jobs
        self.sh += '### ' + ' ambra tool workflow SimID ' + self.simid + '\n'
        self.sh += '\n'
        self.sh += self.what + '\n'

        with open(self.filename,'w') as f:
             f.write(self.sh)
             
    def get_slurm_script_filename(self):
        return self.filename
             
#usage example
#slm = ambra_slurm_single( '', '' )
#slm.write_slurm_script()
#print(bsh.get_slurm_script_filename())
