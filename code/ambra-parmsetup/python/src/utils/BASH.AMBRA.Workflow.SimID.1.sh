#!/bin/bash

module purge
module load Compilers/Intel/Intel2018
module load MPI/Intel/IntelMPI2018
module load Libraries/IntelMKL2018
module list
export OMP_NUM_THREADS=20
export OMP_PLACES=cores
export MKL_NUM_THREADS=1
echo $OMP_NUM_THREADS
echo $OMP_PLACES
echo $MKL_NUM_THREADS
ulimit -s unlimited
echo "ulimit env option"
ulimit


###  ambra tool workflow SimID 1

mpiexec -n 1 ./ambra_fourier 1>BASH.AMBRA.Workflow.SimID.1.STDOUT.txt 2>BASH.AMBRA.Workflow.SimID.1.STDERR.txt

