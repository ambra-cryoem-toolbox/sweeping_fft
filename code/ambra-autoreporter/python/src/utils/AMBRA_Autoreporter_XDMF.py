# -*- coding: utf-8 -*-
"""
created 2020/11/10, Markus K\"uhbach, m.kuehbach@mpie.de
Python utility class to be used in Jupyter notebooks for creating XDMF supplementary files for visualizing results
"""

import os
import sys
import glob
import numpy as np
import h5py
#from pathlib import Path
XDMF_HEADER_LINE1=r'''<?xml version="1.0" ?>'''
XDMF_HEADER_LINE2=r'''<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>'''
XDMF_HEADER_LINE3=r'''<Xdmf xmlns:xi="http://www.w3.org/2003/XInclude" Version="2.2">'''

FOURIER_META_FFT_IJK='/AmbraFourier/Metadata/FFTDetails/IJK'
FOURIER_META_FFT_IJKTOPO='/AmbraFourier/Metadata/FFTDetails/IJKTopo'
FOURIER_RES_ROIS_FFTMAGN='/AmbraFourier/Results/ROIEnsemble/FFTMagnitude/'

class xdmf():
    def __init__(self, *args, **kwargs ):
        self.xdmf = ''
        
    def visualize_roi_ijk(self, h5fn, roiID ):
        #parse data from H5, check if topology, xyz, and the scalar attribute for ROI roiID exists in the HDF5 file
        hf = h5py.File( h5fn, 'r' )
        if FOURIER_META_FFT_IJK in hf.keys() and FOURIER_META_FFT_IJKTOPO in hf.keys() and FOURIER_RES_ROIS_FFTMAGN + str(roiID) in hf.keys():
            topo = hf[FOURIER_META_FFT_IJKTOPO][:,:]
            ijk = hf[FOURIER_META_FFT_IJK][:,:]
            magn = hf[FOURIER_RES_ROIS_FFTMAGN + str(roiID)][:,:]
            if topo.shape[0] >= 3 and topo.shape[1] == 1 and ijk.shape[0] >= 1 and ijk.shape[1] == 3 and magn.shape[0] == ijk.shape[0] and magn.shape[1] == 1:
                print('Found required datasets in ' + h5fn)
                #create XDMF file 
                self.xdmf = ''
                self.xdmf += XDMF_HEADER_LINE1 + '\n'
                self.xdmf += XDMF_HEADER_LINE2 + '\n'
                self.xdmf += XDMF_HEADER_LINE3 + '\n'
                self.xdmf += r'''  <Domain>''' + '\n'
                self.xdmf += r'''    <Grid Name="ROI"''' + str(roiID) + r''' GridType="Uniform">''' + '\n'
                self.xdmf += r'''        <Topology TopologyType="Mixed" NumberOfElements="''' + str(np.uint64(topo.shape[0]/3)) + r'''">''' + "\n"
                self.xdmf += r'''          <DataItem Dimensions="''' + str(np.uint64(topo.shape[0])) + r'''" NumberType="UInt" Precision="4" Format="HDF">''' + '\n'
                self.xdmf += r'''            ''' + h5fn + r''':/AmbraFourier/Metadata/FFTDetails/IJKTopo''' + '\n'
                self.xdmf += r'''          </DataItem>''' + '\n'
                self.xdmf += r'''        </Topology>''' + '\n'
                self.xdmf += r'''        <Geometry GeometryType="XYZ">''' + '\n'
                #here we interpret the uint32 voxel coordinates as floating point values!
                self.xdmf += r'''          <DataItem Dimensions="''' + str(np.uint64(ijk.shape[0])) + r''' 3" NumberType="Float" Precision="4" Format="HDF">''' + '\n'
                self.xdmf += r'''            ''' + h5fn + r''':/AmbraFourier/Metadata/FFTDetails/IJK''' + '\n'
                self.xdmf += r'''          </DataItem>''' + '\n'
                self.xdmf += r'''        </Geometry>''' + '\n'
                self.xdmf += r'''        <Attribute Name="FFTMagnitude" AttributeType="Scalar" Center="Node">''' + '\n'
                self.xdmf += r'''          <DataItem Dimensions="''' + str(np.uint64(magn.shape[0])) + r''' 1" DataType="Float" Precision="4" Format="HDF">''' + '\n'
                self.xdmf += r'''            ''' + h5fn + r''':/AmbraFourier/Results/ROIEnsemble/FFTMagnitude/''' + str(np.uint64(roiID)) + '\n'
                self.xdmf += r'''          </DataItem>''' + '\n'
                self.xdmf += r'''        </Attribute>''' + '\n'
                self.xdmf += r'''      </Grid>''' + '\n'
                self.xdmf += r'''  </Domain>''' + '\n'
                self.xdmf += r'''</Xdmf>''' + '\n' 
                #write data
                fn = h5fn + '.ROI.' + str(roiID) + '.xdmf'
                xdmffn = open( fn, "wt")
                n = xdmffn.write(self.xdmf)
                xdmffn.close()
                self.xdmf = ''
                print('Created XDMF file for visualizing ROI ' + str(roiID))
            else:
                raise ValueError('Shapes of ijktopo, ijk, magn are inconsistent !')
        else:
            raise ValueError('Arrays ijktopo, ijk, and magn for specific ROI ' + str(roiID) +  ' are not in H5 file !')

#example usage
#fn = 'D:/Markus/MPIE/GITHUB/AMBRA_CRYOEM_TOOLBOX/tutorials/AMBRA.Fourier.Results.SimID.1.h5'
#task = xdmf()
#task.visualize_roi_ijk( fn, 0 )