#!bin/bash

#MK::welcome, we need to execute this from within anbra-cryoem-toolbox/code to build the C/C++/CUDA tools of AMBRA

##DEFINE WHICH TOOLS TO COMPILE, set to 1 to activate else not compiled
build_fourier=1

##set to true to always clear the build folder before compiling
clean_build=false #true #true
##set to true to copy all executables to a single location to be used in a workflow
collect_executables=true

##working directory on MPI30
target_folder="/home/m.kuehbach/GITHUB/AMBRA_CRYOEM_TOOLBOX/code/"
##working directory on TALOS
#target_folder="/talos/scratch/mkuehbac/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/"
target_folder="/talos/u/mkuehbac/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/"

##WHICH MACHINE TO USE
which_machine="mpi30" #"talos" #mpi30" #"mpi30"

##WHICH COMPILER TO USE?
compile_which="itl" #"itl" "gcc" "pgi"

##CHECK CURRENT GITVERSION?
check_codeversion=0 #1

##################################################################################################################################################
##SEMI-AUTOMATIC SECTION##########################################################################################################################
##users need to set environment variables specific for target machine here########################################################################
##################################################################################################################################################
##################################################################################################################################################
##make sure to start from within AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code

##SET UP YOUR ENVIRONMENT VARIABLE USING ENVIRONMENT MODULES
#start clean
echo "Preparing environment"
module purge
module list
#make choice based on compiler
if [ "$compile_which" == "itl" ]; then
	##HERE IS AN EXAMPLE FOR INTEL and NO CUDA
	echo "Loading environment for compiling with Intel and using IMKL"
	if [ "$which_machine" == "mpi30" ]; then
		module load Compilers/Intel/Intel2018
		module load MPI/Intel/IntelMPI2018
		module load Libraries/IntelMKL2018
	fi
	if [ "$which_machine" == "talos" ]; then
		module load cmake
		module load intel
		module load impi
	fi
	MYWHO_COMPILER="ITL"
	MYCCC_COMPILER=icc
	MYCXX_COMPILER=icpc
fi
if [ "$compile_which" == "gcc" ]; then
	##HERE IS AN EXAMPLE FOR GCC and NO CUDA
	echo "Loading environment for compiling with GCC"
	if [ "$which_machine" == "mpi30" ]; then
		module load Compilers/GCC/GCC-7.5.0
		module load MPI/GCC/MPICH-GCC-7.5.0
	fi
	if [ "$which_machine" == "talos" ]; then
		module load cmake
		module load gcc
		module load impi
	fi
	MYWHO_COMPILER="GCC"
	MYCCC_COMPILER=gcc
	MYCXX_COMPILER=g++
fi
if [ "$compile_which" == "pgi" ]; then
	echo "Loading environment for compiling with PGI"
	if [ "$which_machine" == "mpi30" ]; then
		module load Compilers/PGI/PGI-19.4
		module load Compilers/PGI/OpenMPI-PGI-19.4
	fi
	if [ "$which_machine" == "talos" ]; then
		module load cmake
		module load pgi
		##mpi and cuda
	fi
	MYWHO_COMPILER="PGI"	
	MYCCC_COMPILER=pgcc
	MYCXX_COMPILER=pg++
fi	
#show me those
module list
echo "Environment prepared"


##################################################################################################################################################
##AUTOMATIC SECTION###############################################################################################################################
##users should not be required to make any changes here###########################################################################################
##################################################################################################################################################
##################################################################################################################################################

##PREPROCESSING INSTALL EXTERNAL LIBRARIES AND DEPENDENCIES
##tbd

MYGITSHA="unknown"
if [ "$check_codeversion" == 1 ]; then
	##get GitSHA of the project to include into the compile such that we know with which code the tool works
	echo "Identifying the paraprobe version"
	#MYGITSHA="$(git describe --abbrev=50 --always --tags)" 
	MYGITSHA="$(git describe --abbrev=50 --dirty --broken --always --tags)"
	echo $MYGITSHA
fi

##COMPILE THE SHARED TOOL UTILITIES
echo "Preparing ambra-utils ..."
mkdir -p ambra-utils/cxx/build
cd ambra-utils/cxx/build
if [ "$clean_build" = true ]; then
	echo "Cleaning $PWD"
	rm -rf *
fi
echo "Configuring ambra-utils ..."
#rm -rf *
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER .. 1>AMBRA.Utils.CMake.$MYWHO_COMPILER.STDOUT.txt 2>AMBRA.Utils.CMake.$MYWHO_COMPILER.STDERR.txt
echo "Compiling ambra-utils ..."
make 1>AMBRA.Utils.Make.$MYWHO_COMPILER.STDOUT.txt 2>AMBRA.Utils.Make.$MYWHO_COMPILER.STDERR.txt
#DO NOT DELETE THESE GENERATED FILES below TOOLS WILL INCLUDE SOME OF THEM
cd ../../../

#dont forget to set Cuda and multi gpu environment
if [ "$build_fourier" == 1 ]; then
	echo "Preparing ambra-fourier ..."
	mkdir -p ambra-fourier/cxx/build
	cd ambra-fourier/cxx/build
	if [ "$clean_build" = true ]; then
		echo "Cleaning $PWD"
		rm -rf *
	fi
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$MYCCC_COMPILER -DCMAKE_CXX_COMPILER=$MYCXX_COMPILER -DMYGITSHA=$MYGITSHA .. 1>AMBRA.Fourier.CMake.$MYWHO_COMPILER.STDOUT.txt 2>AMBRA.Fourier.CMake.$MYWHO_COMPILER.STDERR.txt
	make 1>AMBRA.Fourier.Make.$MYWHO_COMPILER.STDOUT.txt 2>AMBRA.Fourier.Make.$MYWHO_COMPILER.STDERR.txt
	chmod +x ambra_fourier
	if [ "$collect_executables" = true ]; then
		cp ambra_fourier $target_folder/ambra_fourier
	fi
	#rm -rf *
	cd ../../../
fi

#add additional tools

#useful resources for further improvement and simplifications
#https://stackoverflow.com/questions/24460486/cmake-build-type-is-not-being-used-in-cmakelists-txt
