# -*- coding: utf-8 -*-
"""
created 2020/10/23, Markus K\"uhbach, m.kuehbach@mpie.de
exemplifying how to convert TIF files from Luca Bertinetti from Bio Dresden to HDF5 for use with ambra-fourier
"""

#Python packages and dependencies
import os
import sys
#sys.path.append('../src/')
import glob, subprocess
import pandas as pd
import numpy as np
import re
import h5py
#import periodictable as pse
from skimage import io

class ambra_transcoder():
    """
    class for converting TIFF image stacks to HDF5 for doing 3D FFT transforms
    """
    def __init__(self, *args, **kwargs):
        pass
    
    def tiff2hdf5(self, prefix, tiff_fnm, simid):
        #load user input, specify which filename of HDF5 to convert the image into
        #prefix = 'D:/LucaBertinetti3DFFT/'
        #fnm = 'PaLi_bin.tif'
        ###MK::add existence checks
        fn = prefix + tiff_fnm
        im = io.imread( fn )
        #get infos of the dataset
        print(im.dtype)
        print(im.shape)
        io.imshow(im[900,:,:])
        print(fn + ' loaded successfully')
        #transcode to HDF5
        h5fn = 'AMBRA.Transcoder.SimID.' + str(simid) + '.h5'
        h5w = h5py.File( h5fn, 'w')
        #create top-level groups
        h5w.create_group('/AmbraTranscoder/Metadata/ToolEnvironment')
        h5w.create_group('/AmbraTranscoder/Metadata/ToolConfiguration')
        #write datasets
        h5w.create_dataset('/AmbraTranscoder/Results/Imagestack', data = im )
        ###MK::will be contiguous data layout last dimension changing fastest
        h5w.close()
        print(h5fn + ' created successfully')
        
    def debug_proc_vf2hdf5(self, prefix, tiff_fnm_prefix, istart, iend, simid):
        """
        ##MK::mean for development purposes only!
        """
        prefix = 'D:/LucaBertinetti3DFFT/proc_50_zip/vf/'
        tiff_fnm = '006_tomod_PALI_script_test00'
        istart = 0
        iend = 4 #19
        simid = 43020
        img = np.zeros( (iend, 30, 20), np.float32 )
        for i in np.arange(istart, iend):
            if i > 9:
                fn = prefix + tiff_fnm + str(i) + '.tif'
            else:
                fn = prefix + tiff_fnm + '0' + str(i) + '.tif'
            im = io.imread( fn )
            print(im.dtype)
            print(im.shape)
            io.imshow(im[:,:])
            img[i,:,:] = im[:,:] #i,:,:
            del im
        #transcode to HDF5
        h5fn = 'AMBRA.Transcoder.SimID.' + str(simid) + '.h5'
        h5w = h5py.File( h5fn, 'w')
        #create top-level groups
        h5w.create_group('/AmbraTranscoder/Metadata/ToolEnvironment')
        h5w.create_group('/AmbraTranscoder/Metadata/ToolConfiguration')
        #write datasets
        h5w.create_dataset('/AmbraTranscoder/Results/Vf', shape = (iend, 30, 20), data = img ) #iend, 30, 20
        ###MK::will be contiguous data layout last dimension changing fastest
        h5w.close()
        print(h5fn + ' created successfully')
        
    def debug_tensor3d(self):
        prefix = 'D:/LucaBertinetti3DFFT/proc_50_zip/vf/'
        simid = 243
        img = np.float32(np.arange(2*4*3).reshape((2,4,3), order='C'))
        #now last index, here len 3 changes fastest
        #first index, here len 2, changes slowest
        print(img.dtype)
        print(img.shape)
        #transcode to HDF5
        h5fn = 'AMBRA.Transcoder.SimID.' + str(simid) + '.h5'
        h5w = h5py.File( h5fn, 'w')
        #create top-level groups
        h5w.create_group('/AmbraTranscoder/Metadata/ToolEnvironment')
        h5w.create_group('/AmbraTranscoder/Metadata/ToolConfiguration')
        #write datasets
        h5w.create_dataset('/AmbraTranscoder/Results/Vf', shape = (2,4,3), data = img ) #iend, 30, 20
        ###MK::will be contiguous data layout last dimension changing fastest
        h5w.close()
        print(h5fn + ' created successfully')

        
#example usage
#prefix = 'D:/LucaBertinetti3DFFT/'
#fnm = 'PaLi_bin.tif'
#conv = ambra_transcoder()
#conv.tiff2hdf5(prefix,fnm, '2')
        
            
