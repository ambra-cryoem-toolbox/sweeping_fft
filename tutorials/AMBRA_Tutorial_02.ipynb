{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 2: Use *sweeping_fft* tool on talos productively"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last modified 07.12.2020*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial builds on tutorial 1, where I showed how to build *sweeping_fft* on talos.<br>\n",
    "Now we want to learn how to use the tool, especially how to make use of the powerful GPU resoruces of talos."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scientific aim of *sweeping_fft* tool usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Characterize what is the spatial arrangement locally within voluminous three-dimensional image stacks.\n",
    "The example studied here is the pore structure in large volumina of (bio)materials. Specifically, the structure within shells of sea urchins to understand more details of biomaterial synthesis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Experiments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Shells of sea urchins were prepared in the lab and characterized via electron microscopy.\n",
    "<font color=\"orange\">Here is place to write down experimental details.</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Situation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The input data to *sweeping_fft* typically come from serial-sectioning (cryo)electron microscopy experiments. The input data are three-dimensional imagestacks. Our aim from the data science perspective is to:\n",
    "1. Characterize many (thousands, eventually millions) of local regions-of-interest (ROIs).\n",
    "2. For each ROI we want to perform a 3D FFT.\n",
    "\n",
    "This use case is a trivial example of data parallelism. Computing many 3D FFTs is numerically costly.<br>\n",
    "Therefore, we want to use parallel computing with CPUs and GPUs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Expected output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1.) For each ROI, an array of 3D FFT magnitude values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Strategy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use an analysis strategy that has three steps:\n",
    "\n",
    "1. The image stack data from electron microscopy are usually stored as TIF container files. We translate TIF first into HDF5. We use a small Python function for it that is implemented in the ambra-transcoder class. Thereby, we can more transparently add metadata to our datasets and make use of the HDF5 library functionalities for sequential <font color=\"orange\">(and later parallel I/O)</font>. Thereby, we avoid to work with thousands of files, which is very time and resource consuming on virtually all modern computers.\n",
    "\n",
    "2. We compute FFTs with *ambra-fourier*, a tool written in C/C++/CUDA. The tool delegates the processing of the ROIs across multiple CPUs and GPUs.\n",
    "\n",
    "3. Then, we can inspect the FFT results and <font color=\"red\">investigate the structure of the FFT signal. From this, we can characterize in detail the spatial arrangement and orientation of preferential features in the 3D volume of each ROI</font>.\n",
    "\n",
    "We document the workflow using a Jupyter notebook like this one because it allows us to keep the source code, our comments during the analysis steps, and the description of the analysis workflow all in one place."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Workflow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's begin with loading some standard Python packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load relevant Python3 (standard) packages\n",
    "import os\n",
    "import sys\n",
    "import glob\n",
    "from pathlib import Path\n",
    "import numpy as np\n",
    "#check for the existence of specific non-standard packages needed\n",
    "try:\n",
    "    import h5py\n",
    "except ImportError as e:\n",
    "    raise ValueError('Install h5py Python package via e.g. pip install h5py !')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We should detect the operating system and"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "win32\n"
     ]
    }
   ],
   "source": [
    "from sys import platform\n",
    "print(platform)\n",
    "#platform == \"linux\" or platform == \"linux2\":\n",
    "#platform == \"win32\":"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to define our working directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "#define where these tools are located/installed locallly\n",
    "if platform == \"win32\":\n",
    "    basePath = 'D:/Markus/MPIE/GITHUB/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code'\n",
    "    basePath = 'C:/Users/kaiob/eclipse-workspace/ambra-cryoem-toolbox/'\n",
    "if platform == \"linux\":\n",
    "    basePath = '/home/m.kuehbach/GITHUB/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To keep all our analyses for the dataset logically organized we define a simulation ID.<br>\n",
    "This ID is a unique identifier to distinguish results. Results and files with the same SimID belong together. On the cluster files with the same SimID will be silently overwritten by *sweeping_fft*, so make sure that you give different SimID if you want to do multiple analyses!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "SimID = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start now with the workflow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Transcode TIF to HDF5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the *ambra-transcoder* Python utility tool for this task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.path.append(basePath + '/ambra-transcoder/python/src/')\n",
    "from AMBRA_Transcoder_ConvertTIF2HDF import *"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#define *.tif/*.tiff file\n",
    "tiff_fnm = 'PaLi_bin.tif'\n",
    "\n",
    "#we give a unique ID to distinguish dif\n",
    "task = ambra_transcoder()\n",
    "task.tiff2hdf5( '', tiff_fnm, SimID )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Compute FFTs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define what you want to do"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These steps can be executed directly in this Jupyter notebook using Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the *ambra-fourier* and use a workstation or like shown here a computer cluster, like talos, to make the most of many processors and GPUs. There is no graphical user interface, neither on computer clusters in general nor for the *ambra-fourier* tool specifically. Therefore, we need to instruct the program using text files, so-called configuration files. With *ambra-parmsetup* we have a small Python class that helps us with creating such a configuration file conveniently."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "#load ambra tool-specific Python wizard\n",
    "sys.path.append(basePath + '/ambra-parmsetup/python/src/')\n",
    "sys.path.append(basePath + '/ambra-parmsetup/python/src/utils/')\n",
    "from AMBRA_BashSingle import *\n",
    "from AMBRA_SlurmSingle import *\n",
    "from AMBRA_XML import *\n",
    "from AMBRA_Fourier import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define which compiler and how many processes you want to use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "#and configure how many computing nodes and processes you want to use\n",
    "ifo = { 'JobName': 'ambra_fourier',\n",
    "        'SimulationID': SimID,\n",
    "        'Compiler': 'ITL',\n",
    "        'NodesToUse': 1,\n",
    "        'ProcessesPerNode': 2,\n",
    "        'ThreadsPerProcess': 20,\n",
    "        'GPUsToUsePerProcess': 1  } #currently only ITL supported\n",
    "tools = { 'AmbraFourier': 'ambra_fourier' }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we interact with the *ambra-parmsetup* Python wizard. For this we make a small script that creates the configuration file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "#AnalysisJobsSlurm = {}\n",
    "#AnalysisJobsBash = {}\n",
    "#WhatToDo = {}\n",
    "\n",
    "hdf5_fnm = 'AMBRA.Transcoder.SimID.' + str(SimID) + '.h5'\n",
    "task = ambra_fourier( SimID, hdf5_fnm )\n",
    "\n",
    "#define ROI edge length in voxel\n",
    "nedge = 100*np.ones([1, 3])\n",
    "\n",
    "##define the size of the ROIs, currently all ROIs the same size\n",
    "task.set_roi_grid( nedge )\n",
    "\n",
    "#either use explicit positions\n",
    "mypos = np.ones([2, 3])\n",
    "mypos[0,:] = [100, 100, 100]\n",
    "mypos[1,:] = [400, 400, 400]\n",
    "task.set_roi_ensemble_positions( mypos )\n",
    "\n",
    "#or build a regular grid over the entire imagestack\n",
    "task.set_roi_ensemble_grid()\n",
    "\n",
    "task.run( nmpi = ifo['NodesToUse'] * ifo['ProcessesPerNode'] )\n",
    "del task"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let *ambra-parmsetup* create scripts to execute that specific simulation job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "#for an ordinary command line interactive run\n",
    "#bsh = ambra_bash_single( ifo, tools )\n",
    "#bsh.write_bash_script()\n",
    "\n",
    "#for submission to the slurm queing system\n",
    "slm = ambra_slurm_single( ifo, tools )\n",
    "slm.write_slurm_script()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, check that we have the necessary configuration file (and scripts)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " Datentr„ger in Laufwerk D: ist Working\n",
      " Volumeseriennummer: 6226-BD00\n",
      "\n",
      " Verzeichnis von D:\\Markus\\MPIE\\Paper\\Paper24_LucaBertinettiSweepingFFT\n",
      "\n",
      "07.12.2020  13:35             1.362 AMBRA.Fourier.SimID.1.xml\n",
      "               1 Datei(en),          1.362 Bytes\n",
      "               0 Verzeichnis(se), 155.079.397.376 Bytes frei\n",
      " Datentr„ger in Laufwerk D: ist Working\n",
      " Volumeseriennummer: 6226-BD00\n",
      "\n",
      " Verzeichnis von D:\\Markus\\MPIE\\Paper\\Paper24_LucaBertinettiSweepingFFT\n",
      "\n",
      "07.12.2020  13:35             1.480 AMBRA.Slurm.Workflow.SimID.1.sh\n",
      "               1 Datei(en),          1.480 Bytes\n",
      "               0 Verzeichnis(se), 155.079.397.376 Bytes frei\n"
     ]
    }
   ],
   "source": [
    "if platform == \"win32\":\n",
    "    ! dir AMBRA.*.SimID.*.xml\n",
    "    #! dir BASH.AMBRA.Workflow.SimID.*.sh\n",
    "    ! dir AMBRA.Slurm.Workflow.SimID.*.sh\n",
    "if platform == \"linux\":\n",
    "    ! ls AMBRA.*.SimID.*.xml\n",
    "    #! ls BASH.AMBRA.Workflow.SimID.*.sh\n",
    "    ! ls AMBRA.Slurm.Workflow.SimID.*.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have your analysis defined, you can take your datasets (*.h5) and the above created (*.xml) files and move them to talos. On Windows for instance you can use the WinSCP client. Pull your *ambra_fourier* executable that you should have still sitting on talos from tutorial 1 and place it into the same directory, i.e. the directory where you moved the *.h5 and *.xml files to and continue.\n",
    "\n",
    "If you use *sweeping_fft* on a local workstation, you can ignore all details about queing systems and proceed with the commands for interactive usage, the syntax should in most cases be the same. Feel free to contact me if you have questions on this one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Execute FFTs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The preferred way to use talos is the queue. Only very small jobs and these for testing purposes only should be executed on talos interactively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Submit to slurm queing system on talos"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# export AMBRA_SIMID=1\n",
    "# echo $AMBRA_SIMID\n",
    "# sbatch AMBRA.Slurm.Workflow.SimID.$AMBRA_SIMID.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check the status of your job like so"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# squeue -u testfolk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Execute interactively on talos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to run interactively (for testing the tools) or you have your own workstation with *sweeping_fft* compiled (see tutorial 1) then open a console and set the environment with the *module load*, *export*, and *cuda_visible_devices* commands that you learned in the tutorial 1. Of course, if this is your own workstation environment modules have to be installed. This is something your administrator can help you with. \n",
    "\n",
    "On talos only, you need to deactivate the default setting which is that normal users are not by default allowed to run jobs interactively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# unset I_MPI_HYDRA_BOOTSTRAP\n",
    "# unset I_MPI_PMI_LIBRARY"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can run a job interactively on talos like so (for workstations this is likely the same, i.e. executing a normal MPI program)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# export AMBRA_SIMID=1\n",
    "# echo $AMBRA_SIMID\n",
    "# mpiexec -n if[NodesToUse]*ifo[ProcessesToUse] ./ambra_fourier $AMBRA_SIMID AMBRA.Fourier.$AMBRA_SIMID.xml 1>AMBRA.Fourier.SimID.$AMBRA_SIMID.STDOUT.txt 2>AMBRA.Fourier.SimID.$AMBRA_SIMID.STDERR.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Inspect the results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instance, we want to visualize how the FFT signal looks like.<br>\n",
    "We have a Python wizard/utility tool *ambra-autoreporter* which create "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import the paraprobe-autoreporter tools we need\n",
    "sys.path.append(basePath + '/ambra-autoreporter/python/src/utils')\n",
    "from AMBRA_Autoreporter_XDMF import *\n",
    "#for showing images inside this Jupyter notebook\n",
    "from IPython.display import Image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "task = xdmf()\n",
    "#visualize for region 0\n",
    "task.visualize_roi_ijk( 'AMBRA.Fourier.Results.SimID.1.h5', 0 )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now load the *.xdmf file into e.g. Paraview (using Xdmf3ReaderS)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename='AMBRA.Fourier.Results.SimID.1.h5.ROI.0.xdmf.png', width= 600)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
