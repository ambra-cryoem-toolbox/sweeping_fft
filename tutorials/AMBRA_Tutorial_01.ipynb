{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial 1: Build sweeping_fft tool on talos from scratch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Tutorial written by Markus Kühbach, last modified 07.12.2020*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial shows you how to get and prepare the necessary dependencies and compile the *sweeping_fft* tool for a Linux-type CPU/GPU cluster. The tool enables you to employ MPI, OpenMP, and CUDA data parallelism to solve FFT-related characterization of spatial structure in large three-dimensional image stacks using CPUs and GPUs. The tutorial exemplifies for the TALOS computer cluster at MPCDF in Garching. We build a local version, i.e. for an ordinary user with a TALOS account. This does not require administrator rights on TALOS."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Open a terminal to connect to TALOS, e.g. using putty from Windows or ssh from Unix/Mac.<br>\n",
    "Opening a console is always required when interacting with TALOS.\n",
    "\n",
    "\n",
    "2. Download *sweeping_fft* from its GitLab repository.<br>\n",
    "This step is required only once or whenever you want to get a new version of the tool.\n",
    "\n",
    "\n",
    "3. Prepare your local installation of the dependencies, i.e. libraries which *sweeping_fft* needs.<br>\n",
    "HDF5 is the only dependency that we better install locally.<br>\n",
    "All other dependencies (compilers, libraries MKL/CUDA) were installed on TALOS already by the system admins.<br> \n",
    "This step is required only once or whenever you want to get new or different versions of the dependencies (e.g. of HDF5).\n",
    "\n",
    "\n",
    "4. Compile the tool.\n",
    "This step is required only once or whenever the source code changes and you want to have these changes included in your application."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial shows you how to get *sweeping_fft* ready.<br>\n",
    "The next tutorial shows you how to use the tool that you will build now."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5. Define what you want to use the tool for and configure the tool for this.\n",
    "\n",
    "\n",
    "6. Run the tool.\n",
    "\n",
    "\n",
    "7. Inspect the results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Questions, problems, suggestions?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case you are running into problems with this and other tutorials or the paraprobe tools, feel free to contact me:\n",
    "    markus.kuehbach at hu-berlin.de"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sys import platform\n",
    "print(platform)\n",
    "if platform == \"linux\" or platform == \"linux2\":\n",
    "    print('You have a supported operating system')\n",
    "else:\n",
    "    raise ValueError('Your operating system is not supported. Contact me to get assistance!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1: Open a console and login on TALOS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From Windows you can use e.g. putty: <a href=\"www.http://www.putty.org/\">http://www.putty.org/</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=\"red\">For all console commands that follow now, type what is in the Jupyter notebook code cells.<br>**Remove the preceeding pound character!**</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After having established the console session, we need to set up our environment (i.e. state variables in the console) in such a way that we can use the already installed functionalities of TALOS. Setting the environment works on the one hand by loading pre-installed modules and on the other hand by setting some variables explicitly. We need to repeat this setting of the environment whenever we want to work with TALOS, as otherwise the console does not know what you might possibly need. However, of course we DO NOT need to compile always the source code! But at least we need to compile it once, so let's do it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# module load cmake/3.18\n",
    "# module load cuda\n",
    "# module load intel\n",
    "# module load impi\n",
    "# module load mkl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is for using the pre-installed modules. In addition, we need to set a few environment variables explicitly. These control how many OpenMP threads we want use, which of the GPUs we want to use, and whether we want to test the program interactively (**only for testing purposes, i.e. few FFT, small FFTs e.g. 100^3 or soish**) or submit directly to the TALOS batch queue via the slurm job manager.\n",
    "\n",
    "\n",
    "First, set how many OpenMP threads we want to use. If you want just to test something and use only one thread per process you set =1. If you want to use all threads set =20. Remember, TALOS has two multi-core CPUs with 20 cores each. For sweeping_fft each CPU gets one MPI process. Therefore, we use 20 threads per MPI process. Placing the threads at the cores helps with improving internal memory management when the tool allocates data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# export OMP_NUM_THREADS=20\n",
    "# export OMP_PLACES=cores"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set how many OpenMP threads should be used internally by the Intel MKL library. \n",
    "**This has to be 1 currently because each OpenMP threads calls an own FFT** (no nested parallelism)!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# export MKL_NUM_THREADS=1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, specific how many GPUs you want to use. Remember, each TALOS node has two V100 GPUs. These have internally the device IDs 0 and 1. Below code shows you how to enable that each MPI process of your program can find these GPUs and use one of them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# export CUDA_VISIBLE_DEVICES=0,1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the moment if is necessary that each MPI process gets an own GPU.<br>\n",
    "**So DO NOT place more than two MPI processes per computing node!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2: Download the tool"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a folder where you want to place the tool into in your local storage space of TALOS. Here I show this for the ficticious user named *testfolk*. Go into this folder.\n",
    "**Keep in mind that only the /talos/u directories are backupped.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# /talos/u/testfolk\n",
    "# mkdir -p AMBRA_CRYOEM_TOOLBOX\n",
    "# cd AMBRA_CRYOEM_TOOLBOX"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Download the tool into this folder by copying the source code from the Garching GitLab into your local account with the following command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# git clone https://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe.git"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The directory *sweeping_fft* is the root directory. The *sweeping_fft* code resides inside the *sweeping_fft/code* sub-directory. There are multiple sub-directories in this sub-directory. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cd sweeping_fft/code\n",
    "# ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These sub-directories host different pieces of the source code that we need to compile along this tutorial:\n",
    "* *thirdparty*, contains the source code for HDF5\n",
    "* *ambra-utils*, contains all source code for shared functionalities\n",
    "* *ambra-fourier*, contains all source code for the actual tool"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each sub-directory, like ambra-utils or ambra-fourier, has a similar structure with mandatory files:<br>\n",
    "* *build* directory, where we want to store the compiled application in\n",
    "* *src* directory, where we have the source code\n",
    "* *CMakeLists.txt* file containing the compile recipe for creating the tool using cmake"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3: Prepare for compiling paraprobe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compile HDF5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**HDF5**, the Hierarchical Data Format 5, is a specification of a binary container file format and implementation of this format as a scientific computing software library. HDF5 can be used for creating HDF5 container files efficiently. Let's install HDF5 locally, the HDF5 source code is included in *sweeping_fft/code/thirdparty/HDF5*. Currently, I use version 1.10.6.\n",
    "\n",
    "Only advanced users should use a newer version of HDF5 and do so at their own risk, as it will require changes of these configuration steps and possibly even changes in the source code. So, let's stick with the shipped HDF5 version for now. We need to unpack, configure, and compile this library. We do it locally and use the console for it.\n",
    "\n",
    "\n",
    "Configuring and compiling HDF5 may take some time, so I typically grab a coffee. I exemplify again for the test user named *testfolk*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cd /talos/u/testfolk/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/thirdparty/HDF5\n",
    "# tar -xvf CMake-hdf5-1.10.6.tar.gz\n",
    "# cd CMake-hdf5-1.10.6\n",
    "# ./build_unix.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The build_unix.sh command will start HDF5's compilation wizard and will test the HDF5 library thereafter. In return, we should receive messages on the console stating something like *test completed or test successful or so*.<br>There are several tests which may fail but this is not necessarily a problem. If it is, feel free to contact me."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ./HDF5-1.10.6-Linux.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Follow the instructions that you can read in the console. First, that is to accept the license and second to hit yes for setting up an own sub-directory where to place the HDF5 libraries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# accept the license\n",
    "# hit yes for setting up an own subdirectory for HDF5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Eventually, the installer will prompt **Unpacking finished successfully**. This tells us that HDF5 should be ready.<br>\n",
    "The HDF5 HOME is then the complete path to HDF5-1.10.6-Linux.<br>\n",
    "The library files are in the *HDF_Group/HDF5/1.10.6/lib*.<br>\n",
    "The C/C++ source code headers are in *HDF_Group/HDF5/1.10.6/include*.<br>\n",
    "Both are sub-directories of *HDF5-1.10.6-Linux*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to tell *sweeping_fft* where it can find the HDF5. For this we specify the location in the following utility text file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cd /talos/u/testfolk/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/AMBRA.ExternalLibraries.cmake"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this textfile we need to change *testfolk* to your username in the following line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# replace the following string portion under MYPROJECTPATH /talos/u/mkuehbac/ to /talos/u/testfolk/\n",
    "# replace the following string portion under MYHDFPATH /talos/u/mkuehbac/ to /talos/u/testfolk/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 4: Compile the tool"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Firstly, compile the source code for the shared functionalities inside *ambra-utils*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cd /talos/u/testfolk/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/ambra-utils/cxx/build\n",
    "# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc ..\n",
    "# make"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Secondly, compile the source code for the actual tool inside *ambra-fourier*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cd /talos/u/testfolk/AMBRA_CRYOEM_TOOLBOX/sweeping_fft/code/ambra-fourier/cxx/build\n",
    "# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc ..\n",
    "# make\n",
    "# chmod +x ambra_fourier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Congratulations! You have successfully created ambra_fourier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I recommend to run production jobs on the /talos/scratch portion. **Keep in mind though that this part of the cluster is not backupped!**\n",
    "You can just take the *ambra_fourier* executable. The executable knows where to find the libraries."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Now, proceed with the next tutorial which teaches you how to use ambra_fourier ..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
