#!/bin/bash -l
#SBATCH -o ./AMBRA.Fourier.SimID.1.STDOUT.txt.%j
#SBATCH -e ./AMBRA.Fourier.SimID.1.STDERR.txt.%j
#SBATCH -D ./
#SBATCH -J ambra_fourier
#SBATCH --partition=p.talos
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=20
#SBATCH --gres=gpu:2
#SBATCH --time=24:00:00

module load cmake/3.18
module load cuda
module load intel
module load impi
module load mkl
echo "Current modules loaded"
module list
echo "OpenMP NUM THREADS env option"
if [ ! -z $SLURM_CPUS_PER_TASK ] ; then
	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
else
	export OMP_NUM_THREADS=1
fi
echo $OMP_NUM_THREADS
echo "OpenMP PLACES env option"
export OMP_PLACES=cores
echo $OMP_PLACES
###echo "OpenMP NESTED env option"
###export OMP_NESTED=true
###echo $OMP_NESTED
echo "MKL NUM THREADS env option"
export MKL_NUM_THREADS=1
echo $MKL_NUM_THREADS
echo "CUDA VISIBLE DEVICES env option"
export CUDA_VISIBLE_DEVICES=0,1
echo $CUDA_VISIBLE_DEVICES
##set unlimited stack memory
ulimit -s unlimited
echo "ulimit env option"
##set no core files
##ulimit -S -c 0
##display ulimit settings
ulimit
##list allocated TALOS resources nodes
echo $SLURM_JOB_NODELIST
##echo $SLURM_NODELIST
echo $SLURM_JOB_NUM_NODES
##echo $SLURM_NNODES
echo "job $SLURM_JOB_NAME with job id $SLURM_JOB_ID is running on $SLURM_JOB_NUM_NODES node(s): $SLURM_JOB_NODELIST"


###  ambra tool workflow SimID 1

srun  ambra_fourier 1 AMBRA.Fourier.SimID.1.xml

